<?php
/**
* Language file for administration strings
*
*/
return array(

    'administration' 				=> 'Verwaltung',
    'agent_information' 			=> 'Händler Information',
    'agent_id' 						=> 'Händler Nr',
    'name' 							=> 'Name',
    'place' 						=> 'Ort',
    'street' 						=> 'Straße',
    'zipcode' 						=> 'PLZ',
    'email' 						=> 'E-Mail',
    'iban' 							=> 'IBAN',
    'selected_agent' 				=> 'Ausgewählter Händler',
    'found_agents' 					=> 'Gefundene Händler',
    'all' 							=> 'Alles',
    'production_summary_selection' 	=> 'Prod. Übersicht Auswahl',
    'production_summary' 			=> 'Produktionsübersicht',
    'markers' 						=> 'Marker',
    'search' 						=> 'Suchen',
    'searchterm' 					=> 'Suchbegriff',
    'zipcode_from' 					=> 'PLZ von',
    'zipcode_to' 					=> 'PLZ bis',
    'enter_your_search_term_here' 	=> 'Geben Sie hier Ihren Suchbegriff ein',
    'calendar_item' 				=> 'Tagesordnungspunkt',

);
