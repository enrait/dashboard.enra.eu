<?php
/**
* Language file for general ENRA strings
*
*/
return array(

    'dashboard'  			=> 'Dashboard',
    'enra_dashboard'	  	=> 'ENRA verzekeringen Dashboard',
    'enra_bv' 				=> 'ENRA verzekeringen bv',
    'enra' 					=> 'ENRA verzekeringen',
    'my_profile'     		=> 'Mein Profil',
    'account_settings'     	=> 'Konto-Einstellungen',
    'lock'     				=> 'Sperren',
    'logout'     			=> 'Ausloggen',
    'error'     			=> 'Fehler',
    'you_must_be_logged_in'	=> 'Sie m�ssen eingeloggd sein!',
    'home' 					=> 'Home',

);
