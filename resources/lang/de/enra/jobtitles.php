<?php
/**
 * Language file for Job Titles strings
 *
 */
 
use App\Models\AnvaREFBES;

$jobTitles = AnvaREFBES::getDescriptions('00018', 'DE');

$jobTitlesTranslationArray = array();

foreach ($jobTitles as $jobTitlesRow) 
{
	$jobTitlesTranslationArray[$jobTitlesRow->REF_KEY_N] = $jobTitlesRow->REF_OMSCHRIJVING;
}

return $jobTitlesTranslationArray;