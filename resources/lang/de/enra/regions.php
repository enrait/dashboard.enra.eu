<?php
/**
 * Language file for Regions strings
 *
 */
return array(

    '00001' => 'Gebiet 1',
    '00002' => 'Gebiet 2',
    '00003' => 'Gebiet 3',
    '00004' => 'Gebiet 4',
    '00020' => 'Gebiet 20',
    'NL'	=> 'Niederlande',
    'BE' 	=> 'Belgien',
    
);
