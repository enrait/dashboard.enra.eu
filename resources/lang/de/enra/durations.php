<?php
/**
 * Language file for Durations strings
 *
 */
return array(

    '000' => 'Keine Laufzeit',
    '001' => 'Monat',
    '003' => 'Quartal',
    '006' => 'Halbjahr',
    '010' => '10 Monate',
    '012' => '1 Jahr',
    '024' => '2 Jahre',
    '036' => '3 Jahre',
    '048' => '4 Jahre',
    '060' => '5 Jahre',
    
);
