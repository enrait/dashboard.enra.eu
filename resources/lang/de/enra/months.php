<?php
/**
 * Language file for Months strings
 *
 */
return array(

    '01' => 'Jan',
    '02' => 'Feb',
    '03' => 'Mrz',
    '04' => 'Apr',
    '05' => 'Mai',
    '06' => 'Jun',
    '07' => 'Jul',
    '08' => 'Aug',
    '09' => 'Sept',
    '10' => 'Okt',
    '11' => 'Nov',
    '12' => 'Dez',
    
);
