<?php
/**
* Language file for Agent strings
*
*/
return array(

    'agent_id' 						=> 'Händler Nr',
    'name' 							=> 'Name',
    'place' 						=> 'Ort',
    'street' 						=> 'Straße',
    'zipcode' 						=> 'PLZ',
    'email' 						=> 'E-Mail',
    'iban' 							=> 'IBAN',
    'invoice_id' 					=> 'Rechnungsnummer',
    'amount' 						=> 'Betrag',

);
