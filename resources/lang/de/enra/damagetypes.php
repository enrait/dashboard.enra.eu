<?php
/**
 * Language file for Damage Type strings
 *
 */
 
use App\Models\AnvaREFBES;

$damageTypes = AnvaREFBES::getDescriptions('90004', 'DE');

$damageTypesTranslationArray = array();

foreach ($damageTypes as $damageTypesRow) 
{
	$damageTypesTranslationArray[$damageTypesRow->REF_KEY_N] = $damageTypesRow->REF_OMSCHRIJVING;
}

//print_r($damageTypesTranslationArray);
//die();

return $damageTypesTranslationArray;

// return array(
// 
    // '311' => 'Diebstahl komplett',
	// '312' => 'Diebstahl Teile',
	// '313' => 'Diebstahl Folgeschaden',
	// '350' => 'Diebstahl Teile inkl. Akku',
	// '410' => 'Verschleiß',
	// '420' => 'Sturz/Unfall',
	// '430' => 'Reparatur',
	// '440' => 'Mix',
	// '450' => 'Akku Defekt',
	// '460' => 'Unsachgem. Handhabung',
	// '910' => 'UVV Check 1e Jahr',
	// '911' => 'UVV/75 Check und Reparatur 1e Jahr',
	// '912' => 'UVV/125 Check und Reparatur 1e Jahr',
	// '920' => 'UVV Check 2e Jahr',
	// '921' => 'UVV/75 Check und Reparatur 2e Jahr',
	// '922' => 'UVV/125 Check und Reparatur 2e Jahr',
	// '930' => 'UVV Check 3e Jahr',
	// '931' => 'UVV/75 Check und Reparatur 3e Jahr',
	// '932' => 'UVV/125 Check und Reparatur 3e Jahr',
	// '950' => 'Vandalismus',
// 	
// );
