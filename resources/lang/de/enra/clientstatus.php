<?php
/**
* Language file for Client Status strings
*
*/
return array(

	''			=> 'Aktiv',	
	'1'			=> 'Aktiv',
	'2'			=> 'Keine Versicher',
	'3'			=> 'Gestoppt',
	'J'			=> 'Gestoppt',	
	'4'			=> 'Gekündigt',
	'5'			=> 'Interessent',
	
);
