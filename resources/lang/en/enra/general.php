<?php
/**
* Language file for general ENRA strings
*
*/
return array(

    'dashboard'  			=> 'Dashboard',
    'enra_dashboard'	  	=> 'ENRA verzekeringen Dashboard',
    'enra_bv' 				=> 'ENRA verzekeringen bv',
    'enra' 					=> 'ENRA verzekeringen',
    'my_profile'     		=> 'Mijn profiel',
    'account_settings'     	=> 'Account instellingen',
    'lock'     				=> 'Vergrendelen',
    'logout'     			=> 'Uitloggen',
    'error'     			=> 'Error',
    'you_must_be_logged_in'	=> 'You must be logged in!',
    'home' 					=> 'Home',

);
