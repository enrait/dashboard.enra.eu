<?php
/**
 * Language file for Branches strings
 *
 */
return array(

    // Company B203 - Fiets NL
    '02000' => 'E-Bikes',
    '02001' => 'ORB',
    '02002' => 'HSE',
    '02010' => 'Koga',
    '02011' => 'Flyer',

    // Company B202 - Fiets DE
    '02100' => 'E-Bikes',
    '02101' => 'ORB',

    // Company B201 - Fiets BE
    '02200' => 'E-Bikes',
    '02201' => 'ORB',

    // Company P200 - Brom NL
    '02300' => 'Bromfiets',
    '02320' => 'Invalide',
    '02400' => 'E-Scooter',
    '02500' => 'Motoren',

  	// Translations for all
  	'DE' => 'All',
    'NL' => 'All',
);
