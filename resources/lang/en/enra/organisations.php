<?php
/**
 * Language file for Organisations strings
 *
 */
return array(

    '00001' => 'Personeel',
    '00002' => 'Zorginstelling',
    '00003' => 'Medewerkers van dlrs/agenten',
    '00004' => 'ATP Havelaar en Stolk',
    '00005' => 'Motoplus Motorverzekering',
    '00006' => 'ORION Portefeuille',
    '00007' => 'NS personeel',
    '00008' => 'Track en Trace (diefstal dekking)',
    '00009' => 'Track en Trace (casco dekking)',
    '00099' => '?00099',
    '00100' => '?00100',
    '00110' => '?00110',
    '00200' => '?00200',
    '00210' => '?00210',
    '00300' => '?00300',
    '00310' => '?00310',
    '00400' => '?00400',
    '00999' => 'Temp',
    'PK002' => '2% korting via tussenpersoon'
    
);
