<?php
/**
 * Language file for Months strings
 *
 */
return array(

    '01' => 'jan',
    '02' => 'feb',
    '03' => 'mrt',
    '04' => 'apr',
    '05' => 'mei',
    '06' => 'jun',
    '07' => 'jul',
    '08' => 'aug',
    '09' => 'sept',
    '10' => 'okt',
    '11' => 'nov',
    '12' => 'dec',
    
);
