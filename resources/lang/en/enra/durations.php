<?php
/**
 * Language file for Durations strings
 *
 */
return array(

    '000' => 'Geen Looptijd',
    '001' => 'Maand',
    '003' => 'Kwartaal',
    '006' => 'Halfjaar',
    '010' => '10 maanden',
    '012' => '1 jaar',
    '024' => '2 jaar',
    '036' => '3 jaar',
    '048' => '4 jaar',
    '060' => '5 jaar',
    
);
