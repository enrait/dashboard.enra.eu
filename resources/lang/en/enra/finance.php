<?php
/**
* Language file for finance strings
*
*/
return array(

    'finance' 						=> 'Finance',
    'agent_information' 			=> 'Agent informatie',
    'agent_id' 						=> 'Agent Nr',
    'name' 							=> 'Naam',
    'place' 						=> 'Plaats',
    'street' 						=> 'Straat',
    'zipcode' 						=> 'Postcode',
    'email' 						=> 'Email',
    'iban' 							=> 'IBAN',
    'selected_agent' 				=> 'Geselecteerde agent',
    'found_agents' 					=> 'Gevonden agenten',
    'all' 							=> 'All',
    'selected_agent' 				=> 'Prod. overzicht Selectie',
    'production_summary_selection' 	=> 'Prod. overzicht Selectie',
    'production_summary' 			=> 'Productie overzicht',
    'markers' 						=> 'Markers',
    'search' 						=> 'Zoeken',
    'searchterm' 					=> 'Zoekterm',
    'zipcode_from' 					=> 'Postcode van',
    'zipcode_to' 					=> 'Postcode tot',
    'enter_your_search_term_here' 	=> 'Vul hier uw zoekterm in...',
    'invoice_id' 					=> 'Factuurnummer',

    'organisation'      => 'Organisation',
    'select_organisation'      => 'Select organisation'
);
