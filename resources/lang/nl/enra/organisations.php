<?php
/**
 * Language file for Organisations strings
 *
 */
use App\Models\AnvaREFBES;


// Rubrik Label 55000 makes use of REF_RC 10015
$organisations = AnvaREFBES::getDescriptions('10015', 'NL');

$organisationsTranslationArray = array();

foreach ($organisations as $organisationsRow) 
{
	$organisationsTranslationArray[$organisationsRow->REF_KEY_N] = $organisationsRow->REF_OMSCHRIJVING;
}

return $organisationsTranslationArray;
