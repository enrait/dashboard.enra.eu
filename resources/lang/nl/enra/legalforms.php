<?php
/**
 * Language file for Legal Form strings
 *
 */
 
use App\Models\AnvaREFBES;

$legalForms = AnvaREFBES::getDescriptions('55001', 'NL');

$legalFormsTranslationArray = array();

foreach ($legalForms as $legalFormsRow) 
{
	$legalFormsTranslationArray[$legalFormsRow->REF_KEY_N] = $legalFormsRow->REF_OMSCHRIJVING;
}

return $legalFormsTranslationArray;