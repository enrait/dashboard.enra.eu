<?php
/**
* Language file for Client Status strings
*
*/
return array(

	''			=> 'Actief',	
	'1'			=> 'Actief',
	'2'			=> 'Geen verzekeringen',
	'3'			=> 'Gestopt',
	'J'			=> 'Gestopt',	
	'4'			=> 'Beindigd',
	'5'			=> 'Prospect',
	
);
