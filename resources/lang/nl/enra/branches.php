<?php
/**
 * Language file for Branches strings
 *
 */
return array(

	'' => '',

    // Company B203 - Fiets NL
    '02000' => 'NL - E-Bikes',
    '02001' => 'NL - ORB',
    '02002' => 'NL - HSE',
    '02010' => 'NL - Koga',
    '02011' => 'NL - Flyer',

    // Company B202 - Fiets DE
    '02100' => 'DE - E-Bikes',
    '02101' => 'DE - ORB',

    // Company B201 - Fiets BE
    '02200' => 'BE - E-Bikes',
    '02201' => 'BE - ORB',

    // Company P200 - Brom NL
    '02300' => 'NL - Bromfiets',
    '02320' => 'NL - Invalide',
    '02400' => 'NL - E-Scooter',
    '02500' => 'NL - Motoren',

		// Translations for all
		'DE' => 'Alle',
		'NL' => 'Alle',  
);
