<?php
/**
* Language file for Agent strings
*
*/
return array(

    'agent_id' 						=> 'Agent Nr',
    'name' 							=> 'Naam',
    'place' 						=> 'Plaats',
    'street' 						=> 'Straat',
    'zipcode' 						=> 'Postcode',
    'email' 						=> 'Email',
    'iban' 							=> 'IBAN',
    'invoice_id' 					=> 'Factuur Nr',
    'amount' 						=> 'Bedrag',

);
