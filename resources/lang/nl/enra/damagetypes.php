<?php
/**
 * Language file for Damage Type strings
 *
 */
 
use App\Models\AnvaREFBES;

$damageTypes = AnvaREFBES::getDescriptions('90004', 'NL');

$damageTypesTranslationArray = array();

foreach ($damageTypes as $damageTypesRow) 
{
	$damageTypesTranslationArray[$damageTypesRow->REF_KEY_N] = $damageTypesRow->REF_OMSCHRIJVING;
}

return $damageTypesTranslationArray;

// return array(
// 
    // '00100' => 'Verkeer algemeen',
    // '00110' => 'Aanrijding met voetganger',
    // '00111' => 'Aanrijding met voertuig',
    // '00112' => 'Aanrijding met fietser',
    // '00113' => 'Aanrijding met wegmeubilair',
    // '00114' => 'Aanrijding met vast object',
    // '00115' => 'Aanrijding met vervoerde personen',
    // '00116' => 'Aanrijding met dieren',
    // '00117' => 'Aanrijding met brommer',
    // '00140' => 'Eenzijdige aanrijding',
    // '00141' => 'Ongeval door weergesteldheid',
    // '00142' => 'Ongeval door wegconditie',
    // '00143' => 'Ongeval door uitwijken',
    // '00311' => 'Diefstal geheel object',
    // '00312' => 'Diefstal van onderdelen',
    // '00313' => 'Gevolgschade na diefstal',
    // '00314' => 'Beschadiging',
    // '00320' => 'Joy-riding',
    // '00330' => 'Beroving',
    // '00340' => 'Afpersing',
    // '00350' => 'Afpersing',
    // '00360' => 'Verduistering',
    // '00370' => 'Verlies',
    // '00380' => 'Vermissing',
    // '00390' => 'Brand',
    // '00410' => 'Verschleiß',
	// '00420' => 'Sturz/Unfall',
	// '00430' => 'Reparatur',
	// '00440' => 'Mix',
	// '00450' => 'Akku Defekt',
	// '00460' => 'Unsachgem. Handhabung',
    // '00950' => 'Vandalismus',
    // '00950' => 'Onbekende oorzaak/overige',
    // '01000' => '?01000',
// );
