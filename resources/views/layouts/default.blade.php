<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<title> @section('title')
			- {{ trans('enra/general.enra_bv') }}
			@show </title>
		<!--global css starts-->
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}">
		<!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/custom.css') }}"> -->

		<link rel="stylesheet" id="generate-fonts-css" href="//fonts.googleapis.com/css?family=Open+Sans:300,300italic,regular,italic,600,600italic,700,700italic,800,800italic" type="text/css" media="all">

		<link rel="stylesheet" id="generate-style-grid-css" href="https://www.enraonline.de/wp-content/themes/generatepress/css/unsemantic-grid.min.css?ver=1.3.27" type="text/css" media="all">
		<link rel="stylesheet" id="generate-style-css" href="https://www.enraonline.de/wp-content/themes/generatepress/style.css?ver=1.3.27" type="text/css" media="all">
		<style id="generate-style-inline-css" type="text/css">
			body {
				background-color: #efefef;
				color: #3a3a3a;
			}
			a, a:visited {
				color: #1e73be;
				text-decoration: none;
			}
			a:hover, a:focus, a:active {
				color: #000000;
			}
			body .grid-container {
				max-width: 1100px;
			}
			body, button, input, select, textarea {
				font-family: "Open Sans", sans-serif;
				font-size: 17px;
			}
			.main-title {
				font-weight: bold;
				font-size: 45px;
			}
			.site-description {
				font-size: 15px;
			}
			.main-navigation a, .menu-toggle {
				font-size: 15px;
			}
			.main-navigation .main-nav ul ul li a {
				font-size: 14px;
			}
			.widget-title {
				font-size: 20px;
			}
			.sidebar .widget, .footer-widgets .widget {
				font-size: 17px;
			}
			h1 {
				font-weight: 300;
				font-size: 40px;
			}
			h2 {
				font-weight: 300;
				font-size: 30px;
			}
			h3 {
				font-size: 20px;
			}
			.site-info {
				font-size: 16px;
			}
			.site-header {
				background-color: #FFFFFF;
				color: #3a3a3a;
			}
			.site-header a, .site-header a:visited {
				color: #3a3a3a;
			}
			.main-title a, .main-title a:hover, .main-title a:visited {
				color: #222222;
			}
			.site-description {
				color: #999999;
			}
			.main-navigation, .main-navigation ul ul {
				background-color: #222222;
			}
			.navigation-search input[type="search"], .navigation-search input[type="search"]:active {
				color: #3f3f3f;
				background-color: #3f3f3f;
			}
			.navigation-search input[type="search"]:focus {
				color: #FFFFFF;
				background-color: #3f3f3f;
			}
			.main-navigation ul ul {
				background-color: #3f3f3f;
			}
			.main-navigation .main-nav ul li a, .menu-toggle {
				color: #FFFFFF;
			}
			button.menu-toggle:hover, button.menu-toggle:focus, .main-navigation .mobile-bar-items a, .main-navigation .mobile-bar-items a:hover, .main-navigation .mobile-bar-items a:focus {
				color: #FFFFFF;
			}
			.main-navigation .main-nav ul ul li a {
				color: #FFFFFF;
			}
			.main-navigation .main-nav ul li > a:hover, .main-navigation .main-nav ul li > a:focus, .main-navigation .main-nav ul li.sfHover > a {
				color: #FFFFFF;
				background-color: #3f3f3f;
			}
			.main-navigation .main-nav ul ul li > a:hover, .main-navigation .main-nav ul ul li > a:focus, .main-navigation .main-nav ul ul li.sfHover > a {
				color: #FFFFFF;
				background-color: #4f4f4f;
			}
			.main-navigation .main-nav ul .current-menu-item > a, .main-navigation .main-nav ul .current-menu-parent > a, .main-navigation .main-nav ul .current-menu-ancestor > a {
				color: #FFFFFF;
				background-color: #3f3f3f;
			}
			.main-navigation .main-nav ul .current-menu-item > a:hover, .main-navigation .main-nav ul .current-menu-parent > a:hover, .main-navigation .main-nav ul .current-menu-ancestor > a:hover, .main-navigation .main-nav ul .current-menu-item.sfHover > a, .main-navigation .main-nav ul .current-menu-parent.sfHover > a, .main-navigation .main-nav ul .current-menu-ancestor.sfHover > a {
				color: #FFFFFF;
				background-color: #3f3f3f;
			}
			.main-navigation .main-nav ul ul .current-menu-item > a, .main-navigation .main-nav ul ul .current-menu-parent > a, .main-navigation .main-nav ul ul .current-menu-ancestor > a {
				color: #FFFFFF;
				background-color: #4f4f4f;
			}
			.main-navigation .main-nav ul ul .current-menu-item > a:hover, .main-navigation .main-nav ul ul .current-menu-parent > a:hover, .main-navigation .main-nav ul ul .current-menu-ancestor > a:hover, .main-navigation .main-nav ul ul .current-menu-item.sfHover > a, .main-navigation .main-nav ul ul .current-menu-parent.sfHover > a, .main-navigation .main-nav ul ul .current-menu-ancestor.sfHover > a {
				color: #FFFFFF;
				background-color: #4f4f4f;
			}
			.inside-article, .comments-area, .page-header, .one-container .container, .paging-navigation, .inside-page-header {
				background-color: #FFFFFF;
			}
			.entry-meta {
				color: #888888;
			}
			.entry-meta a, .entry-meta a:visited {
				color: #666666;
			}
			.entry-meta a:hover {
				color: #1E73BE;
			}
			.sidebar .widget {
				background-color: #FFFFFF;
			}
			.sidebar .widget .widget-title {
				color: #000000;
			}
			.footer-widgets {
				background-color: #FFFFFF;
			}
			.footer-widgets a, .footer-widgets a:visited {
				color: #1e73be;
			}
			.footer-widgets a:hover {
				color: #000000;
			}
			.footer-widgets .widget-title {
				color: #000000;
			}
			.site-info {
				background-color: #222222;
				color: #ffffff;
			}
			.site-info a, .site-info a:visited {
				color: #ffffff;
			}
			.site-info a:hover {
				color: #606060;
			}
			input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], textarea {
				background-color: #FAFAFA;
				border-color: #CCCCCC;
				color: #666666;
			}
			input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="search"]:focus, textarea:focus {
				background-color: #FFFFFF;
				color: #666666;
				border-color: #BFBFBF;
			}
			button, html input[type="button"], input[type="reset"], input[type="submit"], .button, .button:visited {
				background-color: #666666;
				color: #FFFFFF;
			}
			button:hover, html input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, button:active, html input[type="button"]:active, input[type="reset"]:active, input[type="submit"]:active, .button:active {
				background-color: #3F3F3F;
				color: #FFFFFF;
			}
			.main-navigation .mobile-bar-items a, .main-navigation .mobile-bar-items a:hover, .main-navigation .mobile-bar-items a:focus {
				color: #FFFFFF;
			}
			.inside-header {
				padding: 40px 40px 40px 40px;
			}
			.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content {
				padding: 40px 40px 40px 40px;
			}
			.one-container.right-sidebar .site-main, .one-container.both-right .site-main {
				margin-right: 40px;
			}
			.one-container.left-sidebar .site-main, .one-container.both-left .site-main {
				margin-left: 40px;
			}
			.one-container.both-sidebars .site-main {
				margin: 0px 40px 0px 40px;
			}
			.ignore-x-spacing {
				margin-right: -40px;
				margin-bottom: 40px;
				margin-left: -40px;
			}
			.ignore-xy-spacing {
				margin: -40px -40px 40px -40px;
			}
			.main-navigation .main-nav ul li a, .menu-toggle, .main-navigation .mobile-bar-items a {
				padding-left: 20px;
				padding-right: 20px;
				line-height: 60px;
			}
			.nav-float-right .main-navigation .main-nav ul li a {
				line-height: 60px;
			}
			.main-navigation .main-nav ul ul li a {
				padding: 10px 20px 10px 20px;
			}
			.main-navigation ul ul {
				top: 60px;
			}
			.navigation-search {
				height: 60px;
				line-height: 0px;
			}
			.navigation-search input {
				height: 60px;
				line-height: 0px;
			}
			.widget-area .widget {
				padding: 40px 40px 40px 40px;
			}
			.footer-widgets {
				padding: 40px 0px 40px 0px;
			}
			.site-info {
				padding: 20px 0px 20px 0px;
			}
			.right-sidebar.separate-containers .site-main {
				margin: 20px 20px 20px 0px;
			}
			.left-sidebar.separate-containers .site-main {
				margin: 20px 0px 20px 20px;
			}
			.both-sidebars.separate-containers .site-main {
				margin: 20px;
			}
			.both-right.separate-containers .site-main {
				margin: 20px 20px 20px 0px;
			}
			.separate-containers .site-main {
				margin-top: 20px;
				margin-bottom: 20px;
			}
			.separate-containers .page-header-image, .separate-containers .page-header-content, .separate-containers .page-header-image-single, .separate-containers .page-header-content-single {
				margin-top: 20px;
			}
			.both-left.separate-containers .site-main {
				margin: 20px 0px 20px 20px;
			}
			.separate-containers .inside-right-sidebar, .inside-left-sidebar {
				margin-top: 20px;
				margin-bottom: 20px;
			}
			.separate-containers .widget, .separate-containers .hentry, .separate-containers .page-header, .widget-area .main-navigation {
				margin-bottom: 20px;
			}
			.both-left.separate-containers .inside-left-sidebar {
				margin-right: 10px;
			}
			.both-left.separate-containers .inside-right-sidebar {
				margin-left: 10px;
			}
			.both-right.separate-containers .inside-left-sidebar {
				margin-right: 10px;
			}
			.both-right.separate-containers .inside-right-sidebar {
				margin-left: 10px;
			}
			.one-container .sidebar .widget {
				padding: 0px;
			}
			.main-navigation .mobile-bar-items a {
				padding-left: 20px;
				padding-right: 20px;
				line-height: 60px;
			}
			.menu-item-has-children ul .dropdown-menu-toggle {
				padding-top: 10px;
				padding-bottom: 10px;
				margin-top: -10px;
			}
			.entry-header {
				display: none
			}
			.page-content, .entry-content, .entry-summary {
				margin-top: 0
			}
		</style>

		<link rel="stylesheet" id="generate-mobile-style-css" href="https://www.enraonline.de/wp-content/themes/generatepress/css/mobile.min.css?ver=1.3.27" type="text/css" media="all">
		<link rel="stylesheet" id="fontawesome-css" href="https://www.enraonline.de/wp-content/themes/generatepress/css/font-awesome.min.css?ver=4.5.0" type="text/css" media="all">
		<style type="text/css">
			body {
				background-color: #efefef;
				color: #3a3a3a;
			}
			a, a:visited {
				color: #1e73be;
				text-decoration: none;
			}
			a:hover, a:focus, a:active {
				color: #000000;
			}
			body .grid-container {
				max-width: 1100px;
			}
			body, button, input, select, textarea {
				font-family: "Open Sans", sans-serif; /*font-family: Arial, Helvetica, sans-serif;*/
				font-size: 14px;
			}
			span.wpcf7-list-item {
				display: block;
			}
			.page-id-8 iframe {
				margin-top: -40px;
			}
			.main-title {
				font-weight: bold;
				font-size: 40px;
			}
			.site-description {
				font-size: 15px;
			}
			.main-navigation a, .menu-toggle {
				font-size: 15px;
			}
			.main-navigation .main-nav ul ul li a {
				font-size: 14px;
			}
			.widget-title {
				font-size: 20px;
			}
			.sidebar .widget, .footer-widgets .widget {
				font-size: 14px;
			}
			h1 {
				font-weight: 300;
				font-size: 40px;
			}
			h2 {
				font-weight: 300;
				font-size: 30px;
			}
			h3 {
				font-size: 20px;
			}
			.site-info {
				font-size: 14px;
			}
			.site-header {
				background-color: #FFFFFF;
				color: #3a3a3a;
			}
			.site-header a, .site-header a:visited {
				color: #3a3a3a;
			}
			.main-title a, .main-title a:hover, .main-title a:visited {
				color: #222222;
			}
			.site-description {
				color: #999999;
			}
			.main-navigation, .main-navigation ul ul {
				background-color: #d6dff7;
			}
			.navigation-search input[type="search"], .navigation-search input[type="search"]:active {
				color: #00448F;
				background-color: #7495e2;
			}
			.navigation-search input {
				opacity: 1;
			}
			.navigation-search input[type="search"]:focus {
				color: #00448F;
				background-color: #7495e2;
			}
			.main-navigation ul ul {
				background-color: #d6dff7;
			}
			.main-navigation .main-nav ul li a, .menu-toggle {
				color: #00448F; /* font-weight: bold; */
			}
			button.menu-toggle:hover, button.menu-toggle:active, button.menu-toggle:focus, .main-navigation .mobile-bar-items a, .main-navigation .mobile-bar-items a:hover, .main-navigation .mobile-bar-items a:focus {
				color: #FFFFFF;
			}
			.main-navigation .main-nav ul ul li a {
				color: #FFFFFF;
			}
			.main-navigation .main-nav ul li > a:hover, .main-navigation .main-nav ul li.sfHover > a {
				color: #00448F; /* font-weight: bold; */
				background-color: #9fb4f2;
			}
			.main-navigation .main-nav ul ul li > a:hover, .main-navigation .main-nav ul ul li.sfHover > a {
				color: #FFFFFF;
				background-color: #4f4f4f;
			}
			.main-navigation .main-nav ul .current-menu-item > a, .main-navigation .main-nav ul .current-menu-parent > a, .main-navigation .main-nav ul .current-menu-ancestor > a {
				color: #00448F; /* font-weight: bold; */
				background-color: #9fb4f2;
			}
			.main-navigation .main-nav ul .current-menu-item > a:hover, .main-navigation .main-nav ul .current-menu-parent > a:hover, .main-navigation .main-nav ul .current-menu-ancestor > a:hover, .main-navigation .main-nav ul .current-menu-item.sfHover > a, .main-navigation .main-nav ul .current-menu-parent.sfHover > a, .main-navigation .main-nav ul .current-menu-ancestor.sfHover > a {
				color: #00448F; /* font-weight: bold; */
				background-color: #9fb4f2;
			}
			.main-navigation .main-nav ul ul .current-menu-item > a, .main-navigation .main-nav ul ul .current-menu-parent > a, .main-navigation .main-nav ul ul .current-menu-ancestor > a {
				color: #FFFFFF;
				background-color: #4f4f4f;
			}
			.main-navigation .main-nav ul ul .current-menu-item > a:hover, .main-navigation .main-nav ul ul .current-menu-parent > a:hover, .main-navigation .main-nav ul ul .current-menu-ancestor > a:hover, .main-navigation .main-nav ul ul .current-menu-item.sfHover > a, .main-navigation .main-nav ul ul .current-menu-parent.sfHover > a, .main-navigation .main-nav ul ul .current-menu-ancestor.sfHover > a {
				color: #FFFFFF;
				background-color: #4f4f4f;
			}
			.inside-article, .comments-area, .page-header, .one-container .container, .paging-navigation, .inside-page-header {
				background-color: #FFFFFF;
			}
			.entry-meta {
				color: #888888;
			}
			.entry-meta a, .entry-meta a:visited {
				color: #666666;
			}
			.entry-meta a:hover {
				color: #1E73BE;
			}
			.sidebar .widget {
				background-color: #FFFFFF;
			}
			.sidebar .widget .widget-title {
				color: #000000;
			}
			.footer-widgets {
				background-color: #FFFFFF;
			}
			.footer-widgets a, .footer-widgets a:visited {
				color: #1e73be;
			}
			.footer-widgets a:hover {
				color: #000000;
			}
			.footer-widgets .widget-title {
				color: #000000;
			}
			.site-info {
				background-color: #d6dff7;
				color: #00448F;
			}
			.site-info a, .site-info a:visited {
				color: #00448F;
			}
			.site-info a:hover {
				color: #222222;
			}
			input[type="text"], input[type="email"], input[type="url"], input[type="password"], input[type="search"], textarea {
				background-color: #FAFAFA;
				border-color: #CCCCCC;
				color: #666666;
			}
			input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="search"]:focus, textarea:focus {
				background-color: #FFFFFF;
				color: #666666;
				border-color: #BFBFBF;
			}
			button, html input[type="button"], input[type="reset"], input[type="submit"], .button, .button:visited {
				background-color: #d6dff7;
				color: #00448F;
			}
			button:hover, html input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .button:hover, button:active, html input[type="button"]:active, input[type="reset"]:active, input[type="submit"]:active, .button:active {
				background-color: #9fb4f2;
				color: #00448F;
			}
			.main-navigation .mobile-bar-items a, .main-navigation .mobile-bar-items a:hover, .main-navigation .mobile-bar-items a:focus {
				color: #00448F;
			}
			.inside-header {
				padding: 20px 20px 20px 20px;
			}
			.inside-header .site-logo {
				float: left;
			}
			.inside-header .site-branding {
				float: right;
			}
			.inside-header .site-logo img {
				height: 80px;
			}
			.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content {
				padding: 20px;
			}/* .page-id-8.separate-containers .inside-article, .separate-containers .comments-area, .separate-containers .page-header, .separate-containers .paging-navigation, .one-container .site-content { padding: 0 20px 20px 20px; } */
			.one-container.right-sidebar .site-main, .one-container.both-right .site-main {
				margin-right: 20px;
			}
			.one-container.left-sidebar .site-main, .one-container.both-left .site-main {
				margin-left: 20px;
			}
			.one-container.both-sidebars .site-main {
				margin: 0px 20px 0px 20px;
			}
			.ignore-x-spacing {
				margin-right: -20px;
				margin-bottom: 20px;
				margin-left: -20px;
			}
			.ignore-xy-spacing {
				margin: -20px -20px 20px -20px;
			}
			.main-navigation .main-nav ul li a, .menu-toggle, .main-navigation .mobile-bar-items a {
				padding-left: 20px;
				padding-right: 20px;
				line-height: 40px;
			}
			.nav-float-right .main-navigation .main-nav ul li a {
				line-height: 40px;
			}
			.main-navigation .main-nav ul ul li a {
				padding: 20px 20px 20px 20px;
			}
			.main-navigation ul ul {
				top: 60px;
			}
			.navigation-search {
				height: 40px;
				line-height: 0px;
			}
			.navigation-search input {
				height: 40px;
				line-height: 0px;
			}
			.widget-area .widget {
				padding: 20px;
			}
			.footer-widgets {
				padding: 20px 0px 20px 0px;
			}
			.site-info {
				padding: 10px 0px 10px 0px;
			}
			.right-sidebar.separate-containers .site-main {
				margin: 20px 20px 20px 0px;
			}
			.left-sidebar.separate-containers .site-main {
				margin: 20px 0px 20px 20px;
			}
			.both-sidebars.separate-containers .site-main {
				margin: 20px;
			}
			.both-right.separate-containers .site-main {
				margin: 20px 20px 20px 0px;
			}
			.separate-containers .site-main {
				margin-top: 20px;
				margin-bottom: 20px;
			}/* .page-id-8.separate-containers .site-main { margin-top: 0; margin-bottom: 20px; } */
			.separate-containers .page-header-image, .separate-containers .page-header-content, .separate-containers .page-header-image-single, .separate-containers .page-header-content-single {
				margin-top: 20px;
			}
			.both-left.separate-containers .site-main {
				margin: 20px 0px 20px 20px;
			}
			.separate-containers .inside-right-sidebar, .inside-left-sidebar {
				margin-top: 20px;
				margin-bottom: 20px;
			}
			.separate-containers .widget, .separate-containers .hentry, .separate-containers .page-header, .widget-area .main-navigation {
				margin-bottom: 20px;
			}
			.both-left.separate-containers .inside-left-sidebar {
				margin-right: 20px;
			}
			.both-left.separate-containers .inside-right-sidebar {
				margin-left: 20px;
			}
			.both-right.separate-containers .inside-left-sidebar {
				margin-right: 20px;
			}
			.both-right.separate-containers .inside-right-sidebar {
				margin-left: 20px;
			}
			.one-container .sidebar .widget {
				padding: 0px;
			}
			.inside-footer-widgets .inner-padding {
				padding: 0 20px;
			}
			.main-navigation .mobile-bar-items a {
				padding-left: 20px;
				padding-right: 20px;
				line-height: 40px;
			}
			.entry-header {
				display: none
			}
			.page-content, .entry-content, .entry-summary {
				margin-top: 0
			}
			@media (max-width: 1024px) {
				.inside-footer-widgets > div {
					margin-bottom: 50px
				}
				.active-footer-widgets-1 .footer-widget-1, .active-footer-widgets-3 .footer-widget-3, .active-footer-widgets-5 .footer-widget-5 {
					width: 100%
				}
			}
			@media (max-width: 768px) {
				.main-navigation li a.dropdown-toggle, .mobile-bar-items {
					position: absolute;
					right: 0;
					top: 0
				}
				.header-widget, .woocommerce .woocommerce-ordering select {
					max-width: 100%
				}
				.content-area, .sidebar {
					float: none;
					width: 100%;
					left: 0;
					right: 0
				}
				.main-title a {
					font-size: 80%
				}
				.main-title {
					line-height: 1em
				}
				.main-navigation.toggled .main-nav > ul, .menu-toggle {
					display: block
				}
				.main-navigation ul li.sfHover > ul {
					display: none
				}
				button.menu-toggle {
					background-color: transparent;
					width: 100%;
					border: 0
				}
				button.menu-toggle:active, button.menu-toggle:focus, button.menu-toggle:hover {
					background-color: transparent;
					border: 0;
					outline: 0
				}
				.children.toggled-on, .sub-menu.toggled-on {
					display: block !important
				}
				.main-navigation .main-nav ul li .dropdown-toggle {
					display: block
				}
				.main-navigation ul {
					display: none
				}
				.nav-float-left .main-navigation, .nav-float-right .main-navigation {
					float: none;
					margin-bottom: 20px
				}
				.nav-float-left .site-branding, .nav-float-left .site-logo {
					float: none
				}
				.navigation-search {
					bottom: auto;
					top: 0
				}
				.mobile-bar-items {
					display: block;
					z-index: 21;
					list-style-type: none
				}
				.mobile-bar-items a {
					display: inline-block
				}
				.main-navigation .menu li.search-item {
					display: none !important
				}
				li.search-item a {
					padding-left: 20px;
					padding-right: 20px
				}
				.site-main {
					margin-left: 0 !important;
					margin-right: 0 !important
				}
				body:not(.no-sidebar) .site-main {
					margin-bottom: 0 !important
				}
				.fluid-header .inside-header, .menu-toggle {
					text-align: center
				}
				.fluid-header .inside-header .site-branding {
					margin-bottom: 10px;
				}
				.fluid-header .inside-header .site-branding, .fluid-header .inside-header .site-logo {
					float: none
				}
				.nav-search-enabled .main-navigation .menu-toggle {
					text-align: left
				}
				.main-nav .sf-menu > li {
					float: none;
					clear: both
				}
				.nav-aligned-center.nav-above-header .main-navigation .sf-menu > li, .nav-aligned-center.nav-below-header .main-navigation .sf-menu > li, .nav-aligned-right.nav-above-header .main-navigation .sf-menu > li, .nav-aligned-right.nav-below-header .main-navigation .sf-menu > li {
					display: block;
					margin: 0;
					text-align: left
				}
				.main-navigation .main-nav ul ul {
					position: relative;
					top: 0;
					left: 0;
					width: 100%
				}
				.header-widget {
					float: none;
					text-align: center
				}
				.main-navigation {
					text-align: left
				}
				.navigation-search, .navigation-search input {
					width: 100%;
					max-width: 100%
				}
				.alignleft, .alignright {
					float: none;
					display: block;
					margin-left: 0;
					margin-right: 0
				}
				.post-image-aligned-left .post-image, .post-image-aligned-right .post-image {
					float: none;
					margin: 2em 0;
					text-align: center
				}
				.sf-menu .menu-item-has-children > a:first-child {
					padding-right: 1em
				}
				.sf-menu .menu-item-has-children > a:first-child:after {
					display: none
				}
				.site-info {
					padding-left: 10px;
					padding-right: 10px
				}
				.edd_download {
					display: block;
					float: none !important;
					margin-bottom: 1.5em;
					width: 100% !important
				}
				.woocommerce .woocommerce-ordering, .woocommerce-page .woocommerce-ordering {
					float: none
				}
			}
		</style>

		<!--end of global css-->
		<!--page level css-->
		@yield('header_styles')
		<!--end of page level css-->
	</head>

	<body itemtype="https://schema.org/WebPage" itemscope="itemscope" class="home singular page page-id-5 page-template-default generatepress no-sidebar nav-below-header fluid-header separate-containers active-footer-widgets-3  nav-aligned-left header-aligned-left dropdown-hover">
		<!-- Header Start -->
		<header itemtype="https://schema.org/WPHeader" itemscope="itemscope" id="masthead" class="site-header">
			<div class="inside-header grid-container grid-parent">
				<div class="site-branding">
					<p class="main-title" itemprop="headline">
						<a href="/" title="{{ trans('enra/general.enra_dashboard') }}" rel="home">{{ trans('enra/general.dashboard') }}</a>
					</p>
					<p class="site-description">
						{{ trans('enra/general.enra') }}
					</p>
				</div>
				<div class="site-logo">
					<a href="/" title="{{ trans('enra/general.enra_dashboard') }}" rel="home"> <img class="header-image" src="https://www.enraonline.de/wp-content/uploads/2016/01/ENRA-Logo-WEB-ex-Payoff.png" alt="{{ trans('enra/general.enra_dashboard') }}" title="{{ trans('enra/general.enra_dashboard') }}"> </a>
				</div>
			</div>
		</header>
		<!-- //Header End -->

		<!-- Navigation Start -->
		<nav itemtype="https://schema.org/SiteNavigationElement" itemscope="itemscope" id="site-navigation" class="main-navigation">
			<div class="inside-navigation grid-container grid-parent">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<span class="mobile-menu">Menu</span>
				</button>
				<div id="primary-menu" class="main-nav">
					<ul id="menu-primair-menu" class=" menu sf-menu">
						
						@include('layouts/menus/menu_custom')
						
					</ul>
				</div>
			</div><!-- .inside-navigation -->
		</nav>
		<!-- //Navigation End -->

		<!-- slider / breadcrumbs section -->
		@yield('top')

		<!-- Content Section Start -->
		<div id="page" class="hfeed site grid-container container grid-parent">
			<div id="content" class="site-content">
				@yield('content')
			</div>
			<!-- //Content Section End -->
		</div>

		<!-- Footer Section Start -->
		<div class="site-footer">
			<div id="footer-widgets" class="site footer-widgets">
				<div class="inside-footer-widgets grid-container grid-parent">
					<div class="footer-widget-1 grid-parent grid-33 tablet-grid-50">
						<aside id="text-3" class="widget inner-padding widget_text">
							<div class="textwidget"></div>
						</aside>
					</div>
					<div class="footer-widget-2 grid-parent grid-33 tablet-grid-50">
						<aside id="text-5" class="widget inner-padding widget_text">
							<div class="textwidget"></div>
						</aside>
					</div>
					<div class="footer-widget-3 grid-parent grid-33 tablet-grid-50">
						<aside id="text-4" class="widget inner-padding widget_text">
							<div class="textwidget"></div>
						</aside>
					</div>
				</div>
			</div>
			<footer class="site-info" itemtype="https://schema.org/WPFooter" itemscope="itemscope">
				<div class="inside-site-info grid-container grid-parent">
					<span class="copyright">Copyright © 2017</span> · <a href="https://www.enra.eu/de" target="_blank" title="ENRA verzekeringen B.V." itemprop="url">ENRA verzekeringen B.V.</a>
				</div>
			</footer><!-- .site-info -->
		</div>
		<!-- //Footer Section End -->

		<!--global js starts-->
		<script src="{{ asset('assets/js/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
		<!--livicons-->
		<script src="{{ asset('assets/vendors/livicons/minified/raphael-min.js') }}"></script>
		<script src="{{ asset('assets/vendors/livicons/minified/livicons-1.4.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/frontend/josh_frontend.js') }}"></script>
		<!--global js end-->
		<!-- begin page level js -->
		@yield('footer_scripts')
		<!-- end page level js -->
	</body>

</html>
