@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{ trans('enra/general.dashboard') }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    
    <link href="{{ asset('assets/vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/calendar_custom.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" media="all" href="{{ asset('assets/vendors/jvectormap/jquery-jvectormap.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/only_dashboard.css') }}" />
    
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
    <h1>{{ trans('enra/general.enra_dashboard') }}</h1>
    <ol class="breadcrumb">
        <li class="active">
            <a href="#">
                <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i> {{ trans('enra/general.dashboard') }}
            </a>
        </li>
    </ol>
</section>
        
@stop

{{-- page level scripts --}}
@section('footer_scripts')
        

@stop
