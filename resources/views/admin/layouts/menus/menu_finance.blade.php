@if (Sentinel::hasAnyAccess('admin', 'finance.*'))

<li {!! (Request::is('admin/finance/agentinformation') ? 'class="active"' : '') !!}>
    <a href="#"> <i class="livicon" data-name="doc-portrait" data-c="#67C5DF" data-hc="#67C5DF" data-size="18" data-loop="true"></i> <span class="title">{{ trans('enra/finance.finance') }}</span> <span class="fa arrow"></span> </a>
    <ul class="sub-menu">
        <li {!! (Request::is('admin/finance/agentinformation') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/finance/agentinformation') }}"> <i class="fa fa-angle-double-right"></i> {{ trans('enra/finance.agent_information') }} </a>
        </li>
    </ul>
</li>

@endif