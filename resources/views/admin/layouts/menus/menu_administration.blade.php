@if (Sentinel::hasAnyAccess('admin', 'administration.*'))

<li {!! (Request::is('admin/administration/agentinformation') ? 'class="active"' : '') !!}>
    <a href="#"> <i class="livicon" data-name="doc-portrait" data-c="#67C5DF" data-hc="#67C5DF" data-size="18" data-loop="true"></i> <span class="title">{{ trans('enra/administration.administration') }}</span> <span class="fa arrow"></span> </a>
    <ul class="sub-menu">
        <li {!! (Request::is('admin/administration/agentinformation') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('admin/administration/agentinformation') }}"> <i class="fa fa-angle-double-right"></i> {{ trans('enra/administration.agent_information') }} </a>
        </li>
    </ul>
</li>

@endif