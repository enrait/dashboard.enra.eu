@if (Sentinel::hasAnyAccess('admin', 'sales.*'))

<li {!! (Request::is('admin/sales/agentinformation') || Request::is('admin/sales/productionsummary') || Request::is('admin/sales/productionplusminussummary') || Request::is('admin/sales/crm') || Request::is('admin/sales/crm/contactmoments') ? 'class="active"' : '') !!}>
	<a href="#"> <i class="livicon" data-name="doc-portrait" data-c="#67C5DF" data-hc="#67C5DF" data-size="18" data-loop="true"></i> <span class="title">{{ trans('enra/sales.sales') }}</span> <span class="fa arrow"></span> </a>
	<ul class="sub-menu">
		<li {!! (Request::is('admin/sales/agentinformation') || Request::is('admin/sales/productionsummary') || Request::is('admin/sales/productionplusminussummary') ? 'class="active"' : '') !!}>
			<a href="#"> <i class="livicon" data-name="doc-portrait" data-c="#67C5DF" data-hc="#67C5DF" data-size="18" data-loop="true"></i> <span class="title">{{ trans('enra/sales.anva') }}</span> <span class="fa arrow"></span> </a>
			<ul class="sub-menu">
				<li {!! (Request::is('admin/sales/agentinformation')? 'class="active"' : '') !!}>
					<a href="{{ URL::to('admin/sales/agentinformation') }}"> <i class="fa fa-angle-double-right"></i> {{ trans('enra/sales.agent_information') }} </a>
				</li>
				<li {!! (Request::is('admin/sales/productionsummary') ? 'class="active"' : '') !!}>
					<a href="{{ URL::to('admin/sales/productionsummary') }}"> <i class="fa fa-angle-double-right"></i> {{ trans('enra/sales.production_summary') }} </a>
				</li>
				<li {!! (Request::is('admin/sales/productionplusminussummary') ? 'class="active"' : '') !!}>
					<a href="{{ URL::to('admin/sales/productionplusminussummary') }}"> <i class="fa fa-angle-double-right"></i> {{ trans('enra/sales.plusminus_summary') }} </a>
				</li>
			</ul>
		</li>
		<li {!! (Request::is('admin/sales/crm') || Request::is('admin/sales/crm/contactmoments') ? 'class="active"' : '') !!}>
			<a href="#"> <i class="livicon" data-name="doc-portrait" data-c="#67C5DF" data-hc="#67C5DF" data-size="18" data-loop="true"></i> <span class="title">{{ trans('enra/sales.crm_menu') }}</span> <span class="fa arrow"></span> </a>
			<ul class="sub-menu">
				<li {!! (Request::is('admin/sales/crm') ? 'class="active"' : '') !!}>
					<a href="{{ URL::to('admin/sales/crm') }}"> <i class="fa fa-angle-double-right"></i> {{ trans('enra/sales.crm') }} </a>
				</li>
				<li {!! (Request::is('admin/sales/crm/contactmoments') ? 'class="active"' : '') !!}>
					<a href="{{ URL::to('admin/sales/crm/contactmoments') }}"> <i class="fa fa-angle-double-right"></i> {{ trans('enra/sales.contact_moments') }} </a>
				</li>
			</ul>
		</li>
	</ul>
</li>

@endif