@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Productie overzicht - ENRA verzekeringen
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/portlet.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/iCheck/skins/all.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/formelements.css') }}" rel="stylesheet" type="text/css" />
<!-- end of page level css-->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
	<h1>Productie overzicht</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="14" data-color="#000"></i> Dashboard </a>
		</li>
		<li class="active">
			Productie overzicht
		</li>
	</ol>
</section>
<section class="content">

    <section id="sectionAgentSummary">
        
        
    </section>

</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js-->

<script src="{{ asset('assets/vendors/select2/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/iCheck/icheck.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/agent_summary.js') }}" type="text/javascript"></script>

<script>
jQuery(document).ready(function() {
    
    initAgentSummary(); 
    
    @if ($selectData['showPDF']) 
    
        var form = document.formAgentRevenueAmountFilter;
      
        original = form.action;
          
        form.action = "/admin/agents/agentsummary/pdf";
        form.target = "_blank";
        form.submit();
          
        form.action = original;
        form.target = "_self";
    
    @endif

});

</script>
<!-- end of page level js-->
@stop
