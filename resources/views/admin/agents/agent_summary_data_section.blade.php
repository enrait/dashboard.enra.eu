<section id="{{ $sectionId }}">
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> Filter selectie</h3>
				<span class="pull-right"> <i class="extraSection fa fa-fw fa-plus-circle clickable"></i> <i class="fa fa-fw fa-chevron-down clickable panel-collapsed"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body" style="display: none;">
				<form accept-charset="utf-8" class="form-vertical formAgentSummary" name="formAgentSummary" method="POST" action="/api/agents/agentsummary/" novalidate="">
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<div class="form-group">
								<label class="control-label "> Jaar </label>
								<select name="year[]" class="form-control select2 select2_year" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['year']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['year']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-8 col-md-8">
							<div class="form-group">
								<label class="control-label "> Maand </label>
								<select name="month[]" class="form-control select2 select2_month" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['month']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['month']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="form-group">
								<label class="control-label "> Land </label>
								<select name="country[]" class="form-control select2 select2_country" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['country']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['country']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="form-group">
								<label class="control-label "> Regio </label>
								<select name="region[]" class="form-control select2 select2_region" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['region']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['region']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<div class="form-group">
								<label class="control-label "> Maatschappij </label>
								<select name="company[]" class="form-control select2 select2_company" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['company']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['company']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="form-group">
								<label class="control-label "> Branch </label>
								<select name="branch[]" class="form-control select2 select2_branch" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['branche']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['branche']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="form-group">
								<label class="control-label "> Object soort </label>
								<select name="objecttype[]" class="form-control select2 select2_objecttype" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['objecttype']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['objecttype']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label class="control-label "> collectiviteit </label>
								<select name="organisation[]" class="form-control select2 select2_organisation" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['organisation']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['organisation']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label class="control-label "> Agent </label>
								<select name="agent[]" class="form-control select2 select2_agent" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['agent']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['agent']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectValue }} - {{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label class="control-label "> Dekking </label>
								<select name="coverage[]" class="form-control select2 select2_coverage" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['coverage']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['coverage']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectValue }} - {{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label class="control-label "> Looptijd </label>
								<select name="duration[]" class="form-control select2 select2_duration" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['duration']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['duration']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<div class="form-group">
								<label class="control-label"> Schade soort </label>
								<select name="damagetype[]" class="form-control select2 select2_damagetype" multiple="multiple" style="width: 100%">
									<option value=""></option>
									@foreach ($selectData['damagetype']['data'] as $selectValue => $selectDescription)
									<option {{ in_array($selectValue, $selectData['damagetype']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6 text-right">
							<div class="form-group">
								<label class="control-label">Toon schade</label>
								<label>
									<input type="checkbox" name="showDamage" class="minimal" {{ $selectData['showDamage'] ? 'checked' : '' }}>
								</label>
							</div>
							<div class="form-group">
								<a class="btn btn-primary" href="javascript:void(0)" onclick="window.open( '/admin/agents/agentsummary/pdf/?' + $('.formAgentSummary').serialize() );return false;" role="button">PDF output</a>
								<input type="submit" class="btn btn-primary" value="Filter toepassen">
							</div>
						</div>
					</div>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="sectionId" value="{{ $sectionId }}">
				</form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="table" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> Nieuw afgesloten verzekeringen : <strong>per maand - aantal</strong></h3>
				<span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Jaar</th>
							@foreach ($tableAgentRevenueAmount['columns'] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
							<th class="numeric">{{ $tableAgentRevenueAmountColumn }}</th>
							@endforeach
							<th class="numeric"><strong>Totaal</strong></th>
							<th class="numeric">% t.o.v. vorig jaar</th>
						</tr>
					</thead>
					<tbody>
						@foreach (array_reverse($tableAgentRevenueAmount['rows']) as $tableAgentRevenueAmountRow)
						<tr>
							<td>{{ isset($tableAgentRevenueAmountRow['year']) ? $tableAgentRevenueAmountRow['year'] : '' }}</td>
							@foreach ($tableAgentRevenueAmount['columns'] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
							<td class="numeric">{{ isset($tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey]) ? $tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey] : '' }}</td>
							@endforeach
							<td class="numeric"><strong>{{ isset($tableAgentRevenueAmountRow['total']) ? $tableAgentRevenueAmountRow['total'] : '' }}</strong></td>
							<td class="numeric {{ isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] : '0'), 0, ',', '.') }}%</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-5">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="table" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> Nieuw afgesloten verzekeringen :
				<br />
				<strong>per looptijd en dekkingsvorm - percentage</strong></h3>
				<span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Jaar</th>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumn)
							<th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumn)
							<th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach ($tableYearsCoverageDuration as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : '' }}</td>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
							@endforeach
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-lg-7">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="table" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> Nieuw afgesloten verzekeringen :
				<br />
				<strong>per looptijd en dekkingsvorm - aantal</strong></h3>
				<span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Jaar</th>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumn)
							<th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumn)
							<th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
							@endforeach
							<th class="numeric"><strong>Totaal</strong></th>
							<!-- <th class="numeric"><strong>Premie €</strong></th> -->
							@foreach ($tableAgentCommissionSum['columns'] as $tableAgentCommissionSumColumn)
							<th class="numeric">{{ $tableAgentCommissionSumColumn }}</th>
							@endforeach

							@if ($selectData['showDamage'])
							@foreach ($tableAgentDamageSum['columns'] as $tableAgentDamageSumColumn)
							<th class="numeric">{{ $tableAgentDamageSumColumn }}</th>
							@endforeach
							@endif

						</tr>
					</thead>
					<tbody>
						@foreach ($tableYearsCoverageDuration as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
							<td class="numeric">{{ isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? $tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : '' }}</td>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
							<td class="numeric">{{ isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? $tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : '' }}</td>
							@endforeach
							<td class="numeric"><strong>{{ isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? $tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : '' }}</strong></td>
							<!-- <td class="currency">{{ isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? number_format($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'], 0, ',', '.') : '' }}</td> -->
							<td class="currency">{{ isset($tableAgentCommissionSum['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($tableAgentCommissionSum['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td>
							<!-- <td class="currency">{{ isset($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td> -->

							@if ($selectData['showDamage'])
							<td class="numeric">{{ (number_format(((isset($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total'] : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format(((isset($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total'] * -1 : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 0, ',', '.') . '%') : ''}}</td>
							@endif

						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@if ($selectData['showDamage'])

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="table" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> Schadecijfer verzekeringen : <strong>per schadesoort - percentage</strong></h3>
				<span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Jaar</th>
							@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumn)
							<th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach (array_reverse($tableAgentDamageAmount['rows']) as $tableAgentDamageAmountRow)
						<tr>
							<td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
							@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
							@endforeach
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="table" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> Schadecijfer verzekeringen : <strong>per schadesoort - bedrag</strong></h3>
				<span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body">
				<table class="table table-bordered table-striped table-condensed flip-content">
					<thead class="flip-content">
						<tr>
							<th>Jaar</th>
							@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumn)
							<th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
							@endforeach
							<th class="numeric"><strong>Totaal €</strong></th>
							<th class="numeric">% t.o.v. vorig jaar</th>
						</tr>
					</thead>
					<tbody>
						@foreach (array_reverse($tableAgentDamageAmount['rows']) as $tableAgentDamageAmountRow)
						<tr>
							<td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
							@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
							<td class="currency">{{ isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? number_format($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey], 0, ',', '.') : '' }}</td>
							@endforeach
							<td class="currency"><strong>{{ isset($tableAgentDamageAmountRow['total']) ? number_format($tableAgentDamageAmountRow['total'], 0, ',', '.') : '' }}</strong></td>
							<td class="numeric {{ isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] : ''), 0, ',', '.') }}%</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endif

</section>