<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="table" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> {{ trans('enra/administration.selected_agent') }}</h3>
				<span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body">
				<table id="tableAgentData" class="table table-bordered table-striped table-condensed">
					<tbody>
						@foreach ($agentData as $agentDataKey => $agentDataValue)
							@if ( $agentDataValue != '' && $agentDataValue != '+0000000.00' && $agentDataValue != '0000000.00' && $agentDataValue != '+0000000.  ' && $agentDataValue != '+0000000.+0' )
								<tr>
									<td><strong>{{ $agentDataKey }}</strong></td>
									<td>{{ $agentDataValue }}</td>
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>
