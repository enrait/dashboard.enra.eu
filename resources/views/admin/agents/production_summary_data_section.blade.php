<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-fw fa-filter"></i> {{ trans('enra/sales.filter_selection') }}
                </h3>
                <span class="pull-right"> <i class="fa fa-fw fa-chevron-down clickable panel-collapsed"></i></span>
            </div>
            <div class="panel-body" style="display: none;">
                <form accept-charset="utf-8" class="form-vertical formAgentSummary" name="formAgentSummary"
                      method="POST" action="/api/agents/productionsummary/" novalidate="">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.year') }} </label>
                                <select name="year[]" class="form-control select2 select2_year" multiple="multiple"
                                        style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['year']['data'] ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['year']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.month') }} </label>
                                <select name="month[]" class="form-control select2 select2_month" multiple="multiple"
                                        style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['month']['data'] ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['month']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.country') }} </label>
                                <select name="country[]" class="form-control select2 select2_country"
                                        multiple="multiple" style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['country']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['country']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.branch') }} </label>
                                <select name="branch[]" class="form-control select2 select2_branch" multiple="multiple"
                                        style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['branche']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['branche']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-sm-4 col-md-4">
						<div class="form-group">
						<label class="control-label "> Maatschappij </label>
						<select name="company[]" class="form-control select2 select2_company" multiple="multiple" style="width: 100%">
						<option value=""></option>
						@foreach ($selectData['company']['data']  ?? [] as $selectValue => $selectDescription)
                            <option {{ in_array($selectValue, $selectData['company']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>


                        @endforeach
                        </select>
                        </div>
                        </div> -->
                        <!-- <div class="col-sm-4 col-md-4">
						<div class="form-group">
						<label class="control-label "> Branch </label>
						<select name="branch[]" class="form-control select2 select2_branch" multiple="multiple" style="width: 100%">
						<option value=""></option>
						@foreach ($selectData['branche']['data']  ?? [] as $selectValue => $selectDescription)
                            <option {{ in_array($selectValue, $selectData['branche']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>


                        @endforeach
                        </select>
                        </div>
                        </div> -->
                        <!-- <div class="col-sm-4 col-md-4">
						<div class="form-group">
						<label class="control-label "> Object soort </label>
						<select name="objecttype[]" class="form-control select2 select2_objecttype" multiple="multiple" style="width: 100%">
						<option value=""></option>
						@foreach ($selectData['objecttype']['data']  ?? [] as $selectValue => $selectDescription)
                            <option {{ in_array($selectValue, $selectData['objecttype']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>


                        @endforeach
                        </select>
                        </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <!-- <div class="col-sm-6 col-md-6">
						<div class="form-group">
						<label class="control-label "> Collectiviteit </label>
						<select name="organisation[]" class="form-control select2 select2_organisation" multiple="multiple" style="width: 100%">
						<option value=""></option>
						@foreach ($selectData['organisation']['data'] ?? [] as $selectValue => $selectDescription)
                            <option {{ in_array($selectValue, $selectData['organisation']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>


                        @endforeach
                        </select>
                        </div>
                        </div> -->
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.region') }} </label>
                                <select name="region[]" class="form-control select2 select2_region" multiple="multiple"
                                        style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['region']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['region']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.agent') }} </label>
                                <select name="agent[]" class="form-control select2 select2_agent" multiple="multiple"
                                        style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['agent']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['agent']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectValue }}
                                            - {{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.duration') }} </label>
                                <select name="duration[]" class="form-control select2 select2_duration"
                                        multiple="multiple" style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['duration']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['duration']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <div class="form-group">
                                <label class="control-label "> {{ trans('enra/sales.coverage') }} </label>
                                <select name="coverage[]" class="form-control select2 select2_coverage"
                                        multiple="multiple" style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['coverage']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['coverage']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectValue }}
                                            - {{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label"> {{ trans('enra/sales.classification') }} </label>
                                <select name="classification[]" class="form-control select2 select2_classification"
                                        multiple="multiple" style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['classification']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['classification']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <div class="form-group">
                                <label class="control-label"> {{ trans('enra/sales.damage_type') }} </label>
                                <select name="damagetype[]" class="form-control select2 select2_damagetype"
                                        multiple="multiple" style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['damagetype']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['damagetype']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label"> {{ trans('enra/sales.weighting_factor') }} </label>
                                <select name="weightingfactor[]" class="form-control select2 select2_weightingfactor"
                                        multiple="multiple" style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['weightingfactor']['data']  ?? []  as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['weightingfactor']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <div class="form-group">
                                <label class="control-label"> {{ trans('enra/sales.potential') }} </label>
                                <select name="potential[]" class="form-control select2 select2_potential"
                                        multiple="multiple" style="width: 100%">
                                    <option value=""></option>
                                    @foreach ($selectData['potential']['data']  ?? [] as $selectValue => $selectDescription)
                                        <option
                                            {{ in_array($selectValue, $selectData['potential']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <label class="control-label">{{ trans('enra/sales.show_damage') }} </label>
                                <label>
                                    <input type="checkbox" name="showDamage"
                                           class="minimal" {{ $selectData['showDamage'] ? 'checked' : '' }}>
                                </label>
                            </div>
                            <div class="form-group">
                                <a class="btn btn-primary" href="javascript:void(0)"
                                   onclick="window.open( '/admin/sales/productionsummary/pdf/?' + $('.formAgentSummary').serialize() );return false;"
                                   role="button">{{ trans('enra/sales.pdf_output') }}</a>
                                <button type="submit" id="submitFilter" class="btn btn-primary has-spinner"
                                        value="Submit"><span class="spinner"><i
                                            class="icon-spin glyphicon glyphicon-refresh"></i></span> {{ trans('enra/sales.apply_filter') }}
                                </button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </form>
            </div>
        </div>
    </div>
</div>

@foreach ($sectionsData as $sectionData)

    <section id="production-summary-section-data{{ $sectionData['id'] }}" class="production-summary-section-data">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i
                                class="fa fa-fw fa-table"></i></i> {{ trans('enra/sales.new_insurance') }} -
                            <strong>{{ $sectionData['description'] }}</strong> :
                            <strong>{{ trans('enra/sales.per_month') }} - {{ trans('enra/sales.number_of') }}</strong>
                        </h3>
                        <span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i></span>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                            <tr>
                                <th>{{ trans('enra/sales.year') }}</th>
                                @foreach ($sectionData['tableAgentRevenueAmount']['columns']  ?? [] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
                                    <th class="numeric">{{ $tableAgentRevenueAmountColumn }}</th>
                                @endforeach
                                <th class="numeric"><strong>{{ trans('enra/sales.total') }}</strong></th>
                                <th class="numeric">{{ trans('enra/sales.%_compared_to_last_year') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach (array_reverse($sectionData['tableAgentRevenueAmount']['rows']) as $tableAgentRevenueAmountRow)
                                <tr>
                                    <td>{{ isset($tableAgentRevenueAmountRow['year']) ? $tableAgentRevenueAmountRow['year'] : '' }}</td>
                                    @foreach ($sectionData['tableAgentRevenueAmount']['columns'] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
                                        <td class="numeric">{{ isset($tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey]) ? $tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey] : '' }}</td>
                                    @endforeach
                                    <td class="numeric">
                                        <strong>{{ isset($tableAgentRevenueAmountRow['total']) ? $tableAgentRevenueAmountRow['total'] : '' }}</strong>
                                    </td>
                                    <td class="numeric {{ isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] : '0'), 0, ',', '.') }}
                                        %
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i
                                class="fa fa-fw fa-table"></i></i> {{ trans('enra/sales.new_insurance') }} -
                            <strong>{{ $sectionData['description'] }}</strong> :
                            <br/>
                            <strong>{{ trans('enra/sales.by_duration_and_coverage') }}
                                - {{ trans('enra/sales.percentage') }}</strong></h3>
                        <span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i></span>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                            <tr>
                                <th>{{ trans('enra/sales.year') }}</th>
                                @foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumn)
                                    <th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
                                @endforeach
                                @foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumn)
                                    <th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($sectionData['tableYearsCoverageDuration'] as $tableYearsCoverageDurationRow)
                                <tr>
                                    <td>{{ $tableYearsCoverageDurationRow }}</td>
                                    @foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
                                        <td class="numeric">{{ (number_format( ((isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : '' }}</td>
                                    @endforeach
                                    @foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
                                        <td class="numeric">{{ (number_format( ((isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-lg-7">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i
                                class="fa fa-fw fa-table"></i></i> {{ trans('enra/sales.new_insurance') }} -
                            <strong>{{ $sectionData['description'] }}</strong> :
                            <br/>
                            <strong>{{ trans('enra/sales.by_duration_and_coverage') }}
                                - {{ trans('enra/sales.number_of') }}</strong></h3>
                        <span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i></span>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content">
                            <tr>
                                <th>{{ trans('enra/sales.year') }}</th>
                                @foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumn)
                                    <th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
                                @endforeach
                                @foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumn)
                                    <th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
                                @endforeach
                                <th class="numeric"><strong>{{ trans('enra/sales.total') }}</strong></th>
                                <!-- <th class="numeric"><strong>Premie €</strong></th> -->
                                @foreach ($sectionData['tableAgentCommissionSum']['columns'] as $tableAgentCommissionSumColumn)
                                    <th class="numeric">{{ $tableAgentCommissionSumColumn }}</th>
                                @endforeach

                                @if ($selectData['showDamage'])
                                    @foreach ($sectionData['tableAgentDamageSum']['columns'] as $tableAgentDamageSumColumn)
                                        <th class="numeric">{{ $tableAgentDamageSumColumn }}</th>
                                    @endforeach
                                @endif

                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($sectionData['tableYearsCoverageDuration'] as $tableYearsCoverageDurationRow)
                                <tr>
                                    <td>{{ $tableYearsCoverageDurationRow }}</td>
                                    @foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
                                        <td class="numeric">{{ isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? $sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : '' }}</td>
                                    @endforeach
                                    @foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
                                        <td class="numeric">{{ isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? $sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : '' }}</td>
                                    @endforeach
                                    <td class="numeric">
                                        <strong>{{ isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? $sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : '' }}</strong>
                                    </td>
                                    <!-- <td class="currency">{{ isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? number_format($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium'], 0, ',', '.') : '' }}</td> -->
                                    <td class="currency">{{ isset($sectionData['tableAgentCommissionSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($sectionData['tableAgentCommissionSum']['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td>
                                    <!-- <td class="currency">{{ isset($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td> -->

                                    @if ($selectData['showDamage'])
                                        <td class="numeric">{{ (number_format(((isset($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total'] : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format(((isset($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total'] * -1 : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 0, ',', '.') . '%') : ''}}</td>
                                    @endif

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @if ($selectData['showDamage'])

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i
                                    class="fa fa-fw fa-table"></i></i> {{ trans('enra/sales.mark_damage_insurance') }} -
                                <strong>{{ $sectionData['description'] }}</strong> :
                                <br/>
                                <strong>{{ trans('enra/sales.per_damage_type') }} - {{ trans('enra/sales.percentage') }}
                                    percentage</strong></h3>
                            <span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i></span>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                <tr>
                                    <th>{{ trans('enra/sales.year') }}</th>
                                    @foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumn)
                                        <th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach (array_reverse($sectionData['tableAgentDamageAmount']['rows']) as $tableAgentDamageAmountRow)
                                    <tr>
                                        <td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
                                        @foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
                                            <td class="numeric">{{ (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
                                        @endforeach
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i
                                    class="fa fa-fw fa-table"></i></i> {{ trans('enra/sales.mark_damage_insurance') }} -
                                <strong>{{ $sectionData['description'] }}</strong> :
                                <br/>
                                <strong>{{ trans('enra/sales.per_damage_type') }}per schadesoort
                                    - {{ trans('enra/sales.amount') }}</strong></h3>
                            <span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i></span>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content">
                                <tr>
                                    <th>{{ trans('enra/sales.year') }}</th>
                                    @foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumn)
                                        <th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
                                    @endforeach
                                    <th class="numeric">
                                        <strong>{{ trans('enra/sales.total') }} {{ trans('enra/sales.euro_sign') }}</strong>
                                    </th>
                                    <th class="numeric">{{ trans('enra/sales.%_compared_to_last_year') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach (array_reverse($sectionData['tableAgentDamageAmount']['rows']) as $tableAgentDamageAmountRow)
                                    <tr>
                                        <td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
                                        @foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
                                            <td class="currency">{{ isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? number_format($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey], 0, ',', '.') : '' }}</td>
                                        @endforeach
                                        <td class="currency">
                                            <strong>{{ isset($tableAgentDamageAmountRow['total']) ? number_format($tableAgentDamageAmountRow['total'], 0, ',', '.') : '' }}</strong>
                                        </td>
                                        <td class="numeric {{ isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] : ''), 0, ',', '.') }}
                                            %
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        @endif

    </section>

@endforeach
