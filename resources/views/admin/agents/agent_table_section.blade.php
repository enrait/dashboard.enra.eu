<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="livicon" data-name="table" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> {{ trans('enra/administration.found_agents') }}</h3>
				<span class="pull-right"> <i id="agenttablepanel" class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
			</div>
			<div class="panel-body">
				<table id="searchTypeaheadResult" class="table table-bordered table-striped table-condensed table-hover">
					<thead class="flip-content">
						<tr>
							<th><input type="checkbox" id="searchTypeaheadResultCheckAll" value="{{ trans('enra/administration.all') }}"></th>
							@foreach ($tableSearchTypeaheadResult['columns'] as $tableSearchTypeaheadResultColumnKey => $tableSearchTypeaheadResultColumn)
							@if ($tableSearchTypeaheadResultColumn['show'])
							<th>{{ $tableSearchTypeaheadResultColumn['label'] }}</th>
							@endif
							@endforeach
							<th><input type="button" class="btn btn-xs btn-responsive btn-primary" onclick="showProductionSummaryAll()" value="{{ trans('enra/administration.production_summary_selection') }}"></th>
							<th></th>
						</tr>
					</thead>
					<tbody class="rowlink" data-link="row">
						@foreach ($tableSearchTypeaheadResult['rows'] as $tableSearchTypeaheadResultRow)
						<tr>
							<td class="rowlink-skip"><input type="checkbox" id="searchTypeaheadResultCkBox{{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}" class="searchTypeaheadResultCkBox" value="{{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}"></td>
							@foreach ($tableSearchTypeaheadResult['columns'] as $tableSearchTypeaheadResultColumnKey => $tableSearchTypeaheadResultColumn)
							@if ($tableSearchTypeaheadResultColumn['show'])
							@if ($tableSearchTypeaheadResultColumnKey == 'AGE_AGENTNR')
							<td><a onClick="getAgentData('{{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}'); return false;" href="/api/agents/getAgentSearchResultJson?agentnr={{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}">{{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}</a></td>
							@else
							<td>{{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}</td>
							@endif
							@endif
							@endforeach
							<td class="rowlink-skip"><a href="/admin/sales/productionsummary?agent[]={{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}" target="_blank"><button type="button" class="btn btn-xs btn-responsive btn-primary">{{ trans('enra/administration.production_summary') }}</button></a></td>
							<td class="rowlink-skip"><a href="/api/event?agentnr={{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}" target="_blank"><button type="button" class="btn btn-xs btn-responsive btn-primary">{{ trans('enra/administration.calendar_item') }}</button></a></td>
						</tr>					
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>