<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<!-- <link href="{{ asset('assets/css/bootstrap_table.css') }}" rel="stylesheet" type="text/css" /> -->

		<style>
			html {
				font-size: 10px;
				font-family: sans-serif;
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
			}
			body {
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				font-size: 12px;
				line-height: 1.42857143;
				color: #333333;
				background-color: #ffffff;
				margin: 0;
			}
			body {
				margin: 0;
			}
			table {
				border-collapse: collapse;
				border-spacing: 0;
			}
			td, th {
				padding: 0;
			}

			table {
				background-color: transparent;
			}
			caption {
				padding-top: 8px;
				padding-bottom: 8px;
				color: #777777;
				text-align: left;
			}
			th {
				text-align: left;
			}
			.table {
				width: 100%;
				max-width: 100%;
				margin-bottom: 20px;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 8px;
				line-height: 1.42857143;
				vertical-align: middle;
				border-top: 1px solid #dddddd;
			}
			.table > thead > tr > th {
				vertical-align: bottom;
				border-bottom: 2px solid #dddddd;
			}
			.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td {
				border-top: 0;
			}
			.table > tbody + tbody {
				border-top: 2px solid #dddddd;
			}
			.table .table {
				background-color: #ffffff;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
				padding: 3px;
			}
			.table-bordered {
				border: 1px solid #dddddd;
			}
			.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
				border: 1px solid #dddddd;
			}
			.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
				border-bottom-width: 2px;
			}
			.table-striped > tbody > tr:nth-of-type(odd) {
				background-color: #f9f9f9;
			}
		</style>
		<style>
			.page-break {
				page-break-after: always;
			}
			.currency, .numeric {
				text-align: right;
			}
			.negative {
				color: red;
			}
		</style>
	</head>
	<body>

		<table style="width: 100%;">
			<tr>
				<td> <!--- <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> ---></td>
				<td></td>
				<td align="right"><h1>Productie overzicht</h1></td>
			</tr>
		</table>

		<h2>Nieuw afgesloten verzekeringen</h2>
		<h3>per maand - aantal</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>Jaar</th>
					@foreach ($tableAgentRevenueAmount['columns'] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
					<th class="numeric">{{ $tableAgentRevenueAmountColumn }}</th>
					@endforeach
					<th class="numeric"><strong>Totaal</strong></th>
					<th class="numeric">% t.o.v. vorig jaar</th>
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($tableAgentRevenueAmount['rows']) as $tableAgentRevenueAmountRow)
				<tr>
					<td>{{ isset($tableAgentRevenueAmountRow['year']) ? $tableAgentRevenueAmountRow['year'] : '' }}</td>
					@foreach ($tableAgentRevenueAmount['columns'] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
					<td class="numeric">{{ isset($tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey]) ? $tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey] : '' }}</td>
					@endforeach
					<td class="numeric"><strong>{{ isset($tableAgentRevenueAmountRow['total']) ? $tableAgentRevenueAmountRow['total'] : '' }}</strong></td>
					<td class="numeric {{ isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] : '0'), 0, ',', '.') }}%</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table style="width: 100%;" class="table-condensed">
			<tr>
				<td style="width: 39%;"><h3>per looptijd en dekkingsvorm - percentage</h3>
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Jaar</th>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumn)
							<th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumn)
							<th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach ($tableYearsCoverageDuration as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : '' }}</td>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
							@endforeach
						</tr>
						@endforeach
					</tbody>
				</table></td>
				<td style="width: 2%;"></td>
				<td style="width: 59%;"><h3>per looptijd en dekkingsvorm - aantal</h3>
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Jaar</th>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumn)
							<th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumn)
							<th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
							@endforeach
							<th class="numeric"><strong>Totaal</strong></th>
							<!-- <th class="numeric"><strong>Premie €</strong></th> -->
							@foreach ($tableAgentCommissionSum['columns'] as $tableAgentCommissionSumColumn)
							<th class="numeric">{{ $tableAgentCommissionSumColumn }}</th>
							@endforeach

							@if ($selectData['showDamage'])
							@foreach ($tableAgentDamageSum['columns'] as $tableAgentDamageSumColumn)
							<th class="numeric">{{ $tableAgentDamageSumColumn }}</th>
							@endforeach
							@endif

						</tr>
					</thead>
					<tbody>
						@foreach ($tableYearsCoverageDuration as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($tableAgentDurationAmount['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
							<td class="numeric">{{ isset($tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? $tableAgentDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : '' }}</td>
							@endforeach
							@foreach ($tableAgentCoverageAmount['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
							<td class="numeric">{{ isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? $tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : '' }}</td>
							@endforeach
							<td class="numeric"><strong>{{ isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? $tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : '' }}</strong></td>
							<!-- <td class="currency">{{ isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? number_format($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'], 0, ',', '.') : '' }}</td> -->
							<td class="currency">{{ isset($tableAgentCommissionSum['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($tableAgentCommissionSum['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td>
							<!-- <td class="currency">{{ isset($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td> -->

							@if ($selectData['showDamage'])
							<td class="numeric">{{ (number_format(((isset($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total'] : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format(((isset($tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableAgentDamageSum['rows'][$tableYearsCoverageDurationRow]['total'] * -1 : 0) / (isset($tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$tableAgentCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 0, ',', '.') . '%') : ''}}</td>
							@endif

						</tr>
						@endforeach
					</tbody>
				</table></td>
			</tr>
		</table>

		@if ($selectData['showDamage'])

		<div class="page-break"></div>

		<table style="width: 100%;">
			<tr>
				<td> <!--- <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> ---></td>
				<td></td>
				<td align="right"><h1>Productie overzicht</h1></td>
			</tr>
		</table>

		<h2>Schadecijfer verzekeringen</h2>
		<h3>per schadesoort - percentage</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>Jaar</th>
					@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumn)
					<th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($tableAgentDamageAmount['rows']) as $tableAgentDamageAmountRow)
				<tr>
					<td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
					@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
					<td class="numeric">{{ (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
					@endforeach
				</tr>
				@endforeach
			</tbody>
		</table>

		<h3>per schadesoort - bedrag</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>Jaar</th>
					@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumn)
					<th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
					@endforeach
					<th class="numeric"><strong>Totaal €</strong></th>
					<th class="numeric">% t.o.v. vorig jaar</th>
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($tableAgentDamageAmount['rows']) as $tableAgentDamageAmountRow)
				<tr>
					<td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
					@foreach ($tableAgentDamageAmount['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
					<td class="currency">{{ isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? number_format($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey], 0, ',', '.') : '' }}</td>
					@endforeach
					<td class="currency"><strong>{{ isset($tableAgentDamageAmountRow['total']) ? number_format($tableAgentDamageAmountRow['total'], 0, ',', '.') : '' }}</strong></td>
					<td class="numeric {{ isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] : ''), 0, ',', '.') }}%</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		@endif

		@if (count($selectData['agent']['selected']) == 1)
		
		<div class="page-break"></div>

		<table style="width: 100%;">
			<tr>
				<td> <!--- <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> ---></td>
				<td></td>
				<td align="right"><h1>Productie overzicht</h1></td>
			</tr>
		</table>

		<table class="table table-bordered table-striped table-condensed">
			<tbody>
				@foreach ($agentData as $agentDataKey => $agentDataValue)
				@if ( $agentDataValue != '' && $agentDataValue != '+0000000.00' && $agentDataValue != '0000000.00' && $agentDataValue != '+0000000.  ' && $agentDataValue != '+0000000.+0' )
				<tr>
					<td><strong>{{ $agentDataKey }}</strong></td>
					<td>{{ $agentDataValue }}</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>

		@endif

	</body>
</html>

