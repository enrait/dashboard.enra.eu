<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<!-- <link href="{{ asset('assets/css/bootstrap_table.css') }}" rel="stylesheet" type="text/css" /> -->

		<style>
			html {
				font-size: 10px;
				font-family: sans-serif;
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
			}
			body {
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				font-size: 12px;
				line-height: 1.42857143;
				color: #333333;
				background-color: #ffffff;
				margin: 0;
			}
			body {
				margin: 0;
			}
			table {
				border-collapse: collapse;
				border-spacing: 0;
			}
			td, th {
				padding: 0;
			}

			table {
				background-color: transparent;
			}
			caption {
				padding-top: 8px;
				padding-bottom: 8px;
				color: #777777;
				text-align: left;
			}
			th {
				text-align: left;
			}
			.table {
				width: 100%;
				max-width: 100%;
				margin-bottom: 20px;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 8px;
				line-height: 1.42857143;
				vertical-align: middle;
				border-top: 1px solid #dddddd;
			}
			.table > thead > tr > th {
				vertical-align: bottom;
				border-bottom: 2px solid #dddddd;
			}
			.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td {
				border-top: 0;
			}
			.table > tbody + tbody {
				border-top: 2px solid #dddddd;
			}
			.table .table {
				background-color: #ffffff;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
				padding: 3px;
			}
			.table-bordered {
				border: 1px solid #dddddd;
			}
			.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
				border: 1px solid #dddddd;
			}
			.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
				border-bottom-width: 2px;
			}
			.table-striped > tbody > tr:nth-of-type(odd) {
				background-color: #f9f9f9;
			}
		</style>
		<style>
			.page-break {
				page-break-after: always;
			}
			.currency, .numeric {
				text-align: right;
			}
			.negative {
				color: red;
			}
		</style>
	</head>
	<body>

		<table style="width: 100%;">
			<tr>
				<td> <!--- <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> ---></td>
				<td></td>
				<td align="right"><h1>{{ trans('enra/sales.production_summary') }}</h1></td>
			</tr>
		</table>

		@foreach ($sectionsData as $sectionData)

		<h2>{{ trans('enra/sales.new_insurance') }} - {{ $sectionData['description'] }}</h2>
		<h3>{{ trans('enra/sales.per_month') }} - {{ trans('enra/sales.number_of') }}</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>{{ trans('enra/sales.year') }}</th>
					@foreach ($sectionData['tableAgentRevenueAmount']['columns'] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
					<th class="numeric">{{ $tableAgentRevenueAmountColumn }}</th>
					@endforeach
					<th class="numeric"><strong>{{ trans('enra/sales.total') }}</strong></th>
					<th class="numeric">{{ trans('enra/sales.%_compared_to_last_year') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($sectionData['tableAgentRevenueAmount']['rows']) as $tableAgentRevenueAmountRow)
				<tr>
					<td>{{ isset($tableAgentRevenueAmountRow['year']) ? $tableAgentRevenueAmountRow['year'] : '' }}</td>
					@foreach ($sectionData['tableAgentRevenueAmount']['columns'] as $tableAgentRevenueAmountColumnKey => $tableAgentRevenueAmountColumn)
					<td class="numeric">{{ isset($tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey]) ? $tableAgentRevenueAmountRow[$tableAgentRevenueAmountColumnKey] : '' }}</td>
					@endforeach
					<td class="numeric"><strong>{{ isset($tableAgentRevenueAmountRow['total']) ? $tableAgentRevenueAmountRow['total'] : '' }}</strong></td>
					<td class="numeric {{ isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentRevenueAmountRow['diff']) ? $tableAgentRevenueAmountRow['diff'] : '0'), 0, ',', '.') }}%</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table style="width: 100%;" class="table-condensed">
			<tr>
				<td style="width: 39%;"><h3>{{ trans('enra/sales.by_duration_and_coverage') }} - {{ trans('enra/sales.percentage') }}</h3>
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>{{ trans('enra/sales.year') }}</th>
							@foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumn)
							<th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
							@endforeach
							@foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumn)
							<th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach ($sectionData['tableYearsCoverageDuration'] as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : 0) / (isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : '' }}</td>
							@endforeach
							@foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
							@endforeach
						</tr>
						@endforeach
					</tbody>
				</table></td>
				<td style="width: 2%;"></td>
				<td style="width: 59%;"><h3>{{ trans('enra/sales.by_duration_and_coverage') }} - {{ trans('enra/sales.number_of') }}</h3>
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>{{ trans('enra/sales.year') }}</th>
							@foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumn)
							<th class="numeric">{{ $tableAgentDurationAmountColumn }}</th>
							@endforeach
							@foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumn)
							<th class="numeric">{{ $tableAgentCoverageAmountColumn }}</th>
							@endforeach
							<th class="numeric"><strong>{{ trans('enra/sales.total') }}</strong></th>
							<!-- <th class="numeric"><strong>Premie €</strong></th> -->
							@foreach ($sectionData['tableAgentCommissionSum']['columns'] as $tableAgentCommissionSumColumn)
							<th class="numeric">{{ $tableAgentCommissionSumColumn }}</th>
							@endforeach

							@if ($selectData['showDamage'])
							@foreach ($sectionData['tableAgentDamageSum']['columns'] as $tableAgentDamageSumColumn)
							<th class="numeric">{{ $tableAgentDamageSumColumn }}</th>
							@endforeach
							@endif

						</tr>
					</thead>
					<tbody>
						@foreach ($sectionData['tableYearsCoverageDuration'] as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($sectionData['tableAgentDurationAmount']['columns'] as $tableAgentDurationAmountColumnKey => $tableAgentDurationAmountColumn)
							<td class="numeric">{{ isset($sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey]) ? $sectionData['tableAgentDurationAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentDurationAmountColumnKey] : '' }}</td>
							@endforeach
							@foreach ($sectionData['tableAgentCoverageAmount']['columns'] as $tableAgentCoverageAmountColumnKey => $tableAgentCoverageAmountColumn)
							<td class="numeric">{{ isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey]) ? $sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow][$tableAgentCoverageAmountColumnKey] : '' }}</td>
							@endforeach
							<td class="numeric"><strong>{{ isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total']) ? $sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['total'] : '' }}</strong></td>
							<!-- <td class="currency">{{ isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? number_format($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium'], 0, ',', '.') : '' }}</td> -->
							<td class="currency">{{ isset($sectionData['tableAgentCommissionSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($sectionData['tableAgentCommissionSum']['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td>
							<!-- <td class="currency">{{ isset($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td> -->

							@if ($selectData['showDamage'])
							<td class="numeric">{{ (number_format(((isset($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total'] : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format(((isset($sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$sectionData['tableAgentDamageSum']['rows'][$tableYearsCoverageDurationRow]['total'] * -1 : 0) / (isset($sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$sectionData['tableAgentCoverageAmount']['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 0, ',', '.') . '%') : ''}}</td>
							@endif

						</tr>
						@endforeach
					</tbody>
				</table></td>
			</tr>
		</table>

		@if ($selectData['showDamage'])

		<div class="page-break"></div>

		<table style="width: 100%;">
			<tr>
				<td> <!--- <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> ---></td>
				<td></td>
				<td align="right"><h1>{{ trans('enra/sales.production_summary') }}</h1></td>
			</tr>
		</table>

		<h2>{{ trans('enra/sales.mark_damage_insurance') }} - {{ $sectionData['description'] }}</h2>
		<h3>{{ trans('enra/sales.per_damage_type') }}per schadesoort - {{ trans('enra/sales.percentage') }}</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>{{ trans('enra/sales.year') }}</th>
					@foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumn)
					<th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($sectionData['tableAgentDamageAmount']['rows']) as $tableAgentDamageAmountRow)
				<tr>
					<td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
					@foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
					<td class="numeric">{{ (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? (int)$tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey] : 0) / (isset($tableAgentDamageAmountRow['total']) ? (int)$tableAgentDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
					@endforeach
				</tr>
				@endforeach
			</tbody>
		</table>

		<h3>{{ trans('enra/sales.per_damage_type') }} - {{ trans('enra/sales.amount') }}</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>{{ trans('enra/sales.year') }}</th>
					@foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumn)
					<th class="numeric">{{ $tableAgentDamageAmountColumn }}</th>
					@endforeach
					<th class="numeric"><strong>{{ trans('enra/sales.total') }} {{ trans('enra/sales.euro_sign') }}</strong></th>
					<th class="numeric">{{ trans('enra/sales.%_compared_to_last_year') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($sectionData['tableAgentDamageAmount']['rows']) as $tableAgentDamageAmountRow)
				<tr>
					<td>{{ isset($tableAgentDamageAmountRow['year']) ? $tableAgentDamageAmountRow['year'] : '' }}</td>
					@foreach ($sectionData['tableAgentDamageAmount']['columns'] as $tableAgentDamageAmountColumnKey => $tableAgentDamageAmountColumn)
					<td class="currency">{{ isset($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey]) ? number_format($tableAgentDamageAmountRow[$tableAgentDamageAmountColumnKey], 0, ',', '.') : '' }}</td>
					@endforeach
					<td class="currency"><strong>{{ isset($tableAgentDamageAmountRow['total']) ? number_format($tableAgentDamageAmountRow['total'], 0, ',', '.') : '' }}</strong></td>
					<td class="numeric {{ isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableAgentDamageAmountRow['diff']) ? $tableAgentDamageAmountRow['diff'] : ''), 0, ',', '.') }}%</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		@endif
		
		<div class="page-break"></div>
		
		@endforeach

		@if (count($selectData['agent']['selected']) == 1)

		<table style="width: 100%;">
			<tr>
				<td> <!--- <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> ---></td>
				<td></td>
				<td align="right"><h1>{{ trans('enra/sales.production_summary') }}</h1></td>
			</tr>
		</table>

		<table class="table table-bordered table-striped table-condensed">
			<tbody>
				@foreach ($agentData as $agentDataKey => $agentDataValue)
				@if ( $agentDataValue != '' && $agentDataValue != '+0000000.00' && $agentDataValue != '0000000.00' && $agentDataValue != '+0000000.  ' && $agentDataValue != '+0000000.+0' )
				<tr>
					<td><strong>{{ $agentDataKey }}</strong></td>
					<td>{{ $agentDataValue }}</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>

		@endif

	</body>
</html>

