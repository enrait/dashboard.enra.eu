<script type="text/javascript">
	var formAgentSummary = [],
	    formAgentSummaryDatastringPrevious,
	    ajaxAgentSummary = false,
	    sectionId,
	    pageFirstLoad = true,
	    dataAgentSummary;

	function initAgentSummary()
	{
		formAgentSummary[formAgentSummary.length] = $(".formAgentSummary");

		sectionId = "#sectionAgentSummary";

		formAgentSummaryChanged(formAgentSummary[formAgentSummary.length - 1], true);
	};

	function formAgentSummaryChanged(formObject, forceChange, sectionId)
	{
		var formAgentSummaryDatastring = formObject.serialize();

		if (formAgentSummaryDatastringPrevious != formAgentSummaryDatastring || forceChange)
		{
			//console.log("datastring : " + formAgentSummaryDatastring);

			formAgentSummaryDatastringPrevious = formAgentSummaryDatastring;

			getAgentSummary(formObject, sectionId);
		}
	};

	function getAgentSummary(formObject, sectionId)
	{
		if (!ajaxAgentSummary)
		{
			ajaxAgentSummary = true;

			$('#submitFilter').addClass('active');

			urlPage = window.location.search.substring(1);
			urlParameters = urlPage.split('?');

			if (urlParameters == '' || !pageFirstLoad)
			{
				var request = $.ajax(
				{
					url : "/api/sales/productionsummary/",
					type : "POST",
					beforeSend : function(xhr)
					{
						var token = $('meta[name="csrf_token"]').attr('content');

						if (token)
						{
							return xhr.setRequestHeader('X-CSRF-TOKEN', token);
						}
					},
					//contentType:'application/json',
					data : formObject.serialize(),
					//data: JSON.stringify(formAgentSummary.serialize()),
					//dataType:'json',
				});
			}
			else
			{
				var request = $.ajax(
				{
					url : "/api/sales/productionsummary/?" + urlParameters,
					type : "POST",
					beforeSend : function(xhr)
					{
						var token = $('meta[name="csrf_token"]').attr('content');

						if (token)
						{
							return xhr.setRequestHeader('X-CSRF-TOKEN', token);
						}
					},
					//contentType:'application/json',
					//data: urlParameters,
					//data: JSON.stringify(formAgentSummary.serialize()),
					//dataType:'json',
				});
			}

			pageFirstLoad = false;

			request.done(function(data)
			{
				//console.log( data );

				dataAgentSummary = data;

				displayAgentSummary(dataAgentSummary);

				ajaxAgentSummary = false;

				$('#submitFilter').removeClass('active');
			});

			request.fail(function(jqXHR, textStatus)
			{
				alert("Request failed: " + textStatus);

				ajaxAgentSummary = false;

				$('#submitFilter').removeClass('active');
			});
		};
	};

	function displayAgentSummary(data)
	{
		var htmlContent = '';

		htmlContent = data;

		//console.log( sectionId );
		//console.log( htmlContent );

		$("#sectionAgentSummary").html(htmlContent);
		//$("#sectionAgentSummary").append(htmlContent);
		//$(sectionId).append(htmlContent);

		formAgentSummary = $(".formAgentSummary");

		formAgentSummary.on('submit', (function(e)
		{

			if (!e.isDefaultPrevented())
			{
				formAgentSummaryChanged(formAgentSummary, true);

				e.preventDefault();
			}

		}));

		$('.extraSection').on('click', (function(e)
		{

			$("#sectionAgentSummary").append("<p>Test</p>");

		}));

		//iCheck for checkbox and radio inputs
		$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck(
		{
			checkboxClass : 'icheckbox_minimal',
			radioClass : 'iradio_minimal'
		});

		$(".select2_year").select2(
		{
			//placeholder : "Selecteer een jaar",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_month").select2(
		{
			//placeholder : "Selecteer een maand",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_country").select2(
		{
			//placeholder : "Selecteer een land",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_region").select2(
		{
			//placeholder : "Selecteer een regio",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_company").select2(
		{
			//placeholder : "Selecteer een maatschappij",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_branch").select2(
		{
			//placeholder : "Selecteer een branch",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_objecttype").select2(
		{
			//placeholder : "Selecteer een object soort",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_organisation").select2(
		{
			//placeholder : "Selecteer een collectiviteit",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_agent").select2(
		{
			//placeholder : "Selecteer een agent",
			//allowClear: true,
		});

		// $(".select2_agent").select2(
		// {
		// initSelection : function(element, callback)
		// {
		// var selection = _.find(data, function(metric)
		// {
		// return metric.id === element.val();
		// })
		// callback(selection);
		// },
		// query : function(options)
		// {
		// var pageSize = 100;
		// var startIndex = (options.page - 1) * pageSize;
		// var filteredData = data;
		// var stripDiacritics = window.Select2.util.stripDiacritics;
		//
		// if (options.term && options.term.length > 0)
		// {
		// if (!options.context)
		// {
		// var term = stripDiacritics(options.term.toLowerCase());
		// options.context = data.filter(function(metric)
		// {
		// //since data is very big... save the stripDiacritics result for later
		// //to speed up next search.
		// //this does modify the original array!
		// if (!metric.stripped_text)
		// {
		// metric.stripped_text = stripDiacritics(metric.text.toLowerCase());
		// }
		// return (metric.stripped_text.indexOf(term) !== -1);
		// });
		// }
		// filteredData = options.context;
		// }
		//
		// options.callback(
		// {
		// context : filteredData,
		// results : filteredData.slice(startIndex, startIndex + pageSize),
		// more : (startIndex + pageSize) < filteredData.length
		// });
		// },
		// placeholder : "Select a metric"
		// });

		$(".select2_coverage").select2(
		{
			placeholder : "Selecteer een dekking",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_duration").select2(
		{
			//placeholder : "Selecteer een looptijd",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_damagetype").select2(
		{
			//placeholder : "Selecteer een schade soort",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_classification").select2(
		{
			//placeholder : "Selecteer een classification variant",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_weightingfactor").select2(
		{
			//placeholder : "Selecteer een weightingfactor variant",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

		$(".select2_potential").select2(
		{
			//placeholder : "Selecteer een potential variant",
			//allowClear: true,
			escapeMarkup : function(m)
			{
				return m;
			}
		});

	};
</script>
<script>
jQuery(document).ready(function() {
    
    initAgentSummary(); 
    
    @if ($selectData['showPDF']) 
    
        var form = document.formAgentRevenueAmountFilter;
      
        original = form.action;
          
        form.action = "/admin/sales/productionsummary/pdf";
        form.target = "_blank";
        form.submit();
          
        form.action = original;
        form.target = "_self";
    
    @endif
    
    $(document).on('click', '.panel div.clickable', function (e) {
	    var $this = $(this);
	    if (!$this.hasClass('panel-collapsed')) {
	        $this.parents('.panel').find('.panel-body').slideUp();
	        $this.addClass('panel-collapsed');
	        $this.find('i.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
	    } else {
	        $this.parents('.panel').find('.panel-body').slideDown();
	        $this.removeClass('panel-collapsed');
	        $this.find('i.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
	    }
	});

});
</script>