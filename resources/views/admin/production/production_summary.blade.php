@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{ trans('enra/sales.production_summary') }} - {{ trans('enra/general.enra_bv') }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/portlet.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/iCheck/skins/all.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/formelements.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/css/pages/production_summary.css') }}" rel="stylesheet" type="text/css" />

<!-- end of page level css-->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
	<h1>{{ trans('enra/sales.production_summary') }}</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}"><i class="fa fa-2x fa-home"></i>{{ trans('enra/general.dashboard') }} </a>
		</li>
		<li class="active">
			{{ trans('enra/sales.production_summary') }} 
		</li>
	</ol>
</section>
<section class="content">

    <section id="sectionAgentSummary">
        
        
    </section>

</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js-->

<script src="{{ asset('assets/vendors/select2/select2.full.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/iCheck/icheck.js') }}" type="text/javascript"></script>

@include('admin/production/production_summary_js')

<!-- end of page level js-->
@stop
