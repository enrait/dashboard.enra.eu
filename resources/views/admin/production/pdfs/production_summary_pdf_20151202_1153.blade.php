<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

		<!-- <link href="{{ asset('assets/css/bootstrap_table.css') }}" rel="stylesheet" type="text/css" /> -->

		<style>
			html {
				font-size: 10px;
				font-family: sans-serif;
				-ms-text-size-adjust: 100%;
				-webkit-text-size-adjust: 100%;
				-webkit-tap-highlight-color: rgba(0, 0, 0, 0);
			}
			body {
				font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
				font-size: 12px;
				line-height: 1.42857143;
				color: #333333;
				background-color: #ffffff;
				margin: 0;
			}
			body {
				margin: 0;
			}
			table {
				border-collapse: collapse;
				border-spacing: 0;
			}
			td, th {
				padding: 0;
			}

			table {
				background-color: transparent;
			}
			caption {
				padding-top: 8px;
				padding-bottom: 8px;
				color: #777777;
				text-align: left;
			}
			th {
				text-align: left;
			}
			.table {
				width: 100%;
				max-width: 100%;
				margin-bottom: 20px;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
				padding: 8px;
				line-height: 1.42857143;
				vertical-align: middle;
				border-top: 1px solid #dddddd;
			}
			.table > thead > tr > th {
				vertical-align: bottom;
				border-bottom: 2px solid #dddddd;
			}
			.table > caption + thead > tr:first-child > th, .table > colgroup + thead > tr:first-child > th, .table > thead:first-child > tr:first-child > th, .table > caption + thead > tr:first-child > td, .table > colgroup + thead > tr:first-child > td, .table > thead:first-child > tr:first-child > td {
				border-top: 0;
			}
			.table > tbody + tbody {
				border-top: 2px solid #dddddd;
			}
			.table .table {
				background-color: #ffffff;
			}
			.table-condensed > thead > tr > th, .table-condensed > tbody > tr > th, .table-condensed > tfoot > tr > th, .table-condensed > thead > tr > td, .table-condensed > tbody > tr > td, .table-condensed > tfoot > tr > td {
				padding: 3px;
			}
			.table-bordered {
				border: 1px solid #dddddd;
			}
			.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
				border: 1px solid #dddddd;
			}
			.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
				border-bottom-width: 2px;
			}
			.table-striped > tbody > tr:nth-of-type(odd) {
				background-color: #f9f9f9;
			}
		</style>
		<style>
			.page-break {
				page-break-after: always;
			}
			.currency, .numeric {
				text-align: right;
			}
			.negative {
				color: red;
			}
		</style>
	</head>
	<body>
		<table style="width: 100%;">
		    <tr>
		        <td>
		            <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> 
		        </td>
		        <td>
		            
		        </td>
		        <td align="right">
		            <h1>Agent overzicht</h1>
		        </td>
		    </tr>		
		</table>

		<h2>Nieuw afgesloten verzekeringen</h2>
		<h3>per maand - aantal</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>Jaar</th>
					@foreach ($tableDealerRevenueAmount['columns'] as $tableDealerRevenueAmountColumnKey => $tableDealerRevenueAmountColumn)
					<th class="numeric">{{ $tableDealerRevenueAmountColumn }}</th>
					@endforeach
					<th class="numeric"><strong>Totaal</strong></th>
					<th class="numeric">% t.o.v. vorig jaar</th>
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($tableDealerRevenueAmount['rows']) as $tableDealerRevenueAmountRow)
				<tr>
					<td>{{ isset($tableDealerRevenueAmountRow['year']) ? $tableDealerRevenueAmountRow['year'] : '' }}</td>
					@foreach ($tableDealerRevenueAmount['columns'] as $tableDealerRevenueAmountColumnKey => $tableDealerRevenueAmountColumn)
					<td class="numeric">{{ isset($tableDealerRevenueAmountRow[$tableDealerRevenueAmountColumnKey]) ? $tableDealerRevenueAmountRow[$tableDealerRevenueAmountColumnKey] : '' }}</td>
					@endforeach
					<td class="numeric"><strong>{{ isset($tableDealerRevenueAmountRow['total']) ? $tableDealerRevenueAmountRow['total'] : '' }}</strong></td>
					<td class="numeric {{ isset($tableDealerRevenueAmountRow['diff']) ? $tableDealerRevenueAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableDealerRevenueAmountRow['diff']) ? $tableDealerRevenueAmountRow['diff'] : '0'), 0, ',', '.') }}%</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<table style="width: 100%;" class="table-condensed">
			<tr>
				<td style="width: 39%;"><h3>per looptijd en dekkingsvorm - percentage</h3>
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Jaar</th>
							@foreach ($tableDealerDurationAmount['columns'] as $tableDealerDurationAmountColumn)
							<th class="numeric">{{ $tableDealerDurationAmountColumn }}</th>
							@endforeach
							@foreach ($tableDealerCoverageAmount['columns'] as $tableDealerCoverageAmountColumn)
							<th class="numeric">{{ $tableDealerCoverageAmountColumn }}</th>
							@endforeach
						</tr>
					</thead>
					<tbody>
						@foreach ($tableYearsCoverageDuration as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($tableDealerDurationAmount['columns'] as $tableDealerDurationAmountColumnKey => $tableDealerDurationAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerDurationAmountColumnKey]) ? (int)$tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerDurationAmountColumnKey] : 0) / (isset($tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerDurationAmountColumnKey]) ? (int)$tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerDurationAmountColumnKey] : 0) / (isset($tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : '' }}</td>
							@endforeach
							@foreach ($tableDealerCoverageAmount['columns'] as $tableDealerCoverageAmountColumnKey => $tableDealerCoverageAmountColumn)
							<td class="numeric">{{ (number_format( ((isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerCoverageAmountColumnKey]) ? (int)$tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerCoverageAmountColumnKey] : 0) / (isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerCoverageAmountColumnKey]) ? (int)$tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerCoverageAmountColumnKey] : 0) / (isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
							@endforeach
						</tr>
						@endforeach
					</tbody>
				</table></td>
				<td style="width: 2%;"></td>
				<td style="width: 59%;"><h3>per looptijd en dekkingsvorm - aantal</h3>
				<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>Jaar</th>
							@foreach ($tableDealerDurationAmount['columns'] as $tableDealerDurationAmountColumn)
							<th class="numeric">{{ $tableDealerDurationAmountColumn }}</th>
							@endforeach
							@foreach ($tableDealerCoverageAmount['columns'] as $tableDealerCoverageAmountColumn)
							<th class="numeric">{{ $tableDealerCoverageAmountColumn }}</th>
							@endforeach
							<th class="numeric"><strong>Totaal</strong></th>
							<!-- <th class="numeric"><strong>Premie €</strong></th> -->
							@foreach ($tableDealerCommissionSum['columns'] as $tableDealerCommissionSumColumn)
							<th class="numeric">{{ $tableDealerCommissionSumColumn }}</th>
							@endforeach
							
							@if ($selectData['showDamage'])
							@foreach ($tableDealerDamageSum['columns'] as $tableDealerDamageSumColumn)
							<th class="numeric">{{ $tableDealerDamageSumColumn }}</th>
							@endforeach
							@endif
							
						</tr>
					</thead>
					<tbody>
						@foreach ($tableYearsCoverageDuration as $tableYearsCoverageDurationRow)
						<tr>
							<td>{{ $tableYearsCoverageDurationRow }}</td>
							@foreach ($tableDealerDurationAmount['columns'] as $tableDealerDurationAmountColumnKey => $tableDealerDurationAmountColumn)
							<td class="numeric">{{ isset($tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerDurationAmountColumnKey]) ? $tableDealerDurationAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerDurationAmountColumnKey] : '' }}</td>
							@endforeach
							@foreach ($tableDealerCoverageAmount['columns'] as $tableDealerCoverageAmountColumnKey => $tableDealerCoverageAmountColumn)
							<td class="numeric">{{ isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerCoverageAmountColumnKey]) ? $tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow][$tableDealerCoverageAmountColumnKey] : '' }}</td>
							@endforeach
							<td class="numeric"><strong>{{ isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total']) ? $tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['total'] : '' }}</strong></td>
							<!-- <td class="currency">{{ isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? number_format($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'], 0, ',', '.') : '' }}</td> -->
							<td class="currency">{{ isset($tableDealerCommissionSum['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($tableDealerCommissionSum['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td>
							<!-- <td class="currency">{{ isset($tableDealerDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? number_format($tableDealerDamageSum['rows'][$tableYearsCoverageDurationRow]['total'], 0, ',', '.') : '' }}</td> -->
							
							@if ($selectData['showDamage'])
							<td class="numeric">{{ (number_format(((isset($tableDealerDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableDealerDamageSum['rows'][$tableYearsCoverageDurationRow]['total'] : 0) / (isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format(((isset($tableDealerDamageSum['rows'][$tableYearsCoverageDurationRow]['total']) ? (int)$tableDealerDamageSum['rows'][$tableYearsCoverageDurationRow]['total'] * -1 : 0) / (isset($tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium']) ? (int)$tableDealerCoverageAmount['rows'][$tableYearsCoverageDurationRow]['totalpremium'] : 0)) * 100, 0, ',', '.') . '%') : ''}}</td>
                            @endif
                            
						</tr>
						@endforeach
					</tbody>
				</table></td>
			</tr>
		</table>
		
		@if ($selectData['showDamage'])

		<div class="page-break"></div>
		
		<table style="width: 100%;">
            <tr>
                <td>
                    <img src="{{ asset('assets/img/logo.png') }}" alt="logo"> 
                </td>
                <td>
                    
                </td>
                <td align="right">
                    <h1>Dealer overzicht</h1>
                </td>
            </tr>       
        </table>

		<h2>Schadecijfer verzekeringen</h2>
		<h3>per schadesoort - percentage</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>Jaar</th>
					@foreach ($tableDealerDamageAmount['columns'] as $tableDealerDamageAmountColumn)
					<th class="numeric">{{ $tableDealerDamageAmountColumn }}</th>
					@endforeach
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($tableDealerDamageAmount['rows']) as $tableDealerDamageAmountRow)
				<tr>
					<td>{{ isset($tableDealerDamageAmountRow['year']) ? $tableDealerDamageAmountRow['year'] : '' }}</td>
					@foreach ($tableDealerDamageAmount['columns'] as $tableDealerDamageAmountColumnKey => $tableDealerDamageAmountColumn)
					<td class="numeric">{{ (number_format( ((isset($tableDealerDamageAmountRow[$tableDealerDamageAmountColumnKey]) ? (int)$tableDealerDamageAmountRow[$tableDealerDamageAmountColumnKey] : 0) / (isset($tableDealerDamageAmountRow['total']) ? (int)$tableDealerDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') != '0,00%' ? (number_format( ((isset($tableDealerDamageAmountRow[$tableDealerDamageAmountColumnKey]) ? (int)$tableDealerDamageAmountRow[$tableDealerDamageAmountColumnKey] : 0) / (isset($tableDealerDamageAmountRow['total']) ? (int)$tableDealerDamageAmountRow['total'] : 0)) * 100, 2, ',', '.') . '%') : ''}}</td>
					@endforeach
				</tr>
				@endforeach
			</tbody>
		</table>

		<h3>per schadesoort - bedrag</h3>

		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>Jaar</th>
					@foreach ($tableDealerDamageAmount['columns'] as $tableDealerDamageAmountColumn)
					<th class="numeric">{{ $tableDealerDamageAmountColumn }}</th>
					@endforeach
					<th class="numeric"><strong>Totaal €</strong></th>
					<th class="numeric">% t.o.v. vorig jaar</th>
				</tr>
			</thead>
			<tbody>
				@foreach (array_reverse($tableDealerDamageAmount['rows']) as $tableDealerDamageAmountRow)
				<tr>
					<td>{{ isset($tableDealerDamageAmountRow['year']) ? $tableDealerDamageAmountRow['year'] : '' }}</td>
					@foreach ($tableDealerDamageAmount['columns'] as $tableDealerDamageAmountColumnKey => $tableDealerDamageAmountColumn)
					<td class="currency">{{ isset($tableDealerDamageAmountRow[$tableDealerDamageAmountColumnKey]) ? number_format($tableDealerDamageAmountRow[$tableDealerDamageAmountColumnKey], 0, ',', '.') : '' }}</td>
					@endforeach
					<td class="currency"><strong>{{ isset($tableDealerDamageAmountRow['total']) ? number_format($tableDealerDamageAmountRow['total'], 0, ',', '.') : '' }}</strong></td>
					<td class="numeric {{ isset($tableDealerDamageAmountRow['diff']) ? $tableDealerDamageAmountRow['diff'] < 0 ? 'negative' : '' : '' }}">{{ number_format((isset($tableDealerDamageAmountRow['diff']) ? $tableDealerDamageAmountRow['diff'] : ''), 0, ',', '.') }}%</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		
		@endif

	</body>
</html>

