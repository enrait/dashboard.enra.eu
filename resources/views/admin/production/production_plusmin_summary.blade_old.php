@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{ trans('enra/sales.plusminus_summary') }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
	<h1>{{ trans('enra/sales.plusminus_summary') }}</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}"><i class="fa fa-2x fa-home"></i>{{ trans('enra/general.dashboard') }} </a>
		</li>
		<li class="active">
			{{ trans('enra/sales.plusminus_summary') }}
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary ">
				<div class="panel-heading">
					<h4 class="panel-title"><i class="fa fa-bar-chart-o"></i> {{ trans('enra/sales.plusminus_list') }} </h4>
				</div>
				<br />
				<div class="panel-body">
					<div class="table-responsive">
						<table id="table" class="display responsive nowrap" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>{{ trans('enra/sales.AGE_AGENTNR') }}</th>
									<th>{{ trans('enra/sales.AGB_55002') }}</th>
									<th>{{ trans('enra/sales.POL_BRANCHE_SPEC') }}</th>
									<th>{{ trans('enra/sales.AGE_NAAM') }}</th>
									<th>{{ trans('enra/sales.AGE_POSTCODE') }}</th>
									<th>{{ trans('enra/sales.AGE_PLAATS') }}</th>
									<th>{{ trans('enra/sales.AGE_INGDAT') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_VJ') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DJ') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DIFF') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DIFF_PERC') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_VJM') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DJM') }}</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>{{ trans('enra/sales.AGE_AGENTNR') }}</th>
									<th>{{ trans('enra/sales.AGB_55002') }}</th>
									<th>{{ trans('enra/sales.POL_BRANCHE_SPEC') }}</th>
									<th>{{ trans('enra/sales.AGE_NAAM') }}</th>
									<th>{{ trans('enra/sales.AGE_POSTCODE') }}</th>
									<th>{{ trans('enra/sales.AGE_PLAATS') }}</th>
									<th>{{ trans('enra/sales.AGE_INGDAT') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_VJ') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DJ') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DIFF') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DIFF_PERC') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_VJM') }}</th>
									<th>{{ trans('enra/sales.POL_TOTAAL_DJM') }}</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script>

	$.fn.datepicker.dates['nl'] = {
		days: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"],
		daysShort: ["zo", "ma", "di", "wo", "do", "vr", "za", "zo"],
		daysMin: ["zo", "ma", "di", "wo", "do", "vr", "za", "zo"],
		months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
		monthsShort: ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"],
		today: "Vandaag",
		clear: "Wissen",
		weekStart: 1,
		format: "yyyy-mm-dd"
	};

	$.fn.datepicker.dates['de'] = {
		days: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
		daysShort: ["Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", "Son"],
		daysMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
		months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
		monthsShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
		today: "Heute",
		clear: "Löschen",
		weekStart: 1,
		format: "yyyy.mm.dd"
	};
	
	$.extend( $.fn.datepicker.defaults, {
		todayBtn : "linked",
		language : "{{ App::getLocale() }}",
		autoclose : true
	});

$(document).ready(function() 
{
	var urlQueryString = '{{ $selectData['urlQueryString'] }}';
	var ajaxUrl = '{{ route('sales.ProductionPlusMinusSummaryTableData') }}?' + urlQueryString.replace(/&amp;/g,'&');
	
	var table = $('#table').DataTable( 
	{
		@if(App::getLocale() == 'nl')
		
		language: { url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"},
		
		@endif
		
		@if(App::getLocale() == 'de')
		
		language: { url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"},
		
		@endif
		
		@if(App::getLocale() == 'en')
		
		language: { url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json"},
		
		@endif
		
		ajax: ajaxUrl,
		//processing: true,
		//serverSide: true,
		//deferRender: true,
		//stateSave: true,
		responsive: true,
		columns: [
			{ data: 'AGE_AGENTNR', name: 'AGE_AGENTNR',  },
			{ data: 'AGB_55002', name: 'AGB_55002' },
			{ data: 'POL_BRANCHE_SPEC', name: 'POL_BRANCHE_SPEC' },
			{ data: 'AGE_NAAM', name: 'AGE_NAAM' },
			{ data: 'AGE_POSTCODE', name: 'AGE_POSTCODE' },
			{ data: 'AGE_PLAATS', name: 'AGE_PLAATS' },
			{ data: 'AGE_INGDAT', name: 'AGE_INGDAT' },
			{ data: 'POL_TOTAAL_VJ', name: 'POL_TOTAAL_VJ' },
			{ data: 'POL_TOTAAL_DJ', name: 'POL_TOTAAL_DJ' },
			{ data: 'POL_TOTAAL_DIFF', name: 'POL_TOTAAL_DIFF' },
			{ data: 'POL_TOTAAL_DIFF_PERC', name: 'POL_TOTAAL_DIFF_PERC' },
			{ data: 'POL_TOTAAL_VJM', name: 'POL_TOTAAL_VJM' },
			{ data: 'POL_TOTAAL_DJM', name: 'POL_TOTAAL_DJM' },
		],
	 	dom: 'irBtlp',
	 	//dom: 'Bfrtip',
	 	buttons: ['excel']
	});
	
	var colIndex = 0;
	var htmlSearch = '<tr>';
	
	// Setup - add a text input to each footer cell
    $('#table tfoot th').each( function () {
        var title = $(this).text();

    	if(colIndex==1)
        {
        	htmlSearch = htmlSearch + '<th><select name="region[]" id="inputCol' + colIndex + '" style="width: 100%; font-weight: normal;">';
        	htmlSearch = htmlSearch + '<option value=""></option>';
        	
        	@foreach ($selectData['region']['data'] as $selectValue => $selectDescription)
				//htmlSearch = htmlSearch + '<option {{ in_array($selectValue, $selectData['region']['selected']) ? 'selected' : '' }} value="{{ $selectValue }}">{{ $selectDescription }}</option>';
				htmlSearch = htmlSearch + '<option value="{{ $selectValue }}">{{ $selectDescription }}</option>';
			@endforeach
        	
        	htmlSearch = htmlSearch + '</select></th>';
        }
        else
        {
        	htmlSearch = htmlSearch + '<th><input id="inputCol' + colIndex + '" type="text" placeholder="'+title+'" style="width: 100%; font-weight: normal;"/></th>';
        }

        colIndex++;
    } );
    
    htmlSearch = htmlSearch + '</tr>';
    
    $('#table thead').append(htmlSearch);
    
    // Apply the search
    table.columns().every( function () {
		var column = this;
 		
        $( '#inputCol'+column.index()).on( 'keyup change', function ( event )
        {
        	var target = $( event.target );
        	
        	if(target.is('input'))
        	{
        		//console.log('isInput');
        		
	            if ( column.search() !== this.value ) {    
		            column
		                .search(this.value)
		                .draw();
	            };
	        }
	        else if(target.is('select'))
	        {
	        	//console.log('isSelect');
	        	
	        	if ( column.search() !== $("option:selected", this).text()) {    
		            column
		                .search($("option:selected", this).text())
		                .draw();
	           };
	        };
        } );
    } );

	table.on('draw', function()
	{
		$('.livicon').each(function()
		{
			$(this).updateLivicon();
		});
		
		@if(App::getLocale() == 'nl')
		
		$("#inputCol6").datepicker(
		{
			format : "yyyy-mm-dd",
			todayBtn : "linked",
			language : "nl",
			autoclose : true
		});
		
		@endif
		
		@if(App::getLocale() == 'de')
		
		$("#inputCol6").datepicker(
		{
			format : "yyyy.mm.dd",
			todayBtn : "linked",
			language : "de",
			autoclose : true
		});
		
		@endif
		
		$("#inputCol6").datepicker().on('changeDate', function (ev) 
		{
		    var column = table.column(6);
		    
		    if ( column.search() !== this.value ) 
            {    
	            column
	                .search(this.value)
	                .draw();
            };
		});
	});

	
});
</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="client_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>
<script>
	$(function()
	{
		$('body').on('hidden.bs.modal', '.modal', function()
		{
			$(this).removeData('bs.modal');
		});
	}); 
</script>
@stop