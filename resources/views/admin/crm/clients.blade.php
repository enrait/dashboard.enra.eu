@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{ trans('enra/sales.crm') }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
	<h1>{{ trans('enra/sales.clients') }}</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}"><i class="fa fa-2x fa-home"></i>{{ trans('enra/general.dashboard') }} </a>
		</li>
		<li class="active">
			{{ trans('enra/sales.clients') }}
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary ">
				<div class="panel-heading">
					<h4 class="panel-title"><i class="fa fa-group"></i> {{ trans('enra/sales.clients_list') }} </h4>
				</div>
				<br />
				<div class="panel-body">
					<div class="table-responsive">
						<table id="table" class="display responsive nowrap" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th>{{ trans('enra/sales.anva_agentnr') }}</th>
									<th>{{ trans('enra/sales.region') }}</th>
									<th>{{ trans('enra/sales.name') }}</th>
									<th>{{ trans('enra/sales.visiting_zipcode') }}</th>
									<th>{{ trans('enra/sales.visiting_city') }}</th>
									<th>{{ trans('enra/sales.status') }}</th>
									<th>{{ trans('enra/sales.last_visit') }}</th>
									<!-- <th></th> -->
									<!-- <th>{{ trans('enra/sales.actions') }}</th> -->
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>{{ trans('enra/sales.anva_agentnr') }}</th>
									<th>{{ trans('enra/sales.region') }}</th>
									<th>{{ trans('enra/sales.name') }}</th>
									<th>{{ trans('enra/sales.visiting_zipcode') }}</th>
									<th>{{ trans('enra/sales.visiting_city') }}</th>
									<th>{{ trans('enra/sales.status') }}</th>
									<th>{{ trans('enra/sales.last_visit') }}</th>
									<!-- <th></th> -->
									<!-- <th>{{ trans('enra/sales.actions') }}</th> -->
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script>

	$.fn.datepicker.dates['nl'] = {
		days: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"],
		daysShort: ["zo", "ma", "di", "wo", "do", "vr", "za", "zo"],
		daysMin: ["zo", "ma", "di", "wo", "do", "vr", "za", "zo"],
		months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
		monthsShort: ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"],
		today: "Vandaag",
		clear: "Wissen",
		weekStart: 1,
		format: "yyyy-mm-dd"
	};

	$.fn.datepicker.dates['de'] = {
		days: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
		daysShort: ["Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", "Son"],
		daysMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
		months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
		monthsShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
		today: "Heute",
		clear: "Löschen",
		weekStart: 1,
		format: "yyyy.mm.dd"
	};

	$.extend( $.fn.datepicker.defaults, {
		todayBtn : "linked",
		language : "{{ App::getLocale() }}",
		autoclose : true
	});

$(document).ready(function()
{
	var urlQueryString = '{{ $selectData['urlQueryString'] }}';
	var ajaxUrl = '{{ route('sales.ClientsTableData') }}?' + urlQueryString.replace(/&amp;/g,'&');

	var table = $('#table').DataTable(
	{
		@if(App::getLocale() == 'nl')

		language: { url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"},

		@endif

		@if(App::getLocale() == 'de')

		language: { url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"},

		@endif

		@if(App::getLocale() == 'en')

		language: { url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json"},

		@endif

		ajax: ajaxUrl,
		// processing: true,
		// serverSide: true,
		//deferRender: true,
		//stateSave: true,
		responsive: true,
		columns: [
			{ data: 'anva_agentnr', name: 'anva_agentnr',  },
			{ data: 'region', name: 'region' },
			{ data: 'name', name: 'name' },
			{ data: 'visiting_zipcode', name: 'visiting_zipcode' },
			{ data: 'visiting_city', name: 'visiting_city' },
			{ data: 'status', name: 'status' },
			{ data: 'last_visit', name: 'last_visit' },
		],
	 	dom: 'iBrtlp',
        buttons: [
            {
                text: '{{ trans('enra/sales.add_client') }}',
                className: 'add-client btn btn-primary btn-md btn-responsive',
                action: function ( e, dt, node, config ) {

                	//console.log(formatNew());

                	table.buttons().disable();

					$('#table').before(formatNew());

					$('select.anva_agentnr').html( getAnvaAgentsSelectOptions());

					$('.livicon').each(function()
					{
						$(this).updateLivicon();
					});

					$('#undo-new-client').on('click', function()
					{
						$('#new-client').remove();

						table.buttons().enable();
					});

					$('#save-new-client').on('click', function()
					{
						$( "#new-client-info" ).submit();
					});
                }
            }
        ]
	});

	var colIndex = 0;
	var htmlSearch = '<tr>';

	// Setup - add a text input to each footer cell
    $('#table tfoot th').each( function () {
        var title = $(this).text();

        if(colIndex==1)
        {
        	htmlSearch = htmlSearch + '<th><select name="region[]" id="inputCol' + colIndex + '" style="width: 100%; font-weight: normal;">';
        	htmlSearch = htmlSearch + '<option value=""></option>';

        	@foreach ($selectData['region']['data'] as $selectValue => $selectDescription)
				htmlSearch = htmlSearch + '<option value="{{ $selectValue }}">{{ $selectDescription }}</option>';
			@endforeach

        	htmlSearch = htmlSearch + '</select></th>';
        }
        else if(colIndex==5)
        {
        	htmlSearch = htmlSearch + '<th><select name="clientstatus[]" id="inputCol' + colIndex + '" style="width: 100%; font-weight: normal;">';
        	htmlSearch = htmlSearch + '<option value=""></option>';

        	@foreach ($selectData['clientstatus']['data'] as $selectValue => $selectDescription)
				htmlSearch = htmlSearch + '<option value="{{ $selectValue }}">{{ $selectDescription }}</option>';
			@endforeach

        	htmlSearch = htmlSearch + '</select></th>';
        }
        else
        {
        	htmlSearch = htmlSearch + '<th><input id="inputCol' + colIndex + '" type="text" placeholder="'+title+'" style="width: 100%; font-weight: normal;"/></th>';
        }

        colIndex++;
    } );

    htmlSearch = htmlSearch + '</tr>';

    $('#table thead').append(htmlSearch);

    // Apply the search
    table.columns().every( function () {
		var column = this;

        $( '#inputCol'+column.index()).on( 'keyup change', function ( event )
        {
        	var target = $( event.target );

        	if(target.is('input'))
        	{
        		//console.log('isInput');

	            if ( column.search() !== this.value ) {
		            column
		                .search(this.value)
		                .draw();
	            };
	        }
	        else if(target.is('select'))
	        {
	        	//console.log('isSelect');

	        	if ( column.search() !== $("option:selected", this).text()) {
		            column
		                .search($("option:selected", this).text())
		                .draw();
	           };
	        };
        } );
    } );

	table.on('draw', function()
	{
		$('.livicon').each(function()
		{
			$(this).updateLivicon();
		});

		@if(App::getLocale() == 'nl')

		$("#inputCol6").datepicker(
		{
			format : "yyyy-mm-dd",
			todayBtn : "linked",
			language : "nl",
			autoclose : true
		});

		@endif

		@if(App::getLocale() == 'de')

		$("#inputCol6").datepicker(
		{
			format : "yyyy.mm.dd",
			todayBtn : "linked",
			language : "de",
			autoclose : true
		});

		@endif

		$("#inputCol6").datepicker().on('changeDate', function (ev)
		{
		    var column = table.column(6);

		    if ( column.search() !== this.value )
            {
	            column
	                .search(this.value)
	                .draw();
            };
		});
	});

	$('#table tbody').on('click', 'tr:not(.row-expand)', function()
	{
		var trParent = $(this).closest('tr');
		var row = table.row( trParent );

		if ( row.child.isShown() )
		{
			// This row is already open - close it
			row.child.hide();
			trParent.removeClass('shown');
		}
		else
		{
			table.rows({page:'current'}).eq(0).each( function ( idx ) {
			  var tableRow = table.row( idx );

			  if ( tableRow.child.isShown() )
			  {
			    tableRow.child.hide();
			    $(tableRow.node()).removeClass('shown');
			  }
			});

			// Open this row
			row.child( formatShow(row.data()), 'row-expand' ).show();
			trParent.addClass('shown');

			$('.livicon').each(function()
			{
				$(this).updateLivicon();
			});

			$('#table tbody').on('click', 'a.edit-client', function()
			//$('#table tbody').click(function(e)
			{
				var trParent = $(this).closest('tr');
				var trParentPrev = trParent.prev('tr');
				var row = table.row( trParentPrev );
				var rowData = row.data();

				row.child( formatEdit(rowData), 'row-expand' ).show();

				$('select.anva_agentnr').html( getAnvaAgentsSelectOptions());
				//$('select.anva_agentnr').val(rowData.anva_agentnr);

				$('.livicon').each(function()
				{
					$(this).updateLivicon();
				});

				$('#table tbody').on('click', 'a.undo-client', function()
				{
					var trParent = $(this).closest('tr');
					var trParentPrev = trParent.prev('tr');
					var row = table.row( trParentPrev );
					var rowData = row.data();

					row.child( formatShow(rowData), 'row-expand' ).show();

					$('.livicon').each(function()
					{
						$(this).updateLivicon();
					});
				});

				$('#table tbody').on('click', 'a.save-client', function()
				{
					$( "#edit-client-info" ).submit();
				});

				$('#table tbody').on('click', 'a.remove-client', function()
				{
					$( "input[name='delete']" ).val('1');
					$( "#edit-client-info" ).submit();
				});
			});
		}
	});
});

function getAnvaAgentsSelectOptions () {

	var returnValue = [ '<option value=""></option>', @foreach ($selectData['agent']['data'] as $selectValue => $selectDescription) '<option value="{{ $selectValue }}">{{ $selectValue }} - {{ $selectDescription }}</option>',  @endforeach ];

	return returnValue;
}

	/* Formatting function for row details - modify as you need */
	function formatNew() {
	// `rowData ` is the original data object for the row
	@include('admin/crm/crm_clients_list_row_expand_new');
	}

	/* Formatting function for row details - modify as you need */
	function formatShow ( rowData ) {
	// `rowData ` is the original data object for the row
	@include('admin/crm/crm_clients_list_row_expand_show');
	}

	/* Formatting function for row details - modify as you need */
	function formatEdit ( rowData ) {
	// `rowData ` is the original data object for the row
	@include('admin/crm/crm_clients_list_row_expand_edit');
	}
</script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="client_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>
<script>
	$(function()
	{
		$('body').on('hidden.bs.modal', '.modal', function()
		{
			$(this).removeData('bs.modal');
		});
	});
</script>
@stop
