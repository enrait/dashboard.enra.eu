'<div class="table-row-expand show-mode">'+
	'<div class="row">'+
		'<div class="col-sm-6">'+
		'</div>'+
		'<div class="col-sm-6 text-right">'+
			'<a class="edit-contact-moment"><i class="livicon" data-name="edit" data-size="32" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="{{ trans('enra/sales.edit_contact_moment_info') }}"></i></a>'+
		'</div>'+	
	'</div>'+
	'<div class="row">'+
		'<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.contact_at') }}</td>'+
		            '<td>'+(rowData.contact_at != null ? rowData.contact_at : '')+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.contact_type') }}</td>'+
		            '<td>'+rowData.type+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.action_at') }}</td>'+
		            '<td>'+(rowData.action_at != null ? rowData.action_at : '')+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.action_status') }}</td>'+
		            '<td>'+rowData.action_status+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
	'<div class="row">'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.contact_post') }}</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		        	'<td style="font-weight: normal;"><textarea disabled rows="5" style="border: none;">'+rowData.post+'</textarea></td>'+	            
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.action_post') }}</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td style="font-weight: normal;"><textarea disabled rows="5" style="border: none;">'+rowData.action_post+'</textarea></td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
'</div>';