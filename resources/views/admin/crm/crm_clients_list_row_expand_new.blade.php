htmlShow = '<div id="new-client" class="table-row-expand">'+
	'<form id="new-client-info" action="{{route('sales.crm') }}" method="post">'+
		'<input type="hidden" name="_token" value="{{ csrf_token() }}">'+
		'<div class="row">'+
			'<div class="col-sm-6">'+
				'<h4 style="color: #F89A14; margin-top: 0; margin-bottom: 0;"></h4>'+
			'</div>'+
			'<div class="col-sm-6 text-right">'+
				'<a id="save-new-client"><i class="livicon" data-name="save" data-size="32" data-loop="true" data-c="#F89A14" data-hc="#F89A14" title="{{ trans('enra/sales.save_client_info') }}"></i></a>'+
				'<a id="undo-new-client"><i class="livicon" data-name="undo" data-size="32" data-loop="true" data-c="#00bc8c" data-hc="#00bc8c" title="{{ trans('enra/sales.undo_client_info') }}"></i></a>'+	
			'</div>'+	
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.name') }}</td>'+
			            '<td><input type="text" name="name" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.status') }}</td>'+
			            '<td><select name="status">'+	
				        	@foreach ($selectData['clientstatus']['data'] as $selectValue => $selectDescription)				
								'<option value="{{ $selectValue }}">{{ $selectDescription }}</option>'+
							@endforeach
			        	'</select></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.anva_agentnr') }}</td>'+
			            '<td><select class="anva_agentnr" name="anva_agentnr">'+
			        	'</select></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.extra_info') }}</td>'+
			            '<td><input type="text" name="extra_info" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_full_name') }}</td>'+
			            '<td><input style="width: 49%; margin-right: 1%;" type="text" name="contact_first_name" value=""><input style="width: 50%;" type="text" name="contact_last_name" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_jobtitle') }}</td>'+
			            '<td><input type="text" name="contact_jobtitle" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_phonenumber') }}</td>'+
			            '<td><input type="text" name="contact_phonenumber" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_email_address') }}</td>'+
			            '<td><input type="text" name="contact_email_address" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_street_housenumber') }}</td>'+
			            '<td><input style="width: 80%; margin-right: 1%;" type="text" name="visiting_street" value=""><input style="width: 19%;" type="text" name="visiting_housenumber" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_zipcode_city') }}</td>'+
			            '<td><input style="width: 29%; margin-right: 1%;" type="text" name="visiting_zipcode" value=""><input style="width: 70%;" type="text" name="visiting_city" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_country') }}</td>'+
			            '<td><input type="text" name="visiting_country" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.postal_street_housenumber') }}</td>'+
			            '<td><input style="width: 80%; margin-right: 1%;" type="text" name="postal_street" value=""><input style="width: 19%;" type="text" name="postal_housenumber" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.postal_zipcode_city') }}</td>'+
			            '<td><input style="width: 29%; margin-right: 1%;" type="text" name="postal_zipcode" value=""><input style="width: 70%;" type="text" name="postal_city" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.postal_country') }}</td>'+
			            '<td><input type="text" name="postal_country" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.phonenumber') }}</td>'+
			            '<td><input type="text" name="phonenumber" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.faxnumber') }}</td>'+
			            '<td><input type="text" name="faxnumber" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.email_address') }}</td>'+
			            '<td><input type="text" name="email_address" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.website') }}</td>'+
			            '<td><input type="text" name="website" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.start_date') }}</td>'+
			            '<td><div id="start_date" style="position: relative;"><input type="text" name="start_date" value="" data-provide="datepicker" data-date-container="#start_date"></div></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.last_visit') }}</td>'+
			            '<td><div id="last_visit" style="position: relative;"><input type="text" name="last_visit" value="" data-provide="datepicker" data-date-container="#last_visit"></div></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visit_frequency') }}</td>'+
			            '<td><input type="text" name="visit_frequency" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.next_visit') }}</td>'+
			            '<td><div id="next_visit" style="position: relative;"><input type="text" name="next_visit" value="" data-provide="datepicker" data-date-container="#next_visit"></div></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.legal_form') }}</td>'+
			            '<td><input type="text" name="legal_form" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.chamber_of_commerce_id') }}</td>'+
			            '<td><input type="text" name="chamber_of_commerce_id" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.bovag_member_id') }}</td>'+
			            '<td><input type="text" name="bovag_member_id" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.organization_member') }}</td>'+
			            '<td><input type="text" name="organization_member" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.software') }}</td>'+
			            '<td><input type="text" name="software" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.qualification') }}</td>'+
			            '<td><select name="qualification">'+
	        				'<option value=""></option>'+	
				        	@foreach ($selectData['classification']['data'] as $selectValue => $selectDescription)				
								'<option value="{{ $selectValue }}">{{ $selectValue }} - {{ $selectDescription }}</option>'+
							@endforeach
			        	'</select></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.shop_layout') }}</td>'+
			            '<td><input type="text" name="shop_layout" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.location') }}</td>'+
			            '<td><input type="text" name="location" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.m2verk') }}</td>'+
			            '<td><input type="text" name="m2verk" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.m2werk') }}</td>'+
			            '<td><input type="text" name="m2werk" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.conc1') }}</td>'+
			            '<td><input type="text" name="conc1" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.conc2') }}</td>'+
			            '<td><input type="text" name="conc2" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.branche') }}</td>'+
			            '<td><input type="text" name="branche" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.region') }}</td>'+
			            '<td><select name="region">'+
	        				'<option value=""></option>'+	
				        	@foreach ($selectData['region']['data'] as $selectValue => $selectDescription)				
								'<option value="{{ $selectValue }}">{{ $selectValue }} - {{ $selectDescription }}</option>'+
							@endforeach
			        	'</select></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.shop_closed') }}</td>'+
			            '<td><input type="text" name="shop_closed" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.provision') }}</td>'+
			            '<td><input type="text" name="provision" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.bankaccount') }}</td>'+
			            '<td><input type="text" name="bankaccount" value=""></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.kto') }}</td>'+
			            '<td><input type="text" name="kto" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_longitude') }}</td>'+
			            '<td><input type="text" name="visiting_longitude" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_latitude') }}</td>'+
			            '<td><input type="text" name="visiting_latitude" value=""></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
	'</form>'+
'</div>';

return htmlShow;