'<div class="table-row-expand edit-mode">'+
	'<form id="edit-contact-moment-info" action="{{route('sales.contactmoments') }}" method="post">'+
		'<input type="hidden" name="_token" value="{{ csrf_token() }}">'+
		'<input type="hidden" name="id" value="'+rowData.id+'">'+
		'<input type="hidden" name="client[]" value="'+rowData.client_id+'">'+
		'<input type="hidden" name="client_id" value="'+rowData.client_id+'">'+
		'<input type="hidden" name="user_id" value="{{ Sentinel::getUser()->id }}">'+
		'<input type="hidden" name="delete" value="0">'+
		'<div class="row">'+
			'<div class="col-sm-6">'+
				'<h4 style="color: #F89A14; margin-top: 0; margin-bottom: 0;"></h4>'+
			'</div>'+
			'<div class="col-sm-6 text-right">'+
				'<a class="remove-contact-moment"><i class="livicon" data-name="minus-alt" data-size="32" data-loop="true" data-c="#EF6F6C" data-hc="#EF6F6C" title="{{ trans('enra/sales.delete_contact_moment_info') }}"></i></a>'+
				'<a class="save-contact-moment"><i class="livicon" data-name="save" data-size="32" data-loop="true" data-c="#F89A14" data-hc="#F89A14" title="{{ trans('enra/sales.save_contact_moment_info') }}"></i></a>'+
				'<a class="undo-contact-moment"><i class="livicon" data-name="undo" data-size="32" data-loop="true" data-c="#00bc8c" data-hc="#00bc8c" title="{{ trans('enra/sales.undo_contact_moment_info') }}"></i></a>'+	
			'</div>'+	
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_at') }}</td>'+
			            '<td><div id="contact_at" style="position: relative;"><input type="text" name="contact_at" value="'+(rowData.contact_at != null ? rowData.contact_at : '')+'" data-provide="datepicker" data-date-container="#contact_at"></div></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_type') }}</td>'+
			            '<td><select name="type"> @foreach ($selectData['contactmomenttype']['data'] as $selectValue => $selectDescription) <option '  + (rowData.type == htmlDecode('{{ $selectDescription }}') ? 'selected' : '') + ' value="{{ $selectValue }}">{{ $selectDescription }}</option> @endforeach </select></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.action_at') }}</td>'+
			            '<td><div id="action_at" style="position: relative;"><input type="text" name="action_at" value="'+(rowData.action_at != null ? rowData.action_at : '')+'" data-provide="datepicker" data-date-container="#action_at"></div></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.action_status') }}</td>'+
			            '<td><input type="text" name="action_status" value="'+rowData.action_status+'"></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_post') }}</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			        	'<td style="font-weight: normal;"><textarea rows="5" name="post">'+rowData.post+'</textarea></td>'+	            
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.action_post') }}</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td style="font-weight: normal;"><textarea rows="5" name="action_post">'+rowData.action_post+'</textarea></td>'+	
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
	'</form>'+
'</div>';