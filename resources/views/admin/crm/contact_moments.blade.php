@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    {{ trans('enra/sales.contact_moments') }}
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')

@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>{{ trans('enra/sales.contact_moments') }}</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}"><i class="fa fa-2x fa-home"></i>{{ trans('enra/general.dashboard') }}
                </a>
            </li>
            <li class="active">
                {{ trans('enra/sales.contact_moments') }}
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content paddingleft_right15">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary ">
                    <div class="panel-heading">
                        <h4 class="panel-title"><i
                                class="fa fa-comment"></i> {{ trans('enra/sales.contact_moments_list') }} </h4>
                    </div>
                    <br/>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="table" class="display responsive" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>{{ trans('enra/sales.anva_agentnr') }}</th>
                                    <th>{{ trans('enra/sales.region') }}</th>
                                    <th>{{ trans('enra/sales.user_name') }}</th>
                                    <th>{{ trans('enra/sales.contact_at') }}</th>
                                    <th>{{ trans('enra/sales.contact_type') }}</th>
                                    <th>{{ trans('enra/sales.action_at') }}</th>
                                    <th>{{ trans('enra/sales.action_status') }}</th>
                                </tr>
                                <tr>
                                    <th colspan="4">{{ trans('enra/sales.contact_post') }}</th>
                                    <th colspan="3">{{ trans('enra/sales.action_post') }}</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>{{ trans('enra/sales.anva_agentnr') }}</th>
                                    <th>{{ trans('enra/sales.region') }}</th>
                                    <th>{{ trans('enra/sales.user_name') }}</th>
                                    <th>{{ trans('enra/sales.contact_at') }}</th>
                                    <th>{{ trans('enra/sales.contact_type') }}</th>
                                    <th>{{ trans('enra/sales.action_at') }}</th>
                                    <th>{{ trans('enra/sales.action_status') }}</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script>
        $(document).ready(function () {
            var urlQueryString = '{{ $selectData['urlQueryString'] }}';
            var ajaxUrl = '{{ route('sales.ContactMomentsTableData') }}?' + urlQueryString.replace(/&amp;/g, '&');

            var table = $('#table').DataTable(
                {
                    @if(App::getLocale() == 'nl')

                    language: {url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"},

                    @endif

                        @if(App::getLocale() == 'de')

                    language: {url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"},

                    @endif

                        @if(App::getLocale() == 'en')

                    language: {url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json"},

                    @endif
                    ajax: ajaxUrl,
                    // processing: true,
                    // serverSide: true,
                    deferRender: true,
                    stateSave: true,
                    responsive: true,
                    columns: [
                        {data: 'anva_agentnr', name: 'anva_agentnr'},
                        {data: 'region', name: 'region'},
                        {data: 'user_name', name: 'user_name'},
                        {data: 'contact_at', name: 'contact_at'},
                        {data: 'type', name: 'type'},
                        {data: 'action_at', name: 'action_at'},
                        {data: 'action_status', name: 'action_status'},
                    ],
                    order: [[3, 'desc']],
                    drawCallback: function (settings) {
                        //var api = this.api();
                        var rowsData = table.rows({page: 'current'}).data();
                        var rows = table.rows({page: 'current'}).nodes();

                        //console.log('drawCallback');

                        rowsData.each(function (data, i) {
                            rowClass = $(rows).eq(i).attr("class");
                            rowHTML = $(rows).eq(i).html();

                            var newHTML = '<tr role="row" class="' + rowClass + '"><td colspan="4">' + data.post + '</td><td colspan="3">' + data.action_post + '</td></tr>';

                            $(rows).eq(i).after(newHTML);
                        });

                        $('tbody tr').hover(
                            function () {
                                var elementClass = $(this).attr("class");

                                //console.log(elementClass);

                                $(this).prev("." + elementClass).addClass("hover");
                                $(this).addClass("hover");
                                $(this).next("." + elementClass).addClass("hover");
                            },
                            function () {
                                $(this).prev().removeClass("hover");
                                $(this).removeClass("hover");
                                $(this).next().removeClass("hover");
                            }
                        );
                    },
                    dom: 'irtlp',
                    @if( count($selectData['client']['selected']) == 1 )
                    dom: 'iBrtlp',
                    buttons: [
                        {
                            text: '{{ trans('enra/sales.add_contact_moment') }}',
                            className: 'add-contact-moment btn btn-primary btn-md btn-responsive',
                            action: function (e, dt, node, config) {

                                //console.log(formatNew());

                                table.buttons().disable();

                                $('#table').before(formatNew());

                                $('.livicon').each(function () {
                                    $(this).updateLivicon();
                                });

                                $('#undo-new-contact-moment').on('click', function () {
                                    $('#new-contact-moment').remove();

                                    table.buttons().enable();
                                });

                                $('#save-new-contact-moment').on('click', function () {
                                    $("#new-contact-moment-info").submit();
                                });
                            }
                        }
                    ]
                    @endif
                });

            var colIndex = 0;
            var htmlSearch = '<tr>';

            // Setup - add a text input to each footer cell
            $('#table tfoot th').each(function () {
                var title = $(this).text();

                if (colIndex == 1) {
                    htmlSearch = htmlSearch + '<th><select name="region[]" id="inputCol' + colIndex + '" style="width: 100%; font-weight: normal;">';
                    htmlSearch = htmlSearch + '<option value=""></option>';

                    @foreach ($selectData['region']['data'] as $selectValue => $selectDescription)
                        htmlSearch = htmlSearch + '<option value="{{ $selectValue }}">{{ $selectDescription }}</option>';
                    @endforeach

                        htmlSearch = htmlSearch + '</select></th>';
                } else if (colIndex == 4) {
                    htmlSearch = htmlSearch + '<th><select id="inputCol' + colIndex + '" style="width: 100%; font-weight: normal;">';
                    htmlSearch = htmlSearch + '<option value=""></option>';

                    @foreach ($selectData['contactmomenttype']['data'] as $selectValue => $selectDescription)
                        htmlSearch = htmlSearch + '<option value="{{ $selectValue }}">{{ $selectDescription }}</option>';
                    @endforeach

                        htmlSearch = htmlSearch + '</select></th>';
                } else {
                    htmlSearch = htmlSearch + '<th><input id="inputCol' + colIndex + '" type="text" placeholder="' + title + '" style="width: 100%; font-weight: normal;"/></th>';
                }

                colIndex++;
            });

            htmlSearch = htmlSearch + '</tr>';

            $('#table thead').append(htmlSearch);

            // Apply the search
            table.columns().every(function () {
                var column = this;

                $('#inputCol' + column.index()).on('keyup change', function () {
                    var target = $(event.target);

                    if (target.is('input')) {
                        //console.log('isInput');

                        if (column.search() !== this.value) {
                            column
                                .search(this.value)
                                .draw();
                        }
                        ;
                    } else if (target.is('select')) {
                        //console.log('isSelect');

                        if (column.search() !== $("option:selected", this).text()) {
                            column
                                .search($("option:selected", this).text())
                                .draw();
                        }
                        ;
                    }
                    ;

                });
            });

            table.on('draw', function () {
                $('.livicon').each(function () {
                    $(this).updateLivicon();
                });

                @if(App::getLocale() == 'nl')

                $("#inputCol3").datepicker(
                    {
                        format: "yyyy-mm-dd",
                        todayBtn: "linked",
                        language: "nl",
                        autoclose: true
                    });

                @endif

                @if(App::getLocale() == 'de')

                $("#inputCol3").datepicker(
                    {
                        format: "yyyy.mm.dd",
                        todayBtn: "linked",
                        language: "de",
                        autoclose: true
                    });

                @endif

                $("#inputCol3").datepicker().on('changeDate', function (ev) {
                    var column = table.column(3);

                    if (column.search() !== this.value) {
                        column
                            .search(this.value)
                            .draw();
                    }
                    ;
                });

                @if(App::getLocale() == 'nl')

                $("#inputCol5").datepicker(
                    {
                        format: "yyyy-mm-dd",
                        todayBtn: "linked",
                        language: "nl",
                        autoclose: true
                    });

                @endif

                @if(App::getLocale() == 'de')

                $("#inputCol5").datepicker(
                    {
                        format: "yyyy.mm.dd",
                        todayBtn: "linked",
                        language: "de",
                        autoclose: true
                    });

                @endif

                $("#inputCol5").datepicker().on('changeDate', function (ev) {
                    var column = table.column(5);

                    if (column.search() !== this.value) {
                        column
                            .search(this.value)
                            .draw();
                    }
                    ;
                });

                @if(App::getLocale() == 'nl')

                $('.datepicker').datepicker({
                    format: "yyyy-mm-dd",
                    todayBtn: "linked",
                    language: "nl",
                    autoclose: true
                });

                @endif

                @if(App::getLocale() == 'de')

                $('.datepicker').datepicker({
                    format: "yyyy.mm.dd",
                    todayBtn: "linked",
                    language: "de",
                    autoclose: true
                });

                @endif

            });

            $('#table tbody').on('click', 'tr:not(.row-expand)', function () {
                var trParent = $(this).closest('tr');
                var trParentPrev = trParent.prev('tr');
                var row = table.row(trParentPrev.hasClass('hover') ? trParentPrev : trParent);

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();
                    trParent.removeClass('shown');
                    trParentPrev.removeClass('shown');
                } else {
                    table.rows({page: 'current'}).eq(0).each(function (idx) {
                        var tableRow = table.row(idx);

                        if (tableRow.child.isShown()) {
                            tableRow.child.hide();
                            $(tableRow.node()).removeClass('shown');
                        }
                    });

                    // Open this row
                    row.child(formatShow(row.data()), 'row-expand').show();

                    if (trParentPrev.hasClass('hover')) {
                        trParentPrev.addClass('shown');
                    } else {
                        trParent.addClass('shown');
                    }

                    $('.livicon').each(function () {
                        $(this).updateLivicon();
                    });

                    $('#table tbody').on('click', 'a.edit-contact-moment', function () {
                        var trParent = $(this).closest('tr');
                        var trParentPrev = trParent.prev('tr');
                        var row = table.row(trParentPrev);

                        row.child(formatEdit(row.data()), 'row-expand').show();

                        $('.livicon').each(function () {
                            $(this).updateLivicon();
                        });

                        $('#table tbody').on('click', 'a.undo-contact-moment', function () {
                            var trParent = $(this).closest('tr');
                            var trParentPrev = trParent.prev('tr');
                            var row = table.row(trParentPrev);

                            row.child(formatShow(row.data()), 'row-expand').show();

                            $('.livicon').each(function () {
                                $(this).updateLivicon();
                            });
                        });

                        $('#table tbody').on('click', 'a.save-contact-moment', function () {
                            $("#edit-contact-moment-info").submit();
                        });

                        $('#table tbody').on('click', 'a.remove-contact-moment', function () {
                            $("input[name='delete']").val('1');
                            $("#edit-contact-moment-info").submit();
                        });
                    });
                }
            });
        });

        /* Formatting function for row details - modify as you need */
        function formatNew() {
            // `rowData ` is the original data object for the row
            return @include('admin/crm/crm_contact_moments_list_row_expand_new');
        }

        /* Formatting function for row details - modify as you need */
        function formatShow(rowData) {
            // `rowData ` is the original data object for the row
            return @include('admin/crm/crm_contact_moments_list_row_expand_show');
        }

        /* Formatting function for row details - modify as you need */
        function formatEdit(rowData) {
            // `rowData ` is the original data object for the row
            return @include('admin/crm/crm_contact_moments_list_row_expand_edit');
        }

        $.fn.datepicker.dates['nl'] = {
            days: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag", "zondag"],
            daysShort: ["zo", "ma", "di", "wo", "do", "vr", "za", "zo"],
            daysMin: ["zo", "ma", "di", "wo", "do", "vr", "za", "zo"],
            months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
            monthsShort: ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"],
            today: "Vandaag",
            clear: "Wissen",
            weekStart: 1,
            format: "yyyy-mm-dd"
        };

        $.fn.datepicker.dates['de'] = {
            days: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
            daysShort: ["Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", "Son"],
            daysMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
            months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            monthsShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
            today: "Heute",
            clear: "Löschen",
            weekStart: 1,
            format: "yyyy.mm.dd"
        };

        $.extend($.fn.datepicker.defaults, {
            todayBtn: "linked",
            language: "{{ App::getLocale() }}",
            autoclose: true
        });

    </script>

    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog"
         aria-labelledby="client_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content"></div>
        </div>
    </div>
    <script>
        $(function () {
            $('body').on('hidden.bs.modal', '.modal', function () {
                $(this).removeData('bs.modal');
            });
        });
    </script>
@stop
