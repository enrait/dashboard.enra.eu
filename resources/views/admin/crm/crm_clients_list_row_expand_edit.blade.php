htmlShow = '<div class="table-row-expand edit-mode">'+
	'<form id="edit-client-info" action="{{route('sales.crm') }}" method="post">'+
		'<input type="hidden" name="_token" value="{{ csrf_token() }}">'+
		'<input type="hidden" name="id" value="'+rowData.id+'">'+
		'<input type="hidden" name="delete" value="0">'+
		'<div class="row">'+
			'<div class="col-sm-6">'+
				'<h4 style="color: #F89A14; margin-top: 0; margin-bottom: 0;">'+(rowData.anva_agentnr.trim() == '' ? '' : '{{ trans('enra/sales.is_an_anva_client') }}')+'</h4>'+
			'</div>'+
			'<div class="col-sm-6 text-right">'+
				'<a class="remove-client"><i class="livicon" data-name="minus-alt" data-size="32" data-loop="true" data-c="#EF6F6C" data-hc="#EF6F6C" title="{{ trans('enra/sales.delete_client_info') }}"></i></a>'+
				'<a class="save-client"><i class="livicon" data-name="save" data-size="32" data-loop="true" data-c="#F89A14" data-hc="#F89A14" title="{{ trans('enra/sales.save_client_info') }}"></i></a>'+
				'<a class="undo-client"><i class="livicon" data-name="undo" data-size="32" data-loop="true" data-c="#00bc8c" data-hc="#00bc8c" title="{{ trans('enra/sales.undo_client_info') }}"></i></a>'+	
			'</div>'+	
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.name') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="name" value="'+rowData.name+'">' : rowData.name)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.status') }}</td>'+
			            '<td><select name="status">	@foreach ($selectData['clientstatus']['data'] as $selectValue => $selectDescription) <option '  + (rowData.status == htmlDecode('{{ $selectDescription }}') ? 'selected' : '') + ' value="{{ $selectValue }}">{{ $selectDescription }}</option> @endforeach </select></td>'+		          
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.anva_agentnr') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<select class="anva_agentnr" name="anva_agentnr"></select>' : rowData.anva_agentnr)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.extra_info') }}</td>'+
			            '<td><input type="text" name="extra_info" value="'+rowData.extra_info+'"></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>';

var index;

for (index = 0; index < rowData.contactInfo.length; ++index) {
	htmlShow = htmlShow+'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_full_name') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input style="width: 49%; margin-right: 1%;" type="text" name="contact_first_name" value="'+rowData.contactInfo[index].contact_first_name+'"><input style="width: 50%;" type="text" name="contact_last_name" value="'+rowData.contactInfo[index].contact_last_name+'">' : rowData.contactInfo[index].contact_first_name+' '+rowData.contactInfo[index].contact_last_name)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_jobtitle') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="contact_jobtitle" value="'+rowData.contactInfo[index].contact_jobtitle+'">' : rowData.contactInfo[index].contact_jobtitle)+'</td>'+
			        '</tr>'+
				'</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
		    	'<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_phonenumber') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="contact_phonenumber" value="'+rowData.contactInfo[index].contact_phonenumber+'">' : rowData.contactInfo[index].contact_phonenumber)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.contact_email_address') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="contact_email_address" value="'+rowData.contactInfo[index].contact_email_address+'">' : rowData.contactInfo[index].contact_email_address)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>';
}
	
htmlShow = htmlShow+'<div class="row">'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_street_housenumber') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input style="width: 80%; margin-right: 1%;" type="text" name="visiting_street" value="'+rowData.visiting_street+'"><input style="width: 19%;" type="text" name="visiting_housenumber" value="'+rowData.visiting_housenumber+'">' : rowData.visiting_street+' '+rowData.visiting_housenumber)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_zipcode_city') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input style="width: 29%; margin-right: 1%;" type="text" name="visiting_zipcode" value="'+rowData.visiting_zipcode+'"><input style="width: 70%;" type="text" name="visiting_city" value="'+rowData.visiting_city+'">' : rowData.visiting_zipcode+' '+rowData.visiting_city)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_country') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="visiting_country" value="'+rowData.visiting_country+'">' : rowData.visiting_country)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.postal_street_housenumber') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input style="width: 80%; margin-right: 1%;" type="text" name="postal_street" value="'+rowData.postal_street+'"><input style="width: 19%;" type="text" name="postal_housenumber" value="'+rowData.postal_housenumber+'">' : rowData.postal_street+' '+rowData.postal_housenumber)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.postal_zipcode_city') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input style="width: 29%; margin-right: 1%;" type="text" name="postal_zipcode" value="'+rowData.postal_zipcode+'"><input style="width: 70%;" type="text" name="postal_city" value="'+rowData.postal_city+'">' : rowData.postal_zipcode+' '+rowData.postal_city)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.postal_country') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="postal_country" value="'+rowData.postal_country+'">' : rowData.postal_country)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.phonenumber') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="phonenumber" value="'+rowData.phonenumber+'">' : rowData.phonenumber)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.faxnumber') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="faxnumber" value="'+rowData.faxnumber+'">' : rowData.faxnumber)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.email_address') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="email_address" value="'+rowData.email_address+'">' : rowData.email_address)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.website') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="website" value="'+rowData.website+'">' : rowData.website)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.start_date') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<div id="start_date" style="position: relative;"><input type="text" name="start_date" value="'+(rowData.start_date != null ? rowData.start_date : '')+'" data-provide="datepicker" data-date-container="#start_date"></div>' : (rowData.start_date != null ? rowData.start_date : ''))+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.last_visit') }}</td>'+
			            '<td><div id="last_visit" style="position: relative;"><input type="text" name="last_visit" value="'+(rowData.last_visit != null ? rowData.last_visit : '')+'" data-provide="datepicker" data-date-container="#last_visit"></div></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visit_frequency') }}</td>'+
			            '<td><input type="text" name="visit_frequency" value="'+rowData.visit_frequency+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.next_visit') }}</td>'+
			            '<td><div id="next_visit" style="position: relative;"><input type="text" name="next_visit" value="'+(rowData.next_visit != null ? rowData.next_visit : '')+'" data-provide="datepicker" data-date-container="#next_visit"></div></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.legal_form') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="legal_form" value="'+rowData.legal_form+'">' : rowData.legal_form)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.chamber_of_commerce_id') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="chamber_of_commerce_id" value="'+rowData.chamber_of_commerce_id+'">' : rowData.chamber_of_commerce_id)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.bovag_member_id') }}</td>'+
			            '<td><input type="text" name="bovag_member_id" value="'+rowData.bovag_member_id+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.organization_member') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="organization_member" value="'+rowData.organization_member+'">' : rowData.organization_member)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.software') }}</td>'+
			            '<td><input type="text" name="software" value="'+rowData.software+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.qualification') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<select name="qualification">	@foreach ($selectData['classification']['data'] as $selectValue => $selectDescription) <option '  + (rowData.qualification == htmlDecode('{{ $selectDescription }}') ? 'selected' : '') + ' value="{{ $selectValue }}">{{ $selectDescription }}</option> @endforeach </select>' : rowData.qualification)+'</td>'+	
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.shop_layout') }}</td>'+
			            '<td><input type="text" name="shop_layout" value="'+rowData.shop_layout+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.location') }}</td>'+
			            '<td><input type="text" name="location" value="'+rowData.location+'"></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.m2verk') }}</td>'+
			            '<td><input type="text" name="m2verk" value="'+rowData.m2verk+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.m2werk') }}</td>'+
			            '<td><input type="text" name="m2werk" value="'+rowData.m2werk+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.conc1') }}</td>'+
			            '<td><input type="text" name="conc1" value="'+rowData.conc1+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.conc2') }}</td>'+
			            '<td><input type="text" name="conc2" value="'+rowData.conc2+'"></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.branche') }}</td>'+
			            '<td><input type="text" name="branche" value="'+rowData.branche+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.region') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<select name="region">	@foreach ($selectData['region']['data'] as $selectValue => $selectDescription) <option '  + (rowData.region == htmlDecode('{{ $selectDescription }}') ? 'selected' : '') + ' value="{{ $selectValue }}">{{ $selectDescription }}</option> @endforeach </select>' : rowData.region)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.shop_closed') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="shop_closed" value="'+rowData.shop_closed+'">' : rowData.shop_closed)+'</td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.provision') }}</td>'+
			            '<td><input type="text" name="provision" value="'+rowData.provision+'"></td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.bankaccount') }}</td>'+
			            '<td>'+(rowData.anva_agentnr.trim() == '' ? '<input type="text" name="bankaccount" value="'+rowData.bankaccount+'">' : rowData.bankaccount)+'</td>'+
			        '</tr>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.kto') }}</td>'+
			            '<td><input type="text" name="kto" value="'+rowData.kto+'"></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
		'<div class="row">'+
		    '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_longitude') }}</td>'+
			            '<td><input type="text" name="visiting_longitude" value="'+rowData.visiting_longitude+'"></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		     '<div class="col-md-6">'+
			    '<table>'+
			        '<tr class="row-expand">'+
			            '<td>{{ trans('enra/sales.visiting_latitude') }}</td>'+
			            '<td><input type="text" name="visiting_latitude" value="'+rowData.visiting_latitude+'"></td>'+
			        '</tr>'+
			    '</table>'+
		    '</div>'+
		'</div>'+
	'</form>'+
'</div>';

return htmlShow;