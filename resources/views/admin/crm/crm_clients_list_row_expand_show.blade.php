htmlShow = '<div class="table-row-expand show-mode">'+
	'<div class="row">'+
		'<div class="col-sm-6">'+
		'</div>'+
		'<div class="col-sm-6 text-right">'+
			'<a href="{{ route('sales.contactmoments') }}?client[]='+rowData.id+'" target="_blank"><i class="livicon" data-name="comments" data-size="32" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="{{ trans('enra/sales.view_contact_history') }}"></i></a>'+
			'<a href="{{ route('sales.productionSummary') }}?agent[]='+rowData.anva_agentnr+'" target="_blank"><i class="livicon" data-name="barchart" data-size="32" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="{{ trans('enra/sales.view_production_summary') }}"></i></a>'+
			'<a href="{{ route('api.event') }}?agentnr='+rowData.anva_agentnr+'" target="_blank"><i class="livicon" data-name="calendar" data-size="32" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="{{ trans('enra/sales.create_calender_event') }}"></i></a>'+
			'<a class="edit-client"><i class="livicon" data-name="edit" data-size="32" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="{{ trans('enra/sales.edit_client_info') }}"></i></a>'+
		'</div>'+	
	'</div>'+
	'<div class="row">'+
		'<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.name') }}</td>'+
		            '<td>'+rowData.name+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.status') }}</td>'+
		            '<td>'+rowData.status+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.anva_agentnr') }}</td>'+
		            '<td>'+rowData.anva_agentnr+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.extra_info') }}</td>'+
		            '<td>'+rowData.extra_info+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>';

var index;

for (index = 0; index < rowData.contactInfo.length; ++index) {
	htmlShow = htmlShow+'<div class="row">'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.contact_full_name') }}</td>'+
		            '<td>'+rowData.contactInfo[index].contact_first_name+' '+rowData.contactInfo[index].contact_last_name+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.contact_jobtitle') }}</td>'+
		            '<td>'+rowData.contactInfo[index].contact_jobtitle+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.contact_phonenumber') }}</td>'+
		            '<td><a href="tel:'+rowData.contactInfo[index].contact_phonenumber+'">'+rowData.contactInfo[index].contact_phonenumber+'</a></td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.contact_email_address') }}</td>'+
		            '<td><a href="mailto:'+rowData.contactInfo[index].contact_email_address+'">'+rowData.contactInfo[index].contact_email_address+'</a></td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>';
}
	
htmlShow = htmlShow+'<div class="row">'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.visiting_street_housenumber') }}</td>'+
		            '<td>'+rowData.visiting_street+' '+rowData.visiting_housenumber+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.visiting_zipcode_city') }}</td>'+
		            '<td>'+rowData.visiting_zipcode+' '+rowData.visiting_city+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.visiting_country') }}</td>'+
		            '<td>'+rowData.visiting_country+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.postal_street_housenumber') }}</td>'+
		            '<td>'+rowData.postal_street+' '+rowData.postal_housenumber+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.postal_zipcode_city') }}</td>'+
		            '<td>'+rowData.postal_zipcode+' '+rowData.postal_city+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.postal_country') }}</td>'+
		            '<td>'+rowData.postal_country+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
	'<div class="row">'+
		'<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.phonenumber') }}</td>'+
		            '<td><a href="tel:'+rowData.phonenumber+'">'+rowData.phonenumber+'</a></td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.faxnumber') }}</td>'+
		            '<td>'+rowData.faxnumber+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.email_address') }}</td>'+
		            '<td><a href="mailto:'+rowData.email_address+'">'+rowData.email_address+'</a></td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.website') }}</td>'+
		            '<td><a href="http://'+rowData.website+'" target="_blank">'+rowData.website+'</a></td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
	'<div class="row">'+
		'<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.start_date') }}</td>'+
		            '<td>'+(rowData.start_date != null ? rowData.start_date : '')+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.last_visit') }}</td>'+
		            '<td>'+(rowData.last_visit != null ? rowData.last_visit : '')+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.visit_frequency') }}</td>'+
		            '<td>'+rowData.visit_frequency+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.next_visit') }}</td>'+
		            '<td>'+(rowData.next_visit != null ? rowData.next_visit : '')+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.legal_form') }}</td>'+
		            '<td>'+rowData.legal_form+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.chamber_of_commerce_id') }}</td>'+
		            '<td>'+rowData.chamber_of_commerce_id+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.bovag_member_id') }}</td>'+
		            '<td>'+rowData.bovag_member_id+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.organization_member') }}</td>'+
		            '<td>'+rowData.organization_member+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
	'<div class="row">'+
		'<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.software') }}</td>'+
		            '<td>'+rowData.software+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.qualification') }}</td>'+
		            '<td>'+rowData.qualification+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.shop_layout') }}</td>'+
		            '<td>'+rowData.shop_layout+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.location') }}</td>'+
		            '<td>'+rowData.location+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.m2verk') }}</td>'+
		            '<td>'+rowData.m2verk+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.m2werk') }}</td>'+
		            '<td>'+rowData.m2werk+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.conc1') }}</td>'+
		            '<td>'+rowData.conc1+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.conc2') }}</td>'+
		            '<td>'+rowData.conc2+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
	'<div class="row">'+
		'<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.branche') }}</td>'+
		            '<td>'+rowData.branche+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.region') }}</td>'+
		            '<td>'+rowData.region+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.shop_closed') }}</td>'+
		            '<td>'+rowData.shop_closed+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.provision') }}</td>'+
		            '<td>'+rowData.provision+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.bankaccount') }}</td>'+
		            '<td>'+rowData.bankaccount+'</td>'+
		        '</tr>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.kto') }}</td>'+
		            '<td>'+rowData.kto+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
	'<div class="row">'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.visiting_longitude') }}</td>'+
		            '<td>'+rowData.visiting_longitude+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	    '<div class="col-md-6">'+
		    '<table>'+
		        '<tr class="row-expand">'+
		            '<td>{{ trans('enra/sales.visiting_latitude') }}</td>'+
		            '<td>'+rowData.visiting_latitude+'</td>'+
		        '</tr>'+
		    '</table>'+
	    '</div>'+
	'</div>'+
'</div>';

return htmlShow;