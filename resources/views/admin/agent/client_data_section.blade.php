<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading clickable">
				<h3 class="panel-title"><i class="fa fa-briefcase"></i> {{ trans('enra/administration.selected_agent') }}
                </h3>
                <span class="pull-right"> <i class="fa fa-chevron-up"></i> </span>
            </div>
            <div class="panel-body" style="background-color: #f9f9f9;">
                @if(count(array($clientData)) > 0)
                    <div class="table-client-data">
                        @if(Route::getCurrentRoute()->getName() == 'sales.agentData')
                            <div class="row row-button">
                                <div class="col-sm-6">
                                    <!--- <a class="edit-client"><button type="button" class="btn btn-md btn-responsive btn-primary" title="{{ trans('enra/sales.edit_client_info') }}"><i class="fa fa-fw fa-edit"></i></button></a> --->
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="{{ route('sales.contactmoments') }}?client[]={{ $clientData->id }}"
                                       target="_blank">
                                        <button type="button" class="btn btn-md btn-responsive btn-primary"
                                                title="{{ trans('enra/sales.view_contact_history') }}"><i
                                                class="fa fa-fw fa-comment"></i></button>
                                    </a>
                                    <a href="{{ route('api.event') }}?agentnr={{ $clientData->anva_agentnr }}"
                                       target="_blank">
                                        <button type="button" class="btn btn-md btn-responsive btn-primary"
                                                title="{{ trans('enra/sales.create_calender_event') }}"><i
                                                class="fa fa-fw fa-calendar"></i></button>
                                    </a>
                                    <a href="{{ route('sales.productionSummary') }}?agent[]={{ $clientData->anva_agentnr }}"
                                       target="_blank">
                                        <button type="button" class="btn btn-md btn-responsive btn-primary"
                                                title="{{ trans('enra/sales.view_production_summary') }}"><i
                                                class="fa fa-fw fa-bar-chart-o"></i></button>
                                    </a>
                                </div>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.name') }}</td>
                                        <td>{{ $clientData->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.status') }}</td>
                                        <td>{{ $clientData->status }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.anva_agentnr') }}</td>
                                        <td>{{ $clientData->anva_agentnr }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.contact_full_name') }}</td>
                                        <td>{{ $clientData->contact_first_name.' '.$clientData->contact_last_name }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.contact_phonenumber') }}</td>
                                        <td>
                                            <a href="tel:{{ $clientData->contact_phonenumber }}">{{ $clientData->contact_phonenumber }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.contact_email_address') }}</td>
                                        <td>
                                            <a href="mailto:{{ $clientData->contact_email_address }}">{{ $clientData->contact_email_address }}</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.extra_info') }}</td>
                                        <td>{{ $clientData->extra_info }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.visiting_street_housenumber') }}</td>
                                        <td>{{ $clientData->visiting_street.' '.$clientData->visiting_housenumber }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.visiting_zipcode_city') }}</td>
                                        <td>{{ $clientData->visiting_zipcode.' '.$clientData->visiting_city }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.visiting_country') }}</td>
                                        <td>{{ $clientData->visiting_country }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.postal_street_housenumber') }}</td>
                                        <td>{{ $clientData->postal_street.' '.$clientData->postal_housenumber }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.postal_zipcode_city') }}</td>
                                        <td>{{ $clientData->postal_zipcode.' '.$clientData->postal_city }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.postal_country') }}</td>
                                        <td>{{ $clientData->postal_country }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.phonenumber') }}</td>
                                        <td>
                                            <a href="tel:{{ $clientData->phonenumber }}">{{ $clientData->phonenumber }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.faxnumber') }}</td>
                                        <td>{{ $clientData->faxnumber }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.email_address') }}</td>
                                        <td>
                                            <a href="mailto:{{ $clientData->email_address }}">{{ $clientData->email_address }}</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.website') }}</td>
                                        <td><a href="http://{{ $clientData->website }}"
                                               target="_blank">{{ $clientData->website }}</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.start_date') }}</td>
                                        @if(App::getLocale() == 'de')

                                            <td>{{ $clientData->start_date != null ? date('d.m.Y', strtotime($clientData->start_date)) : '' }}</td>

                                        @else

                                            <td>{{ $clientData->start_date != null ? date('d-m-Y', strtotime($clientData->start_date)) : '' }}</td>

                                        @endif
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.last_visit') }}</td>
                                        @if(App::getLocale() == 'de')

                                            <td>{{ $clientData->last_visit != null ? date('d.m.Y', strtotime($clientData->last_visit)) : '' }}</td>

                                        @else

                                            <td>{{ $clientData->last_visit != null ? date('d-m-Y', strtotime($clientData->last_visit)) : '' }}</td>

                                        @endif
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.visit_frequency') }}</td>
                                        <td>{{ $clientData->visit_frequency }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.next_visit') }}</td>
                                        @if(App::getLocale() == 'de')

                                            <td>{{ $clientData->next_visit != null ? date('d.m.Y', strtotime($clientData->next_visit)) : '' }}</td>

                                        @else

                                            <td>{{ $clientData->next_visit != null ? date('d-m-Y', strtotime($clientData->next_visit)) : '' }}</td>

                                        @endif
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.legal_form') }}</td>
                                        <td>{{ $clientData->legal_form }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.chamber_of_commerce_id') }}</td>
                                        <td>{{ $clientData->chamber_of_commerce_id }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.bovag_member_id') }}</td>
                                        <td>{{ $clientData->bovag_member_id }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.organization_member') }}</td>
                                        <td>{{ $clientData->organization_member }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.software') }}</td>
                                        <td>{{ $clientData->software }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.qualification') }}</td>
                                        <td>{{ $clientData->qualification }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.shop_layout') }}</td>
                                        <td>{{ $clientData->shop_layout }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.location') }}</td>
                                        <td>{{ $clientData->location }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.m2verk') }}</td>
                                        <td>{{ $clientData->m2verk }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.m2werk') }}</td>
                                        <td>{{ $clientData->m2werk }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.conc1') }}</td>
                                        <td>{{ $clientData->conc1 }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.conc2') }}</td>
                                        <td>{{ $clientData->conc2 }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.branche') }}</td>
                                        <td>{{ $clientData->branche }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.region') }}</td>
                                        <td>{{ $clientData->region }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.shop_closed') }}</td>
                                        <td>{{ $clientData->shop_closed }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.provision') }}</td>
                                        <td>{{ $clientData->provision }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.bankaccount') }}</td>
                                        <td>{{ $clientData->bankaccount }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ trans('enra/sales.kto') }}</td>
                                        <td>{{ $clientData->kto}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.visiting_longitude') }}</td>
                                        <td>{{ $clientData->visiting_longitude }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table>
                                    <tr>
                                        <td>{{ trans('enra/sales.visiting_latitude') }}</td>
                                        <td>{{ $clientData->visiting_latitude }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
			</div>
		</div>
	</div>
</div>
