<script type="text/javascript">
	var formAgentInformation,
	    formAgentInformationDatastringPrevious,
	    ajaxAgentTable = false,
	    dataFoundAgentData,
	    ajaxAgentData = false,
	    dataAgentData;

	@if(Route::getCurrentRoute()->getName() == 'sales.agentInformation')

	var urlAdmin = "{{ url("admin/sales/") }}",
		urlApi = "{{ url("api/sales/") }}";

	@endif

	@if(Route::getCurrentRoute()->getName() == 'administration.agentInformation')

	var urlAdmin = "{{ url("admin/administration/") }}",
		urlApi = "{{ url("api/administration/") }}";

	@endif

	@if(Route::getCurrentRoute()->getName() == 'finance.agentInformation')

	var urlAdmin = "{{ url("admin/finance/") }}",
		urlApi = "{{ url("api/finance/") }}";

	@endif

	function initAgentInformation()
	{
		formAgentInformation = $(".formAgentInformation");

		formAgentInformationChanged(formAgentInformation, true);
	};

	function formAgentInformationChanged(formAgentInformation, forceChange)
	{
		var formAgentInformationDatastring = formAgentInformation.serialize();

		if (formAgentInformationDatastringPrevious != formAgentInformationDatastring || forceChange)
		{
			//console.log(formAgentInformationDatastring);

			formAgentInformationDatastringPrevious = formAgentInformationDatastring;

			getAgentTable(formAgentInformation);
		}
	};

	function getAgentTable(formAgentInformation)
	{
		//console.log( 'ajaxAgentTable ' + (ajaxAgentTable ? 'true' : 'false') );

		if (!ajaxAgentTable)
		{
			ajaxAgentTable = true;

			$('#agenttable-panel-header').addClass('active');

			var request = $.ajax(
			{
				url : urlApi + "/agenttable/",
				type : "POST",
				beforeSend : function(xhr)
				{
					var token = $('meta[name="csrf_token"]').attr('content');

					if (token)
					{
						return xhr.setRequestHeader('X-CSRF-TOKEN', token);
					}
				},
				//contentType:'application/json',
				data : formAgentInformation.serialize(),
				//data: JSON.stringify(formAgentInformation.serialize()),
				//dataType:'json',
			});

			request.done(function(data)
			{
				//console.log( data );

				dataFoundAgentData = data;

				displayAgentTable(dataFoundAgentData);

				ajaxAgentTable = false;
			});

			request.fail(function(jqXHR, textStatus)
			{
				alert("Request failed: " + textStatus);

				ajaxAgentTable = false;
			});

		};
	};

	function displayAgentTable(data)
	{
		var htmlContent = '';

		htmlContent = data;

		//console.log( htmlContent );

		$("#sectionAgentTable").html(htmlContent);

		//console.log( htmlContent );

		var countRows = $('#sectionAgentTable tr').length;

		//console.log( countRows );

		// if (countRows == 2)
		// {
		// $('#sectionAgentTable tr a').trigger('click');
		// }

		$("#searchTypeaheadResultCheckAll").change(function()
		{
			$(".searchTypeaheadResultCkBox").prop('checked', $(this).prop("checked"));
		});

		formAgentInformation = $(".formAgentInformation");

		formAgentInformation.on('submit', (function(e)
		{

			if (!e.isDefaultPrevented())
			{
				formAgentInformationChanged(formAgentInformation, false);

				e.preventDefault();
			}

		}));

		filterTable(document.getElementById('typeaheadAgentSearch'), 'searchTypeaheadResult');
		filterTable(document.getElementById('ibanAgentSearch'), 'searchTypeaheadResult');

		//iCheck for checkbox and radio inputs
		$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck(
		{
			checkboxClass : 'icheckbox_minimal',
			radioClass : 'iradio_minimal'
		});

		$('#typeaheadAgentSearch').bind('typeahead:asyncreceive', function(ev, q, source)
		{
			//console.log('asyncreceive:');

			formAgentInformationChanged(formAgentInformation, false);

		});

		$('#zipcodeFromAgentSearch').on('input', function(ev, q, source)
		{
			//console.log('zipcodeFromAgentSearch input');

			formAgentInformationChanged(formAgentInformation, false);

		});

		$('#zipcodeFromAgentSearch').change(function(ev, q, source)
		{
			//console.log('zipcodeFromAgentSearch onchange');

			formAgentInformationChanged(formAgentInformation, true);

		});

		$('#zipcodeToAgentSearch').on('input', function(ev, q, source)
		{
			//console.log('zipcodeToAgentSearch input');

			formAgentInformationChanged(formAgentInformation, false);

		});

		$('#zipcodeToAgentSearch').change(function(ev, q, source)
		{
			//console.log('zipcodeToAgentSearch onchange');

			formAgentInformationChanged(formAgentInformation, true);

		});

		$('#ibanAgentSearch').on('input', function(ev, q, source)
		{
			//console.log('ibanAgentSearch input');

			formAgentInformationChanged(formAgentInformation, false);

		});

		$('#ibanAgentSearch').change(function(ev, q, source)
		{
			//console.log('ibanAgentSearch onchange');

			formAgentInformationChanged(formAgentInformation, true);

		});

		$('#invoiceIdAgentSearch').on('input', function(ev, q, source)
		{
			//console.log('invoiceIdAgentSearch input');

			formAgentInformationChanged(formAgentInformation, false);

		});

		$('#invoiceIdAgentSearch').change(function(ev, q, source)
		{
			//console.log('invoiceIdAgentSearch onchange');

			formAgentInformationChanged(formAgentInformation, true);

		});

		$('#OrganisationSearch').change(function(ev, q, source)
		{
			formAgentInformationChanged(formAgentInformation, true);
		});
	};

	function showProductionSummaryAll()
	{
		var checked = $('.searchTypeaheadResultCkBox:checked').map(function()
		{
			return 'agent%5B%5D=' + $(this).val();
		}).get().join('&');

		var win = window.open('/admin/sales/productionsummary/?' + checked, '_blank');
		win.focus();
	}

	function getAgentData(agentNr)
	{
		if (!ajaxAgentData)
		{
			ajaxAgentData = true;

			//console.log(agentNr);

			var request = $.ajax(
			{
				url : urlApi + "/agentdata/",
				type : "POST",
				beforeSend : function(xhr)
				{
					var token = $('meta[name="csrf_token"]').attr('content');

					if (token)
					{
						return xhr.setRequestHeader('X-CSRF-TOKEN', token);
					}
				},
				//contentType:'application/json',
				data : 'agentnr=' + agentNr,
				//data: JSON.stringify(formAgentInformation.serialize()),
				//dataType:'json',
			});

			request.done(function(data)
			{
				//console.log( data );

				dataAgentData = data;

				displayAgentData(dataAgentData);

				ajaxAgentData = false;
			});

			request.fail(function(jqXHR, textStatus)
			{
				alert("Request failed: " + textStatus);

				ajaxAgentData = false;
			});

		};
	};

	function displayAgentData(data)
	{
		var htmlContent = '';

		htmlContent = data;

		//console.log( htmlContent );

		$("#sectionAgentData").html(htmlContent);

		var $this = $('#agenttablepanel');
		if (!$this.hasClass('panel-collapsed'))
		{
			$this.parents('.panel').find('.panel-body').slideUp();
			$this.parents('.panel').find('.panel-heading').addClass('panel-collapsed');
			$this.removeClass('fa-chevron-up').addClass('fa-chevron-down');
			$('.showhide').attr('title', 'Show Panel content');
		}
	};

	function filterTable(Stxt, table)
	{
		if(Stxt)
		{
			dehighlight(document.getElementById(table));
			if (Stxt.value.length > 0)
				highlight(Stxt.value.toLowerCase(), document.getElementById(table));
		}
	}

	/*
	 * Transform back each
	 * <span>preText <span class="highlighted">Stxt</span> postText</span>
	 * into its original
	 * preText Stxt postText
	 */
	function dehighlight(container)
	{
		for (var i = 0; i < container.childNodes.length; i++)
		{
			var node = container.childNodes[i];
			if (node.attributes && node.attributes['class'] && node.attributes['class'].value == 'highlighted')
			{
				node.parentNode.parentNode.replaceChild(document.createTextNode(node.parentNode.innerHTML.replace(/<[^>]+>/g, "")), node.parentNode);
				// Stop here and process next parent
				return;
			}
			else if (node.nodeType != 3)
			{
				// Keep going onto other elements
				dehighlight(node);
			}
		}
	}

	/*
	 * Create a
	 * <span>preText <span class="highlighted">Stxt</span> postText</span>
	 * around each search Stxt
	 */
	function highlight(Stxt, container)
	{
		for (var i = 0; i < container.childNodes.length; i++)
		{
			var node = container.childNodes[i];
			if (node.nodeType == 3)
			{
				// Text node
				var data = node.data;
				var data_low = data.toLowerCase();
				if (data_low.indexOf(Stxt) >= 0)
				{
					//Stxt found!
					var new_node = document.createElement('span');
					node.parentNode.replaceChild(new_node, node);
					var result;
					while (( result = data_low.indexOf(Stxt)) != -1)
					{
						new_node.appendChild(document.createTextNode(data.substr(0, result)));
						new_node.appendChild(create_node(document.createTextNode(data.substr(result, Stxt.length))));
						data = data.substr(result + Stxt.length);
						data_low = data_low.substr(result + Stxt.length);
					}
					new_node.appendChild(document.createTextNode(data));
				}
			}
			else
			{
				// Keep going onto other elements
				highlight(Stxt, node);
			}
		}
	}

	function create_node(child)
	{
		var node = document.createElement('span');
		node.setAttribute('class', 'highlighted');
		node.attributes['class'].value = 'highlighted';
		node.appendChild(child);
		return node;
	}


	jQuery(document).ready(function()
	{

		initAgentInformation();

		var AgentSearchResult = new Bloodhound(
		{
			datumTokenizer : Bloodhound.tokenizers.obj.whitespace,
			queryTokenizer : Bloodhound.tokenizers.whitespace,
			sufficient : 20,
			remote :
			{
				url : urlApi + '/getAgentSearchResultJson?q=%QUERY',
				wildcard : '%QUERY'
			}
		});

		$('#typeaheadAgentSearch').typeahead(null,
		{
			name : 'agents',
			source : AgentSearchResult,
			//displayKey: 'AGE_NAAM',
			hint : true,
			highlight : true,
			minLength : 4,
			limit : 4
		});

		//$('#typeaheadAgentSearch').bind('typeahead:select', function(ev, suggestion) {
		//  console.log('Selection: ' + suggestion);
		//});

		//$('#typeaheadAgentSearch').bind('typeahead:asyncreceive', function(ev, q, source) {
		//  console.log('asyncreceive:');
		//});

		//$('#typeaheadAgentSearch').bind('typeahead:render', function(ev, suggestions, async, dataset) {
		//  console.log('render:' + suggestions);
		//});

		$(document).on('click', '.panel div.clickable', function (e) {
		    var $this = $(this);
		    if (!$this.hasClass('panel-collapsed')) {
		        $this.parents('.panel').find('.panel-body').slideUp();
		        $this.addClass('panel-collapsed');
		        $this.find('i.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down');
		    } else {
		        $this.parents('.panel').find('.panel-body').slideDown();
		        $this.removeClass('panel-collapsed');
		        $this.find('i.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up');
		    }
		});

	});

</script>

@if(Route::getCurrentRoute()->getName() == 'salaes.agentInformation' || Route::getCurrentRoute()->getName() == 'adaministration.agentInformation')

<!-- Load the Google Maps JS API. Your Google maps key will be rendered. -->
<script src="//maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript">
	var geocoder;
	var map;
	var places;
	var markers = [];

	function initialize()
	{
		// create the geocoder
		geocoder = new google.maps.Geocoder();

		// set some default map details, initial center point, zoom and style
		var mapOptions =
		{
			center : new google.maps.LatLng(40.74649, -74.0094),
			zoom : 7,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		// create the map and reference the div#map-canvas container
		map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
		// fetch the existing places (ajax)
		// and put them on the map
		fetchPlaces();
	}

	// when page is ready, initialize the map!
	google.maps.event.addDomListener(window, 'load', initialize);
	// add location button event
	jQuery("form").submit(function(e)
	{
		// the name form field value
		var name = jQuery("#place_name").val();

		// get the location text field value
		var loc = jQuery("#location").val();
		console.log("user entered location = " + loc);
		geocoder.geocode(
		{
			'address' : loc
		}, function(results, status)
		{
			if (status == google.maps.GeocoderStatus.OK)
			{
				// log out results from geocoding
				console.log("geocoding results");
				console.log(results);

				// reposition map to the first returned location
				map.setCenter(results[0].geometry.location);

				// put marker on map
				var marker = new google.maps.Marker(
				{
					map : map,
					position : results[0].geometry.location
				});
				bindInfoWindow(marker, map, infowindow, places[p].name + "<br>" + places[p].geo_name);
				// not currently used but good to keep track of markers
				markers.push(marker);

				// preparing data for form posting
				var lat = results[0].geometry.location.lat();
				var lng = results[0].geometry.location.lng();
				var loc_name = results[0].formatted_address;
				// send first location from results to server as new location
				jQuery.ajax(
				{
					url : '/add_place',
					dataType : 'json',
					type : 'POST',
					data :
					{
						name : name,
						latlng : lat + "," + lng,
						geo_name : loc_name
					},
					success : function(response)
					{
						// success - for now just log it
						console.log(response);
					},
					error : function(err)
					{
						// do error checking
						alert("something went wrong");
						console.error(err);
					}
				});
			}
			else
			{
				alert("Try again. Geocode was not successful for the following reason: " + status);
			}
		});

		e.preventDefault();
		return false;
	});
	// fetch Places JSON from /data/places
	// loop through and populate the map with markers
	var fetchPlaces = function()
	{
		var infowindow = new google.maps.InfoWindow(
		{
			content : ''
		});
		jQuery.ajax(
		{
			url : '/api/googlemaps/testMap',
			dataType : 'json',
			success : function(response)
			{

				if (response.status == 'OK')
				{
					places = response.places;
					// loop through places and add markers
					for (p in places)
					{
						//create gmap latlng obj
						tmpLatLng = new google.maps.LatLng(places[p].geo[0], places[p].geo[1]);
						// make and place map maker.

						console.log('places[p].name' + places[p].name);

						var marker = new google.maps.Marker(
						{
							map : map,
							position : tmpLatLng,
							title : places[p].name + "<br>" + places[p].geo_name
						});

						bindInfoWindow(marker, map, infowindow, '<b>' + places[p].name + "</b><br>" + places[p].geo_name);
						// not currently used but good to keep track of markers
						markers.push(marker);
					}
				}
			}
		})
	};
	// binds a map marker and infoWindow together on click
	var bindInfoWindow = function(marker, map, infowindow, html)
	{
		google.maps.event.addListener(marker, 'click', function()
		{
			infowindow.setContent(html);
			infowindow.open(map, marker);
		});
	}

</script>

@endif
