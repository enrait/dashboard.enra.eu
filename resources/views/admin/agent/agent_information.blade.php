@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{ trans('enra/finance.agent_information') }} - {{ trans('enra/general.enra_bv') }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/portlet.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/iCheck/skins/all.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/formelements.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/vendor/typeahead/typeaheadjs.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/vendor/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

<style type="text/css">
	.highlighted {
		background-color: Yellow;
	}
</style>

<!-- end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
	<h1>{{ trans('enra/finance.agent_information') }}</h1>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}"><i class="fa fa-2x fa-home"></i>{{ trans('enra/general.dashboard') }} </a>
		</li>
		<li class="active">
			{{ trans('enra/finance.agent_information') }}
		</li>
	</ol>
</section>
<section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading clickable">
					<h3 class="panel-title"><i class="fa fa-fw fa-search"></i> {{ trans('enra/finance.search') }}</h3>
					<span class="pull-right"> <i class="fa fa-fw fa-chevron-up"></i></span>
				</div>
				<div class="panel-body">
					<form accept-charset="utf-8" class="form-vertical formAgentInformation" name="formAgentInformation" method="POST" action="http://dashboard.enra.eu/api/agents/agentinformation/" novalidate="">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/finance.searchterm') }} </label>
									<input type="text" class="form-control" name="typeaheadAgentSearch" id="typeaheadAgentSearch" placeholder="{{ trans('enra/finance.enter_your_search_term_here') }}">
								</div>
							</div>

							@if(Route::getCurrentRoute()->getName() == 'sales.agentInformation')

							<div class="col-sm-3 col-md-3">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/administration.zipcode_from') }} </label>
									<input type="text" class="form-control" name="zipcodeFromAgentSearch" id="zipcodeFromAgentSearch" placeholder="{{ trans('enra/administration.enter_your_search_term_here') }}">
								</div>
							</div>
							<div class="col-sm-3 col-md-3">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/administration.zipcode_to') }} </label>
									<input type="text" class="form-control" name="zipcodeToAgentSearch" id="zipcodeToAgentSearch" placeholder="{{ trans('enra/administration.enter_your_search_term_here') }}">
								</div>
							</div>

							@endif

							@if(Route::getCurrentRoute()->getName() == 'finance.agentInformation')

							<div class="col-sm-3 col-md-3">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/finance.iban') }} </label>
									<input type="text" class="form-control" name="ibanAgentSearch" id="ibanAgentSearch" placeholder="{{ trans('enra/finance.enter_your_search_term_here') }}">
								</div>
							</div>
							<div class="col-sm-3 col-md-3">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/finance.invoice_id') }} </label>
									<input type="text" class="form-control" name="invoiceIdAgentSearch" id="invoiceIdAgentSearch" placeholder="{{ trans('enra/finance.enter_your_search_term_here') }}">
								</div>
							</div>

							@endif
						</div>
						@if(Route::getCurrentRoute()->getName() == 'sales.agentInformation' || Route::getCurrentRoute()->getName() == 'administration.agentInformation')
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/finance.organisation') }} </label>
									<select name="Organisation" id="OrganisationSearch" class="selectpicker form-control">
										<option value=''> {{ trans('enra/finance.select_organisation') }} </option>
										@foreach ($selectData['organisation']['data'] as $selectValue => $selectDescription)
											<option value="{{ $selectValue }}">{{ $selectDescription }}</option>';
										@endforeach
									</select>
									<!-- <input type="text" class="form-control" name="Organisation" id="OrganisationSearch" placeholder="00007"> -->
								</div>
							</div>
						@endif
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</form>
				</div>
			</div>
		</div>
	</div>

	<section id="sectionAgentTable">

	</section>

	<section id="sectionAgentData">

	</section>

	@if(Route::getCurrentRoute()->getName() == 'saales.agentInformation' || Route::getCurrentRoute()->getName() == 'aadministration.agentInformation')

	<style>
		/* IMPORTANT - must give map div height */
		#map-canvas {
			height: 400px;
		}
		/* IMPORTANT - fixes webkit infoWindow rendering */
		#map-canvas img {
			max-width: none;
		}
	</style>

	<div id="map-canvas"></div>
	<br>
	<p>
		<a href="/data/places" target="_blank">View Places JSON</a>
	</p>

	@endif

</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

<script src="{{ asset('assets/vendors/select2/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/iCheck/icheck.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/vendor/typeahead/typeahead.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/vendor/jasny/jasny-bootstrap.min.js') }}" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

@include('admin/agent/agent_information_js')

@stop
