<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading clickable">
				<h3 id="agenttable-panel-header" class="panel-title has-spinner"><i class="fa fa-group"></i> {{ trans('enra/administration.found_agents') }} <span class="spinner"><i class="icon-spin glyphicon glyphicon-refresh"></i></span></h3>
				<span class="pull-right"> <i id="agenttablepanel" class="fa fa-chevron-up"></i> </span>
			</div>
			<div class="panel-body">
				<table id="searchTypeaheadResult" class="table table-bordered table-striped table-condensed table-hover">
					<thead class="flip-content">
						<tr>
							@if(Route::getCurrentRoute()->getName() == 'sales.agentInformationTable')
							<th><input type="checkbox" id="searchTypeaheadResultCheckAll" value="{{ trans('enra/administration.all') }}"></th>
							@endif
							@foreach ($tableSearchTypeaheadResult['columns'] as $tableSearchTypeaheadResultColumnKey => $tableSearchTypeaheadResultColumn)
							@if ($tableSearchTypeaheadResultColumn['show'])
							<th>{{ $tableSearchTypeaheadResultColumn['label'] }}</th>
							@endif
							@endforeach
							@if(Route::getCurrentRoute()->getName() == 'sales.agentInformationTable')
							<th class="text-right"><button type="button" class="btn btn-xs btn-responsive btn-primary" onclick="showProductionSummaryAll()" title="{{ trans('enra/administration.production_summary_selection') }}"><i class="fa fa-fw fa-bar-chart-o"></i></button></th>
							@endif
						</tr>
					</thead>
					<tbody class="rowlink" data-link="row">
						@foreach ($tableSearchTypeaheadResult['rows'] as $tableSearchTypeaheadResultRow)
						<tr>
							@if(Route::getCurrentRoute()->getName() == 'sales.agentInformationTable')
							<td class="rowlink-skip"><input type="checkbox" id="searchTypeaheadResultCkBox{{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}" class="searchTypeaheadResultCkBox" value="{{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}"></td>
							@endif
							@foreach ($tableSearchTypeaheadResult['columns'] as $tableSearchTypeaheadResultColumnKey => $tableSearchTypeaheadResultColumn)
							@if ($tableSearchTypeaheadResultColumn['show'])
							@if ($tableSearchTypeaheadResultColumnKey == 'AGE_AGENTNR')
							<td class="cell-{{ $tableSearchTypeaheadResultColumnKey }}"><a onClick="getAgentData('{{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}'); return false;" href="/api/agents/getAgentSearchResultJson?agentnr={{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}">{{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}</a></td>
							@else
							<td class="cell-{{ $tableSearchTypeaheadResultColumnKey }}">{{ isset($tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey]) ? $tableSearchTypeaheadResultRow[$tableSearchTypeaheadResultColumnKey] : '' }}</td>
							@endif
							@endif
							@endforeach
							@if(Route::getCurrentRoute()->getName() == 'sales.agentInformationTable')
							<td class="rowlink-skip text-right">
								<div class="btn-group btn-group-xs" role="group">
									@if(isset($tableSearchTypeaheadResultRow['clientId']) && $tableSearchTypeaheadResultRow['clientId'] != '')
										<a href="{{ route('sales.contactmoments') }}?client[]={{ $tableSearchTypeaheadResultRow['clientId'] }}" target="_blank"><button type="button" class="btn btn-xs btn-responsive btn-primary" title="{{ trans('enra/sales.view_contact_history') }}"><i class="fa fa-fw fa-comment"></i></button></a>
									@endif
									<a href="{{ route('api.event') }}?agentnr={{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}" target="_blank"><button type="button" class="btn btn-xs btn-responsive btn-primary" title="{{ trans('enra/sales.create_calender_event') }}"><i class="fa fa-fw fa-calendar"></i></button></a>
									<a href="mailto:{{ $tableSearchTypeaheadResultRow['AGE2_EMAILADRES']}}"><button type="button" class="btn btn-xs btn-responsive btn-primary" title="{{ trans('enra/administration.email') }}"><i class="fa fa-fw fa-envelope"></i></button></a>
									<a href="{{ route('sales.productionSummary') }}?agent[]={{ $tableSearchTypeaheadResultRow['AGE_AGENTNR'] }}" target="_blank"><button type="button" class="btn btn-xs btn-responsive btn-primary" title="{{ trans('enra/sales.view_production_summary') }}"><i class="fa fa-fw fa-bar-chart-o"></i></button></a>
								</div>
							</td>
							@endif
						</tr>					
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>