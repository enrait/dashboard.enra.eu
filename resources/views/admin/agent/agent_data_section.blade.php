<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading clickable">
				<h3 class="panel-title"><i class="fa fa-briefcase"></i> {{ trans('enra/administration.selected_agent') }}</h3>
				<span class="pull-right"> <i class="fa fa-chevron-up"></i> </span>
			</div>
			<div class="panel-body">
				<table id="tableAgentData" class="table table-bordered table-striped table-condensed">
					<tbody>
						@foreach ($agentData as $agentDataKey => $agentDataValue)
							@if ( $agentDataValue != '' && $agentDataValue != '+0000000.00' && $agentDataValue != '0000000.00' && $agentDataValue != '+0000000.  ' && $agentDataValue != '+0000000.+0' )
								<tr>
									<td><strong>{{ $agentDataKey }}</strong></td>
									<td>{{ $agentDataValue }}</td>
								</tr>
							@endif
						@endforeach
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>
