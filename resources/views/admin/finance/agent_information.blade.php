@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
{{ trans('enra/finance.agent_information') }} - {{ trans('enra/general.enra_bv') }}
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/portlet.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/vendors/iCheck/skins/all.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/pages/formelements.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/vendor/typeahead/typeaheadjs.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/vendor/jasny/jasny-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

<style type="text/css">
	.highlighted {
		background-color: Yellow;
	}
</style>

<!-- end of page level css-->
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
	<h1>{{ trans('enra/finance.agent_information') }}</h1><a href="/api/googlemaps/testMap" target="_blank">Google Maps</a>
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="14" data-color="#000"></i> {{ trans('enra/general.dashboard') }} </a>
		</li>
		<li class="active">
			{{ trans('enra/finance.agent_information') }}
		</li>
	</ol>
</section>
<section class="content">

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title"><i class="livicon" data-name="doc-portrait" data-c="#fff" data-hc="#fff" data-size="18" data-loop="true"></i> {{ trans('enra/finance.search') }}</h3>
					<span class="pull-right"> <i class="fa fa-fw fa-chevron-up clickable"></i> <i class="fa fa-fw fa-times removepanel clickable"></i> </span>
				</div>
				<div class="panel-body">
					<form accept-charset="utf-8" class="form-vertical formAgentInformation" name="formAgentInformation" method="POST" action="http://dashboard.enra.eu/api/agents/agentinformation/" novalidate="">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/finance.searchterm') }} </label>
									<input type="text" class="form-control" name="typeaheadAgentSearch" id="typeaheadAgentSearch" placeholder="{{ trans('enra/finance.enter_your_search_term_here') }}">
								</div>
							</div>
							<div class="col-sm-3 col-md-3">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/finance.zipcode_from') }} </label>
									<input type="text" class="form-control" name="zipcodeFromAgentSearch" id="zipcodeFromAgentSearch" placeholder="{{ trans('enra/finance.enter_your_search_term_here') }}">
								</div>
							</div>
							<div class="col-sm-3 col-md-3">
								<div class="form-group">
									<label class="control-label" for="query"> {{ trans('enra/finance.zipcode_to') }} </label>
									<input type="text" class="form-control" name="zipcodeToAgentSearch" id="zipcodeToAgentSearch" placeholder="{{ trans('enra/finance.enter_your_search_term_here') }}">
								</div>
							</div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</form>
				</div>
			</div>
		</div>
	</div>

	<section id="sectionAgentTable">

	</section>

	<section id="sectionAgentData">

	</section>

	<style>
		/* IMPORTANT - must give map div height */
		#map-canvas {
			height: 400px;
		}
		/* IMPORTANT - fixes webkit infoWindow rendering */
		#map-canvas img {
			max-width: none;
		}
	</style>

	<div id="map-canvas"></div>
	<br>
	<p>
		<a href="/data/places" target="_blank">View Places JSON</a>
	</p>

</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

<script src="{{ asset('assets/vendors/select2/select2.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/iCheck/icheck.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/vendor/typeahead/typeahead.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/vendor/jasny/jasny-bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/agent_information.js') }}" type="text/javascript"></script>

<!-- Load the Google Maps JS API. Your Google maps key will be rendered. -->
<script src="//maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript">
	var geocoder;
	var map;
	var places;
	var markers = [];

	function initialize()
	{
		// create the geocoder
		geocoder = new google.maps.Geocoder();

		// set some default map details, initial center point, zoom and style
		var mapOptions =
		{
			center : new google.maps.LatLng(40.74649, -74.0094),
			zoom : 7,
			mapTypeId : google.maps.MapTypeId.ROADMAP
		};

		// create the map and reference the div#map-canvas container
		map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
		// fetch the existing places (ajax)
		// and put them on the map
		fetchPlaces();
	}

	// when page is ready, initialize the map!
	google.maps.event.addDomListener(window, 'load', initialize);
	// add location button event
	jQuery("form").submit(function(e)
	{
		// the name form field value
		var name = jQuery("#place_name").val();

		// get the location text field value
		var loc = jQuery("#location").val();
		console.log("user entered location = " + loc);
		geocoder.geocode(
		{
			'address' : loc
		}, function(results, status)
		{
			if (status == google.maps.GeocoderStatus.OK)
			{
				// log out results from geocoding
				console.log("geocoding results");
				console.log(results);

				// reposition map to the first returned location
				map.setCenter(results[0].geometry.location);

				// put marker on map
				var marker = new google.maps.Marker(
				{
					map : map,
					position : results[0].geometry.location
				});
				bindInfoWindow(marker, map, infowindow, places[p].name + "<br>" + places[p].geo_name);
				// not currently used but good to keep track of markers
				markers.push(marker);

				// preparing data for form posting
				var lat = results[0].geometry.location.lat();
				var lng = results[0].geometry.location.lng();
				var loc_name = results[0].formatted_address;
				// send first location from results to server as new location
				jQuery.ajax(
				{
					url : '/add_place',
					dataType : 'json',
					type : 'POST',
					data :
					{
						name : name,
						latlng : lat + "," + lng,
						geo_name : loc_name
					},
					success : function(response)
					{
						// success - for now just log it
						console.log(response);
					},
					error : function(err)
					{
						// do error checking
						alert("something went wrong");
						console.error(err);
					}
				});
			}
			else
			{
				alert("Try again. Geocode was not successful for the following reason: " + status);
			}
		});

		e.preventDefault();
		return false;
	});
	// fetch Places JSON from /data/places
	// loop through and populate the map with markers
	var fetchPlaces = function()
	{
		var infowindow = new google.maps.InfoWindow(
		{
			content : ''
		});
		jQuery.ajax(
		{
			url : '/api/googlemaps/testMap',
			dataType : 'json',
			success : function(response)
			{

				if (response.status == 'OK')
				{
					places = response.places;
					// loop through places and add markers
					for (p in places)
					{
						//create gmap latlng obj
						tmpLatLng = new google.maps.LatLng(places[p].geo[0], places[p].geo[1]);
						// make and place map maker.
						
						console.log('places[p].name' + places[p].name);
						
						var marker = new google.maps.Marker(
						{
							map : map,
							position : tmpLatLng,
							title : places[p].name + "<br>" + places[p].geo_name
						});
						
						bindInfoWindow(marker, map, infowindow, '<b>' + places[p].name + "</b><br>" + places[p].geo_name);
						// not currently used but good to keep track of markers
						markers.push(marker);
					}
				}
			}
		})
	};
	// binds a map marker and infoWindow together on click
	var bindInfoWindow = function(marker, map, infowindow, html)
	{
		google.maps.event.addListener(marker, 'click', function()
		{
			infowindow.setContent(html);
			infowindow.open(map, marker);
		});
	}

</script>

<script>
	jQuery(document).ready(function()
	{

		initAgentInformation();

		var AgentSearchResult = new Bloodhound(
		{
			datumTokenizer : Bloodhound.tokenizers.obj.whitespace,
			queryTokenizer : Bloodhound.tokenizers.whitespace,
			sufficient : 20,
			remote :
			{
				url : '/api/agents/getAgentSearchResultJson?q=%QUERY',
				wildcard : '%QUERY'
			}
		});

		$('#typeaheadAgentSearch').typeahead(null,
		{
			name : 'agents',
			source : AgentSearchResult,
			//displayKey: 'AGE_NAAM',
			hint : true,
			highlight : true,
			minLength : 4,
			limit : 4
		});

		//$('#typeaheadAgentSearch').bind('typeahead:select', function(ev, suggestion) {
		//  console.log('Selection: ' + suggestion);
		//});

		//$('#typeaheadAgentSearch').bind('typeahead:asyncreceive', function(ev, q, source) {
		//  console.log('asyncreceive:');
		//});

		//$('#typeaheadAgentSearch').bind('typeahead:render', function(ev, suggestions, async, dataset) {
		//  console.log('render:' + suggestions);
		//});

	}); 
</script>

@stop
