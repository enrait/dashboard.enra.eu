
{
	"type":"FeatureCollection",
	"features":
	[
		{
			"type": "Feature",
			"geometry": 
			{
				"type": "Point",
				"coordinates": [
					5.21726131439209,
					52.69773299817507
				]
			},
			"properties": 
			{
				"name": 5.4,
				"street": "48km SSE of Pondaguitan, Philippines",
				"housenumber": 1348176066,
				"zipcode": 480,
				"city": "http://earthquake.usgs.gov/earthquakes/eventpage/usc000csx3",
				"website": null,
				"phonenumber": 2,
				"faxnumber": 3.4
			}
			
		}
	]
}