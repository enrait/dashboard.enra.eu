<!DOCTYPE html>
<html>
	<head>
		<style>
			html, body {
				height: 100%;
				margin: 0;
				padding: 0;
			}
			#map {
				height: 100%;
			}
			#map #infowindow-content {
		        display: inline;
		      }
			.pac-card {
				margin: 10px 10px 0 0;
				border-radius: 2px 0 0 2px;
				box-sizing: border-box;
				-moz-box-sizing: border-box;
				outline: none;
				box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
				background-color: #fff;
				font-family: Roboto;
			}
			#pac-container {
				padding-bottom: 12px;
				margin-right: 12px;
			}
			.pac-controls {
				display: inline-block;
				padding: 5px 11px;
			}
			.pac-controls label {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 13px;
				/* font-weight: bold; */
			}
			#pac-input {
				background-color: #fff;
				font-family: Arial, Helvetica, sans-serif;
				font-size: 16px;
				/* font-weight: bold; */
				margin-top: 10px;
				margin-left: 12px;
				padding: 4px 12px;
				text-overflow: ellipsis;
				width: 400px;
			}
			#pac-input:focus {
				border-color: #4d90fe;
			}
		</style>
	</head>
	<body>
		<input id="pac-input" class="controls" type="text" placeholder="{{ $dataDealerLocator['mapActionText'] }}">
		<div id="map"></div>
		<script>
var map;
var infowindow;
var cachedGeoJson;

function initMap()
{
	map = new google.maps.Map(document.getElementById('map'),
	{
		zoom : 12,
		center : new google.maps.LatLng({{ $dataDealerLocator['mapCenterLatitude'] }}, {{ $dataDealerLocator['mapCenterlongitude'] }}),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		zoomControl: true,
		streetViewControl: true,
		fullscreenControl: true,
        disableDefaultUI: true
	});
	
	// Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var centerPos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        
        // console.log('position.coords.latitude : ' + position.coords.latitude);
        // console.log('position.coords.longitude : ' + position.coords.longitude);
        
        map.setCenter(centerPos);
       
      });
    }

	// Create the search box and link it to the UI element.
	var input = document.getElementById('pac-input');
	var searchBox = new google.maps.places.SearchBox(input);
	map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
	// Bias the SearchBox results towards current map's viewport.
	map.addListener('bounds_changed', function() {
		searchBox.setBounds(map.getBounds());
		
		var lng0 = map.getBounds().getNorthEast().lng();
		var lat0 = map.getBounds().getNorthEast().lat();
		var lng1 = map.getBounds().getSouthWest().lng();
		var lat1 = map.getBounds().getSouthWest().lat();
      
      	// Load GeoJSON.
		var request = new XMLHttpRequest();
		
		// console.log('place.geometry.location.lng : '+ place.geometry.location.lng());
		// console.log('place.geometry.location.lat : '+ place.geometry.location.lat());
		// console.log('url : ' + '/api/webtools/geojson/{{ $dataDealerLocator['objecttype'] }}/' + place.geometry.location.lng() + '/' + place.geometry.location.lat());
		// 					
		// request.open('GET', '/api/webtools/geojson/{{ $dataDealerLocator['objecttype'] }}/' + place.geometry.location.lng() + '/' + place.geometry.location.lat(), true);

		// console.log('map.getBounds().getNorthEast().lng() : '+ lng0);
		// console.log('map.getBounds().getNorthEast().lat() : '+ lat0);
		// console.log('map.getBounds().getSouthWest().lng() : '+ lng1);
		// console.log('map.getBounds().getSouthWest().lat() : '+ lat1);
		// console.log('url : ' + '/api/webtools/geojson/{{ $dataDealerLocator['objecttype'] }}/' + lng0 + '/' + lat0 + '/' + lng1 + '/' + lat1);
				
		request.open('GET', '/api/webtools/geojson/{{ $dataDealerLocator['objecttype'] }}/' + lng0 + '/' + lat0 + '/' + lng1 + '/' + lat1, true);

		request.onload = function()
		{
			if (request.status >= 200 && request.status < 400)
			{
				// Success!
				cachedGeoJson = JSON.parse(request.responseText);
				
				map.data.addGeoJson(cachedGeoJson,
				{
					idPropertyName : "id"
				});
			}
			else
			{
				// We reached our target server, but it returned an error
			}
		};
		
		request.onerror = function()
		{
			// There was a connection error of some sort
		};
		
		request.send();
	});

	//var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();

		if (places.length == 0) {
			return;
		}

		// Clear out the old markers.
		// markers.forEach(function(marker) {
			// marker.setMap(null);
		// });

		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		
		places.forEach(function(place) {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}
		
		    if (place.geometry.viewport) {
		      // Only geocodes have viewport.
		      bounds.union(place.geometry.viewport);
		    } else {
		      bounds.extend(place.geometry.location);
		    }
		  });
		  
          map.fitBounds(bounds);

        });

infowindow = new google.maps.InfoWindow();

google.maps.event.addListener(map, 'click', function()
{
	infowindow.close();
});

map.data.addListener('click', function(event)
{
	var infoWindowContent =
		"<div style='width:200px; text-align: center;'>" +
		event.feature.getProperty("name") + "<br>" +
		event.feature.getProperty("street") + " " + event.feature.getProperty("housenumber") + "<br>" +
		event.feature.getProperty("zipcode") + " " + event.feature.getProperty("city") + "<br>" + "<br>" +
		'<a href="http://' + event.feature.getProperty("website") + '" target="_blank">' + event.feature.getProperty("website") + "</a><br>" + "<br>" +
		event.feature.getProperty("phonenumber") + "<br>" +
		"</div>"
		;
	
	infowindow.setContent(infoWindowContent);
	infowindow.setPosition(event.feature.getGeometry().get());
	infowindow.setOptions(
	{
	pixelOffset : new google.maps.Size(0, -30)
	});
	
	infowindow.open(map);
	});
}
		</script>
		<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlYhexqoNdF91PbhUmYlxchwBY0QNxTtA&libraries=places&callback=initMap" async defer></script>
	</body>
</html>