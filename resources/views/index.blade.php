@extends('layouts/default')

{{-- Page title --}}
@section('title')
Home
@parent
@stop

{{-- content --}}
@section('content')

<div id="primary" class="content-area grid-parent grid-100 tablet-grid-100">
	<main id="main" class="site-main">

		<article id="post-5" class="post-5 page type-page status-publish hentry" itemtype="http://schema.org/CreativeWork" itemscope="itemscope">
			<div class="inside-article">

				<div class="entry-content" itemprop="text">
					<h1>Willkommen Neu</h1>
				</div><!-- .entry-content -->
			</div><!-- .inside-article -->
		</article><!-- #post-## -->

	</main><!-- #main -->
</div><!-- #primary -->

@stop
