<?php

use App\Http\Controllers\WebToolsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('event', array(
    'as' => 'api.event',
    'uses' => 'AgentController@showEvent'
));

Route::group(array('prefix' => 'webtools'), function()
{
    // Route::get('dealerlocator', array(
    // 'as' => 'webtools.showDealerLocator',
    // 'uses' => 'WebToolsController@showDealerLocator'
    // ));

    Route::get('dealerlocator/{country}/{objecttype}/{longitude}/{latitude}', function($country = '', $objecttype = '', $longitude = '', $latitude = '')
    {
        $WebToolsController = new WebToolsController();

        $showDealerLocatorParameters = array(
            'country' => $country,
            'objecttype' => $objecttype,
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        return $WebToolsController -> showDealerLocator($showDealerLocatorParameters);
    });

    Route::get('dealerlocator/{country}/{objecttype}', function($country = '', $objecttype = '')
    {
        $WebToolsController = new WebToolsController();

        $showDealerLocatorParameters = array(
            'country' => $country,
            'objecttype' => $objecttype,
            'longitude' => '',
            'latitude' => ''
        );

        return $WebToolsController -> showDealerLocator($showDealerLocatorParameters);
    });

    Route::get('dealerlocator/{country}', function($country = '')
    {
        $WebToolsController = new WebToolsController();

        $showDealerLocatorParameters = array(
            'country' => $country,
            'objecttype' => 'all',
            'longitude' => '',
            'latitude' => ''
        );

        return $WebToolsController -> showDealerLocator($showDealerLocatorParameters);
    });

    Route::get('dealerlocator', function()
    {
        $WebToolsController = new WebToolsController();

        $showDealerLocatorParameters = array(
            'country' => 'all',
            'objecttype' => 'all',
            'longitude' => '',
            'latitude' => ''
        );

        //$WebToolsController -> updateClientGeoData();

        return $WebToolsController -> showDealerLocator($showDealerLocatorParameters);
    });

    Route::get('updategeodata', function()
    {
        $WebToolsController = new WebToolsController();

        return $WebToolsController -> updateClientGeoData();
    });

    Route::get('geojson', function()
    {
        $WebToolsController = new WebToolsController();

        $getGeoJSONParameters = array(
            'objecttype' => 'all',
            'longitudetopright' => '21.693663597106934',
            'latitudetopright' => '56.81931407957632',
            'longitudebottomleft' => '-9.727234840393066',
            'latitudebottomleft' => '45.190286699022685'
        );

        return $WebToolsController -> getGeoJSON($getGeoJSONParameters);
    });

    Route::get('geojson/{objecttype}', function($objecttype = '')
    {
        $WebToolsController = new WebToolsController();

        $getGeoJSONParameters = array(
            'objecttype' => $objecttype,
            'longitudetopright' => '21.693663597106934',
            'latitudetopright' => '56.81931407957632',
            'longitudebottomleft' => '-9.727234840393066',
            'latitudebottomleft' => '45.190286699022685'
        );

        return $WebToolsController -> getGeoJSON($getGeoJSONParameters);
    });

    Route::get('geojson/{objecttype}/{longitude}/{latitude}', function($objecttype = '', $longitude = '', $latitude = '')
    {
        $WebToolsController = new WebToolsController();

        $getGeoJSONParameters = array(
            'objecttype' => $objecttype,
            'longitude' => $longitude,
            'latitude' => $latitude
        );

        return $WebToolsController -> getGeoJSON($getGeoJSONParameters);
    });

    Route::get('geojson/{objecttype}/{longitudetopright}/{latitudetopright}/{longitudebottomleft}/{latitudebottomleft}', function($objecttype = '', $longitudetopright = '', $latitudetopright = '', $longitudebottomleft = '', $latitudebottomleft = '')
    {
        $WebToolsController = new WebToolsController();

        $getGeoJSONParameters = array(
            'objecttype' => $objecttype,
            'longitudetopright' => $longitudetopright,
            'latitudetopright' => $latitudetopright,
            'longitudebottomleft' => $longitudebottomleft,
            'latitudebottomleft' => $latitudebottomleft
        );

        return $WebToolsController -> getGeoJSON($getGeoJSONParameters);
    });

    Route::get('encryptedw2d', array(
        'as' => 'api.encryptedw2d',
        'uses' => 'WebToolsController@getEncryptedW2D'
    ));

    Route::get('decryptedw2d', array(
        'as' => 'api.decryptedw2d',
        'uses' => 'WebToolsController@getDecryptedW2D'
    ));

    Route::get('getAgentJson', array(
        'as' => 'webtools.id',
        'uses' => 'AgentController@getAgentJson'
    ));

    Route::get('createW2DSearchRelatie', array(
        'as' => 'webtools.createw2dsearchrelatie',
        'uses' => 'WebToolsController@createW2DSearchRelatie'
    ));

    Route::get('createW2DSearchPolis', array(
        'as' => 'webtools.createw2dsearchpolis',
        'uses' => 'WebToolsController@createW2DSearchPolis'
    ));

    Route::get('createW2DSearchPolisBuild', array(
        'as' => 'webtools.createw2dsearchpolisbuild',
        'uses' => 'WebToolsController@createW2DSearchPolisBuild'
    ));

    Route::get('createW2DSearchWachtpolis', array(
        'as' => 'webtools.createw2dsearchwachtpolis',
        'uses' => 'WebToolsController@createW2DSearchWachtpolis'
    ));

    Route::get('getAnvaEfd2Sql', array(
        'as' => 'webtools.getanvaefd2sql',
        'uses' => 'WebToolsController@getAnvaEfd2Sql'
    ));

    Route::get('doAnvaCloneParseBlocks/{dataSet}/{tableName}', function($dataSet = '', $tableName = '')
    {
        $WebToolsController = new WebToolsController();

        die($WebToolsController -> doAnvaCloneParseBlocks($dataSet, $tableName));
    });
});

Route::group(array('prefix' => 'sales'), function()
{
    Route::post('agenttable', array(
        'as' => 'sales.agentInformationTable',
        'uses' => 'AgentController@showAgentTable'
    ));

    Route::get('getAgentSearchResultJson', array(
        'as' => 'sales.agentSearchResultJson',
        'uses' => 'AgentController@getAgentSearchResultJson'
    ));

    Route::post('agentdata', array(
        'as' => 'sales.agentData',
        'uses' => 'AgentController@showAgentData'
    ));

    Route::get('agentdata', array(
        'as' => 'sales.agentData',
        'uses' => 'AgentController@showAgentData'
    ));

    Route::post('productionsummary', array(
        'as' => 'sales.productionSummary',
        'uses' => 'ProductionController@showProductionSummarySectionData'
    ));

    Route::get('productionsummary', array(
        'as' => 'sales.productionSummary',
        'uses' => 'ProductionController@showProductionSummarySectionData'
    ));

    Route::get('getProductionPlusMinusSummaryTableData', array(
        'as' => 'sales.ProductionPlusMinusSummaryTableData',
        'uses' => 'ProductionController@getProductionPlusMinusSummaryTableData'
    ));

    Route::get('getClientsTableData', array(
        'as' => 'sales.ClientsTableData',
        'uses' => 'CRMController@getClientsTableData'
    ));

    Route::get('getContactMomentsTableData', array(
        'as' => 'sales.ContactMomentsTableData',
        'uses' => 'CRMController@getContactMomentsTableData'
    ));

    Route::get('getExcel', array(
        'as' => 'sales.excel',
        'uses' => 'CRMController@getExcel'
    ));
});

Route::group(array('prefix' => 'administration'), function()
{
    Route::post('agenttable', array(
        'as' => 'administration.agentInformationTable',
        'uses' => 'AgentController@showAgentTable'
    ));

    Route::get('getAgentSearchResultJson', array(
        'as' => 'administration.agentSearchResultJson',
        'uses' => 'AgentController@getAgentSearchResultJson'
    ));

    Route::post('agentdata', array(
        'as' => 'administration.agentData',
        'uses' => 'AgentController@showAgentData'
    ));
});

Route::group(array('prefix' => 'finance'), function()
{
    Route::post('agenttable', array(
        'as' => 'finance.agentInformationTable',
        'uses' => 'AgentController@showAgentTable'
    ));

    Route::get('getAgentSearchResultJson', array(
        'as' => 'finance.agentSearchResultJson',
        'uses' => 'AgentController@getAgentSearchResultJson'
    ));

    Route::post('agentdata', array(
        'as' => 'finance.agentData',
        'uses' => 'AgentController@showAgentData'
    ));
});
