<?php

//   Variables used in this script:
//   $summary     - text title of the event
//   $datestart   - the starting date (in seconds since unix epoch)
//   $dateend     - the ending date (in seconds since unix epoch)
//   $address     - the event's address
//   $uri         - the URL of the event (add http://)
//   $description - text description of the event
//   $filename    - the name of this file for saving (e.g. my-event-name.ics)
//
//  Notes:
//  - the UID should be unique to the event, so in this case I'm just using
//    uniqid to create a uid, but you could do whatever you'd like.
//
//  - iCal requires a date format of "yyyymmddThhiissZ". The "T" and "Z"
//    characters are not placeholders, just plain ol' characters. The "T"
//    character acts as a delimeter between the date (yyyymmdd) and the time
//    (hhiiss), and the "Z" states that the date is in UTC time. Note that if
//    you don't want to use UTC time, you must prepend your date-time values
//    with a TZID property. See RFC 5545 section 3.3.5
//
//  - The Content-Disposition: attachment; header tells the browser to save/open
//    the file. The filename param sets the name of the file, so you could set
//    it as "my-event-name.ics" or something similar.
//
//  - Read up on RFC 5545, the iCalendar specification. There is a lot of helpful
//    info in there, such as formatting rules. There are also many more options
//    to set, including alarms, invitees, busy status, etc.
//
//    https://www.ietf.org/rfc/rfc5545.txt

namespace App\Libraries;

use DateTime;

class IcsHelpers
{
	public function createEvent($eventData)
	{
		$summary = "Lorem ipsum dolor ics amet, lorem ipsum dolor ics amet, lorem ipsum dolor ics amet, lorem ipsum dolor ics amet";
		// text title of the event
		$dateStart = new DateTime("2016-04-22T10:00:00+09:00");
		// the starting date (in seconds since unix epoch)
		$dateEnd = new DateTime("2016-04-22T18:30:00+09:00");
		// the ending date (in seconds since unix epoch)
		$location = "Tokyo, Event Location";
		// the event's location
		$uri = "http://url.to/my/event";
		// the URL of the event (add http://)
		$description = "ICS Entertainment";
		// text description of the event
		$filename = "TestDennis.ics";
		// the name of this file for saving (e.g. my-event-name.ics)
		
		$summary = $eventData['summary'];
		$dateStart = $eventData['dateStart'];
		$dateEnd = $eventData['dateEnd'];
		$location = $eventData['location'];
		$uri = $eventData['uri'];
		$description = $eventData['description'];
		$filename = uniqid() . ".ics";

		header('Content-type: text/calendar; charset=utf-8');
		header('Content-Disposition: attachment; filename=' . $filename);

		$icsDateEnd = $this -> dateToCal($dateEnd -> getTimestamp());
		$icsUid = uniqid();
		$icsDateStamp = $this -> dateToCal(time());
		$icsLocation = $this -> escapeString($location);
		$icsDescription = $this -> escapeString($description);
		$icsUri = $this -> escapeString($uri);
		$icsSummary = $this -> escapeString($summary);
		$icsDateStart = $this -> dateToCal($dateStart -> getTimestamp());

		$returnValue = "BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//hacksw/handcal//NONSGML v1.0//EN
CALSCALE:GREGORIAN
BEGIN:VEVENT
DTEND:{$icsDateEnd}
UID:{$icsUid}
DTSTAMP:{$icsDateStamp}
LOCATION:{$icsLocation}
DESCRIPTION:{$icsDescription}
URL;VALUE=URI:{$icsUri}
SUMMARY:{$icsSummary}
DTSTART:{$icsDateStart}
END:VEVENT
END:VCALENDAR";

		return $returnValue;
	}

	// Converts a unix timestamp to an ics-friendly format
	// NOTE: "Z" means that this timestamp is a UTC timestamp. If you need
	// to set a locale, remove the "\Z" and modify DTEND, DTSTAMP and DTSTART
	// with TZID properties (see RFC 5545 section 3.3.5 for info)
	//
	// Also note that we are using "H" instead of "g" because iCalendar's Time format
	// requires 24-hour time (see RFC 5545 section 3.3.12 for info).
	function dateToCal($timestamp)
	{
		return date('Ymd\THis\Z', $timestamp);
	}

	// Escapes a string of characters
	function escapeString($string)
	{
		return preg_replace('/([\,;])/', '\\\$1', $string);
	}
}
