<?php
namespace App\Libraries;

class StringHelpers
{
	public static function StringBetween($string, $start, $end)
	{
		$string = " " . $string;

		$ini = strpos($string, $start);

		//Log::info('Log message', array(
		//    'string' => $string,
		//    'start' => $start,
		//    'end' => $end,
		//    'ini' => $ini
		//));

		if ($ini == 0)
		{
			return "";
		}

		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;

		return substr($string, $ini, $len);
	}

	public static function Value2Currency($value)
	{
		$returnValue = $value;

		if (is_numeric($value))
		{
			$returnValue = '€ ' . number_format($value, 2);

			$returnValue = str_replace(",", "#", $returnValue);
			$returnValue = str_replace(".", ",", $returnValue);
			$returnValue = str_replace("#", ".", $returnValue);
		}

		return $returnValue;
	}

	public static function countryDate($value, $country, $yearFirst = false)
	{
		$returnValue = $value;

		if ($value != '')
		{
			switch (strtoupper($country))
			{
				case 'NL' :
					if ($yearFirst)
					{
						$returnValue = date('Y-m-d', strtotime($value));
					}
					else
					{
						$returnValue = date('d-m-Y', strtotime($value));
					}

					break;

				case 'BE' :
					if ($yearFirst)
					{
						$returnValue = date('Y-m-d', strtotime($value));
					}
					else
					{
						$returnValue = date('d-m-Y', strtotime($value));
					}

					break;

				case 'DE' :
					if ($yearFirst)
					{
						$returnValue = date('Y.m.d', strtotime($value));
					}
					else
					{
						$returnValue = date('d.m.Y', strtotime($value));
					}

					break;

				default :
					if ($yearFirst)
					{
						$returnValue = date('Y-m-d', strtotime($value));
					}
					else
					{
						$returnValue = date('d-m-Y', strtotime($value));
					}

					break;
			}
		}

		return $returnValue;
	}

}
