<?php
namespace App\Libraries;

class DatabaseHelpers
{

    public static function getSqlQueryWhere($sqlQueryWhereArray)
    {
        $returnValue = "";

        if (count($sqlQueryWhereArray) > 0)
        {
            $sqlQueryWhere = " WHERE ";

            foreach ($sqlQueryWhereArray as $sqlQueryWhereStatement)
            {
                if ($sqlQueryWhereStatement != "" && substr($sqlQueryWhereStatement, strlen($sqlQueryWhereStatement) - 4) != " OR ")
                {
                    $sqlQueryWhere .= $sqlQueryWhereStatement . " AND ";
                }
                else
                {
                    $sqlQueryWhere .= $sqlQueryWhereStatement;
                }
            }

            $returnValue = rtrim($sqlQueryWhere, " AND ");
        }

        return $returnValue;
    }

    public static function getSqlQueryWhereMultiValue($Values, $ColumnName)
    {
        $returnValue = "";

        if (isset($Values) && count($Values) > 0)
        {
        	$Values = array_unique($Values);
			
            $returnValue .= "{$ColumnName} IN (";

            foreach ($Values as $key => $value)
            {
                $returnValue .= "'{$value}', ";
            }

            $returnValue .= ")";

            $returnValue = str_replace(", )", ")", $returnValue);
        }

        return $returnValue;
    }

    public static function getSqlQueryWhereFilter(&$sqlQueryWhereArray, $filter, $filterName, $filterColumnName)
    {
        $returnValue = false;

        if (isset($filter[$filterName]) && count($filter[$filterName]) > 0)
        {
            foreach ($filter[$filterName] as $key => $value)
            {
                $sqlQueryWhereArray[] = "{$filterColumnName} = '{$value}' OR ";
            }

            $sqlQueryWhereArray[count($sqlQueryWhereArray) - 1] = str_replace(" OR ", "", $sqlQueryWhereArray[count($sqlQueryWhereArray) - 1]);

            $returnValue = true;
        }

        return $returnValue;
    }

}
