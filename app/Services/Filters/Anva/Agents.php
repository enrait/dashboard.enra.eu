<?php

namespace App\Services\Filters\Anva;

use App\Libraries\CacheHelpers;
use App\Libraries\DatabaseHelpers;
use Illuminate\Support\Facades\App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class Agents
{
    public static function getProductionPlusMinusSummary(array $filter = [], $groupBranches = false)
    {
        $sqlQueryWhereArray = array();
        $countries = $filter['country'];

        if (!count($countries) > 0) {
            $countries = ['nl', 'be', 'de'];
        }
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($countries, 'country');

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        if ($groupBranches) {
            $sqlQuery = "SELECT *
						FROM EnraDashboard.agent_branche_grouped
						$sqlQueryWhere
						";
        } else {
            $sqlQuery = "SELECT *
			  FROM EnraDashboard.agent_branche AGENTBRANCHE
            $sqlQueryWhere
            ";
        }
        return collect(DB::connection('mysql')->select($sqlQuery));
    }
}
