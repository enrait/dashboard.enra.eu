<?php

namespace App\Services\Filters;

use App\Http\Controllers\AgentController;
use App\Libraries\CacheHelpers;
use App\Models\AnvaFACTUUR;
use App\Models\AnvaPOLBES;
use App\Models\AnvaPOLSCH;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class ProductData
{
    private $cachePrefix = 'ProductionController';
    private $roundDecimals = 0;
    private $filterData;

    public function __construct()
    {
        $this->filterData = new FilterData();
    }

    public function prepareProductionSummarySectionData(Request $request = null): array
    {
        $filterObj = new FilterData($request);
        $filter = $filterObj->initFilter();
        $selectData = FilterData::Request($request);

        $sectionId = 1;
        $sectionsData = [];

        // If there's more than 1 branch selected, get a summary of all branches
        if (count($selectData['branche']['selected']) > 1) {
            // Set description
            $sectionDescription = trans('enra/sales.total');
            // Get values for summary of all branches
            $sectionsData[$sectionId] = $this->getProductionSummarySectionData($request, $sectionId, $sectionDescription, $filter);
        }

        // For each branch ($selectValue = 02100, $selectDescription = 'E-Bikes')
        foreach ($selectData['branche']['data'] as $selectValue => $selectDescription) {
            // Check if this branch is selected
            if (in_array($selectValue, $selectData['branche']['selected'])) {
                // Store filter in other variable
                $sectionFilter = $filter;

                // Remove branch array from the array
                unset($sectionFilter['branch']);

                // Set branch array to only this branch
                $sectionFilter['branch'] = array($selectValue);

                $sectionId++;
                // Get values for this branch
                $sectionsData[$sectionId] = $this->getProductionSummarySectionData($request, $sectionId, $selectDescription, $sectionFilter);
            }
        }

        return [
            'sectionsData' => $sectionsData,
            'selectData' => $selectData
        ];
    }


    public function getProductionSummarySectionData($request, $sectionId, $sectionDescription, $sectionFilter)
    {
        $cacheKey = $this->cachePrefix . __FUNCTION__ . serialize($sectionFilter);
        $sectionData['id'] = $sectionId;
        $sectionData['description'] = $sectionDescription;

        return Cache::remember($cacheKey, now()->addDays(7), function () use ($sectionData, $sectionFilter, $request) {
            $sectionData['tableAgentRevenueAmount'] = $this->getTableAgentRevenueAmount($sectionFilter);
            $sectionData['tableAgentCoverageAmount'] = $this->getTableAgentCoverageAmount($sectionFilter);
            $sectionData['tableAgentDurationAmount'] = $this->getTableAgentDurationAmount($sectionFilter);
            $sectionData['tableAgentCommissionSum'] = $this->getTableAgentCommissionSum($sectionFilter);
            $sectionData['tableAgentDamageSum'] = $sectionFilter['showDamage'] ? $this->getTableAgentDamageSum($sectionFilter) : array();
            $sectionData['tableYearsCoverageDuration'] = $this->getTableYearsCoverageDuration($sectionData['tableAgentDurationAmount'], $sectionData['tableAgentCoverageAmount']);
            $sectionData['tableAgentDamageAmount'] = $sectionFilter['showDamage'] ? $this->getTableAgentDamageAmount($sectionFilter) : array();
            return $sectionData;
        });

        return $sectionData;
    }

    public function getTableAgentCoverageAmount($filter)
    {
        // Get the query
        $sqlQuery = AnvaPOLBES::getAgentAmountSqlQuery('Coverage', $filter);

        $cacheKey = CacheHelpers::getCacheKey('getTableAgentDurationAmount' . $sqlQuery . serialize($filter['branch']));

        //Debugbar::info((Cache::tags(['POLBES', 'WACBES', 'DEKKING_MAX', 'AGBES' ])-> has($cacheKey) ? 'True ' : 'False ') . $cacheKey);

        $tableAgentCoverageAmount = Cache::tags(['POLBES', 'WACBES', 'DEKKING_MAX', 'AGBES'])->remember($cacheKey, Config::get('cache.duration'), function () use ($sqlQuery, $filter) {
            // Get values by sql query
            $agentCoverageAmount = AnvaPOLBES::getAgentAmount($sqlQuery, $filter);

            // Set filter?
            $agentCoverageAmountFilter = $agentCoverageAmount['filter'];

            // Remove filter?
            unset($agentCoverageAmount['filter']);

            $tableAgentCoverageAmount = array(
                'rows' => array(),
                'columns' => array()
            );

            $coverages = $this->filterData->getCoverages();

            // Empty array
            $tableAgentCoverages = [];

            // foreach voor coverage filter?
            foreach ($filter['coverage'] as $key => $value) {
                if (isset($coverages[$value])) {
                    $tableAgentCoverageAmount['columns'][$value] = $coverages[$value];
                }
            }

            foreach ($agentCoverageAmount as $agentCoverageAmountRow) {
                if (in_array($agentCoverageAmountRow->POL_BRANCHE_SPEC, $filter['branch'])) {
                    $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR]['year'] = $agentCoverageAmountRow->POL_ING_JAAR;
                    $tableAgentCoverages[$agentCoverageAmountRow->POL_DEKKING] = 0;

                    if (isset($tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR][$agentCoverageAmountRow->POL_DEKKING])) {
                        $totalYearCoverage = $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR][$agentCoverageAmountRow->POL_DEKKING];
                    } else {
                        $totalYearCoverage = 0;
                    }

                    $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR][$agentCoverageAmountRow->POL_DEKKING] = $totalYearCoverage + $agentCoverageAmountRow->POL_TOTAAL;
                    $tableAgentCoverages[$agentCoverageAmountRow->POL_DEKKING] = $tableAgentCoverages[$agentCoverageAmountRow->POL_DEKKING] + $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR][$agentCoverageAmountRow->POL_DEKKING];

                    if (isset($tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR]['total'])) {
                        $totalYear = $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR]['total'];
                    } else {
                        $totalYear = 0;
                    }

                    $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR]['total'] = $totalYear + $agentCoverageAmountRow->POL_TOTAAL;

                    if (isset($tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR]['totalpremium'])) {
                        $totalYearPremium = $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR]['totalpremium'];
                    } else {
                        $totalYearPremium = 0;
                    }

                    $tableAgentCoverageAmount['rows'][$agentCoverageAmountRow->POL_ING_JAAR]['totalpremium'] = $totalYearPremium + $agentCoverageAmountRow->PREMIE_TOTAAL;
                }
            }

            foreach ($tableAgentCoverageAmount['rows'] as $tableAgentCoverageAmountRow) {
                $totalDifferentPercentageLastYear = 0;

                if (isset($tableAgentCoverageAmount['rows'][(int)$tableAgentCoverageAmountRow['year'] - 1])) {
                    $totalDifferentPercentageLastYear = round(((((float)$tableAgentCoverageAmountRow['total'] - (float)$tableAgentCoverageAmount['rows'][(int)$tableAgentCoverageAmountRow['year'] - 1]['total']) / (float)$tableAgentCoverageAmount['rows'][(int)$tableAgentCoverageAmountRow['year'] - 1]['total']) * 100), $this->roundDecimals);
                }

                $tableAgentCoverageAmount['rows'][$tableAgentCoverageAmountRow['year']]['diff'] = $totalDifferentPercentageLastYear;
            }

            foreach ($tableAgentCoverageAmount['columns'] as $tableColumnCoverageKey => $tableColumnCoverageValue) {
                if (isset($tableAgentCoverages[$tableColumnCoverageKey]) && $tableAgentCoverages[$tableColumnCoverageKey] > 0) {

                } else {
                    unset($tableAgentCoverageAmount['columns'][$tableColumnCoverageKey]);
                }
            }

            ksort($tableAgentCoverageAmount['columns']);

            return $tableAgentCoverageAmount;
        });

        return $tableAgentCoverageAmount;
    }

    public function getTableAgentRevenueAmount($filter)
    {
        $sqlQuery = AnvaPOLBES::getAgentAmountSqlQuery('Month', $filter);

        $cacheKey = CacheHelpers::getCacheKey('getTableAgentRevenueAmount' . $sqlQuery . serialize($filter['branch']));

        $tableAgentRevenueAmount = Cache::tags(['POLBES', 'WACBES', 'DEKKING_MAX', 'AGBES'])
            ->remember($cacheKey, Config::get('cache.duration'), function () use ($sqlQuery, $filter) {
                $agentRevenueAmount = AnvaPOLBES::getAgentAmount($sqlQuery, $filter);

                unset($agentRevenueAmount['filter']);

                $tableAgentRevenueAmount = array(
                    'rows' => array(),
                    'columns' => array()
                );

                $months = $this->filterData->getMonths();

                foreach ($filter['month'] as $key => $value) {
                    if (isset($months[$value])) {
                        $tableAgentRevenueAmount['columns'][$value] = $months[$value];
                    }
                }

                foreach ($agentRevenueAmount as $agentRevenueAmountRow) {
                    if (in_array($agentRevenueAmountRow->POL_BRANCHE_SPEC, $filter['branch'])) {
                        $tableAgentRevenueAmount['rows'][$agentRevenueAmountRow->POL_ING_JAAR]['year'] = $agentRevenueAmountRow->POL_ING_JAAR;

                        if (isset($tableAgentRevenueAmount['rows'][$agentRevenueAmountRow->POL_ING_JAAR][$agentRevenueAmountRow->POL_ING_MAAND])) {
                            $totalYearMonth = $tableAgentRevenueAmount['rows'][$agentRevenueAmountRow->POL_ING_JAAR][$agentRevenueAmountRow->POL_ING_MAAND];
                        } else {
                            $totalYearMonth = 0;
                        }

                        $tableAgentRevenueAmount['rows'][$agentRevenueAmountRow->POL_ING_JAAR][$agentRevenueAmountRow->POL_ING_MAAND] = $totalYearMonth + $agentRevenueAmountRow->POL_TOTAAL;

                        if (isset($tableAgentRevenueAmount['rows'][$agentRevenueAmountRow->POL_ING_JAAR]['total'])) {
                            $totalYear = $tableAgentRevenueAmount['rows'][$agentRevenueAmountRow->POL_ING_JAAR]['total'];
                        } else {
                            $totalYear = 0;
                        }

                        $tableAgentRevenueAmount['rows'][$agentRevenueAmountRow->POL_ING_JAAR]['total'] = $totalYear + $agentRevenueAmountRow->POL_TOTAAL;
                    }
                }

                foreach ($tableAgentRevenueAmount['rows'] as $tableAgentRevenueAmountRow) {
                    $totalDifferentPercentageLastYear = 0;

                    if (isset($tableAgentRevenueAmount['rows'][(int)$tableAgentRevenueAmountRow['year'] - 1])) {
                        $totalDifferentPercentageLastYear = round(((((float)$tableAgentRevenueAmountRow['total'] - (float)$tableAgentRevenueAmount['rows'][(int)$tableAgentRevenueAmountRow['year'] - 1]['total']) / (float)$tableAgentRevenueAmount['rows'][(int)$tableAgentRevenueAmountRow['year'] - 1]['total']) * 100), $this->roundDecimals);
                    }

                    $tableAgentRevenueAmount['rows'][$tableAgentRevenueAmountRow['year']]['diff'] = $totalDifferentPercentageLastYear;
                }

                //ksort($tableAgentRevenueAmount['columns']);

                return $tableAgentRevenueAmount;
            });

        return $tableAgentRevenueAmount;
    }

    public function getTableAgentDurationAmount($filter)
    {
        $sqlQuery = AnvaPOLBES::getAgentAmountSqlQuery('Duration', $filter);

        $cacheKey = CacheHelpers::getCacheKey('getTableAgentDurationAmount' . $sqlQuery . serialize($filter['branch']));

        $tableAgentDurationAmount = Cache::tags(['POLBES', 'WACBES', 'DEKKING_MAX', 'AGBES'])->remember($cacheKey, Config::get('cache.duration'), function () use ($sqlQuery, $filter) {
            $agentDurationAmount = AnvaPOLBES::getAgentAmount($sqlQuery, $filter);

            $agentDurationAmountFilter = $agentDurationAmount['filter'];

            unset($agentDurationAmount['filter']);

            $tableAgentDurationAmount = array(
                'rows' => array(),
                'columns' => array()
            );

            $durations = $this->filterData->getDurations();
            $tableAgentDurations = array();

            foreach ($filter['duration'] as $key => $value) {
                if (isset($durations[$value])) {
                    $tableAgentDurationAmount['columns'][$value] = $durations[$value];
                }
            }

            foreach ($agentDurationAmount as $agentDurationAmountRow) {
                if (in_array($agentDurationAmountRow->POL_BRANCHE_SPEC, $filter['branch'])) {
                    $tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR]['year'] = $agentDurationAmountRow->POL_ING_JAAR;
                    $tableAgentDurations[$agentDurationAmountRow->POL_LOOPTIJD] = 0;

                    if (isset($tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR][$agentDurationAmountRow->POL_LOOPTIJD])) {
                        $totalYearDuration = $tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR][$agentDurationAmountRow->POL_LOOPTIJD];
                    } else {
                        $totalYearDuration = 0;
                    }

                    $tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR][$agentDurationAmountRow->POL_LOOPTIJD] = $totalYearDuration + $agentDurationAmountRow->POL_TOTAAL;
                    $tableAgentDurations[$agentDurationAmountRow->POL_LOOPTIJD] = $tableAgentDurations[$agentDurationAmountRow->POL_LOOPTIJD] + $tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR][$agentDurationAmountRow->POL_LOOPTIJD];

                    if (isset($tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR]['total'])) {
                        $totalYear = $tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR]['total'];
                    } else {
                        $totalYear = 0;
                    }

                    $tableAgentDurationAmount['rows'][$agentDurationAmountRow->POL_ING_JAAR]['total'] = $totalYear + $agentDurationAmountRow->POL_TOTAAL;
                }
            }

            foreach ($tableAgentDurationAmount['rows'] as $tableAgentDurationAmountRow) {
                $totalDifferentPercentageLastYear = 0;

                if (isset($tableAgentDurationAmount['rows'][(int)$tableAgentDurationAmountRow['year'] - 1])) {
                    $totalDifferentPercentageLastYear = round(((((float)$tableAgentDurationAmountRow['total'] - (float)$tableAgentDurationAmount['rows'][(int)$tableAgentDurationAmountRow['year'] - 1]['total']) / (float)$tableAgentDurationAmount['rows'][(int)$tableAgentDurationAmountRow['year'] - 1]['total']) * 100), $this->roundDecimals);
                }

                $tableAgentDurationAmount['rows'][$tableAgentDurationAmountRow['year']]['diff'] = $totalDifferentPercentageLastYear;
            }

            foreach ($tableAgentDurationAmount['columns'] as $tableColumnDurationKey => $tableColumnDurationValue) {
                if (isset($tableAgentDurations[$tableColumnDurationKey]) && $tableAgentDurations[$tableColumnDurationKey] > 0) {

                } else {
                    unset($tableAgentDurationAmount['columns'][$tableColumnDurationKey]);
                }
            }

            ksort($tableAgentDurationAmount['columns']);

            return $tableAgentDurationAmount;
        });

        return $tableAgentDurationAmount;
    }

    public function getTableAgentCommissionSum($filter)
    {
        $agentCommissionSum = AnvaFACTUUR::getAgentCommissionSum($filter);

        $agentCommissionSumFilter = $agentCommissionSum['filter'];

        unset($agentCommissionSum['filter']);

        $tableAgentCommissionSum = array(
            'rows' => array(),
            'columns' => array()
        );

        $tableAgentCommissionSum['columns'] = array('0' => trans('enra/sales.commission') . ' ' . trans('enra/sales.euro_sign'));

        foreach ($agentCommissionSum as $agentCommissionSumRow) {
            if (in_array($agentCommissionSumRow->BRANCHE, $filter['branch'])) {
                $tableAgentCommissionSum['rows'][$agentCommissionSumRow->JAAR]['year'] = $agentCommissionSumRow->JAAR;

                if (isset($tableAgentCommissionSum['rows'][$agentCommissionSumRow->JAAR]['commission'])) {
                    $totalYearCommission = $tableAgentCommissionSum['rows'][$agentCommissionSumRow->JAAR]['commission'];
                } else {
                    $totalYearCommission = 0;
                }

                $tableAgentCommissionSum['rows'][$agentCommissionSumRow->JAAR]['commission'] = $totalYearCommission + $agentCommissionSumRow->PROVISIE;

                if (isset($tableAgentCommissionSum['rows'][$agentCommissionSumRow->JAAR]['total'])) {
                    $totalYear = $tableAgentCommissionSum['rows'][$agentCommissionSumRow->JAAR]['total'];
                } else {
                    $totalYear = 0;
                }

                $tableAgentCommissionSum['rows'][$agentCommissionSumRow->JAAR]['total'] = $totalYear + $agentCommissionSumRow->PROVISIE;
            }
        }

        foreach ($tableAgentCommissionSum['rows'] as $tableAgentCommissionSumRow) {
            $totalDifferentPercentageLastYear = 0;

            $tableAgentCommissionSum['rows'][$tableAgentCommissionSumRow['year']]['diff'] = $totalDifferentPercentageLastYear;
        }

        ksort($tableAgentCommissionSum['columns']);

        return $tableAgentCommissionSum;
    }

    public function getTableAgentDamageSum($filter)
    {
        $agentDamageSum = AnvaPOLSCH::getAgentDamageSum($filter);

        $agentDamageSumFilter = $agentDamageSum['filter'];

        unset($agentDamageSum['filter']);

        $tableAgentDamageSum = array(
            'rows' => array(),
            'columns' => array()
        );

        if ($filter['agent'] != []) {
            $tableAgentDamageSum['columns'] = array(
                '0' => trans('enra/sales.premie_total'),
                '1' => trans('enra/sales.damage_total'),
                '2' => trans('enra/sales.damage_rating')
            );
        }

        foreach ($agentDamageSum as $agentDamageSumRow) {
            if (in_array($agentDamageSumRow->PLS_BRANCHE_SPEC, $filter['branch'])) {
                $tableAgentDamageSum['rows'][$agentDamageSumRow->JAAR]['year'] = $agentDamageSumRow->JAAR;

                if (isset($tableAgentDamageSum['rows'][$agentDamageSumRow->JAAR]['Damage'])) {
                    $totalYearDamage = $tableAgentDamageSum['rows'][$agentDamageSumRow->JAAR]['Damage'];
                } else {
                    $totalYearDamage = 0;
                }

                $tableAgentDamageSum['rows'][$agentDamageSumRow->JAAR]['Damage'] = $totalYearDamage + $agentDamageSumRow->SCHADEBEDRAG;

                if (isset($tableAgentDamageSum['rows'][$agentDamageSumRow->JAAR]['total'])) {
                    $totalYear = $tableAgentDamageSum['rows'][$agentDamageSumRow->JAAR]['total'];
                } else {
                    $totalYear = 0;
                }

                $tableAgentDamageSum['rows'][$agentDamageSumRow->JAAR]['total'] = $totalYear + $agentDamageSumRow->SCHADEBEDRAG;
            }
        }

        foreach ($tableAgentDamageSum['rows'] as $tableAgentDamageSumRow) {
            $totalDifferentPercentageLastYear = 0;

            if (isset($tableAgentDamageSum['rows'][(int)$tableAgentDamageSumRow['year'] - 1])) {
                if ((float)$tableAgentDamageSum['rows'][(int)$tableAgentDamageSumRow['year'] - 1]['total'] != 0) {
                    $totalDifferentPercentageLastYear = round(((((float)$tableAgentDamageSumRow['total'] - (float)$tableAgentDamageSum['rows'][(int)$tableAgentDamageSumRow['year'] - 1]['total']) / (float)$tableAgentDamageSum['rows'][(int)$tableAgentDamageSumRow['year'] - 1]['total']) * 100), $this->roundDecimals);
                }
            }

            $tableAgentDamageSum['rows'][$tableAgentDamageSumRow['year']]['diff'] = $totalDifferentPercentageLastYear;
        }

        ksort($tableAgentDamageSum['columns']);

        if ($filter['agent'] != []) {
            $filter['country'] = Config::get('app.locale');
            $apiData = $this->getSchadequoteFromAPI($filter);
            ksort($apiData);
            $tableAgentDamageSum['apiData'] = $apiData;
        }

        return $tableAgentDamageSum;
    }

    private function getSchadequoteFromAPI($filter)
    {
        $curl = curl_init();
        /*
        * Testing and local key
        */
        // $key = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNjMzhlOWY2MWZkOWVhODgyYmZmMGYwMThkZmRmOGUxMzg4Yzg3M2VmMjZhMGYxMzkyZjU5NzZjNDEwZTZiNTA2NDljZTExNDZmM2NhMWYzIn0.eyJhdWQiOiI0IiwianRpIjoiM2MzOGU5ZjYxZmQ5ZWE4ODJiZmYwZjAxOGRmZGY4ZTEzODhjODczZWYyNmEwZjEzOTJmNTk3NmM0MTBlNmI1MDY0OWNlMTE0NmYzY2ExZjMiLCJpYXQiOjE1Njc3NjEzMzIsIm5iZiI6MTU2Nzc2MTMzMiwiZXhwIjoxNTk5MzgzNzMyLCJzdWIiOiIxIiwic2NvcGVzIjpbInVzZS16aXBjb2RlLWFwaSJdfQ.rg2YKbLz8NhEaLLIVVYj-LMn5BnsPqcfJSxFEVzcvu0PYssNd4DNFQdAwTYCi7CTYk69-C63lEzVHBGrOcxLZkBWG-b2nT3xTY0lRM-8lEChCBdJVJioLvqqUH1dt0ljArem-nQ0qyJSQ6Kss7eVqXdZ8ScQUyfEeCJ9_bTyB87Iht0qKd1R4tIAK93xirOsrXiqxg1SBIjj-Rw0ctJanR2TBRSD5QFlQ7Znb_BGnZEOcf2s9jicAeEYXOeFKH-qljFYQOmCz7hQMOyNIBx3bp7mZM0FDBvDt9HJe8s70wd_cZQOLXrsoGNJ2JtnL9fYzG8GvlVMgpFR_L7-FE49RtjE-bO4oi_i2bcZQQVSqdOgxODAqUAT4vEozRJm470NO1uS4TBQCYhtnvT-C2wwcWJVC5OF6IvVm7bTV16Wm7GHdObd_JAYjveORK_Vpn0Fp1OcZVyOyk8awGlj4q-CfpxzDHKQVgrIKGG7G9UlKxpV-SLLlgqFTSxQ-E8CIVp24blQbkZlN2Pjmndp0KyDwZ4RKRN0i8erILUDEsLwI0xWii_85FlMHP3HN0yrfYHlZcxQRUYP72MERNq4GrRYw02fbO-xyGbCAvTW5waP96Y06yFGCLBmHAvgHGO6kNcbLW5WjtLAhkFwsaRapPfsq0ahlV1PSuujC2L43HYxfUs';

        $key = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjkzOWEzZjNiZDljM2VlOWQ4NmUyZWVmNzNhNTY4MGU3YThjODU2MjU4NDM5ZWNiZTViNjhhOTk5MTU3NGQ4MWM3ZjkyZDljYWMzMmViYzI2In0.eyJhdWQiOiI0IiwianRpIjoiOTM5YTNmM2JkOWMzZWU5ZDg2ZTJlZWY3M2E1NjgwZTdhOGM4NTYyNTg0MzllY2JlNWI2OGE5OTkxNTc0ZDgxYzdmOTJkOWNhYzMyZWJjMjYiLCJpYXQiOjE2MDI0OTI0MzEsIm5iZiI6MTYwMjQ5MjQzMSwiZXhwIjoxOTE4MDI1MjMwLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.h-vnGuXdBHJy76LyIL3oQ0Bsdvlb9JBDMnjgYPpThVH8LHiezvaQaOJRQWS_0qZf6YjJA1U_QiEGZxC8i_fP26DTnFs1d1a4Udyyo6SRjhnK9qalC-IICFQit7uLFwrB4BuFXXB9VaqCAkDcqxBnvhEZJrf0tFjvIsG2a9-mAOs4UOFCrxFI65YMxhpOQgTD4hH08dPP9oZYTKu-GL1NUzwTYE8Aoau7p6Z2QLSv8sk9jOqEXft-H1FGePH-Ha01xl50txUQkSfgMkyyj8wa41tZv6QqPW63Wmw-Isxb7GNjcxaVm_08cTljkrRLDFUZf7VTI7KbcAiVvVZ_SeoCmNwcCpPAEfVEFEGTpzbrT4Gzip6X0vuENFAVrk9ru2RhtLjW1PG4wuNwJiyhKSy0EB1WaUZL4X-wYYgxGRZxu3RS6_qr_vzRiper_3tfPzM4JF75h7EzennUg3kB1OrCsmptd2TfEF_XJq9-fEoxapAaSh9K0eDckskS8mGIIeCOhRm2AjoG-rD9_uU-8QNpqMXjWS2TA-IHjLWnczuPlvaqkFScrY_7kpBVIXHWaurmqCNyPT6jcUYdRnJLZQNbV7771A2tYPeJCfV5VVGsNmctKA5MPs0nV4Pgg3UFmPNVKVUg405zarTfKEeSupj-TpjOopFmM3JjHUvnL3K_IfI';
        $url = 'http://api.enra.eu/api/dashboard/oldDashboardGetSchadequote?';
        $url_params = http_build_query($filter);
        $full_url = $url . $url_params;

        // Set request
        curl_setopt_array($curl, array(
            CURLOPT_URL => $full_url,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: " . $key
            )
        ));

        $response = curl_exec($curl);

        // We need better error handling in this function
        $err = curl_error($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $json = json_decode($response, true);
        return $json;
    }

    public function getTableYearsCoverageDuration($tableAgentDurationAmount, $tableAgentCoverageAmount)
    {
        $tableYearsCoverageDuration = array();

        foreach ($tableAgentDurationAmount['rows'] as $tableAgentDurationAmountRow) {
            $tableYearsCoverageDuration[$tableAgentDurationAmountRow['year']] = $tableAgentDurationAmountRow['year'];
        }

        foreach ($tableAgentCoverageAmount['rows'] as $tableAgentCoverageAmountRow) {
            $tableYearsCoverageDuration[$tableAgentCoverageAmountRow['year']] = $tableAgentCoverageAmountRow['year'];
        }

        ksort($tableYearsCoverageDuration);
        $tableYearsCoverageDuration = array_reverse($tableYearsCoverageDuration);

        return $tableYearsCoverageDuration;

    }

    public function getTableAgentDamageAmount($filter)
    {
        $agentDamageAmount = AnvaPOLSCH::getAgentDamageSum($filter);

        $agentDamageAmountFilter = $agentDamageAmount['filter'];

        unset($agentDamageAmount['filter']);

        $tableAgentDamageAmount = array(
            'rows' => array(),
            'columns' => array()
        );


        $damagetypes = $this->filterData->getDamageTypes();

        $tableAgentDamageTypes = array();

        if (count($filter['damagetype']) >= 1) {
            foreach ($filter['damagetype'] as $key => $value) {
                if (isset($damagetypes[$value])) {
                    $tableAgentDamageAmount['columns'][$value] = $damagetypes[$value];
                }
            }
        } else {
            foreach ($damagetypes as $key => $value) {
                $tableAgentDamageAmount['columns'][$key] = $damagetypes[$key];
            }
        }

        foreach ($agentDamageAmount as $agentDamageAmountRow) {
            if (in_array($agentDamageAmountRow->PLS_BRANCHE_SPEC, $filter['branch'])) {

                if ($agentDamageAmountRow->SCHADESOORT != '00311' && $agentDamageAmountRow->SCHADESOORT != '00313' && !in_array('de', $filter['country'])) {
                    $agentDamageAmountRow->SCHADESOORT = '00950';
                }


                $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR]['year'] = $agentDamageAmountRow->JAAR;
                $tableAgentDamageTypes[$agentDamageAmountRow->SCHADESOORT] = 0;

                if (isset($tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR][$agentDamageAmountRow->SCHADESOORT]['amount'])) {
                    $totalYearDuration = $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR][$agentDamageAmountRow->SCHADESOORT]['amount'];
                    $totalYearNumberOf = $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR][$agentDamageAmountRow->SCHADESOORT]['numberof'];
                } else {
                    $totalYearDuration = 0;
                    $totalYearNumberOf = 0;
                }

                $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR][$agentDamageAmountRow->SCHADESOORT]['amount'] = $totalYearDuration + $agentDamageAmountRow->SCHADEBEDRAG;
                $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR][$agentDamageAmountRow->SCHADESOORT]['numberof'] = $totalYearNumberOf + $agentDamageAmountRow->TOTAAL;

                $tableAgentDamageTypes[$agentDamageAmountRow->SCHADESOORT] = $tableAgentDamageTypes[$agentDamageAmountRow->SCHADESOORT]['amount'] + $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR][$agentDamageAmountRow->SCHADESOORT]['amount'];

                if (isset($tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR]['total'])) {
                    $totalYear = $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR]['total'];
                } else {
                    $totalYear = 0;
                }

                $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR]['total'] = $totalYear + $agentDamageAmountRow->SCHADEBEDRAG;


                if (isset($tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR]['totalnumberof'])) {
                    $totalYearTotalNumberOf = $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR]['totalnumberof'];
                } else {
                    $totalYearTotalNumberOf = 0;
                }

                $tableAgentDamageAmount['rows'][$agentDamageAmountRow->JAAR]['totalnumberof'] = $totalYearTotalNumberOf + $agentDamageAmountRow->TOTAAL;
            }
        }

        foreach ($tableAgentDamageAmount['rows'] as $tableAgentDamageAmountRow) {
            $totalDifferentPercentageLastYear = 0;
            $totalDifferentNumberOfPercentageLastYear = 0;

            if (isset($tableAgentDamageAmount['rows'][(int)$tableAgentDamageAmountRow['year'] - 1])) {
                if ((float)$tableAgentDamageAmount['rows'][(int)$tableAgentDamageAmountRow['year'] - 1]['total'] != 0) {
                    $totalDifferentPercentageLastYear = round(((((float)$tableAgentDamageAmountRow['total'] - (float)$tableAgentDamageAmount['rows'][(int)$tableAgentDamageAmountRow['year'] - 1]['total']) / (float)$tableAgentDamageAmount['rows'][(int)$tableAgentDamageAmountRow['year'] - 1]['total']) * 100), $this->roundDecimals);
                    $totalDifferentNumberOfPercentageLastYear = round(((((float)$tableAgentDamageAmountRow['totalnumberof'] - (float)$tableAgentDamageAmount['rows'][(int)$tableAgentDamageAmountRow['year'] - 1]['totalnumberof']) / (float)$tableAgentDamageAmount['rows'][(int)$tableAgentDamageAmountRow['year'] - 1]['totalnumberof']) * 100), $this->roundDecimals);
                }
            }

            $tableAgentDamageAmount['rows'][$tableAgentDamageAmountRow['year']]['diff'] = $totalDifferentPercentageLastYear;
            $tableAgentDamageAmount['rows'][$tableAgentDamageAmountRow['year']]['diffnumberof'] = $totalDifferentNumberOfPercentageLastYear;
        }

        foreach ($tableAgentDamageAmount['columns'] as $tableColumnDamageKey => $tableColumnDamageValue) {
            if (isset($tableAgentDamageTypes[$tableColumnDamageKey]) && $tableAgentDamageTypes[$tableColumnDamageKey] <> 0) {

            } else {
                unset($tableAgentDamageAmount['columns'][$tableColumnDamageKey]);
            }
        }

        // print_r($tableAgentDamageAmount['columns']);
        // die();

        ksort($tableAgentDamageAmount['columns']);

        return $tableAgentDamageAmount;
    }

    public function prepareProductionSummaryPdf(Request $request)
    {
        $selectData = FilterData::Request($request);
        $sectionId = 1;

        if (count($selectData['branche']['selected']) > 1) {
            $sectionDescription = trans('enra/sales.total');
            $sectionsData[$sectionId] = $this->getProductionSummarySectionData($request, $sectionId, $sectionDescription, $selectData);
        }

        foreach ($selectData['branche']['data'] as $selectValue => $selectDescription) {
            if (in_array($selectValue, $selectData['branche']['selected'])) {
                $sectionFilter = $this->filterData;

                unset($sectionFilter['branch']);

                $sectionFilter['branch'] = array($selectValue);

                $sectionId++;

                $sectionsData[$sectionId] = $this->getProductionSummarySectionData($request, $sectionId, $selectDescription, $sectionFilter);
            }
        }

        return [
            'selectData' => $selectData,
            'sectionsData' => $sectionsData,
        ];
    }

}
