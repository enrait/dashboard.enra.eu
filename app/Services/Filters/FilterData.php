<?php

namespace App\Services\Filters;

use App\Libraries\CacheHelpers;
use App\Libraries\DatabaseHelpers;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;


class FilterData
{
    private $cachePrefix = 'FilterSelect';
    private $request;
    private $selectedCountries;


    public function __construct(Request $request = null)
    {
        $this->request = $request;
    }

    public static function Request($request): array
    {
        $filterObject = new FilterData($request);
        $filter = $filterObject->initFilter();
        return $filterObject->getSelectData($filter);
    }

    public function initFilter()
    {
        return $this->initFilterData();
    }

    public function setSelectedCountries(array $selectedCountries): void
    {
        $this->selectedCountries = $selectedCountries;
    }

    public function initFilterData()
    {
        $filter = [];
        $filter['cache'] = true;
        $filterYear = $this->getLastYears();
        $filterMonth = $this->getLastMonths();
        $filterCountry = $this->getCountryAccess();

        if ($this->selectedCountries) {
            $filterCountry = $this->selectedCountries;
        }

        $filterRegion = array_keys($this->getRegions());
        $filterCompany = ['P200', 'B201', 'B202', 'B203'];
        $filterBranch = array_keys($this->getBranches());
        $filterObjectType = array_keys($this->getObjectTypes());
        $filterOrganisation = array_keys($this->getOrganisations());
        $filterCoverage = array_keys($this->getCoverages());
        $filterDuration = ['012', '036', '060'];

        if ($this->request) {
            $filter['cache'] = (bool)$this->request->get('cache', true);
            $filter['year'] = $this->request->get('year') ?? $this->request->session()->get('filterYear', $filterYear);
            $filter['month'] = $this->request->get('month') ?? $this->request->session()->get('filterMonth', $filterMonth);
            $filter['country'] = $this->request->get('country') ?? $this->request->session()->get('filterCountry', $filterCountry);
            $filter['region'] = $this->request->get('region') ?? $this->request->session()->get('filterRegion', $filterRegion);
            $filter['company'] = $this->request->get('company') ?? $this->request->session()->get('filterCompany', $filterCompany);
            $filter['branch'] = $this->request->get('branch') ?? $this->request->session()->get('filterBranch', $filterBranch);
            $filter['objecttype'] = $this->request->get('objecttype') ?? $this->request->session()->get('filterObjecttype', $filterObjectType);
            $filter['organisation'] = $this->request->get('organisation') ?? $this->request->session()->get('filterOrganisation', $filterOrganisation);
            $filter['agent'] = $this->request->get('agent', []);
            $filter['coverage'] = $this->request->get('coverage') ?? $this->request->session()->get('filterCoverage', $filterCoverage);
            $filter['duration'] = $this->request->get('duration') ?? $this->request->session()->get('filterDuration', $filterDuration);
            $filter['damagetype'] = $this->request->get('damagetype') ?? $this->request->session()->get('filterDamagetype', []);
            $filter['classification'] = $this->request->get('classification') ?? $this->request->session()->get('filterClassification', []);
            $filter['potential'] = $this->request->get('potential') ?? $this->request->session()->get('filterPotential', []);
            $filter['weightingfactor'] = $this->request->get('weightingfactor') ?? $this->request->session()->get('filterWeightingfactor', []);
            $filter['showDamage'] = (bool)$this->request->get('showDamage');
            $filter['showPDF'] = (bool)$this->request->get('showPDF');
            $filter['client'] = $this->request->get('client') ?? $this->request->session()->get('filterClient', []);
            $filter['clientstatus'] = $this->request->get('clientstatus') ?? $this->request->session()->get('filterClientstatus', []);
            $filter['contactmomenttype'] = [];

            Session(['filterCompany' => $filter['company']]);
            Session(['filterYear' => $filter['year']]);
            Session(['filterMonth' => $filter['month']]);
            Session(['filterCountry' => $filter['country']]);
            Session(['filterCompany' => $filter['company']]);
            Session(['filterRegion' => $filter['region']]);
            Session(['filterBranch' => $filter['branch']]);
            Session(['filterObjecttype' => $filter['objecttype']]);
            Session(['filterOrganisation' => $filter['organisation']]);
            Session(['filterAgent' => $filter['agent']]);
            Session(['filterCoverage' => $filter['coverage']]);
            Session(['filterDuration' => $filter['duration']]);
            Session(['filterDamagetype' => $filter['damagetype']]);
            Session(['filterClassification' => $filter['classification']]);
            Session(['filterPotential' => $filter['potential']]);
            Session(['filterWeightingfactor' => $filter['weightingfactor']]);
//            Session(['filterShowDamage' => $filter['showDamage']]);
//            Session(['filterShowPDF' => $filter['showPDF']]);
            Session(['filterClient' => $filter['client']]);
            Session(['filterClientstatus' => $filter['clientstatus']]);
//            Session(['filterContactmomenttype' => $filter['contactmomenttype']]);

        } else {
            $filter['year'] = $filterYear;
            $filter['month'] = $filterMonth;
            $filter['country'] = $filterCountry;
            $filter['region'] = $filterRegion;
            $filter['company'] = $filterCompany;
            $filter['branch'] = $filterBranch;
            $filter['objecttype'] = $filterObjectType;
            $filter['organisation'] = $filterOrganisation;
            $filter['agent'] = [];
            $filter['coverage'] = $filterCoverage;
            $filter['duration'] = $filterDuration;
            $filter['damagetype'] = [];
            $filter['classification'] = [];
            $filter['potential'] = [];
            $filter['weightingfactor'] = [];
            $filter['showDamage'] = false;
            $filter['showPDF'] = false;
            $filter['client'] = [];
            $filter['clientstatus'] = [];
            $filter['contactmomenttype'] = [];
        }

        if (in_array('be', $filter['country'])) {
            $filter['country'][] = 'nl';
        }
        $filter['country'] = array_unique($filter['country']);
        $filter['agent'] = in_array('all', $filter['agent']) ? array() : $filter['agent'];
        $filter['coverage'] = array_intersect($filter['coverage'], array_keys($this->getCoverages()));

        return $filter;
    }

    public function getLastYears(): array
    {
        if (date('m') == 1) {
            $returnValue = array(
                date('Y') - 1,
                date('Y') - 2,
                date('Y') - 3
            );
        } else {
            $returnValue = array(
                date('Y'),
                date('Y') - 1,
                date('Y') - 2
            );
        }
        return $returnValue;
    }

    public function getLastMonths(): array
    {
        $returnValue = [];
        $monthNow = 12;

        for ($i = 1; $i <= $monthNow; $i++) {
            $month = '0' . $i;
            $returnValue[] = substr($month, strlen($month) - 2);
        }

        return $returnValue;
    }

    public function getCountryAccess(): array
    {
        $returnValue = [];

        if (Sentinel::check()) {
            if (Sentinel::hasAccess(['country.NL'])) {
                $returnValue[] = 'nl';
            }

            if (Sentinel::hasAccess(['country.DE'])) {
                $returnValue[] = 'de';
            }

            if (Sentinel::hasAccess(['country.BE'])) {
                $returnValue[] = 'be';
            }
        } else {
            $returnValue[] = 'nl';
            $returnValue[] = 'de';
            $returnValue[] = 'be';
        }

        return $returnValue;
    }

    public function getSelectData($filter)
    {
        $selectData = array(
            'cache' => array(
                'selected' => $filter['cache'],
                'data' => $this->getCache()
            ),
            'year' => array(
                'selected' => $filter['year'] ?? false,
                'data' => $this->getYears() ?? [],
            ),
            'month' => array(
                'selected' => $filter['month'] ?? false,
                'data' => $this->getMonths() ?? [],
            ),
            'country' => array(
                'selected' => $filter['country'] ?? false,
                'data' => $this->getCountries() ?? [],
            ),
            'region' => array(
                'selected' => $filter['region'] ?? false,
                'data' => $this->getRegions() ?? [],
            ),
            'company' => array(
                'selected' => $filter['company'] ?? false,
                'data' => $this->getCompanies() ?? [],
            ),
            'branche' => array(
                'selected' => $filter['branch'] ?? false,
                'data' => $this->getBranches() ?? [],
            ),
            'objecttype' => array(
                'selected' => $filter['objecttype'] ?? false,
                'data' => $this->getObjectTypes() ?? [],
            ),
            'organisation' => array(
                'selected' => $filter['organisation'] ?? false,
                'data' => $this->getOrganisations() ?? [],
            ),
            'agent' => array(
                'selected' => $filter['agent'] ?? false,
                'data' => $this->getAgents() ?? [],
            ),
            'coverage' => array(
                'selected' => $filter['coverage'] ?? false,
                'data' => $this->getCoverages() ?? [],
            ),
            'duration' => array(
                'selected' => $filter['duration'] ?? false,
                'data' => $this->getDurations() ?? [],
            ),
            'damagetype' => array(
                'selected' => $filter['damagetype'] ?? false,
                'data' => $this->getDamageTypes() ?? [],
            ),
            'classification' => array(
                'selected' => $filter['classification'] ?? false,
                'data' => $this->getClassifications() ?? [],
            ),
            'potential' => array(
                'selected' => $filter['potential'] ?? false,
                'data' => $this->getPotentials() ?? [],
            ),
            'weightingfactor' => array(
                'selected' => $filter['weightingfactor'] ?? false,
                'data' => $this->getWeightingFactors() ?? [],
            ),
            'showDamage' => $filter['showDamage'],
            'showPDF' => $filter['showPDF'],
            'client' => array(
                'selected' => $filter['client'] ?? false,
                'data' => $this->getClients() ?? [],
            ),
            'clientstatus' => array(
                'selected' => $filter['clientstatus'] ?? false,
                'data' => $this->getClientStatus() ?? [],
            ),
            'contactmomenttype' => array(
                'selected' => $filter['contactmomenttype'] ?? false,
                'data' => $this->getContactMomentType() ?? [],
            ),
            'urlQueryString' => ''
        );

        foreach ($selectData as $selectDataItemKey => $selectDataItemValue) {
            if (isset($selectDataItemValue['selected'])) {
                if (is_array($selectDataItemValue['selected'])) {
                    foreach ($selectDataItemValue['selected'] as $selectDataItemSelectedKey => $selectDataItemSelectedValue) {
                        $selectData['urlQueryString'] = $selectData['urlQueryString'] . '&' . $selectDataItemKey . '[]=' . $selectDataItemSelectedValue;
                    }
                } else {
                    $selectData['urlQueryString'] = $selectData['urlQueryString'] . '&' . $selectDataItemKey . '=' . $selectDataItemValue['selected'];
                }
            } else {
                if ($selectDataItemKey != 'urlQueryString') {
                    $selectData['urlQueryString'] = $selectData['urlQueryString'] . '&' . $selectDataItemKey . '=' . $selectDataItemValue;
                }
            }
        }

        return $selectData;
    }

    public function getCache()
    {
        return array(
            true,
            false
        );
    }

    public function getYears()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Years' . implode(array_keys($this->getSelectedCountries()));
        return Cache::remember($cacheKey, Config::get('cache.duration'), function () {
            $sqlQuery = "
                SELECT DISTINCT POL_ING_JAAR AS INGANGSJAAR
                FROM pol_count
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'Country') .
                " AND POL_ING_JAAR > 1960
                ORDER BY INGANGSJAAR DESC
            ";
            $sqlTable = DB::connection('mysql')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                $returnValue[$sqlTableRow->INGANGSJAAR] = $sqlTableRow->INGANGSJAAR;
            }
        });

        return $returnValue ?? [];
    }

    public function getCountries()
    {
        $returnValue = [];

        if (Sentinel::check()) {
            if (Sentinel::hasAccess(['country.NL'])) {
                $returnValue['nl'] = '';
            }

            if (Sentinel::hasAccess(['country.DE'])) {
                $returnValue['de'] = '';
            }

            if (Sentinel::hasAccess(['country.BE'])) {
                $returnValue['be'] = '';
            }
        } else {
            $returnValue['nl'] = '';
            $returnValue['de'] = '';
            $returnValue['be'] = '';
        }

        if (count($returnValue) == 0) {
            $returnValue['nl'] = '';
        }

        if (isset($returnValue['be'])) {
            $returnValue['nl'] = '';
        }

        foreach ($returnValue as $key => $value) {
            $returnValue[$key] = trans('enra/countries.' . $key, array(), App::getLocale());
        }
        return $returnValue;
    }

    public function getSelectedCountries()
    {
        if (App::runningInConsole()) {
            $countries = ['be', 'de', 'nl'];
        } else {
            $countries = request('country') ?? request()->session()->get('filterCountry') ?? ['nl'];
        }


        $cacheKey = $this->cachePrefix . App::getLocale() . 'Countries' . implode($countries);
        return Cache::remember($cacheKey, now()->addDays(7), function () use ($countries) {
            if (!$countries) {
                if (Sentinel::check()) {
                    if (Sentinel::hasAccess(['country.NL'])) {
                        $countries[] = 'nl';
                    }

                    if (Sentinel::hasAccess(['country.DE'])) {
                        $countries[] = 'de';
                    }

                    if (Sentinel::hasAccess(['country.BE'])) {
                        $countries[] = 'be';
                    }
                }
            }


            if (count($countries) == 0) {
                $countries[] = 'nl';
            }

            if (isset($countries['be'])) {
                $countries[] = 'nl';
            }

            foreach ($countries as $key => $value) {
                $returnValue[$value] = trans('enra/countries.' . $value, [], App::getLocale());
            }
            return $returnValue ?? [];
        }) ?? [];
    }

    public function getMonths()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Months';
        return Cache::remember($cacheKey, now()->addDays(7), function () {
            for ($i = 1; $i <= 12; $i++) {
                $month = '0' . $i;
                $returnValue[substr($month, strlen($month) - 2)] = trans('enra/months.' . substr($month, strlen($month) - 2), [], App::getLocale());
            }
            return $returnValue;
        }) ?? [];
    }

    public function getRegions(): array
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Regions' . implode(array_keys($this->getSelectedCountries()));
        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $sqlQuery = "
                SELECT DISTINCT AGB_55002 AS REGIO
                FROM AGBES
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'AGBES.country') . "
                ORDER BY REGIO
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->REGIO) != '') {
                    $returnValue[$sqlTableRow->REGIO] = trans('enra/regions.' . $sqlTableRow->REGIO, array(), App::getLocale());
                }
            }
            return $returnValue ?? [];
        }) ?? [];
    }

    public function getCompanies()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Companies' . implode(array_keys($this->getSelectedCountries()));
        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $sqlQuery = "
                SELECT DISTINCT POL_MIJNR AS MAATSCHAPPIJ
                FROM POLBES
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'POLBES.country') . "
                ORDER BY MAATSCHAPPIJ
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->MAATSCHAPPIJ) != '') {
                    $returnValue[$sqlTableRow->MAATSCHAPPIJ] = trans('enra/companies.' . $sqlTableRow->MAATSCHAPPIJ, array(), App::getLocale());
                }
            }
            return $returnValue ?? [];
        });
    }

    public function getBranches()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Branches' . implode(array_keys($this->getSelectedCountries()));
        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $sqlQuery = "
                SELECT DISTINCT POL_BRANCHE_SPEC AS BRANCHE
                FROM POLBES
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'POLBES.country') . "
                ORDER BY BRANCHE
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->BRANCHE) != '') {
                    $returnValue[$sqlTableRow->BRANCHE] = trans('enra/branches.' . $sqlTableRow->BRANCHE, array(), App::getLocale());
                }
            }
            return $returnValue ?? [];
        });
    }

    public function getObjectTypes()
    {
        $returnValue = array();

        $cacheKey = $this->cachePrefix . App::getLocale() . 'ObjectTypes' . implode(array_keys($this->getSelectedCountries()));

        if (Cache::has($cacheKey)) {
            $returnValue = Cache::get($cacheKey);
        } else {
            $sqlQuery = "
                SELECT DISTINCT POL_80000 AS OBJECTSOORT
                FROM POLBES
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'POLBES.country') . "
                ORDER BY OBJECTSOORT
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->OBJECTSOORT) != '') {
                    $returnValue[$sqlTableRow->OBJECTSOORT] = trans('enra/objecttypes.' . $sqlTableRow->OBJECTSOORT, array(), App::getLocale());
                }
            }

            Cache::put($cacheKey, $returnValue, Config::get('cache.duration'));
        }

        return $returnValue;
    }

    public function getOrganisations()
    {
        $returnValue = array();

        $cacheKey = $this->cachePrefix . App::getLocale() . 'Organisations' . implode(array_keys($this->getSelectedCountries()));

        if (Cache::has($cacheKey)) {
            $returnValue = Cache::get($cacheKey);
        } else {
            $sqlQuery = "
                SELECT DISTINCT POL_PRODUCENT AS PRODUCENT
                FROM POLBES
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'POLBES.country') . "
                ORDER BY PRODUCENT
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->PRODUCENT) != '') {
                    $returnValue[$sqlTableRow->PRODUCENT] = trans('enra/organisations.' . $sqlTableRow->PRODUCENT, array(), App::getLocale());
                }
            }

            Cache::put($cacheKey, $returnValue, Config::get('cache.duration'));
        }

        return $returnValue;
    }

    public function getAgents()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Agents' . implode(array_keys($this->getSelectedCountries()));
        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $sqlQuery = "
                SELECT DISTINCT AGE_AGENTNR AS AGENTNR, AGE_NAAM AS AGENTNAAM
                FROM AGENT
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'AGENT.country') . "
                AND (AGE_AGENTNR < '30000' OR country != 'de')
                ORDER BY AGENTNR
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->AGENTNR) != '') {
                    $returnValue[$sqlTableRow->AGENTNR] = $sqlTableRow->AGENTNAAM;
                }
            }
            return $returnValue ?? [];
        });

    }

    public function getCoverages()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Coverages' . implode(array_keys($this->getSelectedCountries()));

        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $sqlQuery = "
				SELECT DISTINCT tblDEKKING.DEK_DEKKING AS DEKKINGSCODE, DEKCODE.DKC_OMSCHRIJVING AS DEKKINGSNAAM
				FROM DEKCODE
				RIGHT JOIN (
				SELECT DISTINCT DEKKING.DEK_DEKKING, DEKKING.country
				FROM DEKKING
				) AS tblDEKKING ON (DEKCODE.DKC_DEKKING = tblDEKKING.DEK_DEKKING AND DEKCODE.country = tblDEKKING.country)
				WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'DEKCODE.country') . "
				ORDER BY DEKKINGSCODE
			";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->DEKKINGSCODE) != '') {
                    $returnValue[$sqlTableRow->DEKKINGSCODE] = $sqlTableRow->DEKKINGSNAAM;
                }
            }
            return $returnValue ?? [];
        });
    }

    public function getDurations()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Durations' . implode(array_keys($this->getSelectedCountries()));

        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $returnValue = [];
            $sqlQuery = "
                SELECT DISTINCT POL_LOOPTIJD AS LOOPTIJD
                FROM POLBES
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'POLBES.country') . "
                ORDER BY LOOPTIJD
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->LOOPTIJD) != '') {
                    $returnValue[$sqlTableRow->LOOPTIJD] = trans('enra/durations.' . $sqlTableRow->LOOPTIJD, array(), App::getLocale());
                }
            }
            return $returnValue;
        });
    }

    public function getDamageTypes()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'DamageTypes' . implode(array_keys($this->getSelectedCountries()));

        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $returnValue = [];
            $sqlQuery = "
                SELECT DISTINCT BE1_SCHADESOORT AS SCHADESOORT
                FROM BETSCH01
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'BETSCH01.country') . "
                ORDER BY SCHADESOORT
            ";

            $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->SCHADESOORT) != '') {
                    $returnValue[$sqlTableRow->SCHADESOORT] = trans('enra/damagetypes.' . $sqlTableRow->SCHADESOORT, array(), App::getLocale());
                }
            }

            unset($returnValue['00100']);
            unset($returnValue['00113']);
            unset($returnValue['00320']);
            unset($returnValue['00330']);
            unset($returnValue['00360']);
            unset($returnValue['00380']);
            unset($returnValue['01000']);
            return $returnValue;
        });
    }

    public function getClassifications()
    {
        $returnValue = [];

        if (Config::get('database.connections.mysql_anvaclone.database') == 'AnvaCloneData3') {
            $returnValue = Cache::tags('AGBES')->remember(CacheHelpers::getCacheKey($this->cachePrefix . App::getLocale() . 'Classifications' . implode(array_keys($this->getSelectedCountries()))), Config::get('cache.duration'), function () {
                $returnValue = [];

                $sqlQuery = "
		                SELECT DISTINCT AGB_55034 AS Classification
		                FROM AGBES
		                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'AGBES.country') . "
		                ORDER BY Classification
		            ";

                $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

                foreach ($sqlTable as $sqlTableRow) {
                    if (trim($sqlTableRow->Classification) != '') {
                        $returnValue[$sqlTableRow->Classification] = trans('enra/classifications.' . $sqlTableRow->Classification, array(), App::getLocale());
                    }
                }

                return $returnValue;
            });
        }

        return $returnValue;
    }

    public function getPotentials()
    {
        $returnValue = array();

        if (Config::get('database.connections.mysql_anvaclone.database') == 'AnvaCloneData3') {
            $returnValue = Cache::tags('AGBES')->remember(CacheHelpers::getCacheKey($this->cachePrefix . App::getLocale() . 'Potentials' . implode(array_keys($this->getSelectedCountries()))), Config::get('cache.duration'), function () {
                $returnValue = array();

                $sqlQuery = "
                SELECT DISTINCT AGB_55035 AS Potential
                FROM AGBES
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'AGBES.country') . "
                ORDER BY Potential
            ";

                $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

                foreach ($sqlTable as $sqlTableRow) {
                    if (trim($sqlTableRow->Potential) != '') {
                        $returnValue[$sqlTableRow->Potential] = trans('enra/potentials.' . $sqlTableRow->Potential, array(), App::getLocale());
                    }
                }

                return $returnValue;
            });
        }

        return $returnValue;
    }

    public function getWeightingFactors()
    {
        $returnValue = array();

        if (Config::get('database.connections.mysql_anvaclone.database') == 'AnvaCloneData3') {
            $returnValue = Cache::tags('AGBES')->remember(CacheHelpers::getCacheKey($this->cachePrefix . App::getLocale() . 'WeightingFactors' . implode(array_keys($this->getSelectedCountries()))), Config::get('cache.duration'), function () {
                $returnValue = array();

                $sqlQuery = "
	                SELECT DISTINCT AGB_55036 AS WeightingFactor
	                FROM AGBES
	                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'AGBES.country') . "
	                ORDER BY WeightingFactor
	            ";

                $sqlTable = DB::connection('mysql_anvaclone')->select($sqlQuery);

                foreach ($sqlTable as $sqlTableRow) {
                    if (trim($sqlTableRow->WeightingFactor) != '') {
                        $returnValue[$sqlTableRow->WeightingFactor] = trans('enra/weightingfactors.' . $sqlTableRow->WeightingFactor, array(), App::getLocale());
                    }
                }
                return $returnValue;
            });
        }

        return $returnValue;
    }

    public function getClients()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'Clients' . implode(array_keys($this->getSelectedCountries()));

        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $returnValue = [];
            $sqlQuery = "
                SELECT DISTINCT id AS ClientId, Name AS ClientName
                FROM clients
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'clients.country') . "
                ORDER BY ClientId
            ";

            $sqlTable = DB::connection('mysql')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->ClientId) != '') {
                    $returnValue[$sqlTableRow->ClientId] = $sqlTableRow->ClientName;
                }
            }
            return $returnValue;
        });
    }

    public function getClientStatus()
    {


        $cacheKey = $this->cachePrefix . App::getLocale() . 'ClientStatus' . implode(array_keys($this->getSelectedCountries()));

        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $returnValue = [];
            $sqlQuery = "
                SELECT DISTINCT status
                FROM clients
                WHERE " . DatabaseHelpers::getSqlQueryWhereMultiValue(array_keys($this->getSelectedCountries()), 'clients.country') . "
                ORDER BY status
            ";

            $sqlTable = DB::connection('mysql')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->status) != '') {
                    $returnValue[$sqlTableRow->status] = trans('enra/clientstatus.' . $sqlTableRow->status, array(), App::getLocale());
                }
            }
            return $returnValue;
        });
    }

    public function getContactMomentType()
    {
        $cacheKey = $this->cachePrefix . App::getLocale() . 'ContactMomentType' . implode(array_keys($this->getSelectedCountries()));
        return Cache::remember($cacheKey, now()->addDays(7), function () {
            $sqlQuery = "
                SELECT DISTINCT type
                FROM contact_moments
                ORDER BY type
            ";

            $sqlTable = DB::connection('mysql')->select($sqlQuery);

            foreach ($sqlTable as $sqlTableRow) {
                if (trim($sqlTableRow->type) != '') {
                    $returnValue[$sqlTableRow->type] = trans('enra/contactmomenttypes.' . $sqlTableRow->type, array(), App::getLocale());
                }
            }
            return $returnValue;
        });
    }
}
