<?php

namespace App\Services\TextReplacer\Replacers;

use App\Services\TextReplacer\ReplacerInterface;

class BikeBrandReplacer implements ReplacerInterface
{
    public function replace(array $input): array
    {
        $newRows = [];
        foreach ($input as $row) {
            $newRows[] = $this->replaceRow($row);
        }
        return $newRows;
    }

    private function replaceRow(array $input): array
    {
        $output = str_ireplace([
                'gaz ',
                'gaz.',
                'gaz\\elle',
                'gazaelle',
                'gazalle',
                'gaze3lle',
                'gazeele',
                'gazeelle',
                'gazekke',
                'gazele',
                'gazelee',
                'gazelele',
                'gazell ',
                'gazella',
                'gazellee',
                'gazellekp',
                'gazellen',
                'gazelleorangec7hmbh5',
                'gazelleoranje',
                'gazelleq',
                'gazeller',
                'gazelles',
                'gazellle',
                'gazellre',
                'gazlle',
                'gazrlle',
                'gazwelle',
                'gazzele',
                'gazzelle',
            ]
            , 'gazelle ', $input);
        $output['frame_number'] = preg_replace("/[^A-Za-z0-9]/", '', $output['frame_number']);
        return str_ireplace('Bat.', 'batavus', $output);
    }
}
