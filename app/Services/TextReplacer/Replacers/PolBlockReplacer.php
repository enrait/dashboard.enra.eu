<?php

namespace App\Services\TextReplacer\Replacers;

use App\Services\TextReplacer\ReplacerInterface;

class PolBlockReplacer implements ReplacerInterface
{
    public function replace(array $input): array
    {
        $newRows = [];
        foreach ($input as $row) {
            $newRows[] = $this->replaceRow($row);
        }
        return $newRows;
    }

    private function replaceRow(array $input): array
    {
        $polBlock = explode('', $input['POL_BLOK_1']);
        $newPolBlock['id'] = $input['id'];
        $newPolBlock['POL_BLOK_1'] = null;
        foreach ($polBlock as $block) {
            $newCol = substr($block, 0, 5);
            if ($newCol != '') {
                $newPolBlock['POL_'.$newCol] = substr($block, 5);
            }
        }
        return $newPolBlock;
    }
}
