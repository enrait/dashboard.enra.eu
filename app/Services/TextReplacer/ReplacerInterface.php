<?php

namespace App\Services\TextReplacer;

interface ReplacerInterface
{
    public function replace(array $input): array;
}
