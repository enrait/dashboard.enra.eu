<?php

namespace App\Services\Indexing;

use App\Services\TextReplacer\ReplacerInterface;

interface IndexerInterface
{
    public function index(array $connectionData, array $selectData, ReplacerInterface $replacer = null): void;
}
