<?php

namespace App\Services\Indexing\DbCopy;

use App\Libraries\DatabaseHelpers;
use App\Services\Indexing\IndexerInterface;
use App\Services\TextReplacer\ReplacerInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AgentBrancheGroupedCopy extends AbstractDbCopy implements IndexerInterface
{
    const TABLE_EXTENSION = '_TMP';

    /**
     * @throws Exception
     * Please note: This might be limited by the receiving server.
     */
    public function index(array $connectionData, array $selectData, ReplacerInterface $replacer = null): void
    {
        $inputConnection = $connectionData['inputConnection'];
        $outputConnection = $connectionData['outputConnection'];
        $fromTable = $connectionData['fromTable'];
        $toTable = $connectionData['toTable'];
        $query = $connectionData['query'] ?? null;

        $tableCreateSQL = DB::connection($outputConnection)->select("SHOW CREATE TABLE $toTable");
        $newSQL = str_replace("`$toTable`", '`' . $toTable . self::TABLE_EXTENSION . '`', collect($tableCreateSQL)->pluck('Create Table')->first());

        DB::connection($outputConnection)->statement("DROP TABLE IF EXISTS " . $toTable . self::TABLE_EXTENSION);
        DB::connection($outputConnection)->statement($newSQL);

        try {
            DB::connection($inputConnection)->getPdo();
            DB::connection($outputConnection)->getPdo();
        } catch (\Throwable $e) {
            throw new Exception('Could not connect to 1 or more servers. Indexing has been cancelled: ');
        }

        DB::connection($outputConnection)->disableQueryLog();

        try {
            $newResult = DB::connection($inputConnection)
                ->select($this->returnSQL());
            $newResult = json_decode(json_encode($newResult), true);
            if ($replacer) {
                $newResult = $replacer->replace($newResult);
            }

            $chunkedResults = array_chunk($newResult, 1000);
            foreach ($chunkedResults as $chunk) {
                DB::connection($outputConnection)->table($toTable . self::TABLE_EXTENSION)->insert($chunk);
            }
            DB::connection($outputConnection)->statement("DROP TABLE $toTable");
            DB::connection($outputConnection)->statement("ALTER TABLE $toTable" . self::TABLE_EXTENSION . " RENAME TO $toTable");
        } catch (Exception $exception) {
            throw new Exception('An error occurred during import: ' . $exception->getMessage()
                . $exception->getLine() . '-' . $exception->getFile());
        }
    }

    private function returnSQL(): string
    {

        $sqlQueryWhereArray = array();
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue(['nl', 'be', 'de'], 'AGENT.country');

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        if (date('m') == 1) {
            $yearLast = date('Y') - 2;
            $yearNow = date('Y') - 1;
        } else {
            $yearLast = date('Y') - 1;
            $yearNow = date('Y');
        }

        $monthNow = (date('m') == 1 ? 13 : date('m'));
        $filter['month'] = array();

        for ($i = 1; $i < $monthNow; $i++) {
            $month = '0' . $i;
            $filter['month'][] = substr($month, strlen($month) - 2);
        }

        $monthNow = date('m');

        $sqlQueryWhereMonthTill = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['month'], 'SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) ');
        $sqlQueryWhereCountry = DatabaseHelpers::getSqlQueryWhereMultiValue(['nl', 'be', 'de'], 'POLBES.country');

        return "SELECT AGENTBRANCHE.*,
			      IFNULL(POLCOUNTVJ.POL_TOTAAL, 0) AS POL_TOTAAL_VJ,
			      IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) AS POL_TOTAAL_DJ,
			      IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) - IFNULL(POLCOUNTVJ.POL_TOTAAL, 0) AS POL_TOTAAL_DIFF,
			      IF(IFNULL(POLCOUNTVJ.POL_TOTAAL, 0) = 0
			        AND (IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) - IFNULL(POLCOUNTVJ.POL_TOTAAL, 0)) > 0, '~', ROUND(((IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) - IFNULL(POLCOUNTVJ.POL_TOTAAL, 0)) / IFNULL(POLCOUNTVJ.POL_TOTAAL, 0)) * 100, 0)) AS POL_TOTAAL_DIFF_PERC,
			      IFNULL(POLCOUNTVJM.POL_TOTAAL, 0) AS POL_TOTAAL_VJM,
			      IFNULL(POLCOUNTDJM.POL_TOTAAL, 0) AS POL_TOTAAL_DJM,
			       COALESCE(POLCOUNTVJ.country, 'NL') as country
			  FROM (SELECT AGENT.AGE_AGENTNR,
			         AGBES.AGB_55002,
			         POLBES.POL_BRANCHE_SPEC,
			         AGENT.AGE_NAAM,
			         TRIM(CONCAT_WS(' ', AGENT.AGE_POSTCODE, AGENT.AGE_BUITENL_POSTCODE)) AS AGE_POSTCODE,
			         AGENT.AGE_PLAATS,
			         AGENT.AGE_INGDAT
			  FROM AnvaCloneData1.AGENT
			  LEFT JOIN AnvaCloneData1.AGBES ON (AGBES.AGB_AGENTNR = AGENT.AGE_AGENTNR
			                      AND AGBES.country = AGENT.country)
			  LEFT JOIN AnvaCloneData1.POLBES ON (POLBES.POL_AGENT_1 = AGENT.AGE_AGENTNR
			                AND POLBES.country = AGENT.country)
			  {$sqlQueryWhere}
			  GROUP BY AGENT.country,
			           AGENT.AGE_AGENTNR,
			           POLBES.POL_BRANCHE_SPEC
			  ORDER BY AGENT.country,
			           AGENT.AGE_AGENTNR,
			           POLBES.POL_BRANCHE_SPEC) AS AGENTBRANCHE

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData1.POLBES
			    LEFT JOIN AnvaCloneData1.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearLast}') AND {$sqlQueryWhereMonthTill} AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTVJ ON (POLCOUNTVJ.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTVJ.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData1.POLBES
			    LEFT JOIN AnvaCloneData1.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearNow}') AND {$sqlQueryWhereMonthTill} AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTDJ ON (POLCOUNTDJ.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTDJ.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData1.POLBES
			    LEFT JOIN AnvaCloneData1.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearLast}') AND SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) = '{$monthNow}' AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTVJM ON (POLCOUNTVJM.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTVJM.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData1.POLBES
			    LEFT JOIN AnvaCloneData1.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearNow}') AND SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) = '{$monthNow}' AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTDJM ON (POLCOUNTDJM.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTDJM.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)

                UNION ALL



                SELECT AGENTBRANCHE.*,
			      IFNULL(POLCOUNTVJ.POL_TOTAAL, 0) AS POL_TOTAAL_VJ,
			      IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) AS POL_TOTAAL_DJ,
			      IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) - IFNULL(POLCOUNTVJ.POL_TOTAAL, 0) AS POL_TOTAAL_DIFF,
			      IF(IFNULL(POLCOUNTVJ.POL_TOTAAL, 0) = 0
			        AND (IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) - IFNULL(POLCOUNTVJ.POL_TOTAAL, 0)) > 0, '~', ROUND(((IFNULL(POLCOUNTDJ.POL_TOTAAL, 0) - IFNULL(POLCOUNTVJ.POL_TOTAAL, 0)) / IFNULL(POLCOUNTVJ.POL_TOTAAL, 0)) * 100, 0)) AS POL_TOTAAL_DIFF_PERC,
			      IFNULL(POLCOUNTVJM.POL_TOTAAL, 0) AS POL_TOTAAL_VJM,
			      IFNULL(POLCOUNTDJM.POL_TOTAAL, 0) AS POL_TOTAAL_DJM,
			       COALESCE(POLCOUNTVJ.country, 'DE') as country
			  FROM (SELECT AGENT.AGE_AGENTNR,
			         AGBES.AGB_55002,
			         POLBES.POL_BRANCHE_SPEC,
			         AGENT.AGE_NAAM,
			         TRIM(CONCAT_WS(' ', AGENT.AGE_POSTCODE, AGENT.AGE_BUITENL_POSTCODE)) AS AGE_POSTCODE,
			         AGENT.AGE_PLAATS,
			         AGENT.AGE_INGDAT
			  FROM AnvaCloneData3.AGENT
			  LEFT JOIN AnvaCloneData3.AGBES ON (AGBES.AGB_AGENTNR = AGENT.AGE_AGENTNR
			                      AND AGBES.country = AGENT.country)
			  LEFT JOIN AnvaCloneData3.POLBES ON (POLBES.POL_AGENT_1 = AGENT.AGE_AGENTNR
			                AND POLBES.country = AGENT.country)
			  {$sqlQueryWhere}
			  GROUP BY AGENT.country,
			           AGENT.AGE_AGENTNR,
			           POLBES.POL_BRANCHE_SPEC
			  ORDER BY AGENT.country,
			           AGENT.AGE_AGENTNR,
			           POLBES.POL_BRANCHE_SPEC) AS AGENTBRANCHE

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData3.POLBES
			    LEFT JOIN AnvaCloneData3.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearLast}') AND {$sqlQueryWhereMonthTill} AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTVJ ON (POLCOUNTVJ.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTVJ.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData3.POLBES
			    LEFT JOIN AnvaCloneData3.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearNow}') AND {$sqlQueryWhereMonthTill} AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTDJ ON (POLCOUNTDJ.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTDJ.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData3.POLBES
			    LEFT JOIN AnvaCloneData3.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearLast}') AND SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) = '{$monthNow}' AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTVJM ON (POLCOUNTVJM.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTVJM.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)

			  LEFT JOIN (
			    SELECT SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
			    POLBES.POL_AGENT_1,
			    POLBES.POL_BRANCHE_SPEC,
			    COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
			    POLBES.country
			    FROM AnvaCloneData3.POLBES
			    LEFT JOIN AnvaCloneData3.WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
			    WHERE WACBES.WAC_RELNR IS NULL AND SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) IN ('{$yearNow}') AND SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) = '{$monthNow}' AND {$sqlQueryWhereCountry}
			    GROUP BY POLBES.country,
			    POLBES.POL_BRANCHE_SPEC,
			    POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			    ORDER BY POLBES.POL_AGENT_1,
			    POL_ING_JAAR
			  ) AS POLCOUNTDJM ON (POLCOUNTDJM.POL_AGENT_1 = AGENTBRANCHE.AGE_AGENTNR AND POLCOUNTDJM.POL_BRANCHE_SPEC = AGENTBRANCHE.POL_BRANCHE_SPEC)
            ";
    }
}
