<?php

namespace App\Services\Indexing\DbCopy;

use App\Libraries\DatabaseHelpers;
use App\Services\Indexing\IndexerInterface;
use App\Services\TextReplacer\ReplacerInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PolicyCopy extends AbstractDbCopy implements IndexerInterface
{
    const TABLE_EXTENSION = '_TMP';

    /**
     * @throws Exception
     * Please note: This might be limited by the receiving server.
     */
    public function index(array $connectionData, array $selectData, ReplacerInterface $replacer = null): void
    {
        $inputConnection = $connectionData['inputConnection'];
        $outputConnection = $connectionData['outputConnection'];
        $toTable = $connectionData['toTable'];

        try {
            DB::connection($inputConnection)->getPdo();
            DB::connection($outputConnection)->getPdo();
        } catch (\Throwable $e) {
            throw new Exception('Could not connect to 1 or more servers. Indexing has been cancelled: ');
        }

        DB::connection($outputConnection)->disableQueryLog();

        try {
            echo 'start indexing';
            $this->queryDb($inputConnection, $outputConnection, $toTable, $this->returnAnva1SQL(), $replacer);

            $this->queryDb('mysql_anvaclone_data3', 'mysql_anvaclone_data3', $toTable, $this->returnAnva3SQL(), $replacer);

        } catch (Exception $exception) {
            throw new Exception('An error occurred during import: ' . $exception->getMessage()
                . $exception->getLine() . '-' . $exception->getFile());
        }
    }

    private function queryDb(string $inputConnection, string $outputConnection, string $toTable, string $query, ReplacerInterface $replacer = null)
    {
        $newResult = DB::connection($inputConnection)
            ->select($query);
        $newResult = json_decode(json_encode($newResult), true);
        if ($replacer) {
            $newResult = $replacer->replace($newResult);
        }

        foreach ($newResult as $update) {
            $id = $update['id'];
            unset($update['id']);

            DB::connection($outputConnection)->table($toTable)
                ->where('id', $id)
                ->update($update);
        }
    }

    private function returnAnva1SQL(): string
    {
        return "SELECT * FROM AnvaCloneData1.POLBES WHERE POL_BLOK_1 is not null limit 100000";
    }

    private function returnAnva3SQL(): string
    {
        return "SELECT * FROM AnvaCloneData3.POLBES WHERE POL_BLOK_1 is not null limit 100000";
    }
}
