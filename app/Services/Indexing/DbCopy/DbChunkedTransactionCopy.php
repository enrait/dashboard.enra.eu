<?php

namespace App\Services\Indexing\DbCopy;

use App\Services\Indexing\IndexerInterface;
use App\Services\TextReplacer\ReplacerInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbChunkedTransactionCopy extends AbstractDbCopy implements IndexerInterface
{
    const TABLE_EXTENSION = '_TMP';

    /**
     * @throws Exception
     * Please note: This might be limited by the receiving server.
     */
    public function index(array $connectionData, array $selectData, ReplacerInterface $replacer = null): void
    {
        $inputConnection = $connectionData['inputConnection'];
        $outputConnection = $connectionData['outputConnection'];
        $fromTable = $connectionData['fromTable'];
        $toTable = $connectionData['toTable'];
        $query = $connectionData['query'] ?? null;

        $tableCreateSQL = DB::connection($outputConnection)->select("SHOW CREATE TABLE $toTable");
        $newSQL = str_replace("`$toTable`", '`' . $toTable . self::TABLE_EXTENSION . '`', collect($tableCreateSQL)->pluck('Create Table')->first());

        DB::connection($outputConnection)->statement("DROP TABLE IF EXISTS " . $toTable . self::TABLE_EXTENSION);
        DB::connection($outputConnection)->statement($newSQL);

        try {
            DB::connection($inputConnection)->getPdo();
            DB::connection($outputConnection)->getPdo();
        } catch (\Throwable $e) {
            throw new Exception('Could not connect to 1 or more servers. Indexing has been cancelled: ');
        }

        DB::connection($outputConnection)->disableQueryLog();

        try {
            if ($query) {
                $newResult = DB::connection($inputConnection)
                    ->select($query);
            } else {
                $result = DB::connection($inputConnection)
                    ->table($fromTable)
                    ->select($selectData)
                    ->get();
                $newResult = $result->toArray();
            }
            $newResult = json_decode(json_encode($newResult), true);
            if ($replacer) {
                $newResult = $replacer->replace($newResult);
            }

            $chunkedResults = array_chunk($newResult, 5000);
            foreach ($chunkedResults as $chunk) {
                DB::connection($outputConnection)->table($toTable . self::TABLE_EXTENSION)->insert($chunk);
            }
            DB::connection($outputConnection)->statement("DROP TABLE $toTable");
            DB::connection($outputConnection)->statement("ALTER TABLE $toTable" . self::TABLE_EXTENSION . " RENAME TO $toTable");
        } catch (Exception $exception) {
            throw new Exception('An error occurred during import: ' . $exception->getMessage()
                . $exception->getLine() . '-' . $exception->getFile());
        }
    }
}
