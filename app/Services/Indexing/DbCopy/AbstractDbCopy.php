<?php

namespace App\Services\Indexing\DbCopy;

abstract class AbstractDbCopy
{
    public function __construct()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
    }
}
