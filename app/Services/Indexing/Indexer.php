<?php

namespace App\Services\Indexing;

use App\Services\TextReplacer\ReplacerInterface;

class Indexer
{
    public static function start(IndexerInterface $indexer, array $connectionData,
                                 array            $selectData, ReplacerInterface $replacer = null): void
    {
        $indexer->index($connectionData, $selectData, $replacer);
    }
}
