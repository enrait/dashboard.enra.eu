<?php

namespace App\Services\Sentry;

use GuzzleHttp\Client;

class Monitor
{
    public static function process($command, string $uuid): void
    {
        $command->before(function () use ($uuid) {
            Monitor::checkin($uuid, 'in_progress');
        })
            ->after(function () use ($uuid) {
                Monitor::checkin($uuid, 'ok');
            })
            ->onFailure(function () use ($uuid) {
                Monitor::checkin($uuid, 'error');
            });
    }

    public static function checkin(string $monitorId, string $status = 'in_progress')
    {
        $client = new Client();
        $res = $client->request('POST', "https://sentry.io/api/0/organizations/enra-20/monitors/$monitorId/checkins/",
            ['headers' =>
                [
                    'Authorization' => "DSN " . config('sentry.dsn'),
                    'Content-Type' => 'application/json',
                ],
                'body' => "{\"status\": \"$status\"}",
            ])->getBody()->getContents();
    }
}
