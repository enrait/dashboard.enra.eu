<?php

namespace App\Jobs;

use GoogleMaps;
use App\Models\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateClientCoordinatesJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var \stdClass
     */
    public $client;

    public function __construct($client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $clientData = $this->client;

        // Fetch data from Google
        $googleClientInfo = json_decode(
            GoogleMaps::load('geocoding')->setParam(
                ['address' => $clientData->street . " " . $clientData->housenumber . " " . $clientData->city . " " . $clientData->country]
            )->get(),
            false
        );

        // Fetch the Client to be updated
        $client = Client::findOrFail($this->client->id);

        if (isset($googleClientInfo->results[0])) {
            $client->update([
                'name' => $this->client->name,
                'visiting_street' => $this->client->street,
                'visiting_housenumber' => $this->client->housenumber,
                'visiting_zipcode' => $this->client->zipcode,
                'visiting_city' => $this->client->city,
                'visiting_country' => $this->client->country,
                'visiting_longitude' => $googleClientInfo->results[0]->geometry->location->lng,
                'visiting_latitude' => $googleClientInfo->results[0]->geometry->location->lat,
                'phonenumber' => $this->client->phonenumber,
                'faxnumber' => $this->client->faxnumber,
                'website' => $this->client->website,
            ]);
        } else {
            logger()->error('Client address not found in Google', [$client]);

            $client->update([
                'name' => $this->client->name,
                'visiting_street' => $this->client->street,
                'visiting_housenumber' => $this->client->housenumber,
                'visiting_zipcode' => $this->client->zipcode,
                'visiting_city' => $this->client->city,
                'visiting_country' => $this->client->country,
                'visiting_longitude' => 'NOT FOUND',
                'visiting_latitude' => 'NOT FOUND',
                'phonenumber' => $this->client->phonenumber,
                'faxnumber' => $this->client->faxnumber,
                'website' => $this->client->website,
            ]);
        }
    }
}
