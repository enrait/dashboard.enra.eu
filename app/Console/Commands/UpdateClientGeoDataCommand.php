<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\WebToolsController;
use Session;
use Config;
use App;

use Illuminate\Foundation\Inspiring;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateClientGeoDataCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:Update_Client_Geo_Data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'UpdateClientGeoDataCommand will fetch the Geo Data for the Clients table.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$webToolsController = new WebToolsController();

		$webToolsController -> updateClientGeoData();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}
