<?php

namespace App\Console\Commands;

use App\Services\Filters\FilterData;
use App\Services\Filters\ProductData;
use App\Services\Sentry\Monitor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class PrefillCacheCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:Prefill_Cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'PrefillCacheCommand wil pre-fill the cache with common queries and data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $filterController = new FilterData();
        $productData = new ProductData();

        $countries = array('nl', 'de');

        foreach ($countries as $country) {

            $filterController->setSelectedCountries([$country]);
            $this->info(' getProductData: ' . print_r($productData->prepareProductionSummarySectionData()));

            switch ($country) {
                case 'nl':
                    Config::set('database.connections.mysql_anvaclone.database', env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'));

                    break;

                case 'de':
                    Config::set('database.connections.mysql_anvaclone.database', env('DB_DATABASE_ANVACLONE_DATA3', 'AnvaCloneData3'));
                    break;

                case 'be':
                    Config::set('database.connections.mysql_anvaclone.database', env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'));
                    break;
                default:
                    Config::set('database.connections.mysql_anvaclone.database', env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'));
            }

            Session(['filterCountry' => [$country]]);
            App::setLocale($country);

            $this->info('Country: ' . $country . ' getCache: ' . print_r($filterController->getCache()));
            $this->info('Country: ' . $country . ' getYears: ' . print_r($filterController->getYears()));
            $this->info('Country: ' . $country . ' getMonths: ' . print_r($filterController->getMonths()));
            $this->info('Country: ' . $country . ' getCountries: ' . print_r($filterController->getCountries()));
            $this->info('Country: ' . $country . ' getRegions: ' . print_r($filterController->getRegions()));
            $this->info('Country: ' . $country . ' getCompanies: ' . print_r($filterController->getCompanies()));
            $this->info('Country: ' . $country . ' getBranches: ' . print_r($filterController->getBranches()));
            $this->info('Country: ' . $country . ' getObjectTypes: ' . print_r($filterController->getObjectTypes()));
            $this->info('Country: ' . $country . ' getOrganisations: ' . print_r($filterController->getOrganisations()));
            $this->info('Country: ' . $country . ' getAgents: ' . print_r($filterController->getAgents()));
            $this->info('Country: ' . $country . ' getCoverages: ' . print_r($filterController->getCoverages()));
//            $this->info('Country: ' . $country . ' getDurations: ' . print_r($filterController->getDurations()));
            $this->info('Country: ' . $country . ' getDamageTypes: ' . print_r($filterController->getDamageTypes()));
//            $this->info('Country: ' . $country . ' getClassifications: ' . print_r($filterController->getClassifications()));

//            $this->info('Country: ' . $country . ' getPotentials: ' . print_r($filterController->getPotentials()));
//            $this->info('Country: ' . $country . ' getWeightingFactors: ' . print_r($filterController->getWeightingFactors()));
//            $this->info('Country: ' . $country . ' getClients: ' . print_r($filterController->getClients()));
//            $this->info('Country: ' . $country . ' getClientStatus: ' . print_r($filterController->getClientStatus()));
//            $this->info('Country: ' . $country . ' getContactMomentType: ' . print_r($filterController->getContactMomentType()));
            $this->info('Country: ' . $country . ' App::getLocale() : ' . App::getLocale());
        }


    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
