<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Http\Controllers\WebToolsController;
use Session;
use Config;
use App;

use Illuminate\Foundation\Inspiring;
use Illuminate\Contracts\Bus\SelfHandling;

class CreateW2DSearchRelatie extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:Create_W2DSearchRelatie';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'CreateW2DSearchRelatie will copy the MySQL DB to MSSQL DB.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$webToolsController = new WebToolsController();

		$webToolsController -> createW2DSearchRelatie();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}
