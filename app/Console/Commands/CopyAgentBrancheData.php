<?php

namespace App\Console\Commands;

use App;
use App\Services\Indexing\DbCopy\AgentBrancheCopy;
use App\Services\Indexing\Indexer;
use App\Services\Sentry\Monitor;
use Illuminate\Console\Command;


class CopyAgentBrancheData extends Command
{
    const TABLE_SELECT = [];
    const CONNECTION_DATA = ["inputConnection" => "mysql_anvaclone",
        "outputConnection" => "mysql",
        "fromTable" => "AnvaCloneData1",
        "toTable" => "agent_branche",
        "query" => "",
    ];

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:CopyAgentBranche';

    private $monitorId;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CopyAgentBrancheData will copy the MySQL Agent Branche data from AnvaClone to the dashboard database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->monitorId = '0804bc81-47df-4c0c-979d-7529aa523c97';

        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        Monitor::checkin($this->monitorId);
        Indexer::start(new AgentBrancheCopy(),
            self::CONNECTION_DATA, self::TABLE_SELECT);
        Monitor::checkin($this->monitorId, 'ok');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
