<?php

namespace App\Console\Commands;

use App\Services\Indexing\DbCopy\DbChunkedTransactionCopy;
use App\Services\Indexing\DbCopy\PolicyCopy;
use App\Services\Indexing\Indexer;
use App\Services\TextReplacer\Replacers\BikeBrandReplacer;
use App\Services\TextReplacer\Replacers\PolBlockReplacer;
use Illuminate\Console\Command;
use App;


class CopyEnra4AllPolicySearch extends Command
{

    const TABLE_SELECT = ["frame_number", "bike_brand", "bike_name", "policy", "coverage"];
    const CONNECTION_DATA = ["inputConnection" => "mysql_anvaclone_data1",
        "outputConnection" => "mysql_anvaclone_data1",
        "fromTable" => "POLBES",
        "toTable" => "POLBES",
        "query" => "",
    ];

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:UpdateAnvaDb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'UpdateAnva Db will update PolBlok in anvaclonedata db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        Indexer::start(new PolicyCopy(),
            self::CONNECTION_DATA, self::TABLE_SELECT, new PolBlockReplacer());
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
