<?php

namespace App\Console\Commands;

use App\Services\Indexing\DbCopy\DbChunkedTransactionCopy;
use App\Services\Indexing\Indexer;
use App\Services\TextReplacer\Replacers\BikeBrandReplacer;
use Illuminate\Console\Command;
use App;


class CopyPolCountData extends Command
{
    const TABLE_SELECT = ["POL_ING_JAAR",
        "POL_AGENT_1",
        "POL_TOTAAL",
        "Country",
    ];
    const CONNECTION_DATA = ["inputConnection" => "mysql_anvaclone",
        "outputConnection" => "mysql",
        "fromTable" => "AnvaCloneData1",
        "toTable" => "pol_count",
        "query" => "SELECT CAST(SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS UNSIGNED)  AS POL_ING_JAAR,
                            CAST(SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) AS UNSIGNED) AS POL_ING_MAAND,
							POLBES.POL_AGENT_1,
							COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
							POLBES.country,
							WACBES.WAC_RELNR as WAC_RELNR,
							POLBES.POL_BRANCHE_SPEC
							FROM AnvaCloneData1.POLBES
							LEFT JOIN WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
							WHERE WAC_RELNR is NULL
							GROUP BY POLBES.country,
							POLBES.POL_AGENT_1,
							POL_ING_JAAR
UNION ALL
                            SELECT CAST(SUBSTRING(POLBES.POL_ING_DATUM, 1, 4) AS UNSIGNED)  AS POL_ING_JAAR,
                            CAST(SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) AS UNSIGNED) AS POL_ING_MAAND,
							POLBES.POL_AGENT_1,
							COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
							POLBES.country,
							WACBES.WAC_RELNR as WAC_RELNR,
                            POLBES.POL_BRANCHE_SPEC
							FROM AnvaCloneData3.POLBES
							LEFT JOIN WACBES ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
							WHERE WAC_RELNR is NULL
							GROUP BY POLBES.country,
							POLBES.POL_AGENT_1,
							POL_ING_JAAR",
    ];

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:CopyPolCount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CopyPolCountData will copy the MySQL Policy count data from AnvaClone to the dashboard database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        Indexer::start(new DbChunkedTransactionCopy(),
            self::CONNECTION_DATA, self::TABLE_SELECT);
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
