<?php

namespace App\Console\Commands;

use App;
use App\Services\Indexing\DbCopy\AgentBrancheGroupedCopy;
use App\Services\Indexing\Indexer;
use App\Services\Sentry\Monitor;
use Illuminate\Console\Command;


class CopyAgentGroupedBrancheData extends Command
{
    const TABLE_SELECT = [];
    const CONNECTION_DATA = ["inputConnection" => "mysql_anvaclone",
        "outputConnection" => "mysql",
        "fromTable" => "AnvaCloneData1",
        "toTable" => "agent_branche_grouped",
        "query" => "",
    ];

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:CopyAgentGroupedBranche';

    private $monitorId;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CopyAgentBrancheData will copy the MySQL Agent Branche data from AnvaClone to the dashboard database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->monitorId = '14a4a454-907b-4a4b-ab4d-bb1438cf7b6a';
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        Monitor::checkin($this->monitorId);
        Indexer::start(new AgentBrancheGroupedCopy(),
            self::CONNECTION_DATA, self::TABLE_SELECT);
        Monitor::checkin($this->monitorId, 'ok');
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
