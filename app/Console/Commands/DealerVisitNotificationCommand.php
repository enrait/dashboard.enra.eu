<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Client;
use DB;
use DateTime;
use Mail;

use Illuminate\Foundation\Inspiring;

class DealerVisitNotificationCommand extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:DealerVisit_Notification';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'DealerVisitNotificationCommand will sent an email notification with an overview of the DealerVisit since last run of the DealerVisitNotificationCommand command.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$emailReceivers = array(
			// 'd.tap@enra.nl' => 'Dennis',
			'i.huisman@enra.nl' => 'Iris',
            'r.lussenburg@enra.nl' => 'Raymond',
			// 's.janzen@enra.nl'
			//'r.vanbijnen@enra.nl' => 'Ronald',
			//'w.oostindie@enra.nl' => 'Wim',
			//'dennis@nextvalley.nl' => 'Dennis'
		);

		$dateFrom = new DateTime;
		$dateTo = new DateTime;

		switch ($this->option('period'))
		{
			case 'test' :
				$dateFrom = $dateFrom -> modify('-1 day');
				$dateTo = $dateTo -> modify('-1 day');

				$emailReceivers = array('d.tap@enra.nl' => 'Dennis');

				break;

			case 'week' :
				$dateFrom = $dateFrom -> modify('-1 week');
				$dateTo = $dateTo -> modify('-1 day');

				//$emailReceivers['d.boeijenga@enra.nl'] = 'Douwe';
				//$emailReceivers['r.noordhout@enra.nl'] = 'Rodney';
				//$emailReceivers['s.schroder@enra.nl'] = 'Steven';
				//$emailReceivers['r.lussenburg@enra.nl'] = 'Raymond';

				break;

			case 'month' :
				$dateFrom = $dateFrom -> modify('-1 month');
				$dateTo = $dateTo -> modify('-1 day');

				//$emailReceivers['d.boeijenga@enra.nl'] = 'Douwe';
				//$emailReceivers['r.noordhout@enra.nl'] = 'Rodney';
				//$emailReceivers['s.schroder@enra.nl'] = 'Steven';
				//$emailReceivers['r.lussenburg@enra.nl'] = 'Raymond';

				break;

			case 'year' :
				$dateFrom = $dateFrom -> modify('-1 year');
				$dateTo = $dateTo -> modify('-1 day');

				//$emailReceivers['d.boeijenga@enra.nl'] = 'Douwe';
				//$emailReceivers['r.noordhout@enra.nl'] = 'Rodney';
				//$emailReceivers['s.schroder@enra.nl'] = 'Steven';
				//$emailReceivers['r.lussenburg@enra.nl'] = 'Raymond';

				break;

			case 'day' :
			default :
				$dateFrom = $dateFrom -> modify('-1 day');
				$dateTo = $dateTo -> modify('-1 day');

				break;
		}

		$sqlQuery = "SELECT 
						'' AS Lijstnummer,
						'' AS Verzend,
						DATE_FORMAT(EnraDashboard.contact_moments.contact_at,'%d-%m-%Y') AS EVENT_DATE,
						'' AS 'Product',
						EnraDashboard.clients.anva_agentnr AS Klantnummer,
						IF(AnvaCloneData1.AGENT2.AGE2_EMAILADRES IS NULL, clients.email_address, CONCAT_WS('', AnvaCloneData1.AGENT2.AGE2_EMAILADRES, AnvaCloneData1.AGENT2.AGE2_EMAILADRES_REST)) AS Email,
						IFNULL(AnvaCloneData1.AGENT.AGE_NAAM, EnraDashboard.clients.name) AS LETTER_SIGNATURE,
						'' AS Geslacht,
						'' AS SHORT_NAME,
						'' AS MIDDLE_NAME_LOWER,
						'' AS Name, 
						AnvaCloneData1.AGENT.AGE_NAAM AS Bedrijfsnaam,
						EnraDashboard.clients.organization_member AS 'Soort bedrijf',
						'' AS 'Functieomschrijving',
						'' AS 'Segmentatie'
					FROM EnraDashboard.clients
					LEFT JOIN EnraDashboard.contact_moments ON (EnraDashboard.contact_moments.client_id = EnraDashboard.clients.id)
					LEFT JOIN AnvaCloneData1.AGENT ON (AnvaCloneData1.AGENT.AGE_AGENTNR = EnraDashboard.clients.anva_agentnr)
					LEFT JOIN AnvaCloneData1.AGENT2 ON (AnvaCloneData1.AGENT2.AGE2_AGENTNR = EnraDashboard.clients.anva_agentnr)
					WHERE 
						EnraDashboard.contact_moments.contact_at BETWEEN DATE('" . $dateFrom -> format('Y-m-d') . "') AND DATE('" . $dateTo -> format('Y-m-d') . "') AND
						EnraDashboard.clients.country = 'NL' AND
						EnraDashboard.clients.kto = '1' AND
						EnraDashboard.contact_moments.`type` = '1'
					ORDER BY EnraDashboard.clients.last_visit";

		//$dealerVisits = Client::whereBetween('last_visit', [
		//$dateFrom -> format('Y-m-d'),
		//$dateTo -> format('Y-m-d')]);

		$dealerVisits = DB::connection('mysql')->select($sqlQuery);

		$firstLine = "";
		$attachData = "";

		foreach ($dealerVisits as $dealerVisitsRow)
		{
			$firstLine = "";

			foreach ($dealerVisitsRow as $key => $value)
			{
				$firstLine = $firstLine . $key . ";";
				$attachData = $attachData . $value . ";";
			}

			$firstLine = $firstLine . "\r\n";
			$attachData = $attachData . "\r\n";
		}

		$attachData = $firstLine . $attachData;

		$emailData = array(
			'emailReceivers' => $emailReceivers,
			'emailSubject' => 'Overzicht dealer bezoeken ' . ($dateFrom -> format('d-m-Y') == $dateTo -> format('d-m-Y') ? $dateFrom -> format('d-m-Y') : $dateFrom -> format('d-m-Y') . ' t/m ' . $dateTo -> format('d-m-Y')),
			'emailFrom' => array('dashboard@enra.eu' => 'Dashboard ENRA'),
			'emailBody' => count($dealerVisits) . " dealers bezoeken gevonden",
			'emailAttachment' => $attachData,
			'emailAttachmentName' => 'DealerBezoeken_' . ($dateFrom -> format('Ymd') == $dateTo -> format('Ymd') ? $dateFrom -> format('Ymd') : $dateFrom -> format('Ymd') . '_' . $dateTo -> format('Ymd')) . '.csv',
			'notificationData' => array('dealerVisits' => $dealerVisits)
		);

		Mail::send(array('html' => 'emails.notification.dealervisits'), array('emailData' => $emailData), function($message) use ($emailData)
		{
			$message -> from($emailData['emailFrom']) -> to($emailData['emailReceivers']) -> subject($emailData['emailSubject']) -> attachData($emailData['emailAttachment'], $emailData['emailAttachmentName']);
		});

		$this -> info('Command DealerVisit_Notification has run, Dealer count : ' . count($dealerVisits));

		//Log::info('Command DealerVisit_Notification has run', array(
		//	'option' => $this -> option('period'),
		//	'emailData' => $emailData
		//));
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array( array(
				'period',
				null,
				InputOption::VALUE_OPTIONAL,
				'Wich period? Day, Week, Month, Year.',
				null
			), );
	}

}
