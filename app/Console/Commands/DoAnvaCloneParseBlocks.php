<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use App\Http\Controllers\WebToolsController;
use Session;
use Config;
use App;

use Illuminate\Foundation\Inspiring;
use Illuminate\Contracts\Bus\SelfHandling;

class DoAnvaCloneParseBlocks extends Command
{
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:Do_AnvaCloneParseBlocks';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'DoAnvaCloneParseBlocks will parse a table for BLOCKS.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
        $webToolsController = new WebToolsController();

        $cloneObjects = array(
            "data3" => array(
                "key" => "data3",
                "tables" => array(
                    "NAWBES",
                    "POLBES",
                    "AGBES",
                    "WACBES",
                    "DEKKING",
                    "NAWHIS",
                    "POLSCH",
                    "FACTUUR",
                    "DEKHIS",
                    "POLHIS"
                )
            ),
            "data1" => array(
                "key" => "data1",
                "tables" => array(
                    "NAWBES",
                    "POLBES",
                    "AGBES",
                    "WACBES",
                    "DEKKING",
                    "NAWHIS",
                    "POLSCH",
                    "FACTUUR",
                    "DEKHIS",
                    "POLHIS"
                ),
            )
        );

        if($this->option('dataset') != '')
        {
            $cloneObjects = array($cloneObjects[$this->option('dataset')]);
        }

        foreach ($cloneObjects as $cloneObject)
		{
			foreach ($cloneObject['tables'] as $anvaTableName)
			{
				$webToolsController -> doAnvaCloneParseBlocks($cloneObject['key'], $anvaTableName);
            }
        }

        return 'done!';
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array( array(
            'dataset',
            null,
            InputOption::VALUE_OPTIONAL,
            'Wich Dataset? data1, data3',
            null
        ), );
	}

}
