<?php

namespace App\Console\Commands;

use DB;
use App\Models\Client;
use Illuminate\Console\Command;
use App\Jobs\UpdateClientCoordinatesJob;

class UpdateAllClientsCoordinates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-all-clients-coordinates {--limit=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all clients coordinates';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $AnvaCloneData = array(
			'AnvaCloneData1' => env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'),
			'AnvaCloneData3' => env('DB_DATABASE_ANVACLONE_DATA3', 'AnvaCloneData3'),
		);

		$sqlQuery = "";

		foreach ($AnvaCloneData as $AnvaCloneDataValue) {

			$sqlQuery .= "
				SELECT 
					clients.id AS id,
					AnvaCloneData.AGENT.AGE_AGENTNR AS agentnr,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55023) != '', AnvaCloneData.AGBES.AGB_55023, TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_VOORLETTERS, AnvaCloneData.AGENT.AGE_VOORVOEGSELS, AnvaCloneData.AGENT.AGE_NAAM))) AS name,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55016) != '', AnvaCloneData.AGBES.AGB_55016, AnvaCloneData.AGENT.AGE_STRAAT) AS street,
					IF(TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGBES.AGB_55017, AnvaCloneData.AGBES.AGB_55018))) != '', TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGBES.AGB_55017, AnvaCloneData.AGBES.AGB_55018))), TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_HUISNR, AnvaCloneData.AGENT.AGE_TOEVOEGING)))) AS housenumber,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55019) != '', AnvaCloneData.AGBES.AGB_55019, TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_POSTCODE, AnvaCloneData.AGENT.AGE_BUITENL_POSTCODE))) AS zipcode,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55020) != '', AnvaCloneData.AGBES.AGB_55020, AnvaCloneData.AGENT.AGE_PLAATS) AS city,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55021) != '', AnvaCloneData.AGBES.AGB_55021, AnvaCloneData.AGENT.AGE_LANDCODE) AS country,
					AnvaCloneData.AGBES.AGB_55004 AS website,
					AnvaCloneData.AGBES.AGB_55015 AS visible_on_website,
					AnvaCloneData.AGENT.AGE_TELNR AS phonenumber,
					AnvaCloneData.AGENT.AGE_FAXNR AS faxnumber,
					clients.visiting_longitude AS longitude,
					clients.visiting_latitude AS latitude,
       				clients.updated_at
				FROM clients
				LEFT JOIN AnvaCloneData.AGENT ON (AnvaCloneData.AGENT.AGE_AGENTNR = clients.anva_agentnr)
				LEFT JOIN AnvaCloneData.AGBES ON (AnvaCloneData.AGBES.AGB_AGENTNR = clients.anva_agentnr)
				WHERE AnvaCloneData.AGENT.AGE_AGENTNR IS NOT NULL
				AND AnvaCloneData.AGENT.AGE_SW_GEDEACTIVEERD != 'J'

				#AND (TRIM(clients.visiting_longitude) = '' OR TRIM(clients.visiting_latitude) = '' OR TRIM(clients.visiting_longitude) = 'NOT FOUND' OR TRIM(clients.visiting_latitude) = 'NOT FOUND')
				#AND (clients.anva_agentnr < '30000' OR clients.country != 'de')
				AND clients.deleted_at IS NULL
                AND DATE(`updated_at`) != CURDATE()
			";

			$sqlQuery  = str_replace("AnvaCloneData.", $AnvaCloneDataValue . ".", $sqlQuery);

			if(end($AnvaCloneData) != $AnvaCloneDataValue)
			{
				$sqlQuery .= " UNION ";
			}
		}

		$sqlQuery .= " ORDER BY updated_at ";

        if ($this->option('limit')) {
            $sqlQuery .= ' LIMIT ' . $this->option('limit');
        }

        $clients = collect(DB::select($sqlQuery));

        $this->comment("Updating {$clients->count()} clients");

        foreach ($clients as $client) {
            // The field is a boolean but it's not filled in for every client.
            // So if it's not filled in we assume it must be visible.
            if (strtolower($client->visible_on_website) === 'j' || empty($client->visible_on_website)) {
                dispatch(new UpdateClientCoordinatesJob($client));
            } else {
                $agent = Client::findOrFail($client->id);
                $agent->delete();
            }
        }

        return true;
    }
}
