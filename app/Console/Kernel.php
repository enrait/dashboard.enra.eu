<?php

namespace App\Console;

use App\Console\Commands\CopyAgentBrancheData;
use App\Console\Commands\CopyAgentGroupedBrancheData;
use App\Console\Commands\CopyEnra4AllPolicySearch;
use App\Console\Commands\CopyPolCountData;
use App\Console\Commands\CreateW2DSearchPolis;
use App\Console\Commands\CreateW2DSearchRelatie;
use App\Console\Commands\DealerVisitNotificationCommand;
use App\Console\Commands\DoAnvaCloneParseBlocks;
use App\Console\Commands\Inspire;
use App\Console\Commands\PrefillCacheCommand;
use App\Console\Commands\UpdateAllClientsCoordinates;
use App\Console\Commands\UpdateClientGeoDataCommand;
use App\Services\Indexing\DbCopy\PolicyCopy;
use App\Services\Sentry\Monitor;
use App\Services\TextReplacer\Replacers\PolBlockReplacer;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Inspire::class,
        DealerVisitNotificationCommand::class,
        PrefillCacheCommand::class,
        UpdateClientGeoDataCommand::class,
        CreateW2DSearchPolis::class,
        CreateW2DSearchRelatie::class,
        DoAnvaCloneParseBlocks::class,
        UpdateAllClientsCoordinates::class,
        CopyAgentGroupedBrancheData::class,
        CopyAgentBrancheData::class,
        CopyPolCountData::class,
        CopyEnra4AllPolicySearch::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        Monitor::process($schedule->command('command:Prefill_Cache')->dailyAt('01:00'), 'c7124c50-bb48-4757-a8e7-e32e0042ab19');
        Monitor::process($schedule->command('command:DealerVisit_Notification --period=week')->weeklyOn(1, '00:01'), '4b70fb8c-1ff2-4f36-8f27-b9be75a94d3e');
        Monitor::process($schedule->command('command:Update_Client_Geo_Data')->dailyAt('05:00'), '16de2735-0a76-46d3-bc8f-cf12f4a62d98');

        Monitor::process($schedule->command('import:CopyAgentBranche')->dailyAt('03:00'), '0804bc81-47df-4c0c-979d-7529aa523c97');
        Monitor::process($schedule->command('import:CopyAgentGroupedBranche')->dailyAt('04:00'), '14a4a454-907b-4a4b-ab4d-bb1438cf7b6a');

        # Daily ANVA Clone Parse Blocks
        Monitor::process($schedule->command('command:Do_AnvaCloneParseBlocks --dataset=data1')->dailyAt('05:00'), '9b2390d6-003a-49b5-be14-8f066ca8571f');
        Monitor::process($schedule->command('command:Do_AnvaCloneParseBlocks --dataset=data3')->dailyAt('22:00'), '62e6a6db-c7bb-4432-ab66-045b4e2b5ae4');
    }
}
