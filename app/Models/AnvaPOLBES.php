<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Cartalyst\Sentinel\Sentinel;
use Debugbar;

class AnvaPOLBES extends Model
{
    protected $connection = 'mysql_anvaclone';
    protected $table = "POLBES";
    protected $primaryKey = 'id';
    protected $hidden = array();

    public static function getList()
    {
        $sqlTable = Cache::tags('POLBES')->remember(CacheHelpers::getCacheKey('AnvaPOLBES::get'), Config::get('cache.duration'), function () {
            return AnvaPOLBES::get();
        });

        return $sqlTable;
    }

    public static function getAgentAmountSqlQuery($axY, array $filter = [])
    {
        switch ($axY) {
            case 'Coverage' :
                $sqlGroupBy = ", POL_ING_MAAND";
                // 20151005 DTa do not group because WA and Casco are two records while we want to count only 1
                $sqlOrderBy = ", POL_ING_MAAND";

                break;

            case 'Duration' :
                $sqlGroupBy = ", POL_LOOPTIJD";
                $sqlOrderBy = ", POL_LOOPTIJD";

                break;

            case 'Month' :
            default :
                $sqlGroupBy = ", POL_ING_MAAND";
                $sqlOrderBy = ", POL_ING_MAAND";

                break;
        }

        $sqlQueryWhereArray = array();
        $sqlQueryWhere = "";

        $sqlQueryWhereArray[] = "WACBES.WAC_RELNR IS NULL";

        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['year'], 'SUBSTRING(POLBES.POL_ING_DATUM, 1, 4)');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['month'], 'SUBSTRING(POLBES.POL_ING_DATUM, 5, 2) ');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['country'], 'POLBES.country');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['region'], 'AGBES.AGB_55002');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['company'], 'POLBES.POL_MIJNR');
        //$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['branch'], 'POLBES.POL_BRANCHE_SPEC');
        //$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['organisation'], 'POLBES.POL_PRODUCENT');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['agent'], 'POLBES.POL_AGENT_1');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['coverage'], 'DEKKING.DEK_DEKKING');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['duration'], 'POLBES.POL_LOOPTIJD');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['classification'], 'AGBES.AGB_55034');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['potential'], 'AGBES.AGB_55035');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['weightingfactor'], 'AGBES.AGB_55036');

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        $sqlQuery = "
            SELECT SUBSTRING(POL_ING_DATUM, 1, 4) AS POL_ING_JAAR,
                   SUBSTRING(POL_ING_DATUM, 5, 2) AS POL_ING_MAAND,
                   POL_BRANCHE_SPEC,
                   POL_80000,
                   POL_AGENT_1,
                   DEKKING.DEK_DEKKING AS POL_DEKKING,
                   POL_LOOPTIJD,
                   SUM(IF(INSTR(DEK_PREMIE_NETTO, '-') != 0, DEK_PREMIE_NETTO*-1/100, DEK_PREMIE_NETTO*1/100) * (POL_LOOPTIJD / POL_TERMIJN)) AS PREMIE_TOTAAL,
                   COUNT(DISTINCT POLBES.POL_RELNR, POLBES.POL_VOLG, POLBES.POL_VOLG_SUB) AS POL_TOTAAL,
                   POLBES.POL_RELNR,
                   POLBES.POL_VOLG,
                   POLBES.POL_VOLG_SUB,
                   POLBES.country
            FROM POLBES
            LEFT JOIN WACBES                    ON (POLBES.POL_RELNR = WACBES.WAC_RELNR AND POLBES.POL_VOLG = WACBES.WAC_VOLG AND POLBES.POL_VOLG_SUB = WACBES.WAC_VOLG_SUB AND POLBES.country = WACBES.country)
            LEFT JOIN DEKKING AS DEKKING    ON (POLBES.POL_RELNR = DEKKING.DEK_RELNR AND POLBES.POL_VOLG = DEKKING.DEK_VOLG AND POLBES.POL_VOLG_SUB = DEKKING.DEK_VOLG_SUB AND POLBES.country = DEKKING.country)
            LEFT JOIN AGBES                     ON (POLBES.POL_AGENT_1 = AGBES.AGB_AGENTNR AND POLBES.country = AGBES.country)
            {$sqlQueryWhere}
            GROUP BY POLBES.country,
                     POL_AGENT_1,
                     POL_BRANCHE_SPEC,
                     POL_80000,
                     POL_RELNR,
                     POL_VOLG,
                     POL_VOLG_SUB,
                     POL_ING_JAAR
                     {$sqlGroupBy}
            ORDER BY POL_ING_JAAR
                     {$sqlOrderBy}
            ";
        return $sqlQuery;
    }

    public static function getAgentAmount($sqlQuery, array $filter = array())
    {
        $sqlTable = Cache::tags(['POLBES', 'WACBES', 'DEKKING_MAX', 'AGBES'])->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function () use ($sqlQuery) {
            return DB::connection('mysql_anvaclone')->select($sqlQuery);

        });
        $sqlTable['filter'] = $filter;
        return $sqlTable;
    }


}
