<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Cache;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use Config;
use Sentinel;
use Debugbar;

class AnvaPOLSCH extends Model
{
	protected $connection = 'mysql_anvaclone';
	protected $table = "POLSCH";
	protected $primaryKey = 'id';
	protected $hidden = array();

	public static function getList()
	{
		$sqlTable = Cache::tags('POLSCH')->remember(CacheHelpers::getCacheKey('AnvaPOLSCH::get'), Config::get('cache.duration'), function()
		{
			return AnvaPOLSCH::get();
		});
		
		return $sqlTable;
	}

	public static function getAgentDamageSum(array $filter = array())
	{
		$sqlQueryWhereArray = array();
		$sqlQueryWhere = "";

		$sqlQueryWhereArray[] = "(SCHADE.SCHADESOORT IS NOT NULL)";

		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['year'], 'SUBSTRING(ALGSCH01.SC1_KEY_02, 1, 4)');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['month'], 'SUBSTRING(ALGSCH01.SC1_KEY_02, 5, 2)');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['country'], 'POLSCH.country');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['region'], 'AGBES.AGB_55002');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['company'], 'POLBES.POL_MIJNR');
		//$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['branch'], 'POLSCH.PLS_BRANCHE_SPEC');
		//$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['organisation'], 'POLBES.POL_PRODUCENT');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['agent'], 'POLSCH.PLS_AGENT_1');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['coverage'], 'DEKKING.DEK_DEKKING');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['duration'], 'POLBES.POL_LOOPTIJD');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['damagetype'], 'SCHADESOORT');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['classification'], 'AGBES.AGB_55034');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['potential'], 'AGBES.AGB_55035');
		$sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['weightingfactor'], 'AGBES.AGB_55036');

		$sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

		$sqlQuery = "
            SELECT  POLSCH.PLS_AGENT_1 AS AGENTNR,
                    SCHADE.SCHADESOORT AS SCHADESOORT,
                    SUBSTRING(ALGSCH01.SC1_KEY_02, 1, 4) AS JAAR,
                    SUBSTRING(ALGSCH01.SC1_KEY_02, 5, 2) AS MAAND,
                    ROUND(SUM(IF(INSTR(SCHADE.TOTAAL_BEDRAG, '-') != 0, SCHADE.TOTAAL_BEDRAG*1/100, SCHADE.TOTAAL_BEDRAG*-1/100)), 2) AS SCHADEBEDRAG,
                    COUNT(DISTINCT POLSCH.PLS_SCHADENR) AS TOTAAL,
                    POLSCH.PLS_BRANCHE_SPEC
            FROM    POLSCH
            LEFT JOIN ALGSCH01 ON (ALGSCH01.SC1_SCHADENR = POLSCH.PLS_SCHADENR AND ALGSCH01.SC1_SCHADESUBNR = POLSCH.PLS_SCHADESUBNR AND ALGSCH01.country = POLSCH.country)
            LEFT JOIN 
                (
                    SELECT BETSCH01.country AS country, BETSCH01.BE1_SCHADENR AS SCHADENR, BETSCH01.BE1_SCHADESUBNR AS SCHADESUBNR, BETSCH01.BE1_TOTAAL_BEDRAG AS TOTAAL_BEDRAG, BETSCH01.BE1_SCHADESOORT AS SCHADESOORT
                        FROM BETSCH01
                    UNION ALL
                        SELECT VRHSCH01.country AS country, VRHSCH01.VH1_SCHADENR AS SCHADENR, VRHSCH01.VH1_SCHADESUBNR AS SCHADESUBNR, VRHSCH01.VH1_TOTAAL_BEDRAG AS TOTAAL_BEDRAG, VRHSCH01.VH1_SCHADESOORT AS SCHADESOORT
                        FROM VRHSCH01
                ) AS SCHADE                     ON (SCHADE.SCHADENR = POLSCH.PLS_SCHADENR AND SCHADE.SCHADESUBNR = POLSCH.PLS_SCHADESUBNR AND SCHADE.country = POLSCH.country)
            LEFT JOIN POLBES                    ON (POLBES.POL_POLNR = POLSCH.PLS_POLNR AND POLBES.country = POLSCH.country)
            LEFT JOIN DEKKING_MAX AS DEKKING    ON (POLBES.POL_RELNR = DEKKING.DEK_RELNR AND POLBES.POL_VOLG = DEKKING.DEK_VOLG AND POLBES.POL_VOLG_SUB = DEKKING.DEK_VOLG_SUB AND POLBES.country = DEKKING.country)
            LEFT JOIN AGBES                     ON (POLBES.POL_AGENT_1 = AGBES.AGB_AGENTNR AND POLBES.country = AGBES.country)
            {$sqlQueryWhere}
            GROUP BY POLSCH.PLS_AGENT_1,
                        POLSCH.PLS_BRANCHE_SPEC,
                        POLBES.POL_80000,
                        JAAR,
                        SCHADESOORT
            ORDER BY AGENTNR,
                        JAAR,
                        MAAND
        ";
        
        //Debugbar::info('$sqlQuery : ' . $sqlQuery);
		
		$sqlTable = Cache::tags(['POLSCH', 'ALGSCH01', 'BETSCH01', 'VRHSCH01', 'POLBES', 'DEKKING_MAX', 'AGBES'])->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function() use ($sqlQuery)
		{
			return DB::connection('mysql_anvaclone') -> select($sqlQuery);
		});	

		$sqlTable['filter'] = $filter;

		return $sqlTable;
	}

}
