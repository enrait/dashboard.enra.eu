<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Cache;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use Config;
use Sentinel;
use Debugbar;

class AnvaFACTUUR extends Model
{
    protected $connection = 'mysql_anvaclone';
    protected $table = "FACTUUR";
    protected $primaryKey = 'id';
    protected $hidden = array();

    public static function getList()
    {
        $sqlTable = Cache::tags('FACTUUR')->remember(CacheHelpers::getCacheKey('AnvaFACTUUR::get'), Config::get('cache.duration'), function () {
            return AnvaFACTUUR::get();
        });

        return $sqlTable;
    }

    public static function getAgentCommissionSum(array $filter = array())
    {
        $sqlQueryWhereArray = array();
        $sqlQueryWhere = "";

        $sqlQueryWhereArray[] = "(FACTUUR.FAC_SOORT = '00010' OR FACTUUR.FAC_SOORT = '00020')";

        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['year'], 'SUBSTRING(FACTUUR.FAC_BOEK_DATUM, 1, 4)');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['month'], 'SUBSTRING(FACTUUR.FAC_BOEK_DATUM, 5, 2)');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['country'], 'FACTUUR.country');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['company'], 'FACTUUR.FAC_POLIS_MIJ');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['branch'], 'FACTUUR.FAC_BRANCHE');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['organisation'], 'POLBES.POL_PRODUCENT');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['agent'], 'FACTUUR.FAC_AGENT_1');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['coverage'], 'DEKKING.DEK_DEKKING');
        $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($filter['duration'], 'POLBES.POL_LOOPTIJD');

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        $sqlQuery = "
            SELECT
            	SUBSTRING(FACTUUR.FAC_BOEK_DATUM, 1, 4) AS JAAR,
				ROUND(SUM(IF(INSTR(FACTUUR.FAC_AGENT_1_PROV, '-') != 0, FACTUUR.FAC_AGENT_1_PROV*1/100, FACTUUR.FAC_AGENT_1_PROV*-1/100)), 2) AS PROVISIE,
				MAX(DEKKING.DEK_DEKKING) AS DEKKING_MAX,
				FACTUUR.FAC_POLNR AS POLISNR,
				FACTUUR.id AS ID,
				FACTUUR.FAC_SOORT AS SOORT,
				POLBES.POL_10037,
				FACTUUR.FAC_BRANCHE AS BRANCHE
			FROM FACTUUR
            LEFT JOIN POLBES                    ON (FACTUUR.FAC_POLNR = POLBES.POL_POLNR AND POLBES.POL_POLNR != '')
            LEFT JOIN DEKKING_MAX AS DEKKING    ON (POLBES.POL_RELNR = DEKKING.DEK_RELNR AND POLBES.POL_VOLG = DEKKING.DEK_VOLG AND POLBES.POL_VOLG_SUB = DEKKING.DEK_VOLG_SUB AND POLBES.country = DEKKING.country)
            LEFT JOIN AGBES                     ON (POLBES.POL_AGENT_1 = AGBES.AGB_AGENTNR AND POLBES.country = AGBES.country)
            {$sqlQueryWhere}
            GROUP BY JAAR
			ORDER BY JAAR
        ";

        $sqlTable = Cache::tags(['FACTUUR', 'POLBES', 'DEKKING_MAX', 'AGBES'])->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function () use ($sqlQuery) {
            return DB::connection('mysql_anvaclone')->select($sqlQuery);
        });

        $sqlTable['filter'] = $filter;

        return $sqlTable;
    }

}
