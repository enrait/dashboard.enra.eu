<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Support\Database\CacheQueryBuilder;
use DB;
use Cache;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use Config;
use Sentinel;

class AnvaAGENT extends Model
{
    protected $connection = 'mysql_anvaclone';
    protected $table = "AGENT";
    protected $primaryKey = 'id';
    protected $hidden = array();

    public static function getList()
    {
        $sqlTable = Cache::tags('AGENT')->remember(CacheHelpers::getCacheKey('AnvaAGENT::get'), Config::get('cache.duration'), function () {
            return AnvaAGENT::get();
        });

        return $sqlTable;
    }

    public static function getAgent($agentNr)
    {
        $sqlQueryWhereArray = array();
        $sqlQueryWhere = "";

        $sqlQueryWhereArray[] = "AGE_AGENTNR = '$agentNr'";

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        $sqlQuery = "
            SELECT  *
            FROM AGENT
            LEFT JOIN AGENT2 ON (AGENT2.AGE2_AGENTNR = AGENT.AGE_AGENTNR AND AGENT2.country = AGENT.country)
            LEFT JOIN IBANINT ON (IBANINT.IBANINT_INTERNNR_N = AGENT.AGE_BANKNR AND IBANINT.country = AGENT.country)
            {$sqlQueryWhere}
        ";

        $sqlTable = Cache::tags(['AGENT', 'AGENT2', 'IBANINT'])->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function () use ($sqlQuery) {
            return DB::connection('mysql_anvaclone')->select($sqlQuery);
        });

        return $sqlTable;
    }

    public static function getTypeaheadSearch($searchTerm)
    {
        $sqlQueryWhereArray = array();
        $sqlQueryWhere = "";

        if (array_key_exists('ZipcodeFrom', $searchTerm) && array_key_exists('ZipcodeTo', $searchTerm) && $searchTerm['ZipcodeFrom'] && $searchTerm['ZipcodeTo']) {
            $searchTermZipcodeFrom = trim($searchTerm['ZipcodeFrom']);
            $searchTermZipcodeTo = trim($searchTerm['ZipcodeTo']);

            $sqlQueryWhereArray[] = "( AGENT.AGE_POSTCODE BETWEEN '$searchTermZipcodeFrom' AND '$searchTermZipcodeTo' OR ";
            $sqlQueryWhereArray[] = "AGENT.AGE_BUITENL_POSTCODE BETWEEN '$searchTermZipcodeFrom' AND '$searchTermZipcodeTo' )";
        }

        if (array_key_exists('Typeahead', $searchTerm) && $searchTerm['Typeahead']) {
            $searchTermTypeahead = trim($searchTerm['Typeahead']);

            $sqlQueryWhereArray[] = "( AGENT.AGE_AGENTNR LIKE '%$searchTermTypeahead%' OR ";
            $sqlQueryWhereArray[] = "AGENT.AGE_NAAM LIKE '%$searchTermTypeahead%' OR ";
            $sqlQueryWhereArray[] = "AGENT.AGE_STRAAT LIKE '%$searchTermTypeahead%' OR ";
            $sqlQueryWhereArray[] = "AGENT.AGE_POSTCODE LIKE '%$searchTermTypeahead%' OR ";
            $sqlQueryWhereArray[] = "AGENT.AGE_BUITENL_POSTCODE LIKE '%$searchTermTypeahead%' OR ";
            $sqlQueryWhereArray[] = "AGENT.AGE_PLAATS LIKE '%$searchTermTypeahead%' OR ";
            $sqlQueryWhereArray[] = "CONCAT(AGENT2.AGE2_EMAILADRES, AGENT2.AGE2_EMAILADRES_REST) LIKE '%$searchTermTypeahead%' OR ";
            $sqlQueryWhereArray[] = "AGENT2.AGE2_EMAILADRES_PBI LIKE '%$searchTermTypeahead%' )";
        }

        if (array_key_exists('Iban', $searchTerm) && $searchTerm['Iban']) {
            $searchIban = trim($searchTerm['Iban']);

            $sqlQueryWhereArray[] = "( IBANINT.IBANINT_IBAN LIKE '%$searchIban%' )";
        }

        if (array_key_exists('Organisation', $searchTerm) && $searchTerm['Organisation']) {
            $searchOrganisation = trim($searchTerm['Organisation']);

            $sqlQueryWhereArray[] = "( AGBES.AGB_55000 = '$searchOrganisation' )";
            $sqlQueryWhereArray[] = "( AGBES.AGB_55001 != '')";
        }

        if (array_key_exists('InvoiceId', $searchTerm) && $searchTerm['InvoiceId']) {
            $searchInvoiceId = substr("00000000" . trim($searchTerm['InvoiceId']), strlen("00000000" . trim($searchTerm['InvoiceId'])) - 8);

            $sqlQueryWhereArray[] = "( RCAGBES.RAB_FACTUUR = '$searchInvoiceId' OR ";
            $sqlQueryWhereArray[] = "RCAGHIS.RAH_FACTUUR = '$searchInvoiceId' )";
        }

        if (array_key_exists('Country', $searchTerm) && $searchTerm['Country']) {
            $sqlQueryWhereArray[] = DatabaseHelpers::getSqlQueryWhereMultiValue($searchTerm['Country'], 'AGENT.country');
        }

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        $sqlEnraDashboard = Config::get('database.connections.mysql.database');

        $sqlQuery = "SELECT DISTINCT
            	    AGENT.AGE_AGENTNR,
            		AGENT.AGE_NAAM,
            		AGENT.AGE_PLAATS,
                    AGENT.AGE_STRAAT,
                    CONCAT(AGENT.AGE_POSTCODE,AGENT.AGE_BUITENL_POSTCODE) AS AGE_POSTCODE,
                    CASE WHEN AGENT2.AGE2_EMAILADRES = '' THEN AGENT2.AGE2_EMAILADRES_PBI ELSE CONCAT(AGENT2.AGE2_EMAILADRES, AGENT2.AGE2_EMAILADRES_REST) END AS AGE2_EMAILADRES,
        ";

        if (array_key_exists('InvoiceId', $searchTerm) && $searchTerm['InvoiceId']) {
            $sqlQuery = $sqlQuery . "            CASE WHEN RCAGHIS.RAH_FACTUUR IS NULL THEN RCAGBES.RAB_FACTUUR ELSE RCAGHIS.RAH_FACTUUR END AS RAH_FACTUUR,";
            $sqlQuery = $sqlQuery . "            CASE WHEN RCAGHIS.RAH_INCASSO IS NULL THEN RCAGBES.RAB_INCASSO ELSE RCAGHIS.RAH_INCASSO END AS RAH_INCASSO,";
        }

        $sqlQuery = $sqlQuery . "            IBANINT.IBANINT_IBAN,
        	{$sqlEnraDashboard}.clients.id AS clientId
            FROM AGENT
            LEFT JOIN AGENT2 ON (AGENT2.AGE2_AGENTNR = AGENT.AGE_AGENTNR AND AGENT2.country = AGENT.country)
            LEFT JOIN IBANINT ON (IBANINT.IBANINT_INTERNNR_N = AGENT.AGE_BANKNR AND IBANINT.country = AGENT.country)
						LEFT JOIN AGBES on (AGENT.AGE_AGENTNR = AGBES.AGB_AGENTNR)
			LEFT JOIN {$sqlEnraDashboard}.clients ON ({$sqlEnraDashboard}.clients.anva_agentnr = AGENT.AGE_AGENTNR)
        ";

        if (array_key_exists('InvoiceId', $searchTerm) && $searchTerm['InvoiceId']) {
            $sqlQuery = $sqlQuery . " LEFT JOIN RCAGBES ON (RCAGBES.RAB_AGENTNR = AGENT.AGE_AGENTNR AND RCAGBES.country = AGENT.country AND RCAGBES.RAB_FACTUUR = '$searchInvoiceId')";
            $sqlQuery = $sqlQuery . " LEFT JOIN RCAGHIS ON (RCAGHIS.RAH_AGENTNR = AGENT.AGE_AGENTNR AND RCAGHIS.country = AGENT.country AND RCAGHIS.RAH_FACTUUR = '$searchInvoiceId')";
        }

        $sqlQuery = $sqlQuery . " " . $sqlQueryWhere;

        $sqlTable = Cache::tags(['AGENT', 'AGENT2', 'IBANINT', 'RCAGBES', 'RCAGHIS'])->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function () use ($sqlQuery) {
            return DB::connection('mysql_anvaclone')->select($sqlQuery);
        });

        return $sqlTable;
    }

}
