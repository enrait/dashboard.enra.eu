<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Cache;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use Config;
use Sentinel;
use Debugbar;

class AnvaRCAGBES extends Model
{
    protected $connection = 'mysql_anvaclone';
    protected $table = "RCAGBES";
    protected $primaryKey = 'id';
    protected $hidden = array();

    public static function getList()
    {
        $sqlTable = Cache::tags('RCAGBES')->remember(CacheHelpers::getCacheKey('AnvaRCAGBES::get'), Config::get('cache.duration'), function () {
            return AnvaRCAGBES::get();
        });

        return $sqlTable;
    }
}
