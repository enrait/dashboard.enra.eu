<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use DB;
use Cache;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use App\Http\Controllers\FilterController;
use Config;
use Sentinel;

class Client extends Model
{
	protected $table = "clients";
	protected $primaryKey = 'id';
	protected $hidden = array();

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

	use SoftDeletes;

	protected $dates = ['deleted_at'];

	public static function getList()
	{
		$sqlTable = Cache::tags('clients')->remember(CacheHelpers::getCacheKey('Client::get'), Config::get('cache.duration'), function()
		{
			return Client::get();
		});

		return $sqlTable;
	}

	public function hasAttribute($attr)
	{
		return array_key_exists($attr, $this -> attributes);
	}

	public static function getClientsBySelectData($selectData)
	{
		$sqlQueryWhereArray = array();
		$sqlQueryWhere = "";

		$sqlAnvaClone = Config::get('database.connections.mysql_anvaclone.database');

		if (count($selectData['agent']['selected']) > 0)
		{
			$sqlQueryWhereArray[] = "{$sqlAnvaClone}.AGENT.AGE_AGENTNR IN ('" . implode("', '", $selectData['agent']['selected']) . "')";

			if (count($selectData['country']['selected']) > 0)
			{
				$sqlQueryWhereArray[] = "{$sqlAnvaClone}.AGENT.country IN ('" . implode("', '", $selectData['country']['selected']) . "')";
			}

			$sqlQueryWhereArray[] = "({$sqlAnvaClone}.AGENT.AGE_AGENTNR < '30000' OR {$sqlAnvaClone}.AGENT.country != 'de')";
		}
		else
		{
			if (count($selectData['client']['selected']) > 0)
			{
				$sqlQueryWhereArray[] = "`clients`.`id` IN ('" . implode("', '", $selectData['client']['selected']) . "')";
			}

			if (count($selectData['country']['selected']) > 0)
			{
				$sqlQueryWhereArray[] = "`clients`.`country` IN ('" . implode("', '", $selectData['country']['selected']) . "')";
			}

			$sqlQueryWhereArray[] = "`clients`.`deleted_at` is null";

			$sqlQueryWhereArray[] = "(`clients`.`anva_agentnr` < '30000' OR `clients`.`country` != 'de')";
		}

		$sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

		// if (Sentinel::hasAccess(['country.NL']))
		// {
			// $filter['country'][] = 'nl';
		// }
//
		// if (Sentinel::hasAccess(['country.DE']))
		// {
			// $filter['country'][] = 'de';
		// }
//
		// if (Sentinel::hasAccess(['country.BE']))
		// {
			// $filter['country'][] = 'be';
		// }

		$sqlQuery = "
            SELECT clients.*,
            IFNULL({$sqlAnvaClone}.AGENT.AGE_AGENTNR, '') AS anva_agentnr,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_SW_GEDEACTIVEERD, clients.`status`) AS `status2`,
			IF({$sqlAnvaClone}.AGENT.AGE_NAAM IS NULL, clients.name, TRIM(CONCAT_WS(' ', {$sqlAnvaClone}.AGENT.AGE_VOORLETTERS, {$sqlAnvaClone}.AGENT.AGE_VOORVOEGSELS, {$sqlAnvaClone}.AGENT.AGE_NAAM))) AS name,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_STRAAT, clients.visiting_street) AS visiting_street,
			IF({$sqlAnvaClone}.AGENT.AGE_HUISNR IS NULL, clients.visiting_housenumber, TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', {$sqlAnvaClone}.AGENT.AGE_HUISNR, {$sqlAnvaClone}.AGENT.AGE_TOEVOEGING)))) AS visiting_housenumber,
			IF({$sqlAnvaClone}.AGENT.AGE_POSTCODE IS NULL, clients.visiting_zipcode, TRIM(CONCAT_WS(' ', {$sqlAnvaClone}.AGENT.AGE_POSTCODE, {$sqlAnvaClone}.AGENT.AGE_BUITENL_POSTCODE))) AS visiting_zipcode,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_PLAATS, clients.visiting_city) AS visiting_city,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_LANDCODE, clients.visiting_country) AS visiting_country,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_STRAAT_POST, clients.postal_street) AS postal_street,
			IF({$sqlAnvaClone}.AGENT.AGE_HUISNR_POST IS NULL, clients.postal_housenumber, TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', {$sqlAnvaClone}.AGENT.AGE_HUISNR_POST, {$sqlAnvaClone}.AGENT.AGE_TOEVOEGING_POST)))) AS postal_housenumber,
			IF({$sqlAnvaClone}.AGENT.AGE_POSTCODE_POST IS NULL, clients.postal_zipcode, TRIM(CONCAT_WS(' ', {$sqlAnvaClone}.AGENT.AGE_POSTCODE_POST, {$sqlAnvaClone}.AGENT.AGE_BUITENL_PC_POST))) AS postal_zipcode,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_PLAATS_POST, clients.postal_city) AS postal_city,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_LANDCODE_POST, clients.postal_country) AS postal_country,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_TELNR, clients.phonenumber) AS phonenumber,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_FAXNR, clients.faxnumber) AS faxnumber,
			IF({$sqlAnvaClone}.AGENT2.AGE2_EMAILADRES IS NULL, clients.email_address, CONCAT_WS('', {$sqlAnvaClone}.AGENT2.AGE2_EMAILADRES, {$sqlAnvaClone}.AGENT2.AGE2_EMAILADRES_REST)) AS email_address,
			IFNULL({$sqlAnvaClone}.AGBES.AGB_55004, clients.website) AS website,
			IFNULL({$sqlAnvaClone}.IBANINT.IBANINT_IBAN, clients.bankaccount) AS bankaccount,
			IFNULL({$sqlAnvaClone}.AGENT.AGE_HANDEL, clients.chamber_of_commerce_id) AS chamber_of_commerce_id,
			IFNULL({$sqlAnvaClone}.AGBES.AGB_55000, clients.organization_member) AS organization_member,
			IFNULL({$sqlAnvaClone}.AGBES.AGB_55001, clients.legal_form) AS legal_form,
			IF({$sqlAnvaClone}.AGBES.AGB_55002 = '' || {$sqlAnvaClone}.AGBES.AGB_55002 IS NULL, clients.region, {$sqlAnvaClone}.AGBES.AGB_55002) AS region,
			IFNULL({$sqlAnvaClone}.AGBES.AGB_55003, clients.shop_closed) AS shop_closed,

			IF({$sqlAnvaClone}.BTRBAS01.BB1_VOOR IS NULL, clients.contact_first_name, TRIM(CONCAT_WS(' ', {$sqlAnvaClone}.BTRBAS01.BB1_VOOR, IF({$sqlAnvaClone}.BTRBAS01.BB1_ROEPNAAM <> '', CONCAT_WS('', '(', {$sqlAnvaClone}.BTRBAS01.BB1_ROEPNAAM, ')'), '')))) AS contact_first_name,
			IF({$sqlAnvaClone}.BTRBAS01.BB1_NAAM IS NULL, clients.contact_last_name, TRIM(CONCAT_WS(' ', {$sqlAnvaClone}.BTRBAS01.BB1_VOORVOEGSELS, {$sqlAnvaClone}.BTRBAS01.BB1_NAAM))) AS contact_last_name,
			IFNULL({$sqlAnvaClone}.BTRBAS01.BB1_FUNCTIE, clients.contact_jobtitle) AS contact_jobtitle,
			IF({$sqlAnvaClone}.BTRBAS01.BB1_TELEFOON IS NULL, clients.contact_phonenumber, TRIM(CONCAT_WS('', {$sqlAnvaClone}.BTRBAS01.BB1_TELEFOON, {$sqlAnvaClone}.BTRBAS01.BB1_POST_TELEFOON))) AS contact_phonenumber,
			IFNULL({$sqlAnvaClone}.BTRBAS01.BB1_EMAILADRES, clients.contact_email_address) AS contact_email_address,
		";

		if($sqlAnvaClone == 'AnvaCloneData3')
		{
			$sqlQuery = $sqlQuery . "IFNULL({$sqlAnvaClone}.AGBES.AGB_55034, clients.qualification) AS qualification, ";
		}
		else
		{
			$sqlQuery = $sqlQuery . "'' AS qualification, ";
		}

		$sqlQuery = $sqlQuery . "
			IFNULL({$sqlAnvaClone}.AGENT.AGE_INGDAT, clients.start_date) AS start_date ";

		if (count($selectData['agent']['selected']) > 0)
		{
			$sqlQuery = $sqlQuery . "
				FROM {$sqlAnvaClone}.AGENT
				LEFT JOIN clients ON ({$sqlAnvaClone}.AGENT.AGE_AGENTNR = clients.anva_agentnr AND {$sqlAnvaClone}.AGENT.country = clients.country) ";
		}
		else
		{
			$sqlQuery = $sqlQuery . "
				FROM clients
				LEFT JOIN {$sqlAnvaClone}.AGENT ON ({$sqlAnvaClone}.AGENT.AGE_AGENTNR = clients.anva_agentnr AND {$sqlAnvaClone}.AGENT.country = clients.country) ";
		}

		$sqlQuery = $sqlQuery . "
			LEFT JOIN {$sqlAnvaClone}.AGENT2 ON ({$sqlAnvaClone}.AGENT2.AGE2_AGENTNR = {$sqlAnvaClone}.AGENT.AGE_AGENTNR AND {$sqlAnvaClone}.AGENT2.country = {$sqlAnvaClone}.AGENT.country)
			LEFT JOIN {$sqlAnvaClone}.AGBES ON ({$sqlAnvaClone}.AGBES.AGB_AGENTNR = {$sqlAnvaClone}.AGENT.AGE_AGENTNR AND {$sqlAnvaClone}.AGBES.country = {$sqlAnvaClone}.AGENT.country)
			LEFT JOIN {$sqlAnvaClone}.IBANINT ON ({$sqlAnvaClone}.IBANINT.IBANINT_INTERNNR_N = {$sqlAnvaClone}.AGENT.AGE_BANKNR AND {$sqlAnvaClone}.IBANINT.country = {$sqlAnvaClone}.AGENT.country)
			LEFT JOIN {$sqlAnvaClone}.BTRBAS02 ON ({$sqlAnvaClone}.BTRBAS02.BB2_RELNR = {$sqlAnvaClone}.AGENT.AGE_AGENTNR AND {$sqlAnvaClone}.BTRBAS02.country = {$sqlAnvaClone}.AGENT.country AND {$sqlAnvaClone}.BTRBAS02.BB2_SOORT_BETROKKENE = 'CON')
			LEFT JOIN {$sqlAnvaClone}.BTRBAS01 ON ({$sqlAnvaClone}.BTRBAS01.BB1_VOLGNR_BETROKKENE = {$sqlAnvaClone}.BTRBAS02.BB2_VOLGNR_BETROKKENE AND {$sqlAnvaClone}.BTRBAS01.country = {$sqlAnvaClone}.AGENT.country AND {$sqlAnvaClone}.BTRBAS01.BB1_SOORT_BETROKKENE = 'CON')
			{$sqlQueryWhere}
        ";

		//print_r($sqlQuery);

		info('getClientsBySelectData $sqlQuery : ' . $sqlQuery);

		//die();

		$sqlTable = Cache::tags(['clients', 'AGENT', 'AGENT2', 'IBANINT', 'AGBES'])->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function() use ($sqlQuery)
		{
			info('getClientsBySelectData $sqlQuery No Cache: ' . $sqlQuery);

			return collect(DB::select($sqlQuery));
	 	});
		// dd(DB::select($sqlQuery));
		return $sqlTable;
	}

	public function contactMoment()
	{
		return $this -> hasMany('App\Models\ContactMoment', 'id', 'client_id');
	}

}
