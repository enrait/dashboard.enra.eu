<?php

namespace App\Models;

use http\Client\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use DB;
use Sentinel;

class ContactMoment extends Model
{
    protected $table = "contact_moments";
    protected $primaryKey = 'id';
    protected $hidden = array();

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public static function getList()
    {
        $sqlTable = Cache::tags('contact_moments')->remember(CacheHelpers::getCacheKey('ContactMoment::get'), Config::get('cache.duration'), function () {
            return ContactMoment::get();
        });

        return $sqlTable;
    }

    public function hasAttribute($attr)
    {
        return array_key_exists($attr, $this->attributes);
    }

    public static function getContactMomentsBySelectData($selectData)
    {
        $sqlQueryWhereArray = array();
        $sqlQueryWhere = "";

        if (count($selectData['client']['selected']) > 0) {
            $sqlQueryWhereArray[] = "`clients`.`id` IN ('" . implode("', '", $selectData['client']['selected']) . "')";
        }

        if (count($selectData['country']['selected']) > 0) {
            $sqlQueryWhereArray[] = "`clients`.`country` IN ('" . implode("', '", $selectData['country']['selected']) . "')";
        }

        if (count($selectData['region']['selected']) > 0) {
            //$sqlQueryWhereArray[] = "`clients`.`region` IN ('" . implode("', '", $selectData['region']['selected']) . "')";
        }

        $sqlQueryWhereArray[] = "`contact_moments`.`deleted_at` is null";
        $sqlQueryWhereArray[] = "`clients`.`deleted_at` is null";

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        if (Sentinel::hasAccess(['country.NL'])) {
            $filter['country'][] = 'nl';
        }

        if (Sentinel::hasAccess(['country.DE'])) {
            $filter['country'][] = 'de';
        }

        if (Sentinel::hasAccess(['country.BE'])) {
            $filter['country'][] = 'be';
        }

        $sqlAnvaClone = Config::get('database.connections.mysql_anvaclone.database');

        $sqlQuery = "
            SELECT contact_moments.*,
            IFNULL({$sqlAnvaClone}.AGENT.AGE_AGENTNR, '') AS anva_agentnr,
            IFNULL({$sqlAnvaClone}.AGBES.AGB_55002, clients.region) AS region,
            CONCAT(users.first_name, ' ', users.last_name) AS user_name
			FROM contact_moments
			LEFT JOIN clients ON (clients.id = contact_moments.client_id)
			LEFT JOIN users ON (users.id = contact_moments.user_id)
			LEFT JOIN {$sqlAnvaClone}.AGENT ON ({$sqlAnvaClone}.AGENT.AGE_AGENTNR = clients.anva_agentnr AND {$sqlAnvaClone}.AGENT.country = clients.country)
			LEFT JOIN {$sqlAnvaClone}.AGBES ON ({$sqlAnvaClone}.AGBES.AGB_AGENTNR = {$sqlAnvaClone}.AGENT.AGE_AGENTNR AND {$sqlAnvaClone}.AGBES.country = {$sqlAnvaClone}.AGENT.country)
			{$sqlQueryWhere}
        ";


        $sqlTable = Cache::tags(['contact_moments', 'clients', 'users', 'AGENT', 'AGBES'])
            ->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function () use ($sqlQuery) {
                return collect(DB::select($sqlQuery));
            });
        return $sqlTable;
    }


    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id', 'id');
    }

}
