<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Cache;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use Config;
use Sentinel;

class AnvaREFBES extends Model
{
    protected $connection = 'mysql_anvaclone';
    protected $table = "REFBES";
    protected $primaryKey = 'id';
    protected $hidden = array();

    public static function getList()
    {
        $sqlTable = Cache::tags('REFBES')->remember(CacheHelpers::getCacheKey('AnvaREFBES::get'), Config::get('cache.duration'), function () {
            return AnvaREFBES::get();
        });

        return $sqlTable;
    }

    public static function getDescriptions($REF_RC, $country)
    {
        $sqlQueryWhereArray = array();
        $sqlQueryWhere = "";

        $sqlQueryWhereArray[] = "REF_RC = '$REF_RC'";
        $sqlQueryWhereArray[] = "country = '$country'";

        $sqlQueryWhere = DatabaseHelpers::getSqlQueryWhere($sqlQueryWhereArray);

        $sqlQuery = "
            SELECT REF_KEY_N, REF_OMSCHRIJVING
            FROM REFBES
            {$sqlQueryWhere}
        ";

        //die($sqlQuery);

        $sqlTable = Cache::tags(['REFBES'])->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function () use ($sqlQuery) {
            return DB::connection('mysql_anvaclone')->select($sqlQuery);
        });

        return $sqlTable;
    }

}
