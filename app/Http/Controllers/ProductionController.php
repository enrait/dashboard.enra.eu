<?php

namespace App\Http\Controllers;

use App\Services\Filters\Anva\Agents;
use App\Services\Filters\ProductData;
use Illuminate\Http\Request;
use App\Services\Filters\FilterData;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use App\Models\AnvaPOLBES;
use App\Models\AnvaFACTUUR;
use App\Models\AnvaPOLSCH;
use App;
use App\Libraries\StringHelpers;
use App\Libraries\CacheHelpers;
use Yajra\DataTables\Facades\DataTables;

class ProductionController extends Controller
{
    private $cachePrefix = 'ProductionController';
    private $roundDecimals = 0;

    private $filterData;
    private $productData;

    /**
     * Initializer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->filterData = new FilterData();
        $this->productData = new ProductData();
    }


    public function showProductionSummary(Request $request)
    {
        $selectData = FilterData::Request($request);
        return View('admin/production/production_summary', ['selectData' => $selectData]);
    }

    public function showProductionSummarySectionData(Request $request)
    {
        $productionData = $this->productData->prepareProductionSummarySectionData($request);

        return View('admin/production/production_summary_data_section', [
            'sectionsData' => $productionData['sectionsData'],
            'selectData' => $productionData['selectData'],
        ]);
    }

    public function showProductionSummaryPdf(Request $request)
    {
        $productionData = $this->productData->prepareProductionSummaryPdf($request);
        $agent = new AgentController;
        $pdfHTML = View('admin/production/pdfs/production_summary_pdf', [
            'agentData' => isset($filter['agent'][0]) ? $agent->getAgentJson($filter['agent'][0]) : '',
            'sectionsData' => $productionData['sectionsData'],
            'selectData' => $productionData['selectData']]);
        $pdf = PDF::loadHTML($pdfHTML)->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->stream();
    }

    public function showProductionPlusMinusSummary(Request $request)
    {
        $selectData = FilterData::Request($request);
        return View('admin/production/production_plusmin_summary', ['selectData' => $selectData]);
    }

    public function getProductionPlusMinusSummaryTableData(Request $request)
    {
        $groupBranches = $request->get('groupbranches') ?? false;
        $filterObject = new FilterData($request);

        if (Sentinel::check()) {
            $filter = $filterObject->initFilter();
        } else {
            $filter = $filterObject->initFilterData();
        }
        $selectData = $filterObject->getSelectData($filter);

        $cacheKey = __NAMESPACE__ . __FUNCTION__;
        $selectDataNoCache = $selectData;
        unset($selectDataNoCache['cache']);
        $cacheSearch = serialize($selectDataNoCache);

        if (Cache::has($cacheKey . $cacheSearch) && $selectData['cache']['selected']) {
            $cacheResult = Cache::get($cacheKey . $cacheSearch);
        } else {
            $productionPlusMinusSummary = Agents::getProductionPlusMinusSummary($filter, $groupBranches);

            $cacheResult = Datatables::of($productionPlusMinusSummary)->editColumn('AGB_55002', function ($productionPlusMinusSummaryRecord) {
                return trans('enra/regions.' . $productionPlusMinusSummaryRecord->AGB_55002);

            })->editColumn('AGE_INGDAT', function ($productionPlusMinusSummary) {
                return StringHelpers::countryDate($productionPlusMinusSummary->AGE_INGDAT, Config::get('app.locale'), true);

            })->editColumn('POL_BRANCHE_SPEC', function ($productionPlusMinusSummaryRecord) {
                return trans('enra/branches.' . $productionPlusMinusSummaryRecord->POL_BRANCHE_SPEC);

            })->make(true);

            Cache::put($cacheKey . $cacheSearch, $cacheResult, Config::get('cache.duration'));
        }
        return $cacheResult;
    }
}
