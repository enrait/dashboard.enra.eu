<?php

namespace App\Http\Controllers;

use App\Services\Filters\FilterData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use App\Models\Client;
use App\Models\ContactMoment;

use App\Libraries\StringHelpers;
use App\Libraries\CacheHelpers;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\DataTables;
use Sentinel;

class CRMController extends JoshController
{
    protected $countries = array(
        "" => "Select Country",
        "NL" => "Netherlands",
        "BE" => "Belgium",
        "DE" => "Germany"
    );

    protected $validationRules = array(
        'first_name' => 'required|min:3',
        'last_name' => 'required|min:3',
        'email' => 'required|email|unique:users',
        'password' => 'required|between:3,32',
        'password_confirm' => 'required|same:password',
        'pic' => 'mimes:jpg,jpeg,bmp,png|max:10000'
    );

    public function getClients(Request $request)
    {
        if (Sentinel::check()) {
            $selectData = FilterData::Request($request);
            return View('admin.crm.clients', ['selectData' => $selectData]);
        } else {
            return Redirect::to('admin/signin')->with(trans('enra/general.error'), trans('enra/general.you_must_be_logged_in'));
        }
    }

    public function getClientsTableData(Request $request)
    {
        if (Sentinel::check()) {
            $selectData = FilterData::Request($request);

            $cacheKey = __NAMESPACE__ . __FUNCTION__;
            $selectDataNoCache = $selectData;
            unset($selectDataNoCache['cache']);

            $cacheSearch = serialize($selectDataNoCache);

            $cacheResult = Cache::tags(['clients'])->remember(CacheHelpers::getCacheKey($cacheKey . $cacheSearch),
                Config::get('cache.duration'), function () use ($selectData) {
                    info('getClientsTableData No Cache');

                    $clients = Client::getClientsBySelectData($selectData);

                    foreach ($clients as $keyClient => $client) {

                        if ($client->anva_agentnr) {
                            $agents = $clients->where('anva_agentnr', $client->anva_agentnr);
                            $contactInfo = [];
                            foreach ($agents as $agent) {
                                if (isset($agent->contactInfo)) {
                                    $clients->forget($keyClient);
                                    break;
                                } else {
                                    $contactInfo[count($contactInfo)] = [];
                                    $contactInfo[count($contactInfo) - 1]['contact_first_name'] = $agent->contact_first_name;
                                    $contactInfo[count($contactInfo) - 1]['contact_last_name'] = $agent->contact_last_name;
                                    $contactInfo[count($contactInfo) - 1]['contact_jobtitle'] = is_numeric($agent->contact_jobtitle) ? trans('enra/jobtitles.' . $agent->contact_jobtitle) : $agent->contact_jobtitle;
                                    $contactInfo[count($contactInfo) - 1]['contact_phonenumber'] = $agent->contact_phonenumber;
                                    $contactInfo[count($contactInfo) - 1]['contact_email_address'] = $agent->contact_email_address;

                                    $client->contactInfo = $contactInfo;
                                }
                            }
                        } else {
                            $contactInfo = [];

                            $contactInfo[count($contactInfo)] = [];
                            $contactInfo[count($contactInfo) - 1]['contact_first_name'] = $client->contact_first_name;
                            $contactInfo[count($contactInfo) - 1]['contact_last_name'] = $client->contact_last_name;
                            $contactInfo[count($contactInfo) - 1]['contact_jobtitle'] = is_numeric($client->contact_jobtitle) ? trans('enra/jobtitles.' . $client->contact_jobtitle) : $client->contact_jobtitle;
                            $contactInfo[count($contactInfo) - 1]['contact_phonenumber'] = $client->contact_phonenumber;
                            $contactInfo[count($contactInfo) - 1]['contact_email_address'] = $client->contact_email_address;

                            $client->contactInfo = $contactInfo;
                        }
                    }

                    $cacheResult = Datatables::of($clients)->editColumn('last_visit', function ($client) {
                        return StringHelpers::countryDate($client->last_visit, Config::get('app.locale'), true);
                    })->editColumn('start_date', function ($client) {
                        return StringHelpers::countryDate($client->start_date, Config::get('app.locale'), true);
                    })->editColumn('next_visit', function ($client) {
                        return StringHelpers::countryDate($client->next_visit, Config::get('app.locale'), true);
                    })->editColumn('status', function ($client) {
                        return trans('enra/clientstatus.' . $client->status);
                    })
                        ->editColumn('region', function ($client) {
                            return trans('enra/regions.' . $client->region);
                        })
                        ->editColumn('legal_form', function ($client) {
                            return trans('enra/legalforms.' . $client->legal_form);
                        })->editColumn('organization_member', function ($client) {
                            return trans('enra/organisations.' . $client->organization_member);
                        })->make(true);

                    return $cacheResult;
                });

            return $cacheResult;
        } else {
            return Redirect::to('admin/signin')->with(trans('enra/general.error'), trans('enra/general.you_must_be_logged_in'));
        }
    }

    public function postClientUpdate(Request $request)
    {
        if ($request->get('id') && $request->get('id') != '') {
            $modelClient = Client::find($request->get('id'));

            if ($request->get('delete') == '1') {
                if ($modelClient->delete()) {
                    $selectData = FilterData::Request($request);
                    $selectData['cache']['selected'] = false;
                    Cache::tags('clients')->flush();
                    return View('admin.crm.clients', ['selectData' => $selectData]);
                }
            }

            $clientTemplate = $modelClient;
        } else {
            $modelClient = new Client;

            $clientTemplate = Client::first();
        }

        try {
            $inputAll = $request->all();

            if (!$request->get('region') || $request->get('region') == '') {
                $inputAll['region'] = '00001';
            }

            foreach ($inputAll as $inputKey => $inputValue) {
                //if ($modelClient -> hasAttribute($inputKey))
                if ($clientTemplate->hasAttribute($inputKey)) {
                    $modelClient->$inputKey = $inputValue;
                }
            }

            $country = 'nl';

            if (Sentinel::hasAccess(['country.NL'])) {
                $country = 'NL';
            }

            if (Sentinel::hasAccess(['country.DE'])) {
                $country = 'DE';
            }

            if (Sentinel::hasAccess(['country.BE'])) {
                $country = 'BE';
            }

            $modelClient->country = strtoupper($country);

            if ($modelClient->start_date == '') {
                $modelClient->start_date = NULL;
            }

            if ($modelClient->last_visit == '') {
                $modelClient->last_visit = NULL;
            }

            if ($modelClient->next_visit == '') {
                $modelClient->next_visit = NULL;
            }

            if ($modelClient->save()) {
                // Prepare the success message
                $success = Lang::get('users/message.success.update');
                $selectData = FilterData::Request($request);
                $selectData['cache']['selected'] = false;

                Cache::tags('clients')->flush();

                return View('admin.crm.clients', ['selectData' => $selectData]);
            }
        } catch (LoginRequiredException $e) {
            $error = Lang::get('users/message.user_login_required');
        }
    }

    public function getContactMoments(Request $request)
    {
        $selectData = FilterData::Request($request);
        return View('admin.crm.contact_moments', ['selectData' => $selectData]);
    }

    public function getContactMomentsTableData(Request $request)
    {
        $cacheFilterKey = '_filter_parameters_' . $request->fullUrl();
        $selectData = Cache::remember($cacheFilterKey, Config::get('cache.duration'), function () use ($request) {
            return FilterData::Request($request);
        });

        $cacheSelectKey = '_getContactsBySelectData' . $request->fullUrl();
        return Cache::remember($cacheSelectKey, Config::get('cache.duration'), function () use ($selectData) {
            $contactMoments = ContactMoment::getContactMomentsBySelectData($selectData);
            return Datatables::of($contactMoments)->editColumn('contact_at', function ($contactMoment) {
                return StringHelpers::countryDate($contactMoment->contact_at, Config::get('app.locale'), true);
            })->editColumn('action_at', function ($contactMoment) {
                return StringHelpers::countryDate($contactMoment->action_at, Config::get('app.locale'), true);
            })->editColumn('type', function ($contactMoment) {
                return trans('enra/contactmomenttypes.' . $contactMoment->type);

            })->editColumn('region', function ($contactMoment) {
                return trans('enra/regions.' . $contactMoment->region);
            })->make(true);
        });
    }

    public function postContactMomentUpdate(Request $request)
    {
        if ($request->get('id') && $request->get('id') != '') {
            $modelContactMoment = ContactMoment::find($request->get('id'));

            if ($request->get('delete') == '1') {
                if ($modelContactMoment->delete()) {
                    // Prepare the success message
                    $success = Lang::get('users/message.success.delete');

                    $selectData = FilterData::Request($request);
                    $selectData['cache']['selected'] = false;

                    Cache::tags('contact_moments')->flush();

                    return View('admin.crm.contact_moments', ['selectData' => $selectData]);
                }
            }

            $contactMomentTemplate = $modelContactMoment;
        } else {
            $modelContactMoment = new ContactMoment;

            $contactMomentTemplate = ContactMoment::first();
        }

        try {
            $inputAll = $request->all();

            foreach ($inputAll as $inputKey => $inputValue) {
                if ($contactMomentTemplate->hasAttribute($inputKey) && $inputKey != 'user_id') {
                    $modelContactMoment->$inputKey = $inputValue;
                }

                if ($inputKey == 'client_id') {
                    $client = new Client;

                    $resultClient = $client->find($inputValue);

                    $modelContactMoment->anva_agentnr = $resultClient->anva_agentnr;
                }

                if ($inputKey == 'user_id' && $modelContactMoment->$inputKey == '') {
                    $modelContactMoment->$inputKey = $inputValue;
                }
            }

            if ($modelContactMoment->contact_at == '') {
                $modelContactMoment->contact_at = date("Y-m-d H:i:s");
            }

            if ($modelContactMoment->action_at == '') {
                $modelContactMoment->action_at = NULL;
            }

            if ($modelContactMoment->save()) {
                // Prepare the success message
                $success = Lang::get('users/message.success.update');
                $selectData = FilterData::Request($request);
                $selectData['cache']['selected'] = false;

                Cache::tags('contact_moments')->flush();

                //die("client id : " . $modelContactMoment->client_id);

                $client = Client::find($modelContactMoment->client_id);

                //die("client id : " . $client->id);

                if ($client->country == "NL") {
                    $emailReceivers = array(
                        //'d.tap@enra.nl' => 'Dennis',
                        'salesnl@enra.nl' => 'Sales NL',
                        //'i.huisman@enra.nl' => 'Iris',
                        //'r.vanbijnen@enra.nl' => 'Ronald',
                        //'w.oostindie@enra.nl' => 'Wim',
                        //'dennis@nextvalley.nl' => 'Dennis'
                    );

                    $emailData = array(
                        'emailReceivers' => $emailReceivers,
                        'emailSubject' => Lang::get('enra/sales.contact_moment_notification'),
                        'emailFrom' => array('dashboard@enra.eu' => 'Dashboard ENRA'),
                        'notificationData' => array('contactMoment' => $modelContactMoment->toArray())
                    );

                    Mail::send(array('html' => 'emails.notification.contactmoment'), array('emailData' => $emailData), function ($message) use ($emailData) {
                        $message->from($emailData['emailFrom'])->to($emailData['emailReceivers'])->subject($emailData['emailSubject']);
                    });
                }

                return View('admin.crm.contact_moments', ['selectData' => $selectData]);
            }
        } catch (LoginRequiredException $e) {
            $error = Lang::get('users/message.user_login_required');
        }
    }

    public function getModalDelete($id = null)
    {
        $model = 'users';
        $confirm_route = $error = null;
        try {
            $user = Sentinel::findById($id);

            if ($user->id === Sentinel::getUser()->id) {
                $error = Lang::get('users/message.error.delete');

                return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
            }
        } catch (UserNotFoundException $e) {
            $error = Lang::get('users/message.user_not_found', compact('id'));
            return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
        $confirm_route = route('delete/user', ['id' => $user->id]);
        return View('admin/layouts/modal_confirmation', compact('error', 'model', 'confirm_route'));
    }

    public function getExcel()
    {
        $excelFilename = 'Vertragspartner_Liste_' . date("Ymd");

        Excel::create($excelFilename, function ($excel) {
            $excel->setTitle('ENRA Deutschland Vertragspartner Liste');
            $excel->setCompany('ENRA verzekeringen bv');
            $excel->setCreator('ENRA Dashboard');
            $excel->setDescription('ENRA Deutschland Vertragspartner Liste');

            $excel->sheet('Firmen inkl. ENRA-Partner', function ($sheet) {
                $modelClient = new Client;

                $dbClients = $modelClient->getList();

                $dbClientArray = $dbClients->toArray();

                $excelClientArray = array();

                $excelColumns = array(
                    'Anz. MA' => '',
                    'm² VK / Werk.' => '',
                    'ZR-gesperrt durch Verband' => '',
                    'Gazelle E-Bike' => '',
                    'KOGA-Händler' => '',
                    'Kd.-Bewertung' => '',
                    'Sonst. Vers.' => '',
                    'E-Bike Lenkerh. 2016 (Stück)' => '',
                    'h/f' => '',
                    'vorn' => '',
                    'nachn.' => '',
                    'Türaufkleber' => '',
                    'Begin Vertrag' => 'start_date',
                    'Verb. / Organ' => 'organization_member',
                    'Händler Nr.' => 'anva_agentnr',
                    'Fil. von' => '',
                    'Firma' => 'name',
                    'h/f 1' => '',
                    'Vorname Kont. 1' => 'contact_first_name',
                    'Nachname Kont. 1' => 'contact_last_name',
                    'Funktion Kont. 1' => '',
                    'h/f 2' => '',
                    'Spalte1' => '',
                    'Nachname Kont. 2' => '',
                    'Funktion Kont. 2' => '',
                    'h/f 3' => '',
                    'Vorname Kont. 3' => '',
                    'Nachname Kont. 3' => '',
                    'Funktion Kont. 3' => '',
                    'Str.' => 'visiting_street',
                    'Hausnr.' => 'visiting_housenumber',
                    'Gebiete' => 'region',
                    'PLZ' => 'visiting_zipcode',
                    'Ort' => 'visiting_city',
                    'Tel.' => 'phonenumber',
                    'Fax' => 'faxnumber',
                    'email' => 'email_address',
                    'url' => 'website',
                );

                foreach ($dbClientArray as $key => $value) {
                    foreach ($excelColumns as $excelName => $dbName) {
                        if (array_key_exists($dbName, $dbClientArray[$key])) {
                            $excelClientArray[$key][$excelName] = $dbClientArray[$key][$dbName];
                        } else {
                            $excelClientArray[$key][$excelName] = "-{ not mapped to DB }-";
                        }
                    }
                }

                unset($dbClientArray);

                $sheet->fromArray($excelClientArray);
                $sheet->setAutoSize(true);
                $sheet->setAutoFilter();

            });

        })->export('xlsx');
    }

}
