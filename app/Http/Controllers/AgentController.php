<?php

namespace App\Http\Controllers;

use App\Services\Filters\FilterData;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use View;
use App\Models\AnvaPOLBES;
use App\Models\AnvaRCAGBES;
use App\Models\AnvaAGENT;
use App\Models\AnvaPOLSCH;
use App\Models\Client;
use App;
use App\Libraries\IcsHelpers;
use App\Libraries\StringHelpers;
use App\Libraries\CacheHelpers;
use DateTime;
use Illuminate\Support\Facades\Cache;

class AgentController extends Controller
{
    private $cachePrefix = 'AgentController';

    /**
     * Message bag.
     *
     * @var Illuminate\Support\MessageBag
     */
    protected $messageBag = null;

    /**
     * Initializer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->messageBag = new MessageBag;
    }

    public function showAgentInformation(Request $request)
    {
            $selectData = FilterData::Request($request);
            return View('admin/agent/agent_information', ['selectData' => $selectData]);
    }

    public function showAgentTable(Request $request)
    {

        $searchTerm = [];

        $searchTerm['Organisation'] = $request->get('Organisation');
        $searchTerm['Typeahead'] = $request->get('typeaheadAgentSearch');
        $searchTerm['ZipcodeFrom'] = $request->get('zipcodeFromAgentSearch');
        $searchTerm['ZipcodeTo'] = $request->get('zipcodeToAgentSearch');
        $searchTerm['Iban'] = $request->get('ibanAgentSearch');
        $searchTerm['InvoiceId'] = $request->get('invoiceIdAgentSearch');

        $tableSearchTypeaheadResult = array(
            'rows' => [],
            'columns' => []
        );

        $tableSearchTypeaheadResult['columns'] = array(
            'AGE_AGENTNR' => array(
                'label' => trans('enra/agent.agent_id'),
                'show' => true,
                'format' => 'text'
            ),
            'AGE_NAAM' => array(
                'label' => trans('enra/agent.name'),
                'show' => true,
                'format' => 'text'
            ),
            'AGE_PLAATS' => array(
                'label' => trans('enra/agent.place'),
                'show' => true,
                'format' => 'text'
            ),
            'AGE_STRAAT' => array(
                'label' => trans('enra/agent.street'),
                'show' => true,
                'format' => 'text'
            ),
            'AGE_POSTCODE' => array(
                'label' => trans('enra/agent.zipcode'),
                'show' => true,
                'format' => 'text'
            ),
            'AGE2_EMAILADRES' => array(
                'label' => trans('enra/agent.email'),
                'show' => true,
                'format' => 'text'
            ),
            'IBANINT_IBAN' => array(
                'label' => trans('enra/agent.iban'),
                'show' => false,
                'format' => 'text'
            ),
            'clientId' => array(
                'label' => trans('enra/agent.client_id'),
                'show' => false,
                'format' => 'text'
            ),
            'RAH_FACTUUR' => array(
                'label' => trans('enra/agent.invoice_id'),
                'show' => false,
                'format' => 'text'
            ),
            'RAH_INCASSO' => array(
                'label' => trans('enra/agent.amount'),
                'show' => false,
                'format' => 'currency'
            )
        );

        if (Route::getCurrentRoute()->getName() == 'finance.agentInformationTable') {
            $tableSearchTypeaheadResult['columns']['IBANINT_IBAN']['show'] = true;

            if ($searchTerm['InvoiceId']) {
                $tableSearchTypeaheadResult['columns']['RAH_FACTUUR']['show'] = true;
                $tableSearchTypeaheadResult['columns']['RAH_INCASSO']['show'] = true;
            }
        }

        if ($searchTerm['Typeahead'] || ($searchTerm['ZipcodeFrom'] && $searchTerm['ZipcodeTo']) || $searchTerm['Iban'] || $searchTerm['InvoiceId'] || $searchTerm['Organisation']) //if ( $searchTerm['Typeahead'] || ($searchTerm['ZipcodeFrom'] && $searchTerm['ZipcodeTo']) )
        {
            $searchTermCache = strtolower(implode("", $searchTerm));

            $searchCountry = [];

            if (Sentinel::hasAccess(['country.NL'])) {
                $searchCountry[] = 'nl';
            }

            if (Sentinel::hasAccess(['country.DE'])) {
                $searchCountry[] = 'de';
            }

            if (Sentinel::hasAccess(['country.BE'])) {
                $searchCountry[] = 'nl';
            }

            if (count($searchCountry) == 0) {
                $searchCountry[] = 'nl';
            }

            $searchTerm['Country'] = array_unique($searchCountry);

            $searchTermCache = $searchTermCache . strtolower(implode("", $searchTerm['Country']));

            $cacheKey = CacheHelpers::getCacheKey('getAgentSearchResultJson' . $searchTermCache);

            if (Cache::has($cacheKey)) {
                $tableSearchTypeaheadResult = Cache::get($cacheKey);
            } else {
                $tableSearchTypeaheadResultRows = AnvaAGENT::getTypeaheadSearch($searchTerm);

                foreach ($tableSearchTypeaheadResultRows as $tableSearchTypeaheadResultRow) {
                    $tableSearchTypeaheadResultRowArray = [];

                    foreach ($tableSearchTypeaheadResult['columns'] as $tableSearchTypeaheadResultColumnKey => $tableSearchTypeaheadResultColumn) {
                        $tableSearchTypeaheadResultRowArray[$tableSearchTypeaheadResultColumnKey] = isset($tableSearchTypeaheadResultRow->$tableSearchTypeaheadResultColumnKey) ? $tableSearchTypeaheadResultRow->$tableSearchTypeaheadResultColumnKey : '';

                        if ($tableSearchTypeaheadResult['columns'][$tableSearchTypeaheadResultColumnKey]['format'] == 'currency') {
                            $tableSearchTypeaheadResultRowArray[$tableSearchTypeaheadResultColumnKey] = StringHelpers::Value2Currency($tableSearchTypeaheadResultRowArray[$tableSearchTypeaheadResultColumnKey]);
                        }

                    }

                    $tableSearchTypeaheadResult['rows'][] = $tableSearchTypeaheadResultRowArray;

                }

                Cache::put($cacheKey, $tableSearchTypeaheadResult, Config::get('cache.duration'));
            }

        }

        return View('admin/agent/agent_table_section', [
            'tableSearchTypeaheadResult' => $tableSearchTypeaheadResult,
            'searchTerm' => $searchTerm]);
    }

    public function showAgentData(Request $request)
    {
        $selectData = FilterData::Request($request);

        if ($request->get('agentnr')) {
            $selectData['agent']['selected'] = [];
            $selectData['agent']['selected'][] = $request->get('agentnr');
        }

        $clients = Client::getClientsBySelectData($selectData);

        $clientsArray = $clients->toArray();

        if (count($clientsArray) > 0) {
            $clientArray = $clientsArray[0];
            $clientArray->last_visit = StringHelpers::countryDate($clientArray->last_visit, Config::get('app.locale'), true);
            $clientArray->start_date = StringHelpers::countryDate($clientArray->start_date, Config::get('app.locale'), true);
            $clientArray->next_visit = StringHelpers::countryDate($clientArray->next_visit, Config::get('app.locale'), true);
            $clientArray->status = trans('enra/clientstatus.' . $clientArray->status);
            $clientArray->region = trans('enra/regions.' . $clientArray->region);
            $clientArray->legal_form = trans('enra/legalforms.' . $clientArray->legal_form);
            $clientArray->organization_member = trans('enra/organisations.' . $clientArray->organization_member);
        } else {
            $clientArray = [];
        }

        return View('admin/agent/client_data_section', ['clientData' => $clientArray]);
    }

    public function getAgentJson($agentNr = false, Request $request = null)
    {
        if (Sentinel::check()) {
            $agentJson = [];

            $agentNr = strtolower(request()->get('agentnr')) ?? $agentNr;
            $cacheKey = CacheHelpers::getCacheKey('getAgentJson' . $agentNr);

            Cache::remember($cacheKey, Config::get('cache.duration'), function () use ($agentNr) {
                $agentTable = AnvaAGENT::getAgent($agentNr);
                $agentJson = [];
                foreach ($agentTable as $agentTableRow) {
                    foreach ($agentTableRow as $agentTableRowColumnKey => $agentTableRowColumnValue) {
                        // Filter out those field, because they can return hexidecimals which will
                        if ($agentTableRowColumnKey != 'FILLER_1' && $agentTableRowColumnKey != 'FILLER_2' && $agentTableRowColumnKey != 'FILLER_3' && $agentTableRowColumnKey != 'AGE2_INCASSO_SCHADE') {
                            $agentJson[$agentTableRowColumnKey] = $agentTableRowColumnValue;
                        }
                    }
                }
                return $agentJson;
            });


            return $agentJson;
        }
    }

    public function getAgentSearchResultJson(Request $request)
    {
        if (Sentinel::check()) {
            $searchTypeaheadResult = [];

            if ($request->get('q')) {
                $searchTerm = [];

                $searchTerm['Typeahead'] = strtolower($request->get('q'));

                $searchTermCache = strtolower(implode("", $searchTerm));

                $cacheKey = 'getAgentSearchResultJson' . $searchTermCache;

                if (Cache::has($cacheKey)) {
                    $searchTypeaheadResultTable = Cache::get($cacheKey);
                } else {
                    $searchTypeaheadResultTable = AnvaAGENT::getTypeaheadSearch($searchTerm);

                    Cache::put($cacheKey, $searchTypeaheadResultTable, Config::get('cache.duration'));
                }

                foreach ($searchTypeaheadResultTable as $searchTypeaheadResultTableRow) {
                    foreach ($searchTypeaheadResultTableRow as $searchTypeaheadResultTableRowColumnValue) {
                        if (strpos(strtolower($searchTypeaheadResultTableRowColumnValue), $searchTerm['Typeahead']) !== false) {
                            $searchTypeaheadResult[] = $searchTypeaheadResultTableRowColumnValue;
                        }
                    }
                }
            }

            return array_unique($searchTypeaheadResult);
        } else {
            return Redirect::to('admin/signin')->with(trans('enra/general.error'), trans('enra/general.you_must_be_logged_in'));
        }
    }

    public function getAgentTableJson(Request $request)
    {
        if (Sentinel::check()) {
            $searchTerm = [];

            $searchTerm['ZipcodeFrom'] = $request->get('zipcodeFromAgentSearch');
            $searchTerm['ZipcodeTo'] = $request->get('zipcodeToAgentSearch');
            $searchTerm['Typeahead'] = $request->get('typeaheadAgentSearch');

            $tableSearchTypeaheadResult = array(
                'rows' => [],
                'columns' => []
            );

            $tableSearchTypeaheadResult['columns'] = array(
                'AGE_AGENTNR' => array(
                    'label' => trans('enra/administration.agent_id'),
                    'show' => true
                ),
                'AGE_NAAM' => array(
                    'label' => trans('enra/administration.name'),
                    'show' => true
                ),
                'AGE_PLAATS' => array(
                    'label' => trans('enra/administration.place'),
                    'show' => true
                ),
                'AGE_STRAAT' => array(
                    'label' => trans('enra/administration.street'),
                    'show' => true
                ),
                'AGE_POSTCODE' => array(
                    'label' => trans('enra/administration.zipcode'),
                    'show' => true
                ),
                'AGE2_EMAILADRES' => array(
                    'label' => trans('enra/administration.email'),
                    'show' => true
                ),
                'IBANINT_IBAN' => array(
                    'label' => trans('enra/administration.iban'),
                    'show' => true
                )
            );

            $cacheBuster = '1';

            if (($searchTerm['ZipcodeFrom'] && $searchTerm['ZipcodeTo']) || ($searchTerm['Typeahead'])) {
                $searchTermCache = strtolower(implode("", $searchTerm));

                if (Cache::has('getAgentSearchResultJson' . $searchTermCache . $cacheBuster)) {
                    $tableSearchTypeaheadResult = Cache::get('getAgentSearchResultJson' . $searchTermCache . $cacheBuster);
                } else {
                    $tableSearchTypeaheadResultRows = AnvaAGENT::getTypeaheadSearch($searchTerm);

                    return $tableSearchTypeaheadResultRows;

//                    foreach ($tableSearchTypeaheadResultRows as $tableSearchTypeaheadResultRow) {
//                        $tableSearchTypeaheadResultRowArray = [];
//
//                        foreach ($tableSearchTypeaheadResult['columns'] as $tableSearchTypeaheadResultColumnKey => $tableSearchTypeaheadResultColumn) {
//                            $tableSearchTypeaheadResultRowArray[$tableSearchTypeaheadResultColumnKey] = isset($tableSearchTypeaheadResultRow->$tableSearchTypeaheadResultColumnKey) ? $tableSearchTypeaheadResultRow->$tableSearchTypeaheadResultColumnKey : '';
//
//                            // if (strpos(strtolower($tableSearchTypeaheadResultRowArray[$tableSearchTypeaheadResultColumnKey]), $searchTerm) !== false)
//                            // {
//                            $tableSearchTypeaheadResult['columns'][$tableSearchTypeaheadResultColumnKey]['show'] = true;
//                            // }
//                        }
//
//                        $tableSearchTypeaheadResult['rows'][] = $tableSearchTypeaheadResultRowArray;
//
//                    }

//                    Cache::put('getAgentSearchResultJson' . $searchTermCache . $cacheBuster, $tableSearchTypeaheadResult, Config::get('cache.duration'));
                }

            }

            return $tableSearchTypeaheadResult;

//            return View('admin/agents/agent_table_section', [
//                'tableSearchTypeaheadResult' => $tableSearchTypeaheadResult,
//                'searchTerm' => $searchTerm]);
        } else {
            return Redirect::to('admin/signin')->with(trans('enra/general.error'), trans('enra/general.you_must_be_logged_in'));
        }
    }

    public function showEvent()
    {
        if (Sentinel::check()) {
            $icsHelper = new IcsHelpers;

            $eventData = [];

            $agent = $this->getAgentJson(false);

            if (!$agent) {
                return Response::json([
                ], 404);
            }

            $eventData['summary'] = $agent['AGE_NAAM'] . ", " . $agent['AGE_PLAATS'];
            $eventData['dateStart'] = new DateTime();
            $eventData['dateStart'] = $eventData['dateStart']->modify('+1 day -2 hours');
            $eventData['dateEnd'] = new DateTime();
            $eventData['dateEnd'] = $eventData['dateEnd']->modify('+1 day -1 hours');
            $eventData['location'] = $agent['AGE_STRAAT'] . " " . ltrim($agent['AGE_HUISNR'], "0") . ", " . $agent['AGE_POSTCODE'] . $agent['AGE_BUITENL_POSTCODE'] . " " . $agent['AGE_PLAATS'];
            $eventData['uri'] = "";
            $eventData['description'] = $agent['AGE_AGENTNR'] . "\\n";
            $eventData['description'] = $eventData['description'] . $agent['AGE_NAAM'] . "\\n";
            $eventData['description'] = $eventData['description'] . $agent['AGE_STRAAT'] . " " . ltrim($agent['AGE_HUISNR'], "0") . "\\n";
            $eventData['description'] = $eventData['description'] . $agent['AGE_POSTCODE'] . $agent['AGE_BUITENL_POSTCODE'] . " " . $agent['AGE_PLAATS'] . "\\n";
            $eventData['description'] = $eventData['description'] . $agent['AGE_TELNR'] . "\\n";
            $eventData['description'] = $eventData['description'] . $agent['AGE2_EMAILADRES'] . $agent['AGE2_EMAILADRES_REST'];

            return $icsHelper->createEvent($eventData);
        } else {
            return Redirect::to('admin/signin')->with(trans('enra/general.error'), trans('enra/general.you_must_be_logged_in'));
        }
    }
}
