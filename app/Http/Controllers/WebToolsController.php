<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Libraries\DatabaseHelpers;
use App\Libraries\CacheHelpers;
use DB;
use Input;
use GoogleMaps;
use Storage;
use File;
use Config;
use Cache;

class WebToolsController extends JoshController
{
    public function getAnvaEfd2Sql()
    {
        if (!Input::get('dataset')) {
            $dataSets = array(
                "Data1",
                "Data3"
            );
        } else {
            $dataSets = array(Input::get('dataset'));
        }

        foreach ($dataSets as $dataSet) {
            echo "USE AnvaBuild" . ucfirst($dataSet) . ";<br><br>";

            $tables = array(
                "COLLTOES" => array(
                    "name" => "COLLTOES",
                    "uniqueKey" => "CONCAT(CLT_COLLNR, CLT_BRANCHE, CLT_DEKKING, CLT_DATUM_ING_EEJJ, CLT_DATUM_ING_MM, CLT_DATUM_ING_DD)",
                    "index" => array(),
                    "indexType" => array()
                ),
                "DEKCODE" => array(
                    "name" => "DEKCODE",
                    "uniqueKey" => "DKC_DEKKING",
                    "index" => array(
                        "`DKC_DEKKING` (`DKC_DEKKING`)"
                    ),
                    "indexType" => array()
                ),
                "NOGBOEK" => array(
                    "name" => "NOGBOEK",
                    "uniqueKey" => "CONCAT(NOG_SOORT, NOG_RELNR, NOG_VOLG, NOG_VOLG_SUB, NOG_FACNR)",
                    "index" => array(),
                    "indexType" => array()
                ),
                "WACBES" => array(
                    "name" => "WACBES",
                    "uniqueKey" => "CONCAT(WAC_RELNR, WAC_VOLG, WAC_VOLG_SUB, WAC_VOLG_WACHT)",
                    "index" => array(
                        "`WAC_RELNR_WAC_VOLG_WAC_VOLG_SUB` (`WAC_RELNR`,`WAC_VOLG`,`WAC_VOLG_SUB`)"
                    ),
                    "indexType" => array()
                ),
                "AGBES" => array(
                    "name" => "AGBES",
                    "uniqueKey" => "AGB_AGENTNR",
                    "index" => array(
                        "`AGB_AGENTNR` (`AGB_AGENTNR`)",
                        "`AGB_55002` (`AGB_55002`)"
                    ),
                    "indexType" => array(
                        "`AGB_55002` `AGB_55002` VARCHAR(255) NOT NULL"
                    )
                ),
                "BTRBAS02" => array(
                    "name" => "BTRBAS02",
                    "uniqueKey" => "CONCAT(BB2_RELNR, BB2_VOLG, BB2_VOLG_SUB, BB2_SOORT_BETROKKENE, BB2_VOLGNR_BETROKKENE)",
                    "index" => array(
                        "`BB2_SOORT_BETROKKENE` (`BB2_SOORT_BETROKKENE`)",
                        "`BB2_RELNR` (`BB2_RELNR`)",
                        "`BB2_VOLGNR_BETROKKENE` (`BB2_VOLGNR_BETROKKENE`)"
                    ),
                    "indexType" => array()
                ),
                "RUBRIK" => array(
                    "name" => "RUBRIK",
                    "uniqueKey" => "RUB_LABEL",
                    "index" => array(),
                    "indexType" => array()
                ),
                "REFBES" => array(
                    "name" => "REFBES",
                    "uniqueKey" => "CONCAT(REF_RC, REF_KEY_N)",
                    "index" => array(
                        "`REF_RC` (`REF_RC`)",
                        "`REF_KEY_N` (`REF_KEY_N`)"
                    ),
                    "indexType" => array()
                ),
                "AGENT2" => array(
                    "name" => "AGENT2",
                    "uniqueKey" => "AGE2_AGENTNR",
                    "index" => array(
                        "`AGE2_AGENTNR` (`AGE2_AGENTNR`)",
                        "`AGE2_EMAILADRES` (`AGE2_EMAILADRES`)",
                        "`AGE2_EMAILADRES_PBI` (`AGE2_EMAILADRES_PBI`)"
                    ),
                    "indexType" => array()
                ),
                "VRHSCH01" => array(
                    "name" => "VRHSCH01",
                    "uniqueKey" => "CONCAT(VH1_SCHADENR, VH1_SCHADESUBNR, VH1_VOLGNR_BERICHT)",
                    "index" => array(
                        "`VH1_SCHADENR_VH1_SCHADESUBNR` (`VH1_SCHADENR`,`VH1_SCHADESUBNR`)",
                        "`VH1_SCHADESOORT` (`VH1_SCHADESOORT`)"
                    ),
                    "indexType" => array()
                ),
                "AGENT" => array(
                    "name" => "AGENT",
                    "uniqueKey" => "AGE_AGENTNR",
                    "index" => array(
                        "`AGE_AGENTNR` (`AGE_AGENTNR`)",
                        "`AGE_NAAM` (`AGE_NAAM`)",
                        "`AGE_PLAATS` (`AGE_PLAATS`)",
                        "`AGE_STRAAT` (`AGE_STRAAT`)",
                        "`AGE_POSTCODE` (`AGE_POSTCODE`)",
                        "`AGE_BUITENL_POSTCODE` (`AGE_BUITENL_POSTCODE`)"
                    ),
                    "indexType" => array()
                ),
                "BTRBAS01" => array(
                    "name" => "BTRBAS01",
                    "uniqueKey" => "CONCAT(BB1_SOORT_BETROKKENE, BB1_VOLGNR_BETROKKENE)",
                    "index" => array(
                        "`BB1_VOLGNR_BETROKKENE` (`BB1_VOLGNR_BETROKKENE`)"
                    ),
                    "indexType" => array()
                ),
                "AGEPROVI" => array(
                    "name" => "AGEPROVI",
                    "uniqueKey" => "CONCAT(AGPV_AGENTNR, AGPV_SW_MIJ_COL, AGPV_COLNR, AGPV_BRANCHE, AGPV_DEKKING, AGPV_LOOPTIJD)",
                    "index" => array(),
                    "indexType" => array()
                ),
                "RCAGBES" => array(
                    "name" => "RCAGBES",
                    "uniqueKey" => "CONCAT(RAB_KANTOORCODE, RAB_AGENTNR, RAB_PERIODE, RAB_SOORT, RAB_PER_DD, RAB_POLNR, RAB_PROL_DATUM, RAB_SUB_X)",
                    "index" => array(
                        "`RAB_AGENTNR` (`RAB_AGENTNR`)",
                        "`RAB_PERIODE` (`RAB_PERIODE`)",
                        "`RAB_SOORT` (`RAB_SOORT`)",
                        "`RAB_POLNR` (`RAB_POLNR`)",
                        "`RAB_FACTUUR` (`RAB_FACTUUR`)"
                    ),
                    "indexType" => array()
                ),
                "MLDSCH01" => array(
                    "name" => "MLDSCH01",
                    "uniqueKey" => "CONCAT(ML1_SCHADENR, ML1_SCHADESUBNR, ML1_VOLGNR_BERICHT)",
                    "index" => array(),
                    "indexType" => array()
                ),
                "IBANINT" => array(
                    "name" => "IBANINT",
                    "uniqueKey" => "IBANINT_INTERNNR_N",
                    "index" => array(
                        "`IBANINT_INTERNNR_N` (`IBANINT_INTERNNR_N`)",
                        "`IBANINT_IBAN` (`IBANINT_IBAN`)"
                    ),
                    "indexType" => array()
                ),
                "ALGSCH01" => array(
                    "name" => "ALGSCH01",
                    "uniqueKey" => "CONCAT(SC1_SCHADENR, SC1_SCHADESUBNR)",
                    "index" => array(
                        "`SC1_SCHADENR_SC1_SCHADESUBNR` (`SC1_SCHADENR`,`SC1_SCHADESUBNR`)",
                        "`SC1_SW_AFGEHANDELD` (`SC1_SW_AFGEHANDELD`)",
                        "`SC1_KEY_02` (`SC1_KEY_02`)"
                    ),
                    "indexType" => array()
                ),
                "POSTCODE" => array(
                    "name" => "POSTCODE",
                    "uniqueKey" => "CONCAT(PSTC_POSTCODE_N, PSTC_POSTCODE_A, PSTC_HUISNR_VAN, PSTC_HUISNR_TM)",
                    "index" => array(),
                    "indexType" => array()
                ),
                "RCAGSAL" => array(
                    "name" => "RCAGSAL",
                    "uniqueKey" => "CONCAT(RAS_KANTOORCODE, RAS_AGENTNR, RAS_SALDO_DATUM, RAS_SAL_DATUM, RAS_SAL_TIJD)",
                    "index" => array(),
                    "indexType" => array()
                ),
                "BETSCH01" => array(
                    "name" => "BETSCH01",
                    "uniqueKey" => "CONCAT(BE1_SCHADENR, BE1_SCHADESUBNR, BE1_VOLGNR_BERICHT)",
                    "index" => array(
                        "`BE1_SCHADENR_BE1_SCHADESUBNR` (`BE1_SCHADENR`,`BE1_SCHADESUBNR`)",
                        "`BE1_SCHADESOORT` (`BE1_SCHADESOORT`)"
                    ),
                    "indexType" => array()
                ),
                "CONTACT" => array(
                    "name" => "CONTACT",
                    "uniqueKey" => "CTC_VOLGNR",
                    "index" => array(
                        "`CTC_VOLGNR` (`CTC_VOLGNR`)"
                    ),
                    "indexType" => array()
                ),
                "NAWHIS" => array(
                    "name" => "NAWHIS",
                    "uniqueKey" => "CONCAT(NWH_RELNR, NWH_DATUM, NWH_VOLGNR)",
                    "index" => array(
                        "`NWH_RELNR_NWH_DATUM_NWH_VOLGNR` (`NWH_RELNR`, `NWH_DATUM`, `NWH_VOLGNR`)"
                    ),
                    "indexType" => array()
                ),
                "POLSCH" => array(
                    "name" => "POLSCH",
                    "uniqueKey" => "CONCAT(PLS_SCHADENR, PLS_SCHADESUBNR)",
                    "index" => array(
                        "`PLS_AGENT_1` (`PLS_AGENT_1`)",
                        "`PLS_SCHADENR_PLS_SCHADESUBNR` (`PLS_SCHADENR`,`PLS_SCHADESUBNR`)",
                        "`PLS_RELNR` (`PLS_RELNR`)",
                        "`PLS_ING_DATUM` (`PLS_ING_DATUM`)",
                        "`PLS_BRANCHE_SPEC` (`PLS_BRANCHE_SPEC`)",
                        "`PLS_POLNR` (`PLS_POLNR`)"
                    ),
                    "indexType" => array()
                ),
                "BORDAGPR" => array(
                    "name" => "BORDAGPR",
                    "uniqueKey" => "CONCAT(BOAP_RC, BOAP_KANTOORCODE, BOAP_AGENT, BOAP_POLNR, BOAP_PERIODE, BOAP_SUBNR_X)",
                    "index" => array(),
                    "indexType" => array()
                ),
                "RCAGHIS" => array(
                    "name" => "RCAGHIS",
                    "uniqueKey" => "CONCAT(RAH_KANTOORCODE, RAH_AGENTNR, RAH_SALDO_DATUM, RAH_SAL_DATUM, RAH_SAL_TIJD, FILLER_1)",
                    "index" => array(
                        "`RAH_AGENTNR` (`RAH_AGENTNR`)",
                        "`RAH_SAL_DATUM` (`RAH_SAL_DATUM`)",
                        "`RAH_PERIODE` (`RAH_PERIODE`)",
                        "`RAH_PER_DD` (`RAH_PER_DD`)",
                        "`RAH_SOORT` (`RAH_SOORT`)",
                        "`RAH_POLNR` (`RAH_POLNR`)",
                        "`RAH_FACTUUR` (`RAH_FACTUUR`)"
                    ),
                    "indexType" => array()
                ),
                "NAWBES" => array(
                    "name" => "NAWBES",
                    "uniqueKey" => "NAW_RELNR",
                    "index" => array(
                        "`NAW_RELNR` (`NAW_RELNR`)",
                        "`NAW_POSTCODE` (`NAW_POSTCODE`)",
                        "`NAW_HUISNUMMER` (`NAW_HUISNUMMER`)",
                        "`NAW_01030` (`NAW_01030`)"
                    ),
                    "indexType" => array(
                        "`NAW_01030` `NAW_01030` VARCHAR(255) NOT NULL"
                    )
                ),
                "DEKKING" => array(
                    "name" => "DEKKING",
                    "uniqueKey" => "CONCAT(DEK_RELNR, DEK_VOLG, DEK_VOLG_SUB, DEK_NUMMER)",
                    "index" => array(
                        "`DEK_RELNR_DEK_VOLG_DEK_VOLG_SUB` (`DEK_RELNR`,`DEK_VOLG`,`DEK_VOLG_SUB`)",
                        "`DEK_DEKKING` (`DEK_DEKKING`)"
                    ),
                    "indexType" => array()
                ),
                "POLBES" => array(
                    "name" => "POLBES",
                    "uniqueKey" => "CONCAT(POL_RELNR, POL_VOLG, POL_VOLG_SUB)",
                    "index" => array(
                        "`POL_RELNR_POL_VOLG_POL_VOLG_SUB` (`POL_RELNR`,`POL_VOLG`,`POL_VOLG_SUB`)",
                        "`POL_AGENT_1` (`POL_AGENT_1`)",
                        "`POL_LOOPTIJD` (`POL_LOOPTIJD`)",
                        "`POL_ING_DATUM` (`POL_ING_DATUM`)",
                        "`POL_BRANCHE_SPEC` (`POL_BRANCHE_SPEC`)",
                        "`POL_POLNR` (`POL_POLNR`)",
                        "`POL_MIJNR` (`POL_MIJNR`)",
                        "`POL_PRODUCENT` (`POL_PRODUCENT`)",
                        "`POL_10037` (`POL_10037`)",
                        "`POL_80000` (`POL_80000`)"
                    ),
                    "indexType" => array(
                        "`POL_10037` `POL_10037` VARCHAR(255) NOT NULL",
                        "`POL_80000` `POL_80000` VARCHAR(255) NOT NULL"
                    )
                ),
                "FACTUUR" => array(
                    "name" => "FACTUUR",
                    "uniqueKey" => "CONCAT(FAC_FACNR, FAC_HIST_VOLG)",
                    "index" => array(
                        "`FAC_POLIS_MIJ` (`FAC_POLIS_MIJ`)",
                        "`FAC_SOORT` (`FAC_SOORT`)",
                        "`FAC_BRANCHE` (`FAC_BRANCHE`)",
                        "`FAC_AGENT_1` (`FAC_AGENT_1`)",
                        "`FAC_BOEK_DATUM` (`FAC_BOEK_DATUM`)"
                    ),
                    "indexType" => array()
                ),
                "DEKHIS" => array(
                    "name" => "DEKHIS",
                    "uniqueKey" => "CONCAT(DHS_RELNR, DHS_VOLG, DHS_VOLG_SUB, DHS_HISTORIE_SUB, DHS_NUMMER)",
                    "index" => array(
                        "`DHS_RELNR_DHS_VOLG_DHS_VOLG_SUB_DHS_HISTORIE_SUB_DHS_NUMMER` (`DHS_RELNR`, `DHS_VOLG`, `DHS_VOLG_SUB`, `DHS_HISTORIE_SUB`, `DHS_NUMMER`)"
                    ),
                    "indexType" => array()
                ),
                "POLHIS" => array(
                    "name" => "POLHIS",
                    "uniqueKey" => "CONCAT(PLH_RELNR, PLH_VOLG, PLH_VOLG_SUB, PLH_HISTORIE_VOLG)",
                    "index" => array(
                        "`PLH_RELNR_PLH_VOLG_PLH_VOLG_SUB` (`PLH_RELNR`,`PLH_VOLG`,`PLH_VOLG_SUB`)",
                        "`PLH_AGENT_1` (`PLH_AGENT_1`)",
                        "`PLH_LOOPTIJD` (`PLH_LOOPTIJD`)",
                        "`PLH_ING_DATUM` (`PLH_ING_DATUM`)",
                        "`PLH_BRANCHE_SPEC` (`PLH_BRANCHE_SPEC`)",
                        "`PLH_POLNR` (`PLH_POLNR`)",
                        "`PLH_MIJNR` (`PLH_MIJNR`)",
                        "`PLH_PRODUCENT` (`PLH_PRODUCENT`)"
                    ),
                    "indexType" => array()
                ),
                "FACSPEC" => array(
                    "name" => "FACSPEC",
                    "uniqueKey" => "CONCAT(FACS_FACNR, FACS_HIST_VOLG, FACS_BOEK_VOLG)",
                    "index" => array(
                        "`FACS_FACNR_FACS_HIST_VOLG_FACS_BOEK_VOLG` (`FACS_FACNR`, `FACS_HIST_VOLG`, `FACS_BOEK_VOLG`)"
                    ),
                    "indexType" => array()
                )
            );

            if (!Input::get('table')) {
                foreach ($tables as $table) {
                    $this->getTableSQL($dataSet, $table);
                }
            } else {
                $this->getTableSQL($dataSet, $tables[Input::get('table')]);
            }
        }
    }

    public function getTableSQL($dataSet, $table)
    {
        $blocksFound = false;

        $index = array();

        $indexType = array();

        $AnvaEfdArray = $this->getAnvaEfdArray($table['name']);

        $sqlCreateTemporary = "CREATE TEMPORARY TABLE LineImport (LineString LONGTEXT);<br>";

        $sqlLoadData = "LOAD DATA INFILE '/mnt/cloud/anva-unload/output-" . strtolower($dataSet) . "/" . $table['name'] . ".txt' INTO TABLE LineImport CHARACTER SET cp850 FIELDS ESCAPED BY '' LINES TERMINATED BY '\\r\\n';<br>";

        $sqlDropTable = "DROP TABLE IF EXISTS AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . ";<br>";

        $sqlCreateTable = "CREATE TABLE IF NOT EXISTS AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . " (<br>
			`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,<br>
			`hashlinestring` CHAR(128) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',<br>
			`country` VARCHAR(6) NULL DEFAULT '" . (strtolower($dataSet) == "data3" ? "DE" : "NL") . "' COLLATE 'utf8_unicode_ci',<br>
			`dataset` VARCHAR(6) NULL DEFAULT '" . $dataSet . "' COLLATE 'utf8_unicode_ci'";

        foreach ($AnvaEfdArray as $field) {
            $fieldExplode = explode("_", $field['name'], 2);
            $fieldPrefix = $fieldExplode[0] . "_";

            if ($field['name'] == $fieldPrefix . 'BLOK_1') {

            } elseif ($field['name'] == $fieldPrefix . 'BLOK') {

            } elseif ($field['name'] == $fieldPrefix . 'BLOK_2' or $field['name'] == $fieldPrefix . 'LENGTE') {

            } elseif ($field['name'] == $fieldPrefix . 'BLOK_AANTAL') {
                $blocksFound = true;
            } else {
                $sqlCreateTable = $sqlCreateTable . ",<br>`" . $field['name'] . "` VARCHAR(" . $field['size'] . ") NOT NULL COLLATE 'utf8_unicode_ci'";
            }
        }

        if ($blocksFound) {
            $sqlCreateTable = $sqlCreateTable . ",<br>
			`errors` TEXT NULL COLLATE 'utf8_unicode_ci',<br>
			`lasterror` TEXT NULL COLLATE 'utf8_unicode_ci',<br>
			`" . $fieldPrefix . "LENGTE` VARCHAR(5) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',<br>
			`" . $fieldPrefix . "BLOK_AANTAL` VARCHAR(5) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',<br>
			`" . $fieldPrefix . "BLOK_1` LONGTEXT NULL COLLATE 'utf8_unicode_ci'";
        }

        $sqlCreateTable = $sqlCreateTable . ",<br>
		PRIMARY KEY (`id`),<br>
		INDEX `country` (`country`),
		UNIQUE INDEX `hashlinestring` (`hashlinestring`)
		";

        // if (isset($index[$tableName]))
        // {
        // 	foreach ($index[$tableName] as $key => $value)
        // 	{
        // 		$sqlCreateTable = $sqlCreateTable . ",<br>INDEX " . $value;
        // 	}
        // }

        $sqlCreateTable = $sqlCreateTable . "<br>)<br>
		COLLATE='utf8_unicode_ci'<br>
		ENGINE=MyISAM;<br>";

        $sqlInsertSelect = "INSERT INTO AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . " (<br>
			hashlinestring,<br>
			country,<br>
			dataset";

        foreach ($AnvaEfdArray as $field) {
            if ($field['name'] == $fieldPrefix . 'BLOK' or $field['name'] == $fieldPrefix . 'BLOK_1' or $field['name'] == $fieldPrefix . 'BLOK_2' or $field['name'] == $fieldPrefix . 'LENGTE') {

            } elseif ($field['name'] == $fieldPrefix . 'BLOK_AANTAL') {

            } else {
                $sqlInsertSelect = $sqlInsertSelect . ",<br>" . $field['name'];
            }
        }

        if ($blocksFound) {
            foreach ($AnvaEfdArray as $field) {
                if ($field['name'] == $fieldPrefix . 'BLOK_1' or $field['name'] == $fieldPrefix . 'LENGTE' or $field['name'] == $fieldPrefix . 'BLOK_AANTAL') {
                    $sqlInsertSelect = $sqlInsertSelect . ",<br>" . $field['name'];
                } elseif ($field['name'] == $fieldPrefix . 'BLOK') {
                    $sqlInsertSelect = $sqlInsertSelect . ",<br>" . $field['name'] . "_1";
                } else {

                }
            }
        }

        $sqlInsertSelect = $sqlInsertSelect . ") SELECT<br>
			SHA2(LineImport.LINESTRING, 512),<br>
			'" . (strtolower($dataSet) == "data3" ? "DE" : "NL") . "',<br>
			'" . $dataSet . "'";

        foreach ($AnvaEfdArray as $field) {
            if ($field['name'] == $fieldPrefix . 'BLOK' or $field['name'] == $fieldPrefix . 'BLOK_1') {

            } elseif ($field['name'] == $fieldPrefix . 'BLOK_2' or $field['name'] == $fieldPrefix . 'LENGTE' or $field['name'] == $fieldPrefix . 'BLOK_AANTAL') {

            } else {
                $sqlInsertSelect = $sqlInsertSelect . ",<br>TRIM(SUBSTR(LineImport.LineString, " . (string)((int)$field['offset'] + 1) . ", " . $field['size'] . "))";
            }
        }

        if ($blocksFound) {
            foreach ($AnvaEfdArray as $field) {
                if ($field['name'] == $fieldPrefix . 'BLOK' or $field['name'] == $fieldPrefix . 'BLOK_1') {
                    $sqlInsertSelect = $sqlInsertSelect . ",<br>TRIM(SUBSTR(LineImport.LineString, " . (string)((int)$field['offset'] + 1) . "))";
                } elseif ($field['name'] == $fieldPrefix . 'LENGTE' or $field['name'] == $fieldPrefix . 'BLOK_AANTAL') {
                    $sqlInsertSelect = $sqlInsertSelect . ",<br>TRIM(SUBSTR(LineImport.LineString, " . (string)((int)$field['offset'] + 1) . ", " . $field['size'] . "))";
                } else {

                }
            }
        }

        $sqlInsertSelect = $sqlInsertSelect . "<br>FROM LineImport;<br>";

        $sqlDropTemporary = "DROP TABLE IF EXISTS LineImport;<br>";

        $sqlCreateDekkingMax = "
			DROP TABLE IF EXISTS AnvaClone" . ucfirst($dataSet) . ".`DEKKING_MAX`;

			CREATE TABLE AnvaClone" . ucfirst($dataSet) . ".`DEKKING_MAX` LIKE AnvaClone" . ucfirst($dataSet) . ".`DEKKING`;

			INSERT INTO AnvaClone" . ucfirst($dataSet) . ".DEKKING_MAX (
				id,
				country,
				DEK_RELNR,
				DEK_VOLG,
				DEK_VOLG_SUB,
				DEK_DEKKING,
				DEK_PREMIE_NETTO
			)
			SELECT
				id,
				country,
				DEK_RELNR,
				DEK_VOLG,
				DEK_VOLG_SUB,
				MAX(DEKKING.DEK_DEKKING) AS DEK_DEKKING,
				DEK_PREMIE_NETTO
			FROM AnvaClone" . ucfirst($dataSet) . ".DEKKING
			GROUP BY
				DEKKING.country,
				DEKKING.DEK_RELNR,
				DEKKING.DEK_VOLG,
				DEKKING.DEK_VOLG_SUB;
		";

        "LOAD DATA INFILE '/mnt/cloud/anva-unload/output-" . strtolower($dataSet) . "/" . $table['name'] . ".txt' INTO TABLE LineImport CHARACTER SET cp850 FIELDS ESCAPED BY '' LINES TERMINATED BY '\\r\\n';<br>";

        $sqlFindBlockColumns = "
			DROP TABLE IF EXISTS LineImport;<br>
			<br>
			CREATE TEMPORARY TABLE LineImport (LineString LONGTEXT);<br>
			<br>
			LOAD DATA INFILE '/mnt/cloud/anva-unload/output-" . strtolower($dataSet) . "/" . $table['name'] . ".txt' INTO TABLE LineImport CHARACTER SET cp850 FIELDS ESCAPED BY '' LINES TERMINATED BY '';<br>
			<br>
			DROP TABLE IF EXISTS BlockFields;<br>
			<br>
			CREATE TEMPORARY TABLE BlockFields (field VARCHAR(5));<br>
			<br>
			INSERT INTO BlockFields (field)<br>
			SELECT DISTINCT<br>
			TRIM(SUBSTR(LineImport.LineString, 1, 5)) AS A<br>
			FROM LineImport<br>
			WHERE LENGTH(TRIM(SUBSTR(LineImport.LineString, 1, 5))) = 5<br>
			AND TRIM(SUBSTR(REPLACE(LineImport.LineString, '\\r\\n', ''), 1, 5)) = TRIM(SUBSTR(LineImport.LineString, 1, 5))<br>";

        if ($table['name'] == "AGBES") {
            $sqlFindBlockColumns = $sqlFindBlockColumns . "
				UNION<br>
				SELECT '55004' AS A<br>
				UNION<br>
				SELECT '55008' AS A<br>
				UNION<br>
				SELECT '55009' AS A<br>
				UNION<br>
				SELECT '55010' AS A<br>
				UNION<br>
				SELECT '55011' AS A<br>
				UNION<br>
				SELECT '55012' AS A<br>
				UNION<br>
				SELECT '55015' AS A<br>
				UNION<br>
				SELECT '55016' AS A<br>
				UNION<br>
				SELECT '55017' AS A<br>
				UNION<br>
				SELECT '55018' AS A<br>
				UNION<br>
				SELECT '55019' AS A<br>
				UNION<br>
				SELECT '55020' AS A<br>
				UNION<br>
				SELECT '55021' AS A<br>
				UNION<br>
				SELECT '55027' AS A<br>
				UNION<br>
				SELECT '55028' AS A<br>";
        }

        if ($table['name'] == "WACBES") {
            $sqlFindBlockColumns = $sqlFindBlockColumns . "
				UNION<br>
				SELECT '81019' AS A<br>
				UNION<br>
				SELECT '81022' AS A<br>";
        }

        $sqlFindBlockColumns = $sqlFindBlockColumns . "ORDER BY A;<br>";

        $sqlCreateBlockColumns = "
			DELIMITER $$<br>
			<br>
			DROP FUNCTION IF EXISTS func_AddBlockFieldColumn;<br>
			<br>
			CREATE FUNCTION func_AddBlockFieldColumn(var_FieldName VARCHAR(5)) RETURNS LONGTEXT<br>
				DETERMINISTIC<br>
			BEGIN<br>
				SET @var_FieldName = var_FieldName;<br>
			<br>
				SET @var_query = CONCAT('ADD COLUMN `" . $fieldPrefix . "', @var_FieldName, '` LONGTEXT NOT NULL');<br>
			<br>
				RETURN (@var_query);<br>
			END$$<br>
			<br>
			DELIMITER ;<br>
			<br>
			SET SESSION group_concat_max_len = 1000000;<br>
			SELECT @columns := GROUP_CONCAT(func_AddBlockFieldColumn(field) SEPARATOR ', ') FROM BlockFields;<br>
			<br>
			SELECT @query := CONCAT('ALTER TABLE AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . " ', @columns, ';');<br>
			<br>
			PREPARE stmt FROM @query;<br>
			EXECUTE stmt;<br>
			DEALLOCATE PREPARE stmt;<br>
			<br>
			DROP TABLE IF EXISTS BlockFields;<br><br>
		";

        $sqlCreateKeys = "";

        foreach ($table['indexType'] as $key => $value) {
            $sqlCreateKeys = $sqlCreateKeys . "ALTER TABLE `" . $table['name'] . "` CHANGE COLUMN " . $value . ";<br>";
        }

        foreach ($table['index'] as $key => $value) {
            $sqlCreateKeys = $sqlCreateKeys . "ALTER TABLE `" . $table['name'] . "` ADD INDEX " . $value . ";<br>";
        }

        $sqlCreateUniqueKey = "
			ALTER TABLE `" . $table['name'] . "` CHANGE COLUMN `id` `id` CHAR(128) NOT NULL COLLATE 'utf8_unicode_ci' FIRST;<br>

			UPDATE `" . $table['name'] . "` SET `id` = " . $table['uniqueKey'] . ";<br>
		";

        $sqlCloneUpdate = "
			# Check if table structure from Build and Clone are the same<br><br>

			SET @tablesDrop = NULL;<br>

			SELECT CONCAT('`', 'AnvaClone" . ucfirst($dataSet) . "', '`.`', TABLE_NAME,'`') INTO @tablesDrop FROM
			(
			SELECT
			`information_schema`.`COLUMNS`.TABLE_NAME,
			`information_schema`.`COLUMNS`.COLUMN_NAME,
			`information_schema`.`COLUMNS`.ORDINAL_POSITION,
			`information_schema`.`COLUMNS`.DATA_TYPE,
			`information_schema`.`COLUMNS`.COLUMN_TYPE,
			COUNT(1) ROWCOUNT
			FROM `information_schema`.`COLUMNS`
			WHERE
			`information_schema`.`COLUMNS`.TABLE_SCHEMA IN ('AnvaBuild" . ucfirst($dataSet) . "', 'AnvaClone" . ucfirst($dataSet) . "')
			AND TABLE_NAME = '" . $table['name'] . "'
			GROUP BY
			`information_schema`.`COLUMNS`.COLUMN_NAME,
			`information_schema`.`COLUMNS`.ORDINAL_POSITION,
			`information_schema`.`COLUMNS`.DATA_TYPE,
			`information_schema`.`COLUMNS`.COLUMN_TYPE
			HAVING COUNT(1) = 1
			) AS A
			GROUP BY
			TABLE_NAME;<br><br>

			# If there are differences then delete Clone and Create like Build<br><br>

			SET @tablesDrop = IFNULL(CONCAT('DROP TABLE IF EXISTS ', @tablesDrop),'SELECT NULL;');<br>

			PREPARE stmt1 FROM @tablesDrop;<br>

			EXECUTE stmt1;<br>

			DEALLOCATE PREPARE stmt1;<br><br>

			# If Clone does not exists then create like Build<br><br>

			CREATE TABLE IF NOT EXISTS AnvaClone" . ucfirst($dataSet) . "." . $table['name'] . " LIKE AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . ";<br><br>

			# Delete rows by UniqueKey from Clone that are not in Build<br><br>

			DELETE Clone
			FROM AnvaClone" . ucfirst($dataSet) . "." . $table['name'] . " AS Clone
			LEFT JOIN AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . " AS Build ON (Clone.id = Build.id)
			WHERE Build.hashlinestring IS NULL;<br><br>

			# Delete rows by UniqueKey and HashLineString from Build that are in Clone<br><br>

			DELETE Build
			FROM AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . " AS Build
			LEFT JOIN AnvaClone" . ucfirst($dataSet) . "." . $table['name'] . " AS Clone ON (Build.id = Clone.id)
			WHERE Clone.hashlinestring IS NOT NULL
			AND Clone.hashlinestring = Build.hashlinestring;<br><br>

			# Optimize Build table to save space<br><br>

			OPTIMIZE TABLE AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . ";<br><br>

			# Delete rows by UniqueKey from Clone that are in Build<br><br>

			DELETE Clone
			FROM AnvaClone" . ucfirst($dataSet) . "." . $table['name'] . " AS Clone
			LEFT JOIN AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . " AS Build ON (Clone.id = Build.id)
			WHERE Build.hashlinestring IS NOT NULL;<br><br>

			# Copy/Create rows from Build to/in Clone<br><br>

			INSERT INTO AnvaClone" . ucfirst($dataSet) . "." . $table['name'] . " SELECT * FROM AnvaBuild" . ucfirst($dataSet) . "." . $table['name'] . ";<br>
		";

        if ($blocksFound) {
            echo $sqlFindBlockColumns . "<br>";
        }
        echo $sqlDropTemporary . "<br>";
        echo $sqlCreateTemporary . "<br>";
        echo $sqlLoadData . "<br>";
        echo $sqlDropTable . "<br>";
        echo $sqlCreateTable . "<br>";
        if ($blocksFound) {
            echo $sqlCreateBlockColumns . "<br>";
        }
        echo $sqlInsertSelect . "<br>";

        echo $sqlCreateKeys . "<br>";

        echo $sqlCreateUniqueKey . "<br>";

        echo $sqlCloneUpdate . "<br>";

        echo $sqlDropTemporary . "<br>";

        if ($table['name'] == "DEKKING") {
            echo $sqlCreateDekkingMax . "<br><br>";
        }
    }

    public function getAnvaEfdArray($tableName)
    {
        $returnValue = array();

        $efdXml = Storage::get('efd/' . strtolower($tableName) . '.xml');

        $xmlValue = $this->xml2array($efdXml, $get_attributes = 1, $priority = 'attribute');

        $xmlValue = $xmlValue['table'];

        foreach ($xmlValue as $key => $value) {
            if ($key == 'field') {
                $returnValue = array_merge($returnValue, $this->xmlLoopField($value));
            }
        }

        // echo "<pre>";
        // print_r($returnValue);
        // echo "</pre>";
        // echo "Dennis <br>";

        return $returnValue;
    }

    private function xmlLoopField($fieldArray)
    {
        $returnValue = array();

        // echo "<pre>";
        // print_r($fieldArray);
        // echo "</pre>";
        // echo "xmlLoopField<br>";

        foreach ($fieldArray as $key => $value) {
            // echo "<pre>";
            // print_r($key);
            // echo "</pre>";
            // echo "foreach<br>";

            if ($key === 'attr' && !isset($value['hidden']) && isset($value['name']) && isset($value['offset']) && isset($value['size'])) {
                $returnValue[] = $value;

                // echo "<pre>";
                // print_r($value);
                // echo "</pre>";
                // echo "add Value<br>";
            } elseif ($key !== 'attr' && is_array($value)) {
                $returnValue = array_merge($returnValue, $this->xmlLoopField($value));
            }
        }

        return $returnValue;
    }

//    public function createW2DSearchRelatie()
//    {
//        ini_set('memory_limit', '-1');
//        ini_set('max_execution_time', 7200);
//
//        DB::connection('mssql')->disableQueryLog();
//        //DB::connection('mssql') -> enableQueryLog();
//
//        DB::connection('mssql')->table('tblRelatie')->truncate();
//
//        DB::connection('mysql_4allcontent_search')->table('tblRelatie')->select("rel_dataset", "rel_pk", "rel_actief", "rel_relatienummer", "rel_relatienaam", "rel_postcode", "rel_straat", "rel_huisnummer", "rel_woonplaats", "rel_agentnummer", "rel_agentnaam", "rel_agentfiliaalcode", "rel_agentfiliaalnaam", "rel_insertdatum")
//            // ->  where('rel_relatienummer', '=', '00000010')
//            ->chunk(1000, function ($result) {
//                $recordSet = array();
//
//                foreach ($result as $row) {
//                    $rowSet = array();
//
//                    foreach ($row as $key => $value) {
//                        $rowSet[$key] = mb_convert_encoding($value, "ISO-8859-1", "UTF-8");
//                    }
//
//                    $recordSet[] = $rowSet;
//                }
//
//                DB::connection('mssql')->table('tblRelatie')->insert($recordSet);
//            });
//
//        return "createW2DSearchRelatie - Done!";
//    }

//    /**
//     * @return string
//     * @deprecated
//     */
//    public function createW2DSearchPolis()
//    {
//        ini_set('memory_limit', '-1');
//        ini_set('max_execution_time', 7200);
//
//        DB::connection('mssql')->disableQueryLog();
//        //DB::connection('mssql') -> enableQueryLog();
//
//        DB::connection('mssql')->table('tblPolis')->truncate();
//
//        DB::connection('mysql_4allcontent_search')->table('tblPolis')->select("pol_dataset", "pol_pk", "pol_actief", "pol_iswacht", "pol_polisnummer", "pol_agentnummer", "pol_agentnaam", "pol_agentfiliaalcode", "pol_agentfiliaalnaam", "pol_framenummer", "pol_kenteken_plaatjesnummer", "pol_externkenmerk", "pol_relatiepk", "pol_relatienummer", "pol_relatienaam", "pol_postcode", "pol_huisnummer", "pol_branchenummer", "pol_branchenaam", "pol_insertdatum")->chunk(1000, function ($result) {
//            $recordSet = array();
//
//            foreach ($result as $row) {
//
//                $rowSet = array();
//
//                foreach ($row as $key => $value) {
//                    $rowSet[$key] = mb_convert_encoding($value, "ISO-8859-1", "UTF-8");
//                }
//
//                $recordSet[] = $rowSet;
//            }
//
//            DB::connection('mssql')->table('tblPolis')->insert($recordSet);
//        });
//
//        return "createW2DSearchPolis - Done!";
//    }
//
//    public function createW2DSearchPolisBuild()
//    {
//        ini_set('memory_limit', '-1');
//        ini_set('max_execution_time', 7200);
//
//        DB::connection('mssql')->disableQueryLog();
//        //DB::connection('mssql') -> enableQueryLog();
//
//        DB::connection('mssql')->table('tblPolisBuild')->truncate();
//
//        DB::connection('mysql_4allcontent_search')->table('tblPolis')->select("pol_dataset", "pol_pk", "pol_actief", "pol_iswacht", "pol_polisnummer", "pol_agentnummer", "pol_agentnaam", "pol_agentfiliaalcode", "pol_agentfiliaalnaam", "pol_framenummer", "pol_kenteken_plaatjesnummer", "pol_externkenmerk", "pol_relatiepk", "pol_relatienummer", "pol_relatienaam", "pol_postcode", "pol_huisnummer", "pol_branchenummer", "pol_branchenaam", "pol_insertdatum")->chunk(1000, function ($result) {
//            $recordSet = array();
//
//            foreach ($result as $row) {
//
//                $rowSet = array();
//
//                foreach ($row as $key => $value) {
//                    $rowSet[$key] = mb_convert_encoding($value, "ISO-8859-1", "UTF-8");
//                }
//
//                $recordSet[] = $rowSet;
//            }
//
//            DB::connection('mssql')->table('tblPolisBuild')->insert($recordSet);
//        });
//
//        return "createW2DSearchPolisBuild - Done!";
//    }

    public function getEncryptedW2D()
    {
        $returnValue = "";

        //Key
        $key = 'SuperSecretKey01';

        //Value:
        $value2Encrypt = Input::get('value');

        //To Encrypt:
        $returnValue = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $value2Encrypt, MCRYPT_MODE_ECB));

        //Value:
        //$value2Decrypt = base64_decode($returnValue);

        //To Decrypt:
        //$returnValue = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $value2Decrypt, MCRYPT_MODE_ECB);

        return urlencode($returnValue);
    }

    public function getDecryptedW2D()
    {
        $returnValue = "";

        //Key
        $key = 'SuperSecretKey01';

        //Value:
        $value2Decrypt = base64_decode(Input::get('value'));

        //To Decrypt:
        $returnValue = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $value2Decrypt, MCRYPT_MODE_ECB);

        return $returnValue;
    }

    public function showDealerLocator($showDealerLocatorParameters)
    {
        switch ($showDealerLocatorParameters['country']) {
            case 'be' :
                $showDealerLocatorParameters['longitude'] = $showDealerLocatorParameters['longitude'] == '' ? '4.464801200000011' : $showDealerLocatorParameters['longitude'];
                $showDealerLocatorParameters['latitude'] = $showDealerLocatorParameters['latitude'] == '' ? '51.22071039999999' : $showDealerLocatorParameters['latitude'];

                $dataDealerLocator = array(
                    'country' => $showDealerLocatorParameters['country'],
                    'objecttype' => $showDealerLocatorParameters['objecttype'],
                    'longitude' => $showDealerLocatorParameters['longitude'],
                    'latitude' => $showDealerLocatorParameters['latitude'],
                    'mapCenterlongitude' => $showDealerLocatorParameters['longitude'],
                    'mapCenterLatitude' => $showDealerLocatorParameters['latitude'],
                    'mapZoom' => '10',
                    'mapActionText' => 'Zoek hier op Naam, Postcode, Plaats etc.'
                );

                break;

            case 'de' :
                $showDealerLocatorParameters['longitude'] = $showDealerLocatorParameters['longitude'] == '' ? '7.465298' : $showDealerLocatorParameters['longitude'];
                $showDealerLocatorParameters['latitude'] = $showDealerLocatorParameters['latitude'] == '' ? '51.513587' : $showDealerLocatorParameters['latitude'];

                $dataDealerLocator = array(
                    'country' => $showDealerLocatorParameters['country'],
                    'objecttype' => $showDealerLocatorParameters['objecttype'],
                    'longitude' => $showDealerLocatorParameters['longitude'],
                    'latitude' => $showDealerLocatorParameters['latitude'],
                    'mapCenterlongitude' => $showDealerLocatorParameters['longitude'],
                    'mapCenterLatitude' => $showDealerLocatorParameters['latitude'],
                    'mapZoom' => '10',
                    'mapActionText' => 'Google Maps Suche nach Firma, PLZ, Ort etc.'
                );

                break;

            default :
                $showDealerLocatorParameters['longitude'] = $showDealerLocatorParameters['longitude'] == '' ? '5.21726131439209' : $showDealerLocatorParameters['longitude'];
                $showDealerLocatorParameters['latitude'] = $showDealerLocatorParameters['latitude'] == '' ? '52.69773299817507' : $showDealerLocatorParameters['latitude'];

                $dataDealerLocator = array(
                    'country' => $showDealerLocatorParameters['country'],
                    'objecttype' => $showDealerLocatorParameters['objecttype'],
                    'longitude' => $showDealerLocatorParameters['longitude'],
                    'latitude' => $showDealerLocatorParameters['latitude'],
                    'mapCenterlongitude' => $showDealerLocatorParameters['longitude'],
                    'mapCenterLatitude' => $showDealerLocatorParameters['latitude'],
                    'mapZoom' => '11',
                    'mapActionText' => 'Zoek hier op Naam, Postcode, Plaats etc.',
                );

                break;
        }

        return View('webtools/dealerlocator')->with('dataDealerLocator', $dataDealerLocator);
    }

    public function getGeoJSON($getGeoJSONParameters)
    {
        $returnValue = "";

        $AnvaCloneData = array(
            'AnvaCloneData1' => env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'),
            'AnvaCloneData3' => env('DB_DATABASE_ANVACLONE_DATA3', 'AnvaCloneData3'),
        );

        $geoJson = array(
            "type" => "FeatureCollection",
            "features" => array()
        );

        $sqlQueryWhereData = "";

        switch ($getGeoJSONParameters['objecttype']) {
            case 'motor' :
                $offsetLongitude = .3;
                $offsetLatitude = .3;

                $sqlQueryWhereData = $sqlQueryWhereData . " AND AnvaCloneData.AGBES.AGB_55008 != 'N'";

                unset($AnvaCloneData['AnvaCloneData3']);

                break;

            case 'bromfiets' :
            case 'moped' :
                $offsetLongitude = .3;
                $offsetLatitude = .3;

                $sqlQueryWhereData = $sqlQueryWhereData . " AND AnvaCloneData.AGBES.AGB_55009 != 'N'";

                unset($AnvaCloneData['AnvaCloneData3']);

                break;

            case 'escooter' :
                $offsetLongitude = .3;
                $offsetLatitude = .3;

                $sqlQueryWhereData = $sqlQueryWhereData . " AND AnvaCloneData.AGBES.AGB_55010 != 'N'";

                unset($AnvaCloneData['AnvaCloneData3']);

                break;

            case 'scootmobiel' :
            case 'mobilityscooter' :
                $offsetLongitude = .3;
                $offsetLatitude = .3;

                $sqlQueryWhereData = $sqlQueryWhereData . " AND AnvaCloneData.AGBES.AGB_55011 != 'N'";

                unset($AnvaCloneData['AnvaCloneData3']);

                break;

            case 'fiets' :
            case 'bike' :
                $offsetLongitude = .2;
                $offsetLatitude = .2;

                $sqlQueryWhereData = $sqlQueryWhereData . " AND AnvaCloneData.AGBES.AGB_55012 != 'N'";

                break;

            default : // All
                $offsetLongitude = .2;
                $offsetLatitude = .2;

                break;
        }

        if (isset($getGeoJSONParameters['longitudetopright']) && isset($getGeoJSONParameters['latitudetopright']) && isset($getGeoJSONParameters['longitudebottomleft']) && isset($getGeoJSONParameters['latitudebottomleft'])) {
            $sqlQueryWhereData = $sqlQueryWhereData . " AND clients.visiting_longitude BETWEEN " . $getGeoJSONParameters['longitudebottomleft'] . " AND " . $getGeoJSONParameters['longitudetopright'];
            $sqlQueryWhereData = $sqlQueryWhereData . " AND clients.visiting_latitude BETWEEN " . $getGeoJSONParameters['latitudebottomleft'] . " AND " . $getGeoJSONParameters['latitudetopright'];
        } else {
            $sqlQueryWhereData = $sqlQueryWhereData . " AND clients.visiting_longitude BETWEEN " . ((float)$getGeoJSONParameters['longitude'] - (float)$offsetLongitude) . " AND " . ((float)$getGeoJSONParameters['longitude'] + (float)$offsetLongitude);
            $sqlQueryWhereData = $sqlQueryWhereData . " AND clients.visiting_latitude BETWEEN " . ((float)$getGeoJSONParameters['latitude'] - (float)$offsetLatitude) . " AND " . ((float)$getGeoJSONParameters['latitude'] + (float)$offsetLatitude);
        }

        $sqlQueryWhereData = $sqlQueryWhereData . " AND (clients.anva_agentnr < '30000' OR clients.country != 'de')";

        $sqlQuery = "";

        foreach ($AnvaCloneData as $AnvaCloneDataKey => $AnvaCloneDataValue) {

            $sqlQuery = $sqlQuery . "
				SELECT
					clients.id AS id,
					AnvaCloneData.AGENT.AGE_AGENTNR AS agentnr,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55023) != '', AnvaCloneData.AGBES.AGB_55023, TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_VOORLETTERS, AnvaCloneData.AGENT.AGE_VOORVOEGSELS, AnvaCloneData.AGENT.AGE_NAAM))) AS name,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55016) != '', AnvaCloneData.AGBES.AGB_55016, AnvaCloneData.AGENT.AGE_STRAAT) AS street,
					IF(TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGBES.AGB_55017, AnvaCloneData.AGBES.AGB_55018))) != '', TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGBES.AGB_55017, AnvaCloneData.AGBES.AGB_55018))), TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_HUISNR, AnvaCloneData.AGENT.AGE_TOEVOEGING)))) AS housenumber,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55019) != '', AnvaCloneData.AGBES.AGB_55019, TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_POSTCODE, AnvaCloneData.AGENT.AGE_BUITENL_POSTCODE))) AS zipcode,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55020) != '', AnvaCloneData.AGBES.AGB_55020, AnvaCloneData.AGENT.AGE_PLAATS) AS city,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55021) != '', AnvaCloneData.AGBES.AGB_55021, AnvaCloneData.AGENT.AGE_LANDCODE) AS country,
					AnvaCloneData.AGBES.AGB_55004 AS website,
					AnvaCloneData.AGENT.AGE_TELNR AS phonenumber,
					AnvaCloneData.AGENT.AGE_FAXNR AS faxnumber,
					clients.visiting_longitude AS longitude,
					clients.visiting_latitude AS latitude
				FROM clients
				LEFT JOIN AnvaCloneData.AGENT ON (AnvaCloneData.AGENT.AGE_AGENTNR = clients.anva_agentnr)
				LEFT JOIN AnvaCloneData.AGBES ON (AnvaCloneData.AGBES.AGB_AGENTNR = clients.anva_agentnr)
				WHERE AnvaCloneData.AGENT.AGE_AGENTNR IS NOT NULL
				AND AnvaCloneData.AGENT.AGE_SW_GEDEACTIVEERD != 'J'
				AND AnvaCloneData.AGBES.AGB_55021 != 'DoNotShow'
				AND clients.deleted_at IS NULL
				{$sqlQueryWhereData}
			";

            //DTa 20171116 Zichtbaar J/N werkt niet in ANVA (kan niet op Ja zetten)
            // AND AnvaCloneData.AGBES.AGB_55015 != 'N'
            //
            // is voor nu het volgende
            // AND AnvaCloneData.AGBES.AGB_55021 != 'DoNotShow'

            $sqlQuery = str_replace("AnvaCloneData.", $AnvaCloneDataValue . ".", $sqlQuery);

            if (end($AnvaCloneData) != $AnvaCloneDataValue) {
                $sqlQuery = $sqlQuery . "
					UNION
				";
            }
        }

        //print_r($sqlQuery);
        //die();

        $clients = Cache::tags('clients')->remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function () use ($sqlQuery) {
            return collect(DB::select($sqlQuery));
        });

        foreach ($clients as $client) {
            if (trim($client->longitude) != "" && trim($client->latitude) != "") {
                $geoJsonFeature = array(
                    "type" => "Feature",
                    "geometry" => array(
                        "type" => "Point",
                        "coordinates" => array(
                            floatval($client->longitude),
                            floatval($client->latitude)
                        )
                    ),
                    "properties" => array(
                        "name" => $client->name,
                        "street" => $client->street,
                        "housenumber" => $client->housenumber,
                        "zipcode" => $client->zipcode,
                        "city" => $client->city,
                        "country" => $client->country,
                        "website" => $client->website,
                        "phonenumber" => $client->phonenumber != '' ? 'Tel. ' . $client->phonenumber : '',
                        "faxnumber" => $client->faxnumber
                    )
                );

                $geoJson["features"][] = $geoJsonFeature;
            }
        }

        $returnValue = $geoJson;

        return $returnValue;
    }

    public function updateClientGeoData()
    {
        $returnValue = "";

        $AnvaCloneData = array(
            'AnvaCloneData1' => env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'),
            'AnvaCloneData3' => env('DB_DATABASE_ANVACLONE_DATA3', 'AnvaCloneData3'),
        );

        $sqlQuery = "";

        foreach ($AnvaCloneData as $AnvaCloneDataKey => $AnvaCloneDataValue) {

            $sqlQuery = $sqlQuery . "
				SELECT
					clients.id AS id,
					AnvaCloneData.AGENT.AGE_AGENTNR AS agentnr,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55023) != '', AnvaCloneData.AGBES.AGB_55023, TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_VOORLETTERS, AnvaCloneData.AGENT.AGE_VOORVOEGSELS, AnvaCloneData.AGENT.AGE_NAAM))) AS name,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55016) != '', AnvaCloneData.AGBES.AGB_55016, AnvaCloneData.AGENT.AGE_STRAAT) AS street,
					IF(TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGBES.AGB_55017, AnvaCloneData.AGBES.AGB_55018))) != '', TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGBES.AGB_55017, AnvaCloneData.AGBES.AGB_55018))), TRIM(LEADING '0' FROM TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_HUISNR, AnvaCloneData.AGENT.AGE_TOEVOEGING)))) AS housenumber,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55019) != '', AnvaCloneData.AGBES.AGB_55019, TRIM(CONCAT_WS(' ', AnvaCloneData.AGENT.AGE_POSTCODE, AnvaCloneData.AGENT.AGE_BUITENL_POSTCODE))) AS zipcode,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55020) != '', AnvaCloneData.AGBES.AGB_55020, AnvaCloneData.AGENT.AGE_PLAATS) AS city,
					IF(TRIM(AnvaCloneData.AGBES.AGB_55021) != '', AnvaCloneData.AGBES.AGB_55021, AnvaCloneData.AGENT.AGE_LANDCODE) AS country,
					AnvaCloneData.AGBES.AGB_55004 AS website,
					AnvaCloneData.AGENT.AGE_TELNR AS phonenumber,
					AnvaCloneData.AGENT.AGE_FAXNR AS faxnumber,
					clients.visiting_longitude AS longitude,
					clients.visiting_latitude AS latitude,
       				clients.updated_at
				FROM clients
				LEFT JOIN AnvaCloneData.AGENT ON (AnvaCloneData.AGENT.AGE_AGENTNR = clients.anva_agentnr)
				LEFT JOIN AnvaCloneData.AGBES ON (AnvaCloneData.AGBES.AGB_AGENTNR = clients.anva_agentnr)
				WHERE AnvaCloneData.AGENT.AGE_AGENTNR IS NOT NULL
				AND AnvaCloneData.AGENT.AGE_SW_GEDEACTIVEERD != 'J'

				AND (TRIM(clients.visiting_longitude) = '' OR TRIM(clients.visiting_latitude) = '' OR TRIM(clients.visiting_longitude) = 'NOT FOUND' OR TRIM(clients.visiting_latitude) = 'NOT FOUND')
				AND (clients.anva_agentnr < '30000' OR clients.country != 'de')
				AND clients.deleted_at IS NULL
			";

            //DTa 20171116 Zichtbaar J/N werkt niet in ANVA (kan niet op Ja zetten)

            $sqlQuery = str_replace("AnvaCloneData.", $AnvaCloneDataValue . ".", $sqlQuery);

            if (end($AnvaCloneData) != $AnvaCloneDataValue) {
                $sqlQuery = $sqlQuery . "
					UNION
				";
            }
        }

        $sqlQuery = $sqlQuery . "
			ORDER BY updated_at
		";

        // print_r($sqlQuery);
        // dfie();

        // For some reason this was disabled on production. It was edited on the live server... WHY!?
        // $clients = Cache::tags('clients') -> remember(CacheHelpers::getCacheKey($sqlQuery), Config::get('cache.duration'), function() use ($sqlQuery)
        // {
        // 	return collect(DB::select($sqlQuery));
        // });
        $clients = collect(DB::select($sqlQuery));

        foreach ($clients as $client) {
            $googleClientInfo = json_decode(GoogleMaps::load('geocoding')->setParam(['address' => $client->street . " " . $client->housenumber . " " . $client->city . " " . $client->country])->get());

            $modelClient = new Client;

            $clientUpdate = $modelClient->find($client->id);

            if (isset($clientUpdate->id)) {
                if (isset($googleClientInfo->results[0])) {
                    $clientUpdate->visiting_longitude = $googleClientInfo->results[0]->geometry->location->lng;
                    $clientUpdate->visiting_latitude = $googleClientInfo->results[0]->geometry->location->lat;
                } else {
                    logger()->error('Client address not found in Google', [$client]);

                    $clientUpdate->visiting_longitude = "NOT FOUND";
                    $clientUpdate->visiting_latitude = "NOT FOUND";
                    $clientUpdate->updated_at = date('Y-m-d H:i:s');
                }

                $clientUpdate->save();

                Cache::tags('clients')->flush();

                $client->longitude = $clientUpdate->visiting_longitude;
                $client->latitude = $clientUpdate->visiting_latitude;
            }
        }

        $returnValue = $clients;

        return $returnValue;
    }

    /**
     * xml2array() will convert the given XML text to an array in the XML structure.
     * Link: http://www.bin-co.com/php/scripts/xml2array/
     * Arguments : $contents - The XML text
     *                $get_attributes - 1 or 0. If this is 1 the function will get the attributes as well as the tag values - this results in a different array structure in the return value.
     *                $priority - Can be 'tag' or 'attribute'. This will change the way the resulting array sturcture. For 'tag', the tags are given more importance.
     * Return: The parsed XML in an array form. Use print_r() to see the resulting array structure.
     * Examples: $array =  xml2array(file_get_contents('feed.xml'));
     *              $array =  xml2array(file_get_contents('feed.xml', 1, 'attribute'));
     */
    private function xml2array($contents, $get_attributes = 1, $priority = 'tag')
    {
        if (!$contents)
            return array();

        if (!function_exists('xml_parser_create')) {
            //print "'xml_parser_create()' function not found!";
            return array();
        }

        //Get the XML parser of PHP - PHP must have this module for the parser to work
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8");
        # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);

        if (!$xml_values)
            return;
        //Hmm...

        //Initializations
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();

        $current = &$xml_array;
        //Refference

        //Go through the tags.
        $repeated_tag_index = array();
        //Multiple tags with same name will be turned into an array
        foreach ($xml_values as $data) {
            unset($attributes, $value);
            //Remove existing values, or there will be trouble

            //This command will extract these variables into the foreach scope
            // tag(string), type(string), level(int), attributes(array).
            extract($data);
            //We could use the array by itself, but this cooler.

            $result = array();
            $attributes_data = array();

            if (isset($value)) {
                if ($priority == 'tag')
                    $result = $value;
                else
                    $result['value'] = $value;
                //Put the value in a assoc array if we are in the 'Attribute' mode
            }

            //Set the attributes too.
            if (isset($attributes) and $get_attributes) {
                foreach ($attributes as $attr => $val) {
                    if ($priority == 'tag')
                        $attributes_data[$attr] = $val;
                    else
                        $result['attr'][$attr] = $val;
                    //Set all the attributes in a array called 'attr'
                }
            }

            //See tag status and do the needed.
            if ($type == "open") {
                //The starting of the tag '<tag>'
                $parent[$level - 1] = &$current;
                if (!is_array($current) or (!in_array($tag, array_keys($current)))) {
                    //Insert New tag
                    $current[$tag] = $result;
                    if ($attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag . '_' . $level] = 1;

                    $current = &$current[$tag];

                } else {
                    //There was another element with the same tag name

                    if (isset($current[$tag][0])) {
                        //If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                        $repeated_tag_index[$tag . '_' . $level]++;
                    } else {
                        //This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        );
                        //This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag . '_' . $level] = 2;

                        if (isset($current[$tag . '_attr'])) {
                            //The attribute of the last(0th) tag must be moved as well
                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }

                    }
                    $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                    $current = &$current[$tag][$last_item_index];
                }

            } elseif ($type == "complete") {
                //Tags that ends in 1 line '<tag />'
                //See if the key is already taken.
                if (!isset($current[$tag])) {
                    //New Key
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $attributes_data)
                        $current[$tag . '_attr'] = $attributes_data;

                } else {
                    //If taken, put all things inside a list(array)
                    if (isset($current[$tag][0]) and is_array($current[$tag])) {
                        //If it is already an array...

                        // ...push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;

                        if ($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag . '_' . $level]++;

                    } else {
                        //If it is not an array...
                        $current[$tag] = array(
                            $current[$tag],
                            $result
                        );
                        //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag . '_' . $level] = 1;
                        if ($priority == 'tag' and $get_attributes) {
                            if (isset($current[$tag . '_attr'])) {
                                //The attribute of the last(0th) tag must be moved as well

                                $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                                unset($current[$tag . '_attr']);
                            }

                            if ($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag . '_' . $level]++;
                        //0 and 1 index is already taken
                    }
                }

            } elseif ($type == 'close') {
                //End of tag '</tag>'
                $current = &$parent[$level - 1];
            }
        }

        return ($xml_array);
    }

    public function doAnvaCloneParseBlocks($dataSet, $tableName)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);

        $functionStartTime = date("Y-m-d H:i:s");
        $countRowUpdates = 0;

        echo 'Start doAnvaCloneParseBlocks - $dataSet : ' . $dataSet . ' $tableName : ' . $tableName . ' Tijd : ' . $functionStartTime . "<br>\n";

        //$tableBlocksName = $tableName . '';
        $tableBlocksName = $tableName;

        DB::connection('mysql_dc3db001_anvaclone_' . $dataSet)->disableQueryLog();
        //DB::connection('mysql_dc3db001_anvaclone_' . $dataSet) -> enableQueryLog();

        $tableColumns = DB::connection('mysql_dc3db001_anvaclone_' . $dataSet)->getSchemaBuilder()->getColumnListing($tableBlocksName);

        $tableColumnNameExplode = explode('_', $tableColumns[10] ?? null);
        $tableColumnNamePrefix = $tableColumnNameExplode[0];

        if (!$tableHasBlocks = in_array($tableColumnNamePrefix . '_BLOK_AANTAL', $tableColumns)) {
            echo 'Column ' . $tableColumnNamePrefix . '_BLOK_AANTAL' . ' NOT found' . "<br>\n";
        } else {
            DB::connection('mysql_dc3db001_anvaclone_' . $dataSet)->statement('ALTER TABLE `' . $tableBlocksName . '` DISABLE KEYS;');

            DB::connection('mysql_dc3db001_anvaclone_' . $dataSet)->table($tableBlocksName)
                ->select('id', $tableColumnNamePrefix . '_LENGTE', $tableColumnNamePrefix . '_BLOK_AANTAL', $tableColumnNamePrefix . '_BLOK_1')
                ->where($tableColumnNamePrefix . '_BLOK_1', '!=', '')
                ->orderBy('id','ASC')
                ->chunk(1000000, function ($result) use ($dataSet, $tableBlocksName, $tableColumns, $tableColumnNamePrefix, &$countRowUpdates) {
                foreach ($result as $row) {
                    $rowSet = array();

                    $blockErrors = Null;
                    $blockLastError = '';

                    $blockTotal = (int)$row->{$tableColumnNamePrefix . '_BLOK_AANTAL'};
                    $blockLength = (int)$row->{$tableColumnNamePrefix . '_LENGTE'};

                    if ($blockTotal > 0 && $blockLength > 0) {
                        if (in_array($tableColumnNamePrefix . '_BLOK', $tableColumns)) {
                            $blockValue = $row->{$tableColumnNamePrefix . '_BLOK'};
                        } else {
                            $blockValue = $row->{$tableColumnNamePrefix . '_BLOK_1'};
                        }

                        $blockValue = mb_substr($blockValue, 0, $blockLength - 1);

                        // Waardes blokken uit elkaar halen aan de hand van door ANVA gekozen delimiter, ASCII-karaktercode 6
                        foreach (explode(chr(6), $blockValue) as $blockField) {
                            if (mb_strlen($blockField) <= 7) {
                                if (mb_strlen(trim($blockField)) > 0) {
                                    // Deze veldgroep kan onmogelijk gegevens bevatten (ANVA-veldnr+ANVA-veldlengte = al 7 karakters)
                                    $blockErrors++;
                                    $blockLastError = $blockLastError . ' ErrorNr. ' . $blockErrors . ' - ' . $blockField . ' -> minder dan 7 karakters';
                                }

                                continue;
                            } else {
                                $blockFieldId = mb_substr($blockField, 0, 5);
                                $blockFieldValueLength = (int)mb_substr($blockField, 5, 2);
                                $blockFieldValue = mb_substr($blockField, 7, $blockFieldValueLength);

                                if (mb_strlen($blockFieldValue) !== mb_strlen(mb_substr($blockField, 7)) || trim($blockFieldId) == '') {
                                    $blockErrors++;
                                    $blockLastError = $blockLastError . ' ErrorNr. ' . $blockErrors . ' - ' . 'Wrong Field Length - fieldId : ' . $blockFieldId . ' fieldValueLength : ' . $blockFieldValueLength . ' blockFieldValue : ' . $blockFieldValue . ' blockField : ' . $blockField;
                                }
                                // elseif ($row -> {$tableColumnNamePrefix . '_' . $blockFieldId} == $blockFieldValue)
                                // {
                                // 	// $blockErrors++;
                                // 	// $blockLastError = $blockLastError . ' ErrorNr. ' . $blockErrors . ' - ' . 'No change in blockFieldValue - fieldId : ' . $blockFieldId . ' blockFieldValue : ' . $blockFieldValue;
                                // 	echo 'Row id : ' . $row -> id . ' - ' . 'No change in blockFieldValue - fieldId : ' . $blockFieldId . ' blockFieldValue : ' . $blockFieldValue . "<br>\n";;
                                // }
                                else {
                                    $rowSet[$tableColumnNamePrefix . '_' . $blockFieldId] = $blockFieldValue;
                                }
                            }
                        }
                    } else {
                        if ($blockTotal <= 0) {
                            // $blockErrors++;
                            // $blockLastError = $blockLastError . ' ErrorNr. ' . $blockErrors . ' - ' . 'BLOK_AANTAL <= 0';
                        }

                        if ($blockLength <= 0) {
                            // $blockErrors++;
                            // $blockLastError = $blockLastError . ' ErrorNr. ' . $blockErrors . ' - ' . '_LENGTE <= 0';
                        }
                    }

                    if ($blockErrors > 0) {
                        $rowSet['errors'] = $blockErrors;
                        $rowSet['lasterror'] = $blockLastError;

                        echo date("Y-m-d H:i:s") . ' - Row id : ' . $row->id . ' Error(s) : ' . $rowSet['lasterror'] . "<br>\n";
                    }

                    if (!empty($rowSet)) {
                        $rowSet[$tableColumnNamePrefix . '_BLOK_1'] = '';

                        $countRowUpdates++;

                        // echo 'Count : ' . $countRowUpdates . ' Row id : ' . $row -> id . ' - Update record<br>\n';


                        DB::connection('mysql_dc3db001_anvaclone_' . $dataSet)->table($tableBlocksName)
                            ->where('id', $row->id)
                            ->update($rowSet);
                    }
                }
            });
        }

        DB::connection('mysql_dc3db001_anvaclone_' . $dataSet)->statement('ALTER TABLE `' . $tableBlocksName . '` ENABLE KEYS;');
        DB::connection('mysql_dc3db001_anvaclone_' . $dataSet)->statement('OPTIMIZE TABLE `' . $tableBlocksName . '`;');

        $functionEndTime = date("Y-m-d H:i:s");

        echo 'Einde doAnvaCloneParseBlocks - $dataSet : ' . $dataSet . ' $tableName : ' . $tableName . ' Tijd : ' . $functionEndTime . ' Duur : ' . (strtotime($functionEndTime) - strtotime($functionStartTime)) . " seconden - " . $countRowUpdates . " Row Update(s)<br><br>\n\n";

        return "doAnvaCloneParseBlocks - Done!" . "<br>\n";
    }

}
