<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;

class SentinelAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Sentinel::check())
            return Redirect::to('admin/signin')->with(trans('enra/general.error'), trans('enra/general.you_must_be_logged_in'));
        //elseif(!Sentinel::inRole('admin'))
            //return Redirect::to('my-account');

        return $next($request);
    }
}
