<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Config;
use App;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;


class SetLang
{
    public function handle($request, Closure $next)
    {
        $lang = Config::get('app.locale');
        if (Sentinel::check()) {
            $user = Sentinel::getUser();

            if ($user->country) {
                $lang = strtolower($user->country);
            }

            if (Sentinel::hasAccess(['country.NL'])) {
                Config::set('database.connections.mysql_anvaclone.database', env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'));
            }

            if (Sentinel::hasAccess(['country.DE'])) {
                Config::set('database.connections.mysql_anvaclone.database', env('DB_DATABASE_ANVACLONE_DATA3', 'AnvaCloneData3'));
            }

            if (Sentinel::hasAccess(['country.BE'])) {
                Config::set('database.connections.mysql_anvaclone.database', env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'));
            }
        }

        App::setLocale($lang);

        return $next($request);
    }
}
