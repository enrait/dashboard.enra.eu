<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [
        'mysql' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
        ],

        'mssql' => [
            'driver'	=> 'sqlsrv',
            //'host'		=> '10.254.2.51',
            'host'		=> '10.1.60.40',
            'database'	=> 'enra_4allcontent_search',
            'username'	=> 'search-differ',
            'password'	=> '8797@@0140!!!0742e7cc64',
            'charset'	=> 'SQL_Latin1_General_CP1_CI_AS',
            'collation'	=> 'SQL_Latin1_General_CP1_CI_AS',
            'prefix'    => '',
        ],

        'mysql_4allcontent_search' => [
            'driver'    => 'mysql',
            'host'		=> '10.1.60.101',
            'database'	=> 'enra_4allcontent_search',
            'username'	=> 'mysqlenranl',
            'password'	=> 'NkCKD4n3k1',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'sqlite' => [
            'driver'   => 'sqlite',
            'database' => storage_path('database.sqlite'),
            'prefix'   => '',
        ],

        'mysql_anvaclone' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST_ANVACLONE', '10.1.60.101'),
            'database'  => env('DB_DATABASE_ANVACLONE', 'AnvaCloneData1'),
            'username'  => env('DB_USERNAME', 'mysqlenranl'),
            'password'  => env('DB_PASSWORD', 'NkCKD4n3k1'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],


        'mysql_anvaclone_data1' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST_ANVACLONE', '10.1.60.101'),
            'database'  => env('DB_DATABASE_ANVACLONE_DATA1', 'AnvaCloneData1'),
            'username'  => env('DB_USERNAME', 'mysqlenranl'),
            'password'  => env('DB_PASSWORD', 'NkCKD4n3k1'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'mysql_anvaclone_data3' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST_ANVACLONE', '10.1.60.101'),
            'database'  => env('DB_DATABASE_ANVACLONE_DATA3', 'AnvaCloneData3'),
            'username'  => env('DB_USERNAME', 'mysqlenranl'),
            'password'  => env('DB_PASSWORD', 'NkCKD4n3k1'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'mysql_dc3db001_anvaclone_data1' => [
            'driver'    => 'mysql',
            //'host'      => '10.253.19.24',
            'host'		=> '10.1.60.101',
            'database'  => 'AnvaCloneData1',
            'username'  => 'mysqlenranl',
            'password'  => 'NkCKD4n3k1',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'mysql_dc3db001_anvaclone_data3' => [
            'driver'    => 'mysql',
            //'host'      => '10.253.19.24',
            'host'		=> '10.1.60.101',
            'database'  => 'AnvaCloneData3',
            'username'  => 'mysqlenranl',
            'password'  => 'NkCKD4n3k1',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '1433'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix' => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_'),
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0'),
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1'),
        ],

    ],

];
